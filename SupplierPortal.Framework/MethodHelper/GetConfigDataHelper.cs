﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.AppConfig;

namespace SupplierPortal.Framework.MethodHelper
{
    public static class GetConfigDataHelper
    {
        public static string GetAppConfigByName(string name)
        {
            IAppConfigRepository _repo = new AppConfigRepository();


            string appConfigValue = _repo.GetAppConfigByName(name).Value;


            return appConfigValue;
        }

    }
}
