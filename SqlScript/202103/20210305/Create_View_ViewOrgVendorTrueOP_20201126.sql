IF EXISTS(select * FROM sys.views where name = 'ViewOrgVendorTrueOP')
Begin
	DROP VIEW [dbo].[ViewOrgVendorTrueOP]
End
GO
CREATE VIEW [dbo].[ViewOrgVendorTrueOP] AS
Select		Distinct
			Tbl_User.UserID,
			Tbl_User.Username,
			Tbl_User.SupplierID,
			Tbl_Organization.OrgID
From		Tbl_User
Inner Join	Tbl_Organization
On			Tbl_Organization.SupplierID = Tbl_User.SupplierID
-- On Prod Change please add ip prefix before [FSCatalog].[dbo].[Account]
Inner Join	[FSCatalog].[dbo].[Account] Acc
On			Acc.SupplierShortName = Tbl_Organization.OrgID
And			BuyerShortName = 'TrueOP'