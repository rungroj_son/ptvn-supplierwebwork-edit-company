USE [SupplierPortal]
GO
/****** Object:  StoredProcedure [dbo].[SupplierProfile_Ins]    Script Date: 3/4/2021 9:20:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		CYK
-- Create date: 20150401
-- Description:	Insert Supplier Profile from Registration Data
-- =============================================
ALTER PROCEDURE [dbo].[SupplierProfile_Ins] 
	@RegID AS INT,
	@NewSupplierID AS INT OUTPUT
AS
BEGIN
	
	--BEGIN TRY
	
	DECLARE @OldSupplierID AS INT
	SET @OldSupplierID = 0
	
	SELECT @OldSupplierID = isnull(SupplierID,0)
	FROM Tbl_RegApprove
	WHERE Tbl_RegApprove.RegID = @RegID 
	
    IF @OldSupplierID > 0
    BEGIN
		SET @NewSupplierID = @OldSupplierID
		RETURN 0
    END
   
    ELSE
    BEGIN
		
		BEGIN TRAN T1
		
			
		DECLARE @UpdateDate AS DATETIME,
				@UpdateBy AS INT
		SET @UpdateDate = GETUTCDATE()
	
		INSERT INTO Tbl_PIS_SupplierCheck(CustCode_PIS,LastUpdate)
		SELECT EngShortName,@UpdateDate AS LastUpdate -- Must less than UpdateDate(For matchaing update pis customer condition)
		FROM Tbl_RegApprove
		WHERE Tbl_RegApprove.RegID = @RegID
			AND Tbl_RegApprove.SupplierID = 0
			AND Tbl_RegApprove.EngShortName <> ''
			AND NOT EXISTS(SELECT CustCode_PIS FROM Tbl_PIS_SupplierCheck WHERE CustCode_PIS = Tbl_RegApprove.EngShortName)
			
	
			
		SET @UpdateDate = DATEADD(second,5,@UpdateDate)
		SET @UpdateBy = 1
		
	
		-------------------------
		-- Insert Organization
		-------------------------
		INSERT INTO Tbl_Organization
		(  
		   [OrgID]
		  ,[PursiteID]
		  ,[CountryCode]
		  ,[TaxID]
		  ,[DUNSNumber]
		  ,[CompanyTypeID]
		  ,[BusinessEntityID]
		  ,[OtherBusinessEntity]
		  ,[CompanyName_Local]
		  ,[CompanyName_Inter]
		  ,[InvName_Local]
		  ,[InvName_Inter]
		  ,[BranchNo]
		  ,[BranchName_Local]
		  ,[BranchName_Inter]
		  ,[MainBusinessCode]
		  ,[PhoneNo]
		  ,[PhoneExt]
		  ,[MobileNo]
		  ,[FaxNo]
		  ,[FaxExt]
		  ,[WebSite]
		  ,[Latitude]
		  ,[Longtitude]
		  ,[YearEstablished]
		  ,[RegisteredCapital]
		  ,[CurrencyCode]
		  ,[NumberOfEmp]
		  ,[isPTVNVerified]
		  ,[isAffiliateCP]
		  ,[LastUpdate]
		  ,isDeleted)
		SELECT	ISNULL(Tbl_RegApprove.EngShortName,'') AS OrgID,0 AS PursiteID,Reg.CountryCode AS CountryCode, Reg.TaxID AS TaxID, Reg.DUNSNumber AS DUNSNumber,
				Reg.CompanyTypeID AS CompanyTypeID, Reg.BusinessEntityID AS BusinessEntityID, Reg.OtherBusinessEntity AS OtherBusinessEntity,Reg.CompanyName_Local AS CompanyName_Local,Reg.CompanyName_Inter AS CompanyName_Inter,
				CASE WHEN Tbl_BusinessEntity.ConcatStrLocal = 'P' AND Reg.CountryCode = 'TH'  
					 THEN Tbl_BusinessEntity.BusinessEntityDisplay + '' + Reg.CompanyName_Local 
					 WHEN Tbl_BusinessEntity.ConcatStrLocal = 'S' AND Reg.CountryCode = 'TH'
					 THEN Reg.CompanyName_Local + '' + Tbl_BusinessEntity.BusinessEntityDisplay 
					 WHEN Reg.CountryCode <> 'TH'
					 THEN Reg.CompanyName_Local + ' ' + BusinessEntity_ML.LocaleValue END AS InvName_Local,
					 
				CASE WHEN Tbl_BusinessEntity.ConcatStrInter = 'P' 
					 THEN BusinessEntity_ML.LocaleValue  + ' ' + Reg.CompanyName_Inter 
					 WHEN Tbl_BusinessEntity.ConcatStrInter = 'S' 
					 THEN Reg.CompanyName_Inter  + ' ' + BusinessEntity_ML.LocaleValue END AS InvName_Inter, 
				Reg.BranchNo AS BranchNo, Reg.BranchName_Local AS BranchName_Local, Reg.BranchName_Inter AS BranchName_Inter , 
				Tbl_RegApprove.MainBusinessCode AS MainBusinessCode,
				Reg.PhoneNo AS PhoneNo, Reg.PhoneExt AS PhoneExt, Reg.MobileNo AS MobileNo, Reg.FaxNo AS FaxNo, Reg.FaxExt AS FaxExt,
				Reg.WebSite AS WebSite, Reg.Latitude AS Latitude, Reg.Longtitude AS Longtitude, '' AS YearEstablished,
				Reg.RegisteredCapital AS RegisteredCapital,CASE Reg.CountryCode WHEN 'TH' THEN 'THB' ELSE 'USD' END AS CurrencyCode, 0 AS NumberOfEmp, 
				Tbl_RegApprove.isVerified AS isPTVNVerified , Tbl_RegApprove.isAffiliateCP AS isAffiliateCP , @UpdateDate AS LastUpdate ,0 AS isDeleted 			
		
		FROM Tbl_RegApprove 
		INNER JOIN Tbl_RegInfo Reg ON Reg.RegID = Tbl_RegApprove.RegID
		LEFT OUTER JOIN Tbl_BusinessEntity ON Reg.BusinessEntityID = Tbl_BusinessEntity.Id
		LEFT OUTER JOIN
		(
			SELECT EntityID,LocaleValue
			FROM Tbl_LocalizedProperty
			WHERE LocaleKeyGroup = 'Tbl_BusinessEntity' AND LocaleKey = 'BusinessEntity'
		) BusinessEntity_ML ON Reg.BusinessEntityID = BusinessEntity_ML.EntityID
		WHERE Tbl_RegApprove.RegID = @RegID AND Reg.RegStatusID = 4 --ยืนยันการอนุมัติ
			AND Tbl_RegApprove.SupplierID = 0
	
		DECLARE @SupplierID AS INT

		SELECT @SupplierID = @@IDENTITY 
		IF @SupplierID IS NULL OR @@ROWCOUNT = 0-- Can not Insert Tbl_Organization
		BEGIN
			Print '@SupplierID = ' + CAST(@SupplierID AS NVARCHAR)
			ROLLBACK TRAN T1
			RETURN 1
		END
	
	
		INSERT INTO Tbl_LogOrganization
		(	SupplierID,OrgID,PursiteID,CountryCode,TaxID,DUNSNumber,CompanyTypeID,BusinessEntityID,OtherBusinessEntity,
			CompanyName_Local,CompanyName_Inter,InvName_Local,InvName_Inter,BranchNo,BranchName_Local,BranchName_Inter,
			MainBusinessCode,PhoneNo,PhoneExt,MobileNo,FaxNo,FaxExt,WebSite,Latitude,Longtitude,YearEstablished,RegisteredCapital,CurrencyCode,
			NumberOfEmp,isPTVNVerified,isAffiliateCP,LastUpdate,isDeleted,UpdateBy,UpdateDate,LogAction
		)
		SELECT	SupplierID,OrgID,PursiteID,CountryCode,TaxID,DUNSNumber,CompanyTypeID,BusinessEntityID,OtherBusinessEntity,
				CompanyName_Local,CompanyName_Inter,InvName_Local,InvName_Inter,BranchNo,BranchName_Local,BranchName_Inter,
				MainBusinessCode,PhoneNo,PhoneExt,MobileNo,FaxNo,FaxExt,WebSite,Latitude,Longtitude,YearEstablished,RegisteredCapital,CurrencyCode,
				NumberOfEmp,isPTVNVerified,isAffiliateCP,LastUpdate,isDeleted,
				@UpdateBy AS UpdateBy,@UpdateDate AS UpdateDate,'Add' AS LogAction
		FROM Tbl_Organization
		WHERE SupplierID = @SupplierID
	
	
		-------------------------
		-- Insert Address
		-------------------------
		INSERT INTO Tbl_Address( HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local,
								 SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode)
		SELECT A.HouseNo_Local,A.HouseNo_Inter,A.VillageNo_Local,A.VillageNo_Inter,A.Lane_Local,A.Lane_Inter,A.Road_Local,A.Road_Inter,A.SubDistrict_Local,
			   A.SubDistrict_Inter,A.City_Local,A.City_Inter,A.State_Local,A.State_Inter,A.CountryCode,A.PostalCode
		FROM  Tbl_RegInfo Reg 
		INNER JOIN Tbl_RegAddress A ON Reg.CompanyAddrID = A.AddressID
		WHERE Reg.RegID = @RegID
			
		DECLARE @CompanyAddressID AS INT
		SELECT @CompanyAddressID = @@IDENTITY 
		IF @CompanyAddressID IS NULL -- 
		BEGIN
			Print '@CompanyAddressID = ' + CAST(@CompanyAddressID AS NVARCHAR)
			ROLLBACK TRAN T1
			RETURN 1
		END
		--Address For Main Contact Person
		INSERT INTO Tbl_Address ( HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local,
								  SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode)
		SELECT HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local,
			   SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode
		FROM  Tbl_Address Addr
		WHERE Addr.AddressID = @CompanyAddressID
		
		DECLARE @MapContactAddressID AS INT
		SELECT @MapContactAddressID = @@IDENTITY 
		IF @MapContactAddressID IS NULL 
		BEGIN
			Print '@MapContactAddressID = ' + CAST(@MapContactAddressID AS NVARCHAR)
			ROLLBACK TRAN T1
			RETURN 1
		END
	
		INSERT INTO Tbl_LogAddress(AddressID,HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local,
								 SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode,UpdateBy,UpdateDate,LogAction)
		SELECT AddressID,HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local,
			   SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode,@UpdateBy AS UpdateBy,@UpdateDate AS UpdateDate,'Add' AS LogAction
		FROM Tbl_Address
		WHERE AddressID = @CompanyAddressID
		
		INSERT INTO Tbl_OrgAddress(SupplierID,AddressID,AddressTypeID,SeqNo)  --Both CompanyAddr and DeliveredAddr are same.
		VALUES(@SupplierID,@CompanyAddressID,1,1),
			  (@SupplierID,@CompanyAddressID,2,1)
		      
		INSERT INTO Tbl_LogOrgAddress(SupplierID,AddressID,AddressTypeID,SeqNo,UpdateBy,UpdateDate,LogAction) 
		VALUES(@SupplierID,@CompanyAddressID,1,1,@UpdateBy,@UpdateDate,'Add'),
			  (@SupplierID,@CompanyAddressID,2,1,@UpdateBy,@UpdateDate,'Add')     
		      
	      
		---------------------------------
		-- Insert Primary Contact Person (To be create PortalUser later )
		---------------------------------    
		     
		INSERT INTO Tbl_ContactPerson (
		   [TitleID]
		  ,[FirstName_Local]
		  ,[FirstName_Inter]
		  ,[LastName_Local]
		  ,[LastName_Inter]
		  ,[JobTitleID]
		  ,[OtherJobTitle]
		  ,[Department]
		  ,[PhoneCountryCode]
		  ,[PhoneNo]
		  ,[PhoneExt]
		  ,[MobileCountryCode]
		  ,[MobileNo]
		  ,[FaxNo]
		  ,[FaxExt]
		  ,[Email]
		  ,[isDeleted])
		SELECT   Tbl_RegContact.TitleID
				,Tbl_RegContact.FirstName_Local
				,Tbl_RegContact.FirstName_Local AS FirstName_Inter
				,Tbl_RegContact.LastName_Local
				,Tbl_RegContact.LastName_Local AS LastName_Inter
				,Tbl_RegContact.JobTitleID
				,Tbl_RegContact.OtherJobTitle
				,Tbl_RegContact.Department
				,Tbl_RegContact.PhoneCountryCode
				,Tbl_RegContact.PhoneNo
				,Tbl_RegContact.PhoneExt
				,Tbl_RegContact.MobileCountryCode
				,Tbl_RegContact.MobileNo
				,Tbl_RegContact.FaxNo
				,Tbl_RegContact.FaxExt
				,Tbl_RegContact.Email
				,0
		FROM Tbl_RegContact 
		INNER JOIN Tbl_RegInfo Reg ON Tbl_RegContact.ContactID = Reg.MainContactID
		WHERE Reg.RegID = @RegID
	
		DECLARE @NewContactID AS INT
		SELECT @NewContactID = @@IDENTITY 
		IF @NewContactID IS NULL
		BEGIN
			Print '@NewContactID = ' + CAST(@NewContactID AS NVARCHAR)
			ROLLBACK TRAN T1
			RETURN 1
		END
	
		INSERT INTO Tbl_LogContactPerson (
		   [ContactID]
		  ,[Title]
		  ,[FirstName_Local]
		  ,[FirstName_Inter]
		  ,[LastName_Local]
		  ,[LastName_Inter]
		  ,[JobTitleID]
		  ,[OtherJobTitle]
		  ,[Department]
		  ,[PhoneCountryCode]
		  ,[PhoneNo]
		  ,[PhoneExt]
		  ,[MobileCountryCode]
		  ,[MobileNo]
		  ,[FaxNo]
		  ,[FaxExt]
		  ,[Email]
		  ,[isDeleted]
		  ,[UpdateBy]
		  ,[UpdateDate]
		  ,[LogAction])
		SELECT Tbl_ContactPerson.* ,@UpdateBy,GETDATE(),'Add'
		FROM Tbl_ContactPerson WHERE ContactID = @NewContactID
		
		INSERT INTO Tbl_OrgContactPerson(SupplierID,ContactID,ContactTypeID,isPrimaryContact,NewContactID)
		VALUES(@SupplierID,@NewContactID,0,1,0)
		      
		INSERT INTO Tbl_LogOrgContactPerson(SupplierID,ContactID,ContactTypeID,isPrimaryContact,NewContactID,UpdateBy,UpdateDate,LogAction)
		VALUES(@SupplierID,@NewContactID,0,1,0,@UpdateBy,GETDATE(),'Add')     
		      
		INSERT INTO Tbl_OrgContactAddressMapping(ContactID,AddressID)     
		VALUES(@NewContactID,@MapContactAddressID)
		     
		DECLARE @ContactAdddressMappingID AS INT
		SELECT @ContactAdddressMappingID = @@IDENTITY 
		IF @ContactAdddressMappingID IS NULL
		BEGIN
			Print '@ContactAdddressMappingID = ' + CAST(@ContactAdddressMappingID AS NVARCHAR)
			ROLLBACK TRAN T1
			RETURN 1
		END 
		
		INSERT INTO Tbl_LogOrgContactAddressMapping(Id,ContactID,AddressID,UpdateBy,UpdateDate,LogAction)
		SELECT *,@UpdateBy,GETDATE(),'Add'
		FROM Tbl_OrgContactAddressMapping
		WHERE Id = @ContactAdddressMappingID    
		
		---------------------------------
		-- Insert Org System Mapping (For Free Service)
		---------------------------------    
		IF EXISTS(SELECT * FROM Tbl_RegServiceTypeMapping WHERE RegID = @RegID)   
		BEGIN
			INSERT INTO Tbl_OrgSystemMapping(SupplierID,SystemID,isAllowConnecting,isSuspendService, isCancelService,isSysUserCreated)
			SELECT @SupplierID,Tbl_System.SystemID,1,0,0,0
			FROM Tbl_System
			WHERE Tbl_System.SystemGrpID = 2 and Tbl_System.IsActive = 1 -- Free Service (OA Except eRFX)
				
			INSERT INTO Tbl_LogOrgSystemMapping(SupplierID,SystemID,isAllowConnecting,isSuspendService,isCancelService,isSysUserCreated,UpdateBy,UpdateDate,LogAction)
			SELECT @SupplierID,Tbl_System.SystemID,1,0,0,0,@UpdateBy,@UpdateDate,'Add'
			FROM Tbl_System
			WHERE Tbl_System.SystemGrpID = 2 and Tbl_System.IsActive = 1 -- Free Service (OA Except eRFX)
		END  
	    
		-- Modify Date : 02-03-2021
		-- Modify By : Piyavut.cho		
		-- Description : Add New Service E-Invoice		
		-- Start
		IF EXISTS(SELECT * FROM Tbl_RegServiceTypeMapping WHERE RegID = @RegID AND ServiceTypeID = 3)   
		BEGIN
			INSERT INTO Tbl_OrgSystemMapping(SupplierID,SystemID,isAllowConnecting,isSuspendService, isCancelService,isSysUserCreated)
			SELECT @SupplierID,Tbl_System.SystemID,1,0,0,0
			FROM Tbl_System
			WHERE Tbl_System.SystemGrpID = 7 and Tbl_System.IsActive = 1 -- Service Invoice
				
			INSERT INTO Tbl_LogOrgSystemMapping(SupplierID,SystemID,isAllowConnecting,isSuspendService,isCancelService,isSysUserCreated,UpdateBy,UpdateDate,LogAction)
			SELECT @SupplierID,Tbl_System.SystemID,1,0,0,0,@UpdateBy,@UpdateDate,'Add'
			FROM Tbl_System
			WHERE Tbl_System.SystemGrpID = 7 and Tbl_System.IsActive = 1 -- Service Invoice
		END  
		-- End

		-- Modify Date : 04-03-2021
		-- Modify By : Piyavut.cho		
		-- Description : Add Mapping Menu ServiceType = 3
		-- Start
		IF (SELECT COUNT('*') FROM Tbl_RegServiceTypeMapping WHERE RegID = @RegID and (ServiceTypeID = 3 or ServiceTypeID = 2)) = 1
		BEGIN
			INSERT INTO Tbl_ACL_OrgMenuMapping
			SELECT @SupplierID, MenuID, 1
			FROM Tbl_MenuPortal
			WHERE Tbl_MenuPortal.SystemGrpID = 7
		END  
		-- End

		-----------------------------------------------------------
		-- Insert Tbl_NotificationTopicOrgMapping (For Free Service) 20/4/2016
		-----------------------------------------------------------    
		IF EXISTS(SELECT * FROM Tbl_RegServiceTypeMapping WHERE RegID = @RegID)   
		BEGIN
			INSERT INTO Tbl_NotificationTopicOrgMapping(SupplierID,TopicID,isActive)
			SELECT @SupplierID,Tbl_NotificationTopicConfig.TopicID,1
			FROM Tbl_NotificationTopicConfig
			WHERE Tbl_NotificationTopicConfig.ServiceID IN (2,3) -- Free Service (OA and eRFX)
				
			INSERT INTO Tbl_LogNotificationTopicOrgMapping(SupplierID,TopicID,isActive,UpdateBy,UpdateDate,LogAction)
			SELECT @SupplierID,Tbl_NotificationTopicConfig.TopicID,1,@UpdateBy,@UpdateDate,'Add'
			FROM Tbl_NotificationTopicConfig
			WHERE Tbl_NotificationTopicConfig.ServiceID IN (2,3) -- Free Service (OA and eRFX)
		END  
				
		-----------------------------------------------------------
		-- Insert Tbl_OrgProductCategory (Support SD Project) 2/8/2016 BY PVY
		-----------------------------------------------------------    
		IF EXISTS(SELECT * FROM Tbl_RegProductCategory WHERE RegID = @RegID)   
		BEGIN		
			INSERT INTO Tbl_OrgProductCategory(SupplierID,CategoryID_Lev3,ProductTypeID)
			SELECT @SupplierID,CategoryID_Lev3,ProductTypeID
			FROM Tbl_RegProductCategory 
			WHERE RegID=@RegID	
			
			INSERT INTO Tbl_LogOrgProductCategory(Id, SupplierID, CategoryID_Lev3, ProductTypeID, UpdateBy, UpdateDate, LogAction)
			SELECT Id, SupplierID, CategoryID_Lev3, ProductTypeID, @UpdateBy,@UpdateDate,'Add'
			FROM Tbl_OrgProductCategory
			WHERE SupplierID = @SupplierID		
		END  
				
		---------------------------------------------------------------
		-- Insert Tbl_OrgProductKeyword (Support SD Project) 2/8/2016 BY PVY
		---------------------------------------------------------------    
		IF EXISTS(SELECT * FROM Tbl_RegProductKeyword WHERE RegID = @RegID)   
		BEGIN		
			INSERT INTO Tbl_OrgProductKeyword(SupplierID,Keyword)
			SELECT @SupplierID,Keyword
			FROM Tbl_RegProductKeyword 
			WHERE RegID = @RegID	
			
			INSERT INTO Tbl_LogOrgProductKeyword(SupplierID, Keyword, UpdateBy, UpdateDate, LogAction)
			SELECT SupplierID, Keyword, @UpdateBy,@UpdateDate,'Add'
			FROM Tbl_OrgProductKeyword
			WHERE SupplierID = @SupplierID 
		END  
				
		---------------------------------------------------------------
		-- Insert Tbl_OrgBusinessType (Support SD Project) 2/8/2016 BY PVY
		---------------------------------------------------------------    
		IF EXISTS(SELECT * FROM Tbl_RegBusinessType WHERE RegID = @RegID)   
		BEGIN		
			INSERT INTO Tbl_OrgBusinessType(SupplierID,BusinessTypeID)
			SELECT @SupplierID,BusinessTypeID
			FROM Tbl_RegBusinessType 
			WHERE RegID = @RegID	
			
			INSERT INTO Tbl_LogOrgBusinessType(Id, SupplierID, BusinessTypeID, UpdateBy, UpdateDate, LogAction)
			SELECT Id, SupplierID, BusinessTypeID, @UpdateBy,@UpdateDate,'Add'
			FROM Tbl_OrgBusinessType
			WHERE SupplierID = @SupplierID 
		END  
						
		UPDATE Tbl_RegApprove -- Status will be update later when all step done
		SET SupplierID = @SupplierID,
			ApproveDate = GETUTCDATE() -- To ensure that approval be made after inserting Tbl_PISSupplierCheck (Case New Register with PISShortName)
		WHERE RegID = @RegID
		
		SET @NewSupplierID = @SupplierID
		
	
		--END TRY
		--BEGIN CATCH
		--	SELECT ERROR_NUMBER() AS ErrorNumber,ERROR_MESSAGE() AS ErrorMessage;
		--	Return 1
		--END CATCH; 
	
		COMMIT TRAN T1
		Return 0
	END
	
END
