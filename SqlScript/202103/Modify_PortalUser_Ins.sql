USE [SupplierPortal]
GO
/****** Object:  StoredProcedure [dbo].[PortalUser_Ins]    Script Date: 3/4/2021 2:33:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		CYK
-- Create date: 2/7/15
-- Description:	Inser Portal User (Supplier Verification Program)
-- =============================================
ALTER PROCEDURE [dbo].[PortalUser_Ins] 

    @UserReqID AS INT ,
    @PISUserID AS INT,
	@NewUserID AS INT OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRAN T1
	
	DECLARE @UpdateDate AS DATETIME,
	        @UpdateBy AS INT
		
	SET @UpdateDate = GETUTCDATE()
	SET @UpdateBy = 1
	
	DECLARE @TimeZone AS NVARCHAR(50),
	        @Locale AS NVARCHAR(50),
			@LanguageCode AS NVARCHAR(2),
			@Currency AS NVARCHAR(3)
	
	 
	
	DECLARE @SupplierID AS INT,
			@OrgID AS NVARCHAR(50),
			@CountryCode AS NVARCHAR(2),
			@CompanyAddressID AS INT,
			@NewContactAddressID AS INT
			
	
	SELECT @SupplierID = Tbl_Organization.SupplierID,@OrgID = Tbl_Organization.OrgID,@CountryCode = Tbl_Organization.CountryCode
	       ,@CompanyAddressID = Tbl_Address.AddressID
	FROM Tbl_RegContactRequest
	INNER JOIN Tbl_Organization ON Tbl_RegContactRequest.SupplierID = Tbl_Organization.SupplierID
	INNER JOIN Tbl_OrgAddress ON Tbl_Organization.SupplierID = Tbl_OrgAddress.SupplierID AND AddressTypeID = 1
	INNER JOIN Tbl_Address ON Tbl_OrgAddress.AddressID = Tbl_Address.AddressID
	
	WHERE UserReqId = @UserReqID AND Tbl_RegContactRequest.UserReqStatusID = 4 AND Tbl_Organization.OrgID <> '' AND Tbl_Organization.isDeleted=0
	
	IF @SupplierID IS NULL OR @SupplierID = 0
		BEGIN
			ROLLBACK TRAN T1
			RETURN 1  
		END
	ELSE
		BEGIN
			IF @CountryCode = 'TH'
				BEGIN
					SET @TimeZone = 'Asia/Bangkok'
					SET @Locale = 'th_TH'
				    SET @LanguageCode = 'th'
				    SET @Currency = 'THB'
				END
			ELSE
				BEGIN
					SET @TimeZone = 'Asia/Bangkok'
					SET @Locale = 'en_US'
				    SET @LanguageCode = 'en'
				    SET @Currency = 'USD'
				END 
		END
	
	
	INSERT INTO Tbl_ContactPerson (
       [TitleID]
      ,[FirstName_Local]
      ,[FirstName_Inter]
      ,[LastName_Local]
      ,[LastName_Inter]
      ,[JobTitleID]
      ,[OtherJobTitle]
      ,[Department]
      ,[PhoneCountryCode]
      ,[PhoneNo]
      ,[PhoneExt]
      ,[MobileCountryCode]
      ,[MobileNo]
      ,[FaxNo]
      ,[FaxExt]
      ,[Email]
      ,[isDeleted])
	SELECT   Tbl_RegContactRequest.TitleID
			,Tbl_RegContactRequest.FirstName_Local
			,Tbl_RegContactRequest.FirstName_Local AS FirstName_Inter
			,Tbl_RegContactRequest.LastName_Local
			,Tbl_RegContactRequest.LastName_Local AS LastName_Inter
			,Tbl_RegContactRequest.JobTitleID
			,Tbl_RegContactRequest.OtherJobTitle
			,Tbl_RegContactRequest.Department
			,Tbl_RegContactRequest.PhoneCountryCode
			,Tbl_RegContactRequest.PhoneNo
			,Tbl_RegContactRequest.PhoneExt
			,Tbl_RegContactRequest.MobileCountryCode
			,Tbl_RegContactRequest.MobileNo
			,Tbl_RegContactRequest.FaxNo
			,Tbl_RegContactRequest.FaxExt
			,Tbl_RegContactRequest.Email
			,0
	FROM   Tbl_RegContactRequest
	WHERE UserReqID = @UserReqID AND Tbl_RegContactRequest.UserReqStatusID = 4
	
	DECLARE @NewContactID AS INT
	SELECT @NewContactID = @@IDENTITY 
	IF @NewContactID IS NULL
	BEGIN
		ROLLBACK TRAN T1
		RETURN 1
	END

	INSERT INTO Tbl_LogContactPerson (
       [ContactID]
      ,[Title]
      ,[FirstName_Local]
      ,[FirstName_Inter]
      ,[LastName_Local]
      ,[LastName_Inter]
      ,[JobTitleID]
      ,[OtherJobTitle]
      ,[Department]
      ,[PhoneCountryCode]
      ,[PhoneNo]
      ,[PhoneExt]
      ,[MobileCountryCode]
      ,[MobileNo]
      ,[FaxNo]
      ,[FaxExt]
      ,[Email]
      ,[isDeleted]
      ,[UpdateBy]
      ,[UpdateDate]
      ,[LogAction])
	SELECT Tbl_ContactPerson.* ,@UpdateBy,@UpdateDate,'Add'
	FROM Tbl_ContactPerson WHERE ContactID = @NewContactID
	
	
	INSERT INTO Tbl_OrgContactPerson (SupplierID,ContactID,ContactTypeID,isPrimaryContact,NewContactID)
	VALUES( @SupplierID,@NewContactID,0,0,0)

	INSERT INTO Tbl_LogOrgContactPerson(SupplierID,ContactID,ContactTypeID,isPrimaryContact,NewContactID,UpdateBy,UpdateDate,LogAction)
    VALUES(@SupplierID,@NewContactID,0,0,0,@UpdateBy,@UpdateDate,'Add')
      
    -- Create New Address for New Contact
     INSERT INTO Tbl_Address( HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local
							 ,SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode)  
	 SELECT HouseNo_Local,HouseNo_Inter,VillageNo_Local,VillageNo_Inter,Lane_Local,Lane_Inter,Road_Local,Road_Inter,SubDistrict_Local
							 ,SubDistrict_Inter,City_Local,City_Inter,State_Local,State_Inter,CountryCode,PostalCode
	 FROM Tbl_Address
	 WHERE AddressID = @CompanyAddressID
	 SET @NewContactAddressID = @@IDENTITY
	 IF @NewContactAddressID IS NULL
	 BEGIN
		ROLLBACK TRAN T1
		RETURN 1
	 END
	 
    INSERT INTO Tbl_OrgContactAddressMapping(ContactID,AddressID)
    VALUES(@NewContactID,@NewContactAddressID)
    
    DECLARE @ContactAdddressMappingID AS INT
    SET @ContactAdddressMappingID = @@IDENTITY
    IF @ContactAdddressMappingID IS NULL
	BEGIN
		ROLLBACK TRAN T1
		RETURN 1
	END
    
    INSERT INTO Tbl_LogOrgContactAddressMapping(Id,ContactID,AddressID,UpdateBy,UpdateDate,LogAction)
    SELECT *,@UpdateBy,@UpdateDate,'Add'
    FROM Tbl_OrgContactAddressMapping
    WHERE Id = @ContactAdddressMappingID
    
    
     --Gen Username
     
    DECLARE @PortalUsername AS NVARCHAR(50),
            @DateInput AS DATETIME
    SET @DateInput = @UpdateDate
    
    EXEC UsernamePortal_gen @PortalUsername OUTPUT,@DateInput
    IF @PortalUsername IS NULL
    BEGIN
		ROLLBACK TRAN T1
		RETURN 1
    END
    
    -- Gen Salt For Password
	DECLARE @RandomSaltByte AS VARBINARY(16)
	DECLARE @RandomSalt NVARCHAR(50)
	SET @RandomSaltByte = CRYPT_GEN_RANDOM(16)
	SET @RandomSalt = CAST('' AS XML).value('xs:base64Binary(sql:variable("@RandomSaltByte"))', 'VARCHAR(50)')
	--SELECT @RandomSalt , LEN (@RandomSalt)
  
  
	 -- Insert User
	INSERT INTO Tbl_User (Username,SupplierID,OrgID,ContactID,Password,Salt,IsActivated,TimeZone,Locale,LanguageCode,Currency,DaysToExpire,
	                      PwdRecoveryToken,MigrateFlag,IsDisabled,IsPublic,IsDeleted,MergeToUserID,isAcceptTermOfUse,SendMailFlag)
	VALUES(@PortalUsername,@SupplierID,@OrgID,@NewContactID,'',@RandomSalt,1,@TimeZone,@Locale,@LanguageCode,@Currency,0,
	       '',0,0,0,0,0,1,0)

	SET @NewUserID = @@IDENTITY
    --Print @NewUserID
	IF @NewUserID IS NULL 
	BEGIN
		ROLLBACK TRAN T1
		RETURN 1
	END 

	
	INSERT INTO Tbl_LogUser ( UserID,Username,SupplierID,OrgID,ContactID,Password,Salt,IsActivated,TimeZone,Locale,LanguageCode,Currency,DaysToExpire,PwdRecoveryToken,MigrateFlag
                              ,IsDisabled,IsPublic,IsDeleted,MergeToUserID,isAcceptTermOfUse,SendMailFlag,UpdateBy,UpdateDate,LogAction)
    SELECT * , @UpdateBy,@UpdateDate,'Add'
    FROM Tbl_User 
    WHERE UserID = @NewUserID
    
    
    -- Insert user Role
    INSERT INTO Tbl_UserRole(UserID,RoleID)
    VALUES(@NewUserID,2)
    
    -- Insert UserServiceMapping (Only Free Service)
	INSERT INTO Tbl_UserServiceMapping(UserID,ServiceID,isActive)
	SELECT @NewUserID,ServiceID,1
	FROM Tbl_Service
	WHERE isFreeService = 1
	--VALUES(@NewUserID,2,1),(@NewUserID,3,1),(@NewUserID,4,1)
	INSERT INTO Tbl_LogUserServiceMapping(UserID,ServiceID,isActive,UpdateBy,UpdateDate,LogAction)
	SELECT @NewUserID,ServiceID,1,@UpdateBy,@UpdateDate,'Add'
	FROM Tbl_Service
	WHERE isFreeService = 1

	-- Modify Date : 02-03-2021
	-- Modify By : Piyavut.cho		
	-- Description : Add New Service E-Invoice		
	-- Start
	IF EXISTS(SELECT * FROM Tbl_RegContactRequestServiceTypeMapping WHERE UserReqId = @UserReqID AND ServiceTypeID = 3)   
	BEGIN
		INSERT INTO Tbl_UserServiceMapping(UserID,ServiceID,isActive)
		SELECT @NewUserID,ServiceID,1
		FROM Tbl_System
		WHERE Tbl_System.SystemGrpID = 7 and Tbl_System.IsActive = 1 -- Service Invoice
				
		INSERT INTO Tbl_LogUserServiceMapping(UserID,ServiceID,isActive,UpdateBy,UpdateDate,LogAction)
		SELECT @NewUserID,ServiceID,1,@UpdateBy,@UpdateDate,'Add'
		FROM Tbl_System
		WHERE Tbl_System.SystemGrpID = 7 and Tbl_System.IsActive = 1 -- Service Invoice
	END

	IF (SELECT COUNT('*') FROM Tbl_RegContactRequestServiceTypeMapping WHERE UserReqId = @UserReqID and (ServiceTypeID = 3 or ServiceTypeID = 2)) = 1
		BEGIN
			INSERT INTO Tbl_UserSystemMapping(UserID,SysUserID,SysPassword,SystemID,SysRoleID,isEnableService,MergeFromUserID)
			SELECT @NewUserID,@NewUserID,'',ServiceID,0,1,0
			FROM Tbl_System
			WHERE Tbl_System.SystemGrpID = 7 and Tbl_System.IsActive = 1 
		END  
	-- END
	
	
	UPDATE Tbl_RegContactRequest
	SET UserID = @NewUserID
	WHERE UserReqId = @UserReqID
	
	  INSERT INTO Tbl_LogRegContactRequest ( UserReqId,SupplierID,UserReqStatusID,RequestDate,TitleID,FirstName_Local,FirstName_Inter,LastName_Local,LastName_Inter
									   	  ,JobTitleID,OtherJobTitle,Department,PhoneCountryCode,PhoneNo,PhoneExt,MobileCountryCode,MobileNo,FaxNo,FaxExt,Email,UserID,isDeleted,UpdateBy,UpdateDate, LogAction)
	SELECT *, @PISUserID,@UpdateDate,'Modify'
	FROM Tbl_RegContactRequest
	WHERE UserReqId = @UserReqID
	 
	--INSERT INTO Tbl_RegContactRequestHistory(UserReqId,UserReqStatusID,Remark,UpdateBy,UpdateDate)
	--VALUES(@UserReqID,4,'',@UpdateBy,@UpdateDate)
	
	UPDATE Tbl_Organization
	SET LastUpdate = @UpdateDate
	WHERE SupplierID = @SupplierID
	
	

	COMMIT TRAN T1
	RETURN 0
   
END
