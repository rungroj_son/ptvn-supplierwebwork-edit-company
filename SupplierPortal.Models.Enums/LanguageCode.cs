﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum LanguageCode
    {
        English = 1,
        Thailand = 2
    }
}
