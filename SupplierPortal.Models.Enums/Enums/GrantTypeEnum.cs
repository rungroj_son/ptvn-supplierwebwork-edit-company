﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum GrantTypeEnum
    {
        [SWW(Code = "UserDetail", TypeCode = "user_detail")]
        UserDetail,

        [SWW(Code = "Password", TypeCode = "password")]
        Password
    }
}
