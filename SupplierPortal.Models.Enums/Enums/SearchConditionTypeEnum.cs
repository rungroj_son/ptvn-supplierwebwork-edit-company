﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum SearchConditionTypeEnum
    {
        [SWW(Code = " And ", TypeCode = "And", Description = "And Condition")]
        And,

        [SWW(Code = " Or ", TypeCode = "Or", Description = "Or Condition")]
        Or,

        [SWW(Code = " NOT ", TypeCode = "Not", Description = "Not Condition")]
        Not
    }
}
