﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum ContactManagement
    {
        [Description("sca")]
        CONTACT_ACTIVE = 1,
        [Description("scc")]
        CONTACT_CREATE = 2,
        [Description("sce")]
        CONTACT_EDIT = 3,
        [Description("sch")]
        CONTACT_VIEW_HISTORY = 4,
        [Description("sci")]
        CONTACT_INACTIVE = 5,
        [Description("scm")]
        Contact_Menu = 6,
        [Description("scv")]
        CONTACT_VIEW = 7
    }
}
