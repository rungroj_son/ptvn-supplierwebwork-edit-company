﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.DataAccess.PDPA;

namespace SupplierPortal.BusinessLogic.PDPA
{
    public partial class UserPDPAManager : IUserPDPAManager
    {

        private readonly IUserPDPARepository _userPDPARepository;
        private readonly IUserSessionRepository _userSessionRepository;
        public UserPDPAManager()
        {
            _userPDPARepository = new UserPDPARepository();
            _userSessionRepository = new UserSessionRepository();
        }

        public Users_PDPA GetUserPDPA(string username)
        {
            Users_PDPA result;
            try
            {
                result = new Users_PDPA();
                result = this._userPDPARepository.GetUserPDPA(username);

                if (result == null || result.IsAcceptTerm == false || result.IsCheckConsent == false)
                {
                    result = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int InsertUserPDPA(Users_PDPA user)
        {
            int reuslt = 0;
            Tbl_UserSession userSession; ;
            try
            {
                userSession = new Tbl_UserSession();
                userSession = this._userSessionRepository.GetUserSession(user.Username);
                if (userSession != null)
                {
                    user.Username = userSession.Username;
                    reuslt = this.InertUpdateUserPDPA(user);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reuslt;
        }

        public int InertUpdateUserPDPA(Users_PDPA userPDPA)
        {
            int reuslt = 0;
            Users_PDPA users;
            try
            {
                users = new Users_PDPA();
                users = this._userPDPARepository.GetUserPDPA(userPDPA.Username);

                if (users == null)
                {
                    users = new Users_PDPA()
                    {
                        Username = userPDPA.Username,
                        IsAcceptTerm = true,
                        IsCheckConsent = true
                    };
                    reuslt = this._userPDPARepository.InsertUserPDPA(users);
                }
                else
                {
                    this._userPDPARepository.UpdateUserPDPA(users);
                    reuslt = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reuslt;
        }
    }
}
