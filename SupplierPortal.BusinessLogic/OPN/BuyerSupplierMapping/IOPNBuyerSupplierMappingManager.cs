﻿using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.OPN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.BuyerSupplierMapping
{
    public interface IOPNBuyerSupplierMappingManager
    {
        Task<bool> JobRunningUpdateBuyerSupplierMapping();

        bool PISApproveBuyerSupplierMapping(int regID);

        #region INSERT
        int InsertOPNBuyerSupplierMapping(RegisterOPNSupplierMapping request);
        int InsertOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMappingDto request);
        #endregion
    }
}
