﻿using SupplierPortal.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.OPNOrganization
{
    public interface IOPNOrganizationManager
    {
        #region GET
        OPN_OrganizationDto getOPNOrganizationShortDetail(string taxid, string branchNo);
        #endregion
    }
}
