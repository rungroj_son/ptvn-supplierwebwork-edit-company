﻿using SupplierPortal.Data.CustomModels.CompanyProfile;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Services.CompanyProfileManage;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.CompanyProfile
{
    public partial class ImageCompanyManager : IImageCompanyManager
    {
     
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IOrgAttachmentRepository _orgAttachmentRepository;
        private readonly IComProfileTrackingService _comProfileTrackingService;

        public ImageCompanyManager()
        {
            _organizationRepository = new OrganizationRepository();
            _orgAttachmentRepository = new OrgAttachmentRepository();
            _comProfileTrackingService = new ComProfileTrackingService();
        }

        public string GetSupplierIdByTaxId(string taxId, string branchNumber)
        {
            var data = _organizationRepository.GetOrganizationByTaxIDAndBranchNo(taxId, branchNumber);
            if (data != null)
            {
                return ""+data.SupplierID;
            }
            return "";
        }

        async Task<List<ImageCompanyProfileModel>> IImageCompanyManager.GetImagePowerAndCompany(string taxId, string branchNumber)
        {
            List<ImageCompanyProfileModel> imageCompanyProfileList = new List<ImageCompanyProfileModel>();

            var data = _organizationRepository.GetOrganizationByTaxIDAndBranchNo(taxId, branchNumber);
            if (data != null)
            {

                OrgAttachmentListModels orgData = _comProfileTrackingService.GetOrgAttachmentListBySupplierIDAndDocumentTypeIDOther(data.SupplierID, -1, (int)data.CompanyTypeID);
                if (orgData != null)
                {
                
                    IList<TrackingAttachment> tam = orgData.TrackingAttachment;
                    if(tam.Count > 0)
                    {
                        for (var i = 0; i < tam.Count; i++)
                        {
                            Tbl_OrgAttachment orgAttachmentModel = tam[i].OrgAttachment;

                            ImageCompanyProfileModel img = new ImageCompanyProfileModel
                            {
                                attachmentId = orgAttachmentModel.AttachmentID,
                                attachmentName = orgAttachmentModel.AttachmentName,
                                attachmentNameUnique = orgAttachmentModel.AttachmentNameUnique,
                                documentNameId = orgAttachmentModel.DocumentNameID
                            };

                            imageCompanyProfileList.Add(img);
                        }
                       
                    }
                  

                }

            }
            return imageCompanyProfileList;
        }
    }
}
