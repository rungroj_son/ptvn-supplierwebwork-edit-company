﻿using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.CustomModels.SendEmail;
using SupplierPortal.Data.Models.SupportModel.OPN;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.SC;
using SupplierPortal.Services.APIService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.BusinessLogic.ExternalService
{
    public class ExternalServiceManager : APIConnectionService, IExternalServiceManager
    {
        private string url_opn_supplier_service;
        private string url_opn_invoice_service;
        private string url_email_service;
        private string path_opn_service_supplier;
        private string path_opn_service_supplier_summary;
        private string path_email_service;
        private string url_vault;
        private string path_vault_encrypt;
        private string path_vault_decrypt;

        private string key_vault;
        private string value_vault;

        private string sc_url;
        private string sc_update_endpoint;
        private string sc_ticket_verify_endpoint;
        private string sc_email_verify_endpoint;

        private string path_opn_service_supplier_summary_gr;
        private string opn_supplier_gr_buyer_mp_id;
        private string path_opn_service_supplier_detail;

        public ExternalServiceManager()
        {
            this.url_opn_supplier_service = WebConfigurationManager.AppSettings["url_opn_supplier_service"];
            this.url_opn_invoice_service = WebConfigurationManager.AppSettings["url_opn_invoice_service"];
            this.url_vault = WebConfigurationManager.AppSettings["url_vault"];
            this.url_email_service = WebConfigurationManager.AppSettings["url_email_service"];

            this.path_opn_service_supplier = WebConfigurationManager.AppSettings["path_opn_service_supplier"];
            this.path_opn_service_supplier_summary = WebConfigurationManager.AppSettings["path_opn_service_supplier_summary"];
            this.path_vault_encrypt = WebConfigurationManager.AppSettings["path_vault_encrypt"];
            this.path_vault_decrypt = WebConfigurationManager.AppSettings["path_vault_decrypt"];
            this.path_email_service = WebConfigurationManager.AppSettings["path_email_service"];

            this.key_vault = WebConfigurationManager.AppSettings["key_vault"];
            this.value_vault = WebConfigurationManager.AppSettings["value_vault"];


            this.sc_url = WebConfigurationManager.AppSettings["sc_url"];
            this.sc_update_endpoint = WebConfigurationManager.AppSettings["sc_update_endpoint"];
            this.sc_ticket_verify_endpoint = WebConfigurationManager.AppSettings["sc_ticket_verify_endpoint"];
            this.sc_email_verify_endpoint = WebConfigurationManager.AppSettings["sc_email_verify_endpoint"];

            this.path_opn_service_supplier_detail = WebConfigurationManager.AppSettings["path_opn_service_supplier_detail"];
            this.path_opn_service_supplier_summary_gr = WebConfigurationManager.AppSettings["path_opn_service_supplier_summary_gr"];
            this.opn_supplier_gr_buyer_mp_id = WebConfigurationManager.AppSettings["opn_supplier_gr_buyer_mp_id"];
        }


        public async Task<Boolean> OPNSupplierSerivce(SupplierOPNModel request)
        {
            HttpResponseMessage httpResponse = null;
            bool result = false;
            try
            {
                string url = string.Format("{0}{1}", url_opn_supplier_service, path_opn_service_supplier);

                httpResponse = await this.PutApiConnection(url_opn_supplier_service, url, request);
                JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                string jsonString = JsonConvert.Serialize(request);

                if (httpResponse.IsSuccessStatusCode)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();

                    result = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> CheckInvoice(string supplierMpid, string month, string buyerMpId)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            bool result = false;
            try
            {
                string parameter = string.Format("?supplierMpid={0}&month={1}&isIncludeCurrentMonth=yes&buyerMpId={2}", supplierMpid, month, buyerMpId);
                string url = string.Format(this.path_opn_service_supplier_summary, parameter);
                httpResponse = await this.GetApiConnection(this.url_opn_invoice_service, url);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<int> GetSupplierId(string orgId)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            int result = 0;
            try
            {
                string url = string.Format(this.path_opn_service_supplier_detail, orgId);
                httpResponse = await this.GetApiConnection(this.url_opn_invoice_service, url);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    SupplierMpInfoResponse response = this.streamReaderToObject<SupplierMpInfoResponse>(httpResponse);
                    result = response == null ? 0 : response.supplierId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> CheckGR(string supplierMpId, int supplierId, string supplierUsername, int month, bool isIncludeCurrentMonth)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            bool result = false;
            try
            {
                List<string> buyerCheckList = new List<string>();
                if (!string.IsNullOrEmpty(this.opn_supplier_gr_buyer_mp_id))
                {
                    buyerCheckList.AddRange(this.opn_supplier_gr_buyer_mp_id.Split(',').Select(s => s.Trim()).Distinct().ToList());
                    foreach (string buyerMpId in buyerCheckList)
                    {
                        if (!result)
                        {
                            string url = string.Format("{0}?supplierMpId={1}&supplierId={2}&buyerMpId={3}&supplierUsername={4}&month={5}&isIncludeCurrentMonth={6}",
                                this.path_opn_service_supplier_summary_gr,
                                supplierMpId,
                                supplierId.ToString(),
                                buyerMpId,
                                supplierUsername,
                                month.ToString(),
                                isIncludeCurrentMonth ? "true" : "false"
                            );

                            httpResponse = await this.GetApiConnection(this.url_opn_invoice_service, url);

                            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return result;
        }

        public async Task<List<ConnectJobResponse>> UpdateStatusSupplierConnect(List<ConnectJobRequest> request)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            List<ConnectJobResponse> response;
            try
            {
                response = new List<ConnectJobResponse>();

                string url = string.Format("{0}{1}", sc_url, sc_update_endpoint);
                httpResponse = await this.GetApiConnectionWithOutSecurity(sc_url, sc_url + "api/countries");

                httpResponse = await this.PostApiConnection(sc_url, url, request);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    response = JsonConvert.Deserialize<List<ConnectJobResponse>>(jsonResult);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return response;
        }

        public async Task<VerifyTicketCodeResponseDto> VerifyTicketCodeSupplierConnect(string ticketCode)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            VerifyTicketCodeResponseDto response = new VerifyTicketCodeResponseDto();
            try
            {
                httpResponse = await this.GetApiConnection(sc_url, string.Format(sc_ticket_verify_endpoint, ticketCode));
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response = this.streamReaderToObject<VerifyTicketCodeResponseDto>(httpResponse);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return response;
        }

        public async Task<VerifyTicketCodeResponseDto> VerifyEmailSupplierConnect(string email)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            VerifyTicketCodeResponseDto response = new VerifyTicketCodeResponseDto();
            try
            {
                httpResponse = await this.GetApiConnection(sc_url, string.Format(sc_email_verify_endpoint, email));
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    response = this.streamReaderToObject<VerifyTicketCodeResponseDto>(httpResponse);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return response;
        }

        public async Task<VaultEncryptResponse> VaultEncryptSerivce(VaultEncryptRequest request)
        {
            HttpResponseMessage httpResponse = null;
            VaultEncryptResponse response = null;

            try
            {
                string url = string.Format("{0}{1}", url_vault, path_vault_encrypt);

                List<APIKey> keys = new List<APIKey>();
                keys.Add(new APIKey() { key = key_vault, value = value_vault });

                httpResponse = await this.PostApiConnection(url_vault, url, request, keys);

                if (httpResponse.IsSuccessStatusCode)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    response = JsonConvert.Deserialize<VaultEncryptResponse>(jsonResult);

                    return response;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return response;
        }

        public async Task<VaultDecryptResponse> VaultDecryptSerivce(VaultDecryptRequest request)
        {
            HttpResponseMessage httpResponse = null;
            VaultDecryptResponse response = null;
            try
            {
                string url = string.Format("{0}{1}", url_vault, path_vault_decrypt);

                List<APIKey> keys = new List<APIKey>();
                keys.Add(new APIKey() { key = key_vault, value = value_vault });

                httpResponse = await this.PostApiConnection(url_vault, url, request, keys);

                if (httpResponse.IsSuccessStatusCode)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    response = JsonConvert.Deserialize<VaultDecryptResponse>(jsonResult);

                    return response;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return response;
        }

        public async Task<bool> SCFSendEmail(SendEmailRequest request)
        {
            HttpResponseMessage httpResponse = null;
            bool result = false;
            try
            {
                string url = string.Format("{0}{1}", url_email_service, path_email_service);
                httpResponse = await this.PostApiConnection(url_email_service, url, request);

                if (httpResponse.IsSuccessStatusCode)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();

                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }
            return result;
        }
    }
}
