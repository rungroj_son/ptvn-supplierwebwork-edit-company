﻿using SupplierPortal.Data.CustomModels.RegisterSupplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RegFromConnect
{
    public interface IRegFromConnectManager
    {
        Task<List<ConnectJobResponse>> RunningJobRegisterSupplierAndRequestTojoin();
    }
}
