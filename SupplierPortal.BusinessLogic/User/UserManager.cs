﻿using SupplierPortal.Data.CustomModels.User;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.UAA;
using SupplierPortal.DataAccess.User;
using SupplierPortal.Framework.AccountManage;
using System;

namespace SupplierPortal.BusinessLogic.User
{
    public partial class UserManager : IUserManager
    {
        public readonly IUserRepository _userRepository;
        public readonly SupplierPortal.Data.Models.Repository.User.IUserRepository _userModelRepository;
        public UserManager()
        {
            _userRepository = new UserRepository();
            _userModelRepository = new SupplierPortal.Data.Models.Repository.User.UserRepository();
        }

        public Boolean UpdateUserActiveInActive(int userId)
        {
            bool result = false;
            int logID = 0;
            Tbl_User user = null;
            Tbl_LogUser logUser = null;
            try
            {
                user = new Tbl_User();
                user = _userRepository.GetUserByUserId(userId);

                if (user != null)
                {
                    logUser = new Tbl_LogUser()
                    {
                        UserID = user.UserID,
                        Username = user.Username,
                        OrgID = user.OrgID,
                        SupplierID = user.SupplierID,
                        ContactID = user.ContactID,
                        Password = user.Password,
                        Salt = user.Salt,
                        IsActivated = user.IsActivated,
                        TimeZone = user.TimeZone,
                        Locale = user.Locale,
                        LanguageCode = user.LanguageCode,
                        Currency = user.Currency,
                        DaysToExpire = user.DaysToExpire,
                        PwdRecoveryToken = user.PwdRecoveryToken,
                        MigrateFlag = user.MigrateFlag,
                        IsDisabled = user.IsDisabled,
                        IsPublic = user.IsPublic,
                        IsDeleted = user.IsDeleted,
                        MergeToUserID = user.MergeToUserID,
                        isAcceptTermOfUse = user.isAcceptTermOfUse,
                        SendMailFlag = user.SendMailFlag,
                        UpdateByPIS = null,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Modify",
                    };

                    logID = _userRepository.InsertLogUser(logUser);

                    if (logID > 0)
                    {
                        user.IsDisabled = user.IsDisabled == 0 ? 1 : 0;
                        result = _userRepository.UpdateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                user = null;
                logUser = null;
            }
            return result;
        }

        public UserContactPersonDataModels GetUserInfo(string username)
        {
            UserContactPersonDataModels result;
            try
            {
                result = new UserContactPersonDataModels();
                result = _userModelRepository.GetUserContactInfoByUsername(username);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public UserContactResponse GetUserInfoFromProcurementBoard(string username)
        {
            UserContactResponse result;
            try
            {
                result = new UserContactResponse();
                result = _userModelRepository.GetUserInfoFromProcurementBoard(username);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public UAAOuthResponse GetUserForUAAByUsername(UserRequest request)
        {
            UAAOuthResponse result = null;
            try
            {
                result = new UAAOuthResponse();
                result = _userRepository.GetUserForUAAByUsername(request.username);

                if (result == null)
                {
                    return null;
                }

                string fullPwdStr = request.password + result.salt;
                byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
                string inPassword = Convert.ToBase64String(passwordHash);

                if (result.password != inPassword)
                {
                    return null;
                }

                result.password = String.Empty;
                result.salt = String.Empty;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

    }
}
