﻿using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Consent;
using SupplierPortal.Models.Dto.COC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.COC
{
    public partial interface IUserCOCManager
    {
        ViewOrgVendorConsent ExistsInCodeOfConduct(string username);

        Task<ConsentRequestAcceptModel> GetCodeOfConduct(ViewOrgVendorConsent consent, string username);

        Task<ConsentAcceptModel> AcceptConsent(ViewOrgVendorConsent consent, string username, ConsentAcceptRequest request);
    }
}
