﻿
using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.ContactPerson;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.ContactPerson
{
    public partial class ContactPersonManager : IContactPersonManager
    {
        public readonly IContactPersonRepository _contactPersonRepository;
        public ContactPersonManager()
        {
            _contactPersonRepository = new ContactPersonRepository();
        }

        #region GET
        public Tuple<List<Tbl_ContactPersonDto>, int, int> GetUserContactPersonsOfOrganization(int supplierID, DatatableRequest request)
        {
            Tuple<List<Tbl_ContactPersonDto>, int, int> tuple = null;
            tuple = _contactPersonRepository.GetUserContactPersonsOfOrganization(supplierID, request);


            return tuple;
        }

        public SearchResultResponse<List<ContactWithPagination_Sel_Result>> GetContactWithPagination(SearchResultRequest request)
        {
            SearchResultResponse<List<ContactWithPagination_Sel_Result>> response;
            try
            {
                response = new SearchResultResponse<List<ContactWithPagination_Sel_Result>>();

                List<ContactWithPagination_Sel_Result> listContactResult = new List<ContactWithPagination_Sel_Result>();

                string condition = SearchConditionHelper.CreateCondition(request.advanceSearch);
                string sorting = SortingHelper.CreateSorting(request.sortAscending, request.sortColumn);

                listContactResult = _contactPersonRepository.GetContactWithPagination(request.pageIndex, request.pageSize, condition, sorting);

                if (listContactResult.Any())
                {
                    response.pageIndex = request.pageIndex;
                    response.pageSize = request.pageSize;
                    response.totalRecord = (int)listContactResult.FirstOrDefault().TotalRecord;
                    response.totalItem = listContactResult.Count();
                    response.totalMainRecord = listContactResult.Count();
                    response.totalPage = (response.totalRecord / response.pageSize) + (response.totalRecord % response.pageSize > 0 ? 1 : 0);
                    response.dataResult = listContactResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public bool GetContactPersionByEmail(string email)
        {
            return _contactPersonRepository.CheckEmailContactPersionDuplicate(email);
        }
        #endregion
    }
}
