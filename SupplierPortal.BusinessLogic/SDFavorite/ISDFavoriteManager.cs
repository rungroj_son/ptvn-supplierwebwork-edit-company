﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.SDFavorite
{
    public interface ISDFavoriteManager
    {
        
        #region GET
        List<Tbl_SDFavorite> GetSDFavoriteByEID(int EID, string sysUserId);
        List<Core_SupplierContactFavorite_Result> GetSDFavorites(int eid, int supplierId, int sysUserId);
        Tbl_SDFavorite GetSDFavorite(string sysUserID, int EID, int userID);
        #endregion

        #region INSERT
        int Insert(Tbl_SDFavorite tbl_SDFavorite, int updateBy);
        #endregion

        #region Delete
        int Delete(int id, int updateBy);
        #endregion
    }
}
