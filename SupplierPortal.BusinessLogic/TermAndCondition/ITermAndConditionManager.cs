﻿using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Data.Models.SupportModel.Consent;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.TermAndCondition
{
    public partial interface ITermAndConditionManager
    {
        Task<ConsentRequestAcceptModel> GetTermAndCondition(string username);

        Task<ConsentAcceptModel> AcceptTermAndCondition(string username, ConsentAcceptRequest request);
    }
}
