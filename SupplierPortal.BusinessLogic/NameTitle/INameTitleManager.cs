﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.NameTitle
{
    public partial interface INameTitleManager
    {
        List<Tbl_NameTitle> GetNameTitleList(int languageID);
    }
}
