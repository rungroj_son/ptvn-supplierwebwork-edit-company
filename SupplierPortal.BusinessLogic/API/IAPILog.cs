﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.API
{
    public interface IAPILog
    {
        void insertAPILog(string ReqID, int? SystemID, string Events, DateTime? Events_Time, string IPAddress_Client, string APIKey, string APIName, string Return_Code, string Return_Status, string Return_Message, string Data, string UserGUID);
        Boolean GetSystemListByUsernameForUpdateAPI(string userName);
    }
}
