﻿using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.UserSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.UserSession
{
    public partial class UserSessionManager : IUserSessionManager
    {
        public readonly IUserSessionRepository _userSessionRepository;
        public UserSessionManager()
        {
            _userSessionRepository = new UserSessionRepository();
        }

        public Boolean Update(Tbl_UserSession tbl_UserSession)
        {
            bool result = false;
            try
            {
                if (tbl_UserSession != null)
                {
                    _userSessionRepository.Update(tbl_UserSession);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public Tbl_UserSession GetUserSession(string userGuid)
        {
            Tbl_UserSession result = null;
            try
            {
                result = new Tbl_UserSession();
                result = _userSessionRepository.GetUserSession(userGuid);
            }
            catch (Exception ex)
            {
                result = null;
                throw;
            }
            return result;
        }

        public UserSessionSCF GetUserSessionForSCF(string userGuid)
        {
            UserSessionSCF result = null;
            try
            {
                Tbl_UserSession userSession = new Tbl_UserSession();
                userSession = _userSessionRepository.GetUserSession(userGuid);

                result = new UserSessionSCF();

                if (userSession != null)
                {
                    result.userGuid = userSession.UserGuid;
                    result.username = userSession.Username;
                    result.password = String.Empty;
                    result.browserType = userSession.BrowserType;
                    result.ipAddress = userSession.IPAddress;
                    result.isTimeout = userSession.isTimeout;
                    result.loginTime = userSession.LoginTime;
                    result.lastAccessed = userSession.LastAccessed;
                    result.logoutTime = userSession.LogoutTime;
                    result.sessionData = userSession.SessionData;
                }

            }
            catch (Exception ex)
            {
                result = null;
                throw;
            }
            return result;
        }

    }
}
