﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.BillingCondition
{
    public partial interface IUserBillingConditionManager
    {
        bool CheckAcceeptBillingCondition(string username);

        bool UpdateBillingCondition(string userGUID);
    }
}
