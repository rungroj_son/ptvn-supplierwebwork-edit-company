﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.SC
{
    public class VerifyTicketCodeResponseDto
    {
        public bool isError { get; set; }
        public string errorMsg { get; set; }
    }
}
