﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class GetOrganizationShortDetailRequest
    {
        public string taxId;
        public string branchNo;
    }
}
