﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class Tbl_ContactPersonDto
    {
        public int ContactID { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string FirstName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Local { get; set; }
        public string LastName_Inter { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string OtherJobTitle { get; set; }
        public string Department { get; set; }
        public string PhoneCountryCode { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileCountryCode { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
        public string Email { get; set; }
        public Nullable<int> isDeleted { get; set; }

        public virtual Tbl_JobTitleDto Tbl_JobTitle { get; set; }
        public virtual Tbl_NameTitleDto Tbl_NameTitle { get; set; }
    }
}
