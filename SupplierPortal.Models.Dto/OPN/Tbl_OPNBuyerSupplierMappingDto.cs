﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.OPN
{
    public class Tbl_OPNBuyerSupplierMappingDto
    {
        public int ID { get; set; }
        public Nullable<int> BuyerID { get; set; }
        public Nullable<int> SupplierID { get; set; }
        public Nullable<int> RegID { get; set; }
        public string BuyerOrgID { get; set; }
        public Nullable<bool> PISApproved { get; set; }
        public Nullable<bool> IsJobSuccess { get; set; }
        public string JobErrorMsg { get; set; }
        public string InvitationCode { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    }
}
