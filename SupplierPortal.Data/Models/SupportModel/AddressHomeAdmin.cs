﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public partial class AddressHomeAdmin
    {
        public int SupplierID { get; set; }

        public string Username { get; set; }

        public string BuyerName { get; set; }

        public string Address { get; set; }

        public string Orgid { get; set; }

    }
}
