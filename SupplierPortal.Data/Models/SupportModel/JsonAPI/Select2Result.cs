﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.JsonAPI
{
    public partial class Select2Result
    {
        public string id { get; set; }
        public string text { get; set; }
    }

    public partial class Select2PagedResult
    {
        public int Total { get; set; }
        public List<Select2Result> Results { get; set; }
    }
}
