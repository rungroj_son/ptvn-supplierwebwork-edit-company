﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.AccountPortal
{
   public class NewsDisplayModel
    {
       public Nullable<int> NewsId { get; set; }
       public Nullable<int> LanguageID { get; set; }
       public Nullable<int> NewsGroupID { get; set; }
       public string NewsHeader { get; set; }
       public string NewsDetail { get; set; }
       public Nullable<int> SeqNo { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime ExpireDate { get; set; }
    }
}
