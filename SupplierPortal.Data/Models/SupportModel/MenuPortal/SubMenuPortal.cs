﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class SubMenuPortal
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public string ResourceName { get; set; }
        public Nullable<int> MenuLevel { get; set; }
        public string Section { get; set; }
        public Nullable<int> SeqNo { get; set; }
        public Nullable<int> SystemGrpID { get; set; }
        public Nullable<int> SystemPageID { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string SystemURL { get; set; }
        public string Icon { get; set; }
        public Nullable<int> IsActive { get; set; }
        public Nullable<int> PageID { get; set; }
        public Nullable<int> PrivilegeTypeID { get; set; }

        public Nullable<int> NotificationID { get; set; }
        public bool IsShowNotification { get; set; }
        public int NotificationValue { get; set; }

     
    }
}
