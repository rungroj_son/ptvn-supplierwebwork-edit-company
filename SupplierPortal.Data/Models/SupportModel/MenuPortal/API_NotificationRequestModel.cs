﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class API_NotificationRequestModel
    {
        public Request Request { get; set; }
    }

    public class Request
    {

        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public string APIName { get; set; }
        public DataRequest Data { get; set; }
    }

    public class DataRequest
    {
        public List<LoginID> LoginID { get; set; }
        public SupplierInfo SupplierInfo { get; set; }
    }

    public class LoginID
    {
        public string value { get; set; }
    }

    public class SupplierInfo
    {
        public string SupplierShortName { get; set; }
    }
}
