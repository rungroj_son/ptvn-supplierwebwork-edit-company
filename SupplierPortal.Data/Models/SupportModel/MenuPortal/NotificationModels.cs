﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class NotificationModels
    {
        public Nullable<int> SystemID { get; set; }
        public Nullable<int> SystemGrpID { get; set; }
        public Nullable<int> NotificationID { get; set; }
        public int NotificationValue { get; set; }
    }
}
