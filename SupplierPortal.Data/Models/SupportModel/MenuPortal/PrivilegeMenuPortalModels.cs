﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class PrivilegeMenuPortalModels
    {
        public string Username { get; set; }
        public string PrivilegeCode { get; set; }
        public int MenuID { get; set; }
        public int isAllowAccess { get; set; }
    }
}
