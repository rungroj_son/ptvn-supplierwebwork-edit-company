﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class NotificationModel
    {

        public int TopicID { get; set; }
        public int SystemID { get; set; }
        public dynamic SysUserID { get; set; }
        public dynamic DataKey { get; set; }
        public dynamic NotificationData { get; set; }
        public string StartTime { get; set; }
        public string StopTime { get; set; }
        public dynamic URLParameters { get; set; }
        public string NotificationKey { get; set; }
        public int UserID { get; set; }
        public int isRead { get; set; }
        public int isDeleted { get; set; }
        public int NotificationID { get; set; }
        public string TopicName { get; set; }
        public string Description { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<int> isAddStopTimeWhenRead { get; set; }
        public Nullable<int> StopTimeAdding { get; set; }
        public string TopicStringResource { get; set; }
        public string TopicStringResource_Header { get; set; }
        public string TopicURL { get; set; }
        public string TopicURLParameters { get; set; }
        public Nullable<int> HeaderLength { get; set; }
        public string countdownTimer { get; set; }
        public bool CheckCountdown { get; set; }
        public bool CheckTimeOut { get; set; }
        public string ResourceValue { get; set; }
        public int PageID { get; set; }
    }
}
