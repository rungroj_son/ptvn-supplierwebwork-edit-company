﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public partial class API_UpdateSupplierModel
    {
        public requestUpdate Request { get; set; }
    }

    public class requestUpdate
    {
        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public string APIName { get; set; }
        public dataUpdate Data { get; set; }

    }

    public class dataUpdate
    {
        public supplierUpdate SupplierInfo { get; set; }
        public orgUpdate OrgInfo { get; set; }
        public companyAddresslocalUpdate CompanyAddress_Local { get; set; }
        public companyAddressInterUpdate CompanyAddress_Inter { get; set; }
        public deliveredAddreslocalUpdate DeliveredAddress_Local { get; set; }
        public deliveredAddresInterUpdate DeliveredAddress_Inter { get; set; }
        public addBy EditBy { get; set; }
    }

    public class supplierUpdate
    {
        public string SupplierShortName { get; set; }
    }

    public class orgUpdate
    {
        public string OrgID { get; set; }
        public string TaxID { get; set; }
        public string OrgName_Local { get; set; }
        public string OrgName_Inter { get; set; }
        public string BranchNo { get; set; }
        public int CompanyType { get; set; }
        public int BusinessEntityID { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
    }

    public class companyAddresslocalUpdate
    {
        public string HouseNo { get; set; }
        public string VillageNo { get; set; }
        public string Lane { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class companyAddressInterUpdate
    {
        public string HouseNo { get; set; }
        public string VillageNo { get; set; }
        public string Lane { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class deliveredAddreslocalUpdate
    {
        public string HouseNo { get; set; }
        public string VillageNo { get; set; }
        public string Lane { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class deliveredAddresInterUpdate
    {
        public string HouseNo { get; set; }
        public string VillageNo { get; set; }
        public string Lane { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class addBy
    {
        public List<sysUserID> SysUserID { get; set; }
    }

    public class sysUserID
    {
        public string value { get; set; }
    }
}
