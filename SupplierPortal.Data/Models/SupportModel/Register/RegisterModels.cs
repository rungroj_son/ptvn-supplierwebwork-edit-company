﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class RegisterModels
    {
        public string Proc { get; set; }
        public Nullable<int> RegID { get; set; }
        public Nullable<int> RegStatusID { get; set; }
        public string TicketCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyType")]
        public Nullable<int> CompanyTypeID { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblBisEntityType")]
        public int BusinessEntityID { get; set; }

        //[ResourceDisplayName("_Regis.LblOtherBusinessEntity")]
        public string OtherBusinessEntity { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyName")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Local { get; set; }

        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Inter { get; set; }

        public string CountryCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyCountry")]
        public string CountryName { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblTaxNo.Corp")]
        //[RequiredErrorMessageBase("_Regis.ValidateMsg.RequireTaxID")]
        //[Remote("ValidateTaxID", "Register", AdditionalFields = "CountryCode_VI,CompanyTypeID")]
        public string TaxID { get; set; }

        public string BranchNo { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblMainBusiness")]
        //[RequiredErrorMessageBase("_Regis.ValidateMsg.RequireMainBusiness")]
        public string MainBusiness { get; set; }

        public Nullable<int> CompanyAddrID { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_Regis.ValidateMsg.AddressLength", 0)]
        public string RegCompanyAddress_HouseNo { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireVillageNo")]
        public string RegCompanyAddress_VillageNo { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireLane")]
        public string RegCompanyAddress_Lane { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireRoad")]
        public string RegCompanyAddress_Road { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireSubDistrict")]
        public string RegCompanyAddress_SubDistrict { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireCity")]
        public string RegCompanyAddress_City { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireState")]
        public string RegCompanyAddress_State { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequirePostalCode")]
        [Remote("PostalCodeValidateCompany", "Register", AdditionalFields = "RegCompanyAddress_CountryCode")]
        public string RegCompanyAddress_PostalCode { get; set; }

        //[ResourceDisplayName("_Regis.LblCountry")]

        public string RegCompanyAddress_CountryCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCountry")]
        public string RegCompanyAddress_CountryName { get; set; }

        public Nullable<int> MainContactID { get; set; }

        //[ResourceDisplayName("_Regis.LblTitleName")]
        public int NameTitleID { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblContactName")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireContactFirstName")]
        public string FirstName { get; set; }

        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireContactLastName")]
        public string LastName { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblJobTitle")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireJobTitle")]
        public int JobTitleID { get; set; }

        public string OtherJobTitle { get; set; }

        public string PhoneNoCountryCode { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblTel")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireTel")]
        public string PhoneNo { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblTelExt")]
        public string PhoneExt { get; set; }

        public string MobileNoCountryCode { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblMobileNo")]
        public string MobileNo { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblEmail")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireEmail")]
        [Remote("IsEmailAvailableConfig", "Register")]
        public string Email { get; set; }

        [ResourceDisplayName("_Regis.UserInfo.LblEmailConfirm")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireEmailConfirm")]
        [CompareErrorMessage("Email", "_Regis.ValidateMsg.NotMatchEmail")]
        public string ConfirmEmail { get; set; }

        public bool ServiceCheck1 { get; set; }

        public bool ServiceCheck2 { get; set; }

        public int[] ServiceTypeList { get; set; }

        [ResourceDisplayName("_Regis.RequestUser.UserInfo.LblEmail")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireEmail")]
        [Remote("IsContactEmailAvailableConfig", "Register")]
        public string ContactEmail { get; set; }

        [ResourceDisplayName("_Regis.RequestUser.UserInfo.LblEmailConfirm")]
        [RequiredErrorMessageBase("_Regis.ValidateMsg.RequireEmailConfirm")]
        [CompareErrorMessage("ContactEmail", "_Regis.ValidateMsg.NotMatchEmail")]
        public string ConfirmContactEmail { get; set; }

        //[Remote("CheckOrgContactPersonDuplicate", "Register", AdditionalFields = "LastName_Contact,hidSupplierID,hidService2")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireFirstName")]
        //public string FirstName_Contact { get; set; }

        //[Remote("CheckOrgContactPersonDuplicate", "Register", AdditionalFields = "FirstName_Contact,hidSupplierID,hidService2")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireLastName")]
        //public string LastName_Contact { get; set; }

        //[Remote("CheckOrgContactPersonDuplicate", "Register", AdditionalFields = "FirstName_Contact,LastName_Contact,hidSupplierID,hidService2")]
        //public string FullName_Contact { get; set; }

        public List<BusinessTypeRegModel> OrgBusinessType { get; set; }

        public string ProductKeyword { get; set; }

        public Nullable<decimal> RegisteredCapital { get; set; }

        public string CurrencyCode { get; set; }

        public string CategoryID_Lev3 { get; set; }

        public string InvetationCode { get; set; }

        public OPNSupplierRegister opnSupplierRegister { get; set; }
    }

    public class OPNSupplierRegister
    {
        public int regId { get; set; }
        public string buyerShortName { get; set; }
        public string email { get; set; }
        public int? buyerSupplierId { get; set; }
        public int? buyerSupplierContactId { get; set; }
        public int? supplierUserId { get; set; }
    }

    public class TicketcodeRenderView
    {
        public string Ticketcode { get; set; }
    }

    public class BusinessTypeRegModel
    {
        public int RegID { get; set; }
        public int BusinessTypeID { get; set; }
    }
}