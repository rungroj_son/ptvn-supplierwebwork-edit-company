﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class SendEmailDuplicateContactModels
    {
        public string SupplierID { get; set; }
        public string EmailAdminBSP { get; set; }
        public string DuplicateType { get; set; }
        public string UsernameDuplicate { get; set; }

        public int ContactTitleName { get; set; }
        public string FirstName_Contact { get; set; }
        public string LastName_Contact { get; set; }
        public int ContactJobTitle { get; set; }
        public string ContactOtherJobTitle { get; set; }
        public string ContactPhoneNoCountryCode { get; set; }
        public string ContactPhoneNo { get; set; }
        public string ContactPhoneExt { get; set; }
        public string ContactMobileCountryCode { get; set; }
        public string ContactMobile { get; set; }
        public string ContactEmail { get; set; }
        public string ContactInvitationCode { get; set; }

    }
}
