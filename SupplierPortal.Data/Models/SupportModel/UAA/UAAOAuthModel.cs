﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.UAA
{
    public class UAAOAuthModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
        public string auth_code { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string loginIPAddress { get; set; }
        public string serviceName { get; set; }
        public string jti { get; set; }
    }
}
