﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Manual
{
    public partial class ViewDataDocumentManual
    {
        public List<DocumentGroupModels> allDocumentGroups { get; set; }
        public List<DocumentListModels> allDocuments { get; set; }
    }
}
