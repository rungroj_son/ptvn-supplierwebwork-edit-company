﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class OrgCertifiedStdTableModel
    {
        public int CertId { get; set; }
        public int SupplierID { get; set; }
        public int SeqNo { get; set; }
        public int CertifiedID { get; set; }
        public string StandardName { get; set; }
        public string OtherStandardName { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }

        public IList<Tbl_OrgCertifiedStdAttachment> OrgCertifiedStdAttachment { get; set; }

    }
}
