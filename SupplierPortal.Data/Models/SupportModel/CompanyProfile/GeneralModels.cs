﻿using SupplierPortal.Data.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SupplierPortal.Data.Models.CustomValidate;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class GeneralModels
    {
        public int SupplierID { get; set; }
        public string OrgID { get; set; }
        public string CountryCode { get; set; }
        [ResourceDisplayName("_Regis.LblCountry")]
        public string CountryName { get; set; }
        [ResourceDisplayName("_Regis.LblTaxID")]
        public string TaxID { get; set; }
        [ResourceDisplayName("_Regis.LblDUNSNumber")]
        [RequiredErrorMessageBase("_ComProfile.ValidateMsg.RequireDUNSNumber")]
        public string DUNSNumber { get; set; }
        public int IsChecked { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public Nullable<int> BusinessEntityID { get; set; }
        public string OtherBusinessEntity { get; set; }
        public string CompName { get; set; }
        public string InvName { get; set; }
        public Nullable<int> BranchNo { get; set; }
        public string BranchName { get; set; }
        public Nullable<int> BusinessTypeID { get; set; }
        public string MainBusinessCode { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
        public string WebSite { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<double> Longtitude { get; set; }
        public string YearEstablished { get; set; }
        public Nullable<decimal> RegisteredCapital { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<int> NumberOfEmp { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }

        public bool IsCorperate { get; set; }

    }
}
