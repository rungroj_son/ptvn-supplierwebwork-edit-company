﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    [Serializable()]
    public partial class CompanyFacilityModel
    {
        //public int Id { get; set; }
        public int SupplierID { get; set; }
        public Nullable<int> SeqNo { get; set; }
        public Nullable<int> AddressID { get; set; }
        //[Required]
        public int FacilityID { get; set; }

        public int FacilityTypeID { get; set; }
        [RequiredErrorMessageBase("_ComPro.Facility.ValidateMsg.RequireFacilityType")]
        public string FacilityType { get; set; }

        [RequiredErrorMessageBase("_ComPro.Facility.ValidateMsg.RequireFacilityName")]
        public string FacilityName { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }

        [RegularErrorMessage(@"(https://)?(http://)?(ftp://)?(www\.)?\w+[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z]).[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$", "_ComPro.Facility.ValidateMsg.RequireWebsite")]
        public string WebSite { get; set; }
        
        public string HouseNo_Inter { get; set; }
        public string VillageNo_Inter { get; set; }
        public string Lane_Inter { get; set; }
        public string Road_Inter { get; set; }
        //[RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireSubDistrict")]
        public string SubDistrict_Inter { get; set; }
        //[RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireDistrictCity")]
        public string City_Inter { get; set; }
        //[RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireProvinceState")]
        public string State_Inter { get; set; }

        [RequiredErrorMessageBase("_ComPro.Facility.ValidateMsg.RequireHouseNo")]
        public string HouseNo_Local { get; set; }
        public string VillageNo_Local { get; set; }
        public string Lane_Local { get; set; }
        public string Road_Local { get; set; }
        //[RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireSubDistrict")]
        public string SubDistrict_Local { get; set; }
        //[RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireCity")]
        public string City_Local { get; set; }
        //[RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireState")]
        public string State_Local { get; set; }

        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        //[RegularErrorMessage(@"^[0-9]{5}(?:-[0-9]{4})?$", "_Regis.ValidateMsg.InvalidFormatPostalCode")]
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequirePostalCode")]
        public string PostalCode { get; set; }
        public string Command { get; set; }
        
        //public virtual Tbl_FacilityType Tbl_FacilityType { get; set; }


        //private static string GetStringResource(string resourecName)
        //{
        //    int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();
        //    //string _localizer = "";
        //    ILocaleStringRepository _repo = null;
        //    _repo = new LocaleStringRepository();
        //    string resFormat = _repo.GetResource(resourecName);

        //    if (string.IsNullOrEmpty(resFormat))
        //    {
        //        return resourecName;
        //    }
        //    return resFormat;

        //}

        // [RegularExpression(@"(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",ErrorMessage="")]

        //[RegularExpression(@"(http://)?(www\.)?\w+\.(com|net|edu|org)", ErrorMessage = "_ComPro.Facility.ValidateMsg.RequireWebsite")]
        //[RegularErrorMessage(@"(http://)?(www\.)?\w+\.(com|net|edu|org)", "_ComPro.Facility.ValidateMsg.RequireWebsite")]
        //[RegularErrorMessage(@"^(www\.|http:\/\/|https:\/\/|http:\/\/www\.|https:\/\/www\.)[a-z0-9]+\.[a-z]{2,4}$", "_ComPro.Facility.ValidateMsg.RequireWebsite")]


        //[RegularErrorMessage(@"(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$", "_ComPro.Facility.ValidateMsg.RequireWebsite")]
        //[RegularErrorMessage(@"^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$", "_ComPro.Facility.ValidateMsg.RequireWebsite")]

        //[Required]
        //[DataType(DataType.EmailAddress)]
        //[Display(Name = "Email")]
        //[AdditionalMetadata("Style", "Wide")]

        //[Required]
        //[Url] // Must require http|https it can't passing www.name.com
        //[DataType(DataType.Url)] // Every string be passing
        //[RequiredErrorMessageBase("_ComPro.Facility.ValidateMsg.RequireWebsite")]
        //@"(http://)?(www\.)?\w+\.(com|net|edu|org)"  // Only input www.name.com|net|edu|org  (com|net|edu|org|co.th|th|OR.TH|ac.th|go.th|in.th|)
    }
}
