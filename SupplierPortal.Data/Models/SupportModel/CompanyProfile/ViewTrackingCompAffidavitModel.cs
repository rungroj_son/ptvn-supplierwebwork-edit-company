﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewTrackingCompAffidavitModel
    {
        public int TrackingReqID { get; set; }
        public int TrackingItemID { get; set; }
        public int OldAttachmentID { get; set; }
        public Tbl_OrgAttachment OldAttachment { get; set; }
        public int NewAttachmentID { get; set; }
        public Tbl_OrgAttachment NewAttachment { get; set; }
        public int TrackingStatusID { get; set; }
        public string TrackingStatusName { get; set; }
        public int DocumentNameID { get; set; }
        public string DocumentName { get; set; }

    }
}
