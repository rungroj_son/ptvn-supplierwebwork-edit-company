﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.OPN
{
    public partial class SupplierOPNModel
    {
        public int buyerSupplierId { get; set; }
        public int buyerSupplierContactId { get; set; }
        public int supplierUserId { get; set; }
        public string password { get; set; }
        public string taxId { get; set; }
        public decimal registeredCapital { get; set; }
        public string currency { get; set; }
        public string yearEstablished { get; set; }
        public Boolean manufacturer { get; set; }
        public Boolean distributor { get; set; }
        public Boolean dealer { get; set; }
        public Boolean serviceProvider { get; set; }
        public string address { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string contactName { get; set; }
        public string department { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string paymentMethod { get; set; }
        public string paymentTerm { get; set; }
        public Nullable<DateTime> deliveryDate { get; set; }
        public Boolean supplierDirectory { get; set; }

    }
}
