﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.SupplyManagement
{
    public partial class ViewDataReportModels
    {
        public List<ReportGroupModels> allReportGroups { get; set; }
        public List<ReportListModels> allReports { get; set; }

    }
}
