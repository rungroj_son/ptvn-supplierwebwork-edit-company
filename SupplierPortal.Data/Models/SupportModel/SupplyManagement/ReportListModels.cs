﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.SupplyManagement
{
    public partial class ReportListModels
    {

        public string ReportName { get; set; }
        public string ReportDetails { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string OrgID { get; set; }
        public string UserID { get; set; }
        public int RptID { get; set; }
        public Nullable<int> ReportGroupID { get; set; }
        public Nullable<int> SystemID { get; set; }
        public Nullable<int> SystemPageID { get; set; }
        public string SystemURL { get; set; }
        public Nullable<int> isEnableService { get; set; }

        //public virtual ReportGroupModels ReportGroupModels { get; set; }
    }
}
