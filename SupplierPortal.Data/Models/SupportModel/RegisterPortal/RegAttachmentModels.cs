﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class RegAttachmentListModels
    {

        public IList<CustomerDocNameModels> DocumentName { get; set; }
        public IList<Tbl_RegAttachment> RegAttachment { get; set; }
        public int CompanyTypeID { get; set; }

    }
}
