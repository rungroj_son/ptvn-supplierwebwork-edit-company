﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class ProductandServiceResultModels
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }
}
