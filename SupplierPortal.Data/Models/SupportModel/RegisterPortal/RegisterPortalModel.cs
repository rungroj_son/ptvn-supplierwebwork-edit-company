﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    //TRS @14-11-2014 Helper Class for Register
    public class RegisterPortalModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Confirmemail { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CountryCode { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }

        [Required]
        public string TaxID { get; set; }

        [Required]
        public string CompanyName { get; set; }

        public string BranchName { get; set; }

        public string BranchNo { get; set; }
    }
    //-------------------------------------END---------------------------------------    
    //TRS @18-11-2014 Helper Class for Registercontinue
    public class RegisContinuePortalModel
    {
        [ResourceDisplayName("_Regis.LblEmail")]
        //[RequiredErrorMessage("_Regis.Msg_RequireEmail")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        [Remote("IsEmailAvailableConfig", "RegisterPortal", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        [ResourceDisplayName("_Regis.LblName")]
        //[RequiredErrorMessage("_Regis.Msg_RequireFirstName")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireFirstName")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CountryCode { get; set; }
        [ResourceDisplayName("_Regis.LblCountry")]
        public string CountryName { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public Tbl_Country Country { get; set; }

        [ResourceDisplayName("_Regis.LblTaxID")]
        [TaxIDInfosEditValidate("_Regis.Msg_TaxIDDuplicate_Reg_Await")]
        public string TaxID { get; set; }

        public string TicketCode { get; set; }

        public int RegID { get; set; }

        [ResourceDisplayName("_Regis.LblDUNSNumber")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireDUNSNumber")]
        [DunsValidate("_Regis.Msg_DunsFormat")]
        public string DUNSNumber { get; set; }

        public IEnumerable<Tbl_Language> LanguageList { get; set; }

        public DynamicLanguageModels DynamicLanguageList { get; set; }

       

        [ResourceDisplayName("_Regis.LblCompanyType")]
        public Nullable<int> CompanyTypeID { get; set; }
        public bool IsCorperate { get; set; }

        [ResourceDisplayName("_Regis.LblBusinessEntity")]
        //[RequiredErrorMessage("_Regis.Msg_RequireBusinessEntity")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireBusinessEntity")]
        public Nullable<int> BusinessEntityID { get; set; }
        //public IEnumerable<SelectListItem> BusinessEntityList { get; set; }

        [ResourceDisplayName("_Regis.LblOtherBusinessEntity")]
        public string OtherBusinessEntity { get; set; }

        [ResourceDisplayName("_Regis.LblBusinessType")]
        public Nullable<int> BusinessTypeID { get; set; }
        public bool IsHead { get; set; }

        [ResourceDisplayName("_Regis.LblBranchNo")]
        public string BranchNo { get; set; }
        //public IEnumerable<SelectListItem> BranchNoList { get; set; }

        [ResourceDisplayName("_Regis.LblCompanyName")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCompanyName")]
        public string CompanyName_Local { get; set; }

        [ResourceDisplayName("_Regis.LblCompanyName")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCompanyName")]
        public string CompanyName_Inter { get; set; }

        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCompanyName")]
        public IList<LocalizedPropModel> CompNameLocalizedList { get; set; }

        [ResourceDisplayName("_Regis.LblBranchName")]
        public string BranchName_Local { get; set; }

        [ResourceDisplayName("_Regis.LblBranchName")]
        public string BranchName_Inter { get; set; }

        public IList<LocalizedPropModel> BrNameLocalizedList { get; set; }

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegCompanyAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegCompanyAddress_HouseNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblVillageNo")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegCompanyAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblVillageNo")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegCompanyAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegCompanyAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegCompanyAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegCompanyAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegCompanyAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegCompanyAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegCompanyAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegCompanyAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegCompanyAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegCompanyAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegCompanyAddress_State_Inter { get; set; }



        [ResourceDisplayName("_Regis.LblPostalCode")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "companyaddress")]
        [Remote("PostalCodeValidateCompany", "RegisterPortal", AdditionalFields = "RegCompanyAddress_CountryCode_VI")]
        public string RegCompanyAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.LblCountry")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireCountryCode")]
        public string RegCompanyAddress_CountryCode { get; set; }
        //public IEnumerable<SelectListItem> RegCompanyAddress_CountryList { get; set; }
        //---------------------------------------------------------

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegDeliverAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegDeliverAddress_HouseNo_Inter { get; set; }


        [ResourceDisplayName("_Regis.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegDeliverAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegDeliverAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegDeliverAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegDeliverAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegDeliverAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegDeliverAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegDeliverAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegDeliverAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegDeliverAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegDeliverAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegDeliverAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegDeliverAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.LblPostalCode")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidateDeliver", "RegisterPortal", AdditionalFields = "RegDeliverAddress_CountryCode_VI")]
        public string RegDeliverAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.LblCountry")]
        [RequiredDeliverErrorMessage("_Regis.Msg_RequireCountryCode")]
        public string RegDeliverAddress_CountryCode { get; set; }


        //-----------------Personal----------------------------------------------

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegPersonalAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblHouseNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireHouseNo")]
        public string RegPersonalAddress_HouseNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblVillageNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegPersonalAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.LblVillageNo")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string RegPersonalAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegPersonalAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.LblLane")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string RegPersonalAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegPersonalAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.LblRoad")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string RegPersonalAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegPersonalAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.LblSubDistrict")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public string RegPersonalAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegPersonalAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.LblCity")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireCity")]
        public string RegPersonalAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegPersonalAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.LblState")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireState")]
        public string RegPersonalAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.LblPostalCode")]
        [RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidatePersonal", "RegisterPortal", AdditionalFields = "RegPersonalAddress_CountryCode_VI")]
        public string RegPersonalAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.LblCountry")]
        [RequiredDeliverErrorMessage("_Regis.Msg_RequireCountryCode")]
        public string RegPersonalAddress_CountryCode { get; set; }

        //public IEnumerable<SelectListItem> RegDeliverAddress_CountryList { get; set; }
        //---------------------------------------------------------

        [ResourceDisplayName("_Regis.LblProdServMainBus")]
        public string MainBusinessCode { get; set; }

        public string YearEstablished { get; set; }
        public Nullable<decimal> RegisteredCapital { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<int> NumberOfEmp { get; set; }

        public Nullable<int> CompanyAddrID { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireHouseNo")]
        public IList<LocalizedPropModel> ComAddrHouseNoLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireVillageNo")]
        public IList<LocalizedPropModel> ComAddrVillageNoLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireLane")]
        public IList<LocalizedPropModel> ComAddrLaneLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireRoad")]
        public IList<LocalizedPropModel> ComAddrRoadLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public IList<LocalizedPropModel> ComAddrSubDistrictLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireCity")]
        public IList<LocalizedPropModel> ComAddrCityLocalizedList { get; set; }
        //[RequiredErrorMessage("_Regis.Msg_RequireState")]
        public IList<LocalizedPropModel> ComAddrStateLocalizedList { get; set; }

        public bool IsSameAddress { get; set; }

        public bool IsBtnSubmit { get; set; }

        public bool IsChangeEmail { get; set; }

        public Nullable<int> DeliveredAddrID { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireHouseNo")]
        public IList<LocalizedPropModel> DelAddrHouseNoLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireVillageNo")]
        public IList<LocalizedPropModel> DelAddrVillageNoLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireLane")]
        public IList<LocalizedPropModel> DelAddrLaneLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireRoad")]
        public IList<LocalizedPropModel> DelAddrRoadLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public IList<LocalizedPropModel> DelAddrSubDistrictLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireCity")]
        public IList<LocalizedPropModel> DelAddrCityLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireState")]
        public IList<LocalizedPropModel> DelAddrStateLocalizedList { get; set; }


        public Nullable<int> PersonalAddrID { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireHouseNo")]
        public IList<LocalizedPropModel> PerAddrHouseNoLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireVillageNo")]
        public IList<LocalizedPropModel> PerAddrVillageNoLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireLane")]
        public IList<LocalizedPropModel> PerAddrLaneLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireRoad")]
        public IList<LocalizedPropModel> PerAddrRoadLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireSubDistrict")]
        public IList<LocalizedPropModel> PerAddrSubDistrictLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireCity")]
        public IList<LocalizedPropModel> PerAddrCityLocalizedList { get; set; }
        //[RequiredDeliverErrorMessage("_Regis.Msg_RequireState")]
        public IList<LocalizedPropModel> PerAddrStateLocalizedList { get; set; }

        public int Index { get; set; }

        [ResourceDisplayName("_Regis.LblPhoneNo")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequirePhoneNo")]
        public string PhoneNo { get; set; }

        [ResourceDisplayName("_Regis.LblPhoneExt")]
        public string PhoneExt { get; set; }

        [ResourceDisplayName("_Regis.LblMobileNo")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireMobileNo")]
        public string MobileNo { get; set; }

        [ResourceDisplayName("_Regis.LblFaxNo")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireFaxNo")]
        public string FaxNo { get; set; }

        [ResourceDisplayName("_Regis.LblFaxExt")]
        public string FaxExt { get; set; }

        [ResourceDisplayName("_Regis.LblWebSite")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireWebSite")]
        //[RegularErrorMessage(@"^(www\.|http:\/\/|https:\/\/|http:\/\/www\.|https:\/\/www\.)[a-z0-9]+\.[a-z]{2,4}$", "_Regis.Msg_WebsiteInvalid")]
        public string WebSite { get; set; }

        [ResourceDisplayName("_Regis.LblLatitude")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireLatitude")]
        public Nullable<double> Latitude { get; set; }

        [ResourceDisplayName("_Regis.LblLongtitude")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireLongtitude")]
        public Nullable<double> Longtitude { get; set; }

        public int maxlength { get; set; }

        public Nullable<int> MainContactID { get; set; }
        [ResourceDisplayName("_Regis.LblTitleName")]
        public Nullable<int> RegContact_TitleID { get; set; }
        [ResourceDisplayName("_Regis.LblTitleName")]
        public Nullable<int> RegContact_NameTitleID { get; set; }

        [ResourceDisplayName("_Regis.LblContactName")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireFirstName")]
        public string RegContact_FirstName_Local { get; set; }

        [ResourceDisplayName("_Regis.LblContactName")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireFirstName")]
        public string RegContact_FirstName_Inter { get; set; }


        [RequiredSubmitErrorMessage("_Regis.Msg_RequireLastName")]
        public string RegContact_LastName_Local { get; set; }

        [RequiredSubmitErrorMessage("_Regis.Msg_RequireLastName")]
        public string RegContact_LastName_Inter { get; set; }

        [ResourceDisplayName("_Regis.LblContactJobTitle")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireJobTitle")]
        public Nullable<int> RegContact_JobTitleID { get; set; }
        public string RegContact_OtherJobTitle { get; set; }

        [ResourceDisplayName("_Regis.LblContactDepartment")]
        public string RegContact_Department { get; set; }

        [ResourceDisplayName("_Regis.LblContactPhoneNo")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequirePhoneNo")]
        public string RegContact_PhoneNo { get; set; }

        [ResourceDisplayName("_Regis.LblContactPhoneNoExt")]
        public string RegContact_PhoneExt { get; set; }

        [ResourceDisplayName("_Regis.LblContactFaxNo")]
        public string RegContact_FaxNo { get; set; }

        [ResourceDisplayName("_Regis.LblContactFaxNoExt")]
        public string RegContact_FaxExt { get; set; }

        [ResourceDisplayName("_Regis.LblContactMobile")]
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireMobileNo")]
        public string RegContact_MobileNo { get; set; }

        [ResourceDisplayName("_Regis.LblContactEmail")]
        [RequiredSubmitErrorMessage("_Regis.Msg_RequireEmail")]
        [RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        public string RegContact_Email { get; set; }

        public IList<LocalizedPropModel> ContactFirstNameLocalizedList { get; set; }
        public IList<LocalizedPropModel> ContactLastNameLocalizedList { get; set; }

        public RegProductModel RegProduct { get; set; }
        public IList<RegProductGridViewModel> RegProductGridViewModelList { get; set; }

        [ResourceDisplayName("_Regis.LblDocumentType")]
        public Nullable<int> DocumentTypeID { get; set; }

        [RequiredSubmitErrorMessage("_Regis.MsgRequireDocFile")]
        public HttpPostedFileBase FileAttachment { get; set; }

        public RegAttachmentModel RegAttachment { get; set; }
        public IList<RegAttachmentGridViewModel> RegAttachmentGridViewModelList { get; set; }
    }

    public class LocalizedPropModel
    {
        public Nullable<int> EntityID { get; set; }
        public Nullable<int> LanguageID { get; set; }
        public string LocaleKeyGroup { get; set; }
        public string LocaleKey { get; set; }
        //[RequiredSubmitErrorMessage("_Regis.Msg_RequireCompanyName")]
        public string LocaleValue { get; set; }
        public string Flag { get; set; }
    }

    public class RegAttachmentModel
    {
        public int RegID { get; set; }
        public int AttachmentID { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentNameUnique { get; set; }
        public string SizeAttach { get; set; }
        public Nullable<int> DocumentTypeID { get; set; }
    }

    public class RegAttachmentGridViewModel
    {
        public int AttachmentID { get; set; }
        public Nullable<int> DocumentTypeID { get; set; }
        public string DocumentType { get; set; }
        public string AttachmentName { get; set; }
        public string ActionLog { get; set; }
        public int RowNumber { get; set; }
    }

    public class RegProductModel
    {
        public int RegID { get; set; }

        [ResourceDisplayName("_Regis.LblProdServKeyword")]
        public string ProductCode { get; set; }

        [ResourceDisplayName("_Regis.LblProdServType")]
        public Nullable<int> ProductTypeID { get; set; }
    }

    public class RegProductGridViewModel
    {
        public int RegID { get; set; }
        public int ProdTypeID { get; set; }
        public string OldProdTypeID { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ActionLog { get; set; }
    }
    
    public class LanguageViewModel
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }
        public string FlagImageFileName { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public Nullable<int> isStandard { get; set; }
    }

    public class ResultRegister
    {
        public string Message { get; set; }
        public bool Result { get; set; }
        public string Email { get; set; }
        public string Ticketcode { get; set; }
    }

    public class TempBusinessEntityType
    {
        public int BusinessEntityID { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public string BusinessEntity { get; set; }
        public string LocaleValue { get; set; }
        public Nullable<int> SeqNo { get; set; }
    }

    public class TempForMultiLang
    {
        public Nullable<int> EntityID { get; set; }
        public string EntityValue { get; set; }
    }

    public class TempForMultiLangStrID
    {
        public string EntityID { get; set; }
        public string EntityValue { get; set; }
    }

    public class TempProductModel
    {
        public Nullable<int> ID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }

    public class TempProServModel
    {
        public Nullable<int> ProdTypeID { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }

    public class SetformatPhoneNumberModel
    {
        public string CountryCode { get; set; }
        public string Number { get; set; }
        public string Message { get; set; }
    }

    public class SendingMailModel
    {
        public string Company { get; set; }
        public string AdminName { get; set; }

        public string NameTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string EmailTo { get; set; }
        public string MailContent { get; set; }
        public string Message { get; set; }
        public string Alert { get; set; }


        public bool ChkAddContactOrg { get; set; }
        public int SupplierID { get; set; }
        public int ContactID { get; set; }
        public string ContactEmail { get; set; }
        public int ContactTitleName { get; set; }
        public string FirstName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Local { get; set; }
        public string LastName_Inter { get; set; }
        public int ContactJobTitle { get; set; }
        public string ContactOtherJobTitle { get; set; }
        public string ContactDepartment { get; set; }
        public string ContactPhoneNo { get; set; }
        public string ContactPhoneExt { get; set; }
        public string ContactMobile { get; set; }
        public string ContactFaxNo { get; set; }
        public string ContactFaxExt { get; set; }
    }

    public class ProductSearchModel
    {
        public string Keyword { get; set; }
        public string ProductTypeID { get; set; }
        public string Message { get; set; }
        public string LangID { get; set; }
    }

    public class ProductSearchResultModel
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }

    public class ProductAddModel
    {
        public string ProductCode { get; set; }
    }

    public class ProductTypeAddModel
    {
        public int ProductTypeID { get; set; }
    }

    public class CheckUpdateProductModel
    {
        public int StatusUpdate { get; set; } // 1 = Check / 2 = Un Check
        public int ProductTypeID { get; set; }
    }

    //public class CustomerDocNameModel
    //{
    //    public int DocumentNameID { get; set; }
    //    public string DocumentName { get; set; }
    //    public int SeqNo { get; set; }
    //    public int DocumentTypeID { get; set; }
    //}

    //-------------------------------------END---------------------------------------    
}
