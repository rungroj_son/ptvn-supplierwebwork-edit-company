﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class CompanyTypeModels
    {

        public int CompanyTypeID { get; set; }
        public string CompanyType { get; set; }
    }
}
