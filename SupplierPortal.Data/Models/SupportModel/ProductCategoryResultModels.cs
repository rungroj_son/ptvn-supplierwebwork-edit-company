﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class ProductCategoryResultModels
    {
        public int CategoryID_Lev3 { get; set; }
        public string CategoryName { get; set; }
    }
}
