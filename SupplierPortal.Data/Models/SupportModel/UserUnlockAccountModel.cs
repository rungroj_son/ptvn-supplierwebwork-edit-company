﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class UserUnlockAccountModel
    {
        public int ID { get; set; }

        public string Username { get; set; }

        public string Fullname { get; set; }

        public string CompanyName { get; set; }
    }
}
