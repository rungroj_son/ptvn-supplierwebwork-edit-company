﻿using System;
using System.Collections.Generic;


namespace SupplierPortal.Data.Models.SupportModel.BillingDetail
{
    public partial class BillingAndPaymentModels
    {
        public int SupplierID { get; set; }
        public string DocNo { get; set; }
        public Nullable<int> DocTypeID { get; set; }
        public Nullable<int> DocStatusID { get; set; }
        public string DocType { get; set; }
        public string DocStatus { get; set; }
        public Nullable<System.DateTime> DocDate { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<decimal> Volume { get; set; }
        public string VolumeCurrency { get; set; }
        public Nullable<decimal> InvoiceAmount { get; set; }
        public Nullable<decimal> CNAmount { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }
        public Nullable<decimal> RemainAmount { get; set; }
        public Nullable<System.DateTime> CalcBeginDate { get; set; }
        public Nullable<System.DateTime> CalcEndDate { get; set; }
        public Nullable<decimal> BillingServiceID { get; set; }
        public string BillingService { get; set; }
        public string dateTime { get; set; }
        public Nullable<int> offSet { get; set; }
    }
}
