﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class ContactPersonModels
    {

        public int ContactID { get; set; }
        public string ContactName { get; set; }
    }
}
