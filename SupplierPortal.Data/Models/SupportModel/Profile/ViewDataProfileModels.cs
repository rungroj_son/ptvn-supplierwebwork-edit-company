﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class ViewDataProfileModels
    {
        public GeneralModels GeneralData { get; set; }

        public List<Tbl_OPNBuyerSupplierMappingViewModels> OPNBuyerSupplierMappings { get; set; }

        public ChangePasswordModels ChangePasswordData { get; set; }
    }
}
