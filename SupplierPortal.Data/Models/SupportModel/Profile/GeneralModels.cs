﻿using SupplierPortal.Data.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SupplierPortal.Data.Models.CustomValidate;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class GeneralModels
    {
        public string UserUpdate { get; set; }
        public Nullable<int> UserID { get; set; }
        public string UserLoginID { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireEmail")]
        [Remote("IsEmailAvailableConfig", "Profile", AdditionalFields = "InitialEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        public string Email { get; set; }
        public string InitialEmail { get; set; }
        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireTitleName")]
        public Nullable<int> TitleID { get; set; }
        public Nullable<int> TitleID_Inter { get; set; }


        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireFirstName")]
        public string FirstName_Local { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLastName")]
        public string LastName_Local { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireFirstName")]
        public string FirstName_Inter { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLastName")]
        public string LastName_Inter { get; set; }

        public Nullable<int> JobTitleID { get; set; }
        public string OtherJobTitle { get; set; }
        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireDepartment")]
        public string Department { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequirePhone")]
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }


        public Nullable<int> AddressID { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_Profile.ValidateMsg.AddressLength", 0)]
        public string Address_HouseNo_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblHouseNo")]
        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireHouseNo")]
        //public string Address_HouseNo_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblVillageNo")]
        public string Address_VillageNo_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblVillageNo")]
        //public string Address_VillageNo_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblLane")]
        public string Address_Lane_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblLane")]
        //public string Address_Lane_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblStreet")]
        public string Address_Road_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblStreet")]
        //public string Address_Road_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblSubDistrict")]
        public string Address_SubDistrict_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblSubDistrict")]
        //public string Address_SubDistrict_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblCity")]
        public string Address_City_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblCity")]
        //public string Address_City_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblState")]
        public string Address_State_Local { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblState")]
        //public string Address_State_Inter { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequirePostalCode")]
        [Remote("PostalCodeValidateCompany", "Profile", AdditionalFields = "Address_CountryCode")]
        public string Address_PostalCode { get; set; }

        //[ResourceDisplayName("_Profile.CompanyInfo.Address.LblCountry")]
        public string Address_CountryCode { get; set; }
        public string Address_CountryName { get; set; }




        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireTimeZone")]
        public string TimeZoneIDString { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLocale")]
        public string LocaleName { get; set; }
        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireCurrency")]
        public string CurrencyCode { get; set; }
        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLanguage")]
        public string LanguageCode { get; set; }
        public bool IsPublic { get; set; }
    }
}
