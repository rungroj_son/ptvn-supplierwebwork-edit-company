﻿using System;
using System.Collections.Generic;


namespace SupplierPortal.Data.Models.SupportModel.LinkSystem
{
    public class ResultVerifyUserSessionModel
    {
            public string UserGUID { get; set; }
            public string TimeZone { get; set; }
            public string LanguageCode { get; set; }
            public string PortalLoginID { get; set; }
            public string PortalUsername { get; set; }
            public string PortalUser { get; set; }
            public string NameLocal { get; set; }
            public string NameEng { get; set; }
            public string Organization { get; set; }
            public string OrgID { get; set; }
            public string SupplierID { get; set; }
            public List<SysUserID> SysUserID { get; set; }
            public List<ModifiedSysUserID> ModifiedSysUserID { get; set; }
            public List<Menu> Menu { get; set; }



    }
}

