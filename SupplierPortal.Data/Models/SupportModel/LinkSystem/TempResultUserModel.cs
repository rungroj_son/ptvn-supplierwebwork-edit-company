﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.LinkSystem
{
    public class TempResultUserModel
    {

        public string Username { get; set; }
        public string LanguageCode { get; set; }
    }

}
