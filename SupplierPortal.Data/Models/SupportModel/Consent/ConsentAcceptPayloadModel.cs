﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Consent
{
    public class ConsentAcceptPayloadModel
    {
        public string id { get; set; }
        public string consentApplication { get; set; }
        public string acceptanceDate { get; set; }
        public string expiredDate { get; set; }
        public string consentId { get; set; }
        public string dataSubject { get; set; }
        public string userAgent { get; set; }
        public string country { get; set; }
        public string localIp { get; set; }
        public string osVersion { get; set; }
        public string model { get; set; }
        public string deviceFingerprintId { get; set; }
        public string note { get; set; }
        public string status { get; set; }
    }
}
