﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.DynamicData
{
    public partial class DynamicDataModel
    {

        public dynamic LocalizedProperty { get; set; }
    }
}
