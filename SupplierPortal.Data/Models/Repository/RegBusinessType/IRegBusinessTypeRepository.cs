﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Register;

namespace SupplierPortal.Data.Models.Repository.RegBusinessType
{
    public partial interface IRegBusinessTypeRepository
    {
        void InsertRegBusinessType(Tbl_RegBusinessType model);

        void UpdateRegBusinessType(Tbl_RegBusinessType model);

        IEnumerable<Tbl_RegBusinessType> GetRegBusinessTypeByRegID(int regid);

        void RemoveRegBusinessTypeByRegID(int RegisId);

        void Dispose();
    }
}
