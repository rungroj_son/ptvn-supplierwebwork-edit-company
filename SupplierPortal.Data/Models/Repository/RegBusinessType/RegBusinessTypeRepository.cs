﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogRegBusinessType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegBusinessType
{
    public partial class RegBusinessTypeRepository : IRegBusinessTypeRepository
    {

        private SupplierPortalEntities context;
        ILogRegBusinessTypeRepository _logRegBusinessTypeRepository;

        public RegBusinessTypeRepository()
        {
            context = new SupplierPortalEntities();
            _logRegBusinessTypeRepository = new LogRegBusinessTypeRepository();
        }

        public RegBusinessTypeRepository(
            SupplierPortalEntities ctx,
             LogRegBusinessTypeRepository logRegBusinessTypeRepository
            )
        {
            this.context = ctx;
            _logRegBusinessTypeRepository = logRegBusinessTypeRepository;
        }


        public void InsertRegBusinessType(Tbl_RegBusinessType model)
        {
            try
            {
                int RegID = 0;

                if (model != null)
                {
                    context.Tbl_RegBusinessType.Add(model);
                    context.SaveChanges();

                    RegID = model.RegID;

                    _logRegBusinessTypeRepository.Insert(model.Id,RegID,model.BusinessTypeID, "Add");
                    //_logRegAddressRepository.Insert(addressID, "Add");
                    //_logRegAddressRepository.Dispose();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public void UpdateRegBusinessType(Tbl_RegBusinessType model)
        {
            try
            {
                int RegID = 0;

                if (model != null)
                {
                    context.Tbl_RegBusinessType.Add(model);
                    context.SaveChanges();

                    RegID = model.RegID;

                    _logRegBusinessTypeRepository.Insert(model.Id, RegID, model.BusinessTypeID, "Edit");
                    //_logRegAddressRepository.Insert(addressID, "Add");
                    //_logRegAddressRepository.Dispose();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual IEnumerable<Tbl_RegBusinessType> GetRegBusinessTypeByRegID(int regid)
        {
            return (from list in context.Tbl_RegBusinessType
                    where list.RegID == regid
                    select list).ToList<Tbl_RegBusinessType>();
        }

        public void RemoveRegBusinessTypeByRegID(int RegisId)
        {
            try
            {
                var data = context.Tbl_RegBusinessType.Where(a => a.RegID == RegisId);
                context.Tbl_RegBusinessType.RemoveRange(data);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }



    }
}
