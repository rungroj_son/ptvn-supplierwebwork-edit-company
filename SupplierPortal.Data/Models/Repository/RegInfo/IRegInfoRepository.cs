﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.Register;

namespace SupplierPortal.Data.Models.Repository.RegInfo
{
    public partial interface IRegInfoRepository
    {
        bool Insert(RegisterPortalModel model, string ticketcode, string languageCode);

        IEnumerable<Tbl_RegInfo> GetRegInfoAll();

        IEnumerable<Tbl_RegInfo> GetRegInfoListByTaxID(string taxid);

        IEnumerable<Tbl_RegInfo> GetRegInfoListByTicketcode(string ticketcode);

        IEnumerable<Tbl_RegInfo> GetRegInfoByRegstatusid(int regstatusid);

        IEnumerable<Tbl_RegInfo> GetRegInfoByRegID(int regid);

        IEnumerable<Tbl_RegInfo> GetRegInfoByInfosConcat(string infosconcat);

        bool CheckDuplicateRegInfo(string infosconcat);

        bool CheckDuplicateRegInfoVerifyString(string infosconcat, string oldRegID);

        bool CheckDuplicateRegisterFromConnect(string taxID, string branchNumber, string countryCode);

        int InsertReturnRegID(Tbl_RegInfo model);

        void UpdateRegInfo(Tbl_RegInfo model);

        Tbl_RegInfo GetRegInfoByTicketcode(string ticketcode);

        bool CheckDuplicateRegInfoByTaxIDAndBranchNo(string taxID, string branchNo, string oldRegID);

        Tbl_RegInfo GetRegInfoByTaxIDAndBranchNo(string taxID, string branchNo);

        List<Tbl_RegInfo> GetRegInfoByRegListAndStatusList(List<int> listRegId, List<int> listStatusId);

        void Dispose();
    }
}
