﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;

namespace SupplierPortal.Data.Models.Repository.EmailAccount
{
    public partial class EmailAccountRepository : IEmailAccountRepository
    {


        private SupplierPortalEntities context;


        public EmailAccountRepository()
        {
            context = new SupplierPortalEntities();
        }

        public EmailAccountRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_EmailAccount> GetEmailAccountByEmailAccountID(int id)
        {
            return (from list in context.Tbl_EmailAccount
                    where list.EmailAccountID == id
                    select list).ToList<Tbl_EmailAccount>();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
