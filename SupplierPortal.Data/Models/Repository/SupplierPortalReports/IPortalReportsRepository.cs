﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SupplierPortalReports
{
    public partial interface IPortalReportsRepository
    {

        IEnumerable<Tbl_SupplierPortalReports> List();

        IEnumerable<Tbl_SupplierPortalReports> GetReportsListByUser(string username);




        void Dispose();


    }
}
