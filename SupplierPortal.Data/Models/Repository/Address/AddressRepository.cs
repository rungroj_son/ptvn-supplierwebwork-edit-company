﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogAddress;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.Address
{
    public partial class AddressRepository : IAddressRepository
    {
        private SupplierPortalEntities context;
        ILogAddressRepository _logAddressRepository;

        public AddressRepository()
        {
            context = new SupplierPortalEntities();
            _logAddressRepository = new LogAddressRepository();
        }

        public AddressRepository(
            SupplierPortalEntities ctx,
            LogAddressRepository logAddressRepository
            )
        {
            this.context = ctx;
            _logAddressRepository = logAddressRepository;
        }

        public virtual Tbl_Address GetAddressByUsername(string username)
        {
            var query = from db in context.Tbl_User
                        join org in context.Tbl_Organization on db.SupplierID equals org.SupplierID into a
                        from b in a.DefaultIfEmpty()
                        join oad in context.Tbl_OrgAddress on new { b.SupplierID } equals new { oad.SupplierID } into c
                        from d in c.DefaultIfEmpty()
                        join ad in context.Tbl_Address on new { d.AddressID } equals new { ad.AddressID } into e
                        from f in e.DefaultIfEmpty()
                        where db.Username == username
                        && d.AddressTypeID == 1
                        && b.isDeleted != 1
                        select f;

            return query.FirstOrDefault();


        }


        public virtual Tbl_Address GetAddressByUsernameOrderByDesc(string username)
        {
            var query = (from db in context.Tbl_User
                         join org in context.Tbl_Organization on db.OrgID equals org.OrgID into a
                         from b in a.DefaultIfEmpty()
                         join oad in context.Tbl_OrgAddress on new { b.SupplierID } equals new { oad.SupplierID } into c
                         from d in c.DefaultIfEmpty()
                         join ad in context.Tbl_Address on new { d.AddressID } equals new { ad.AddressID } into e
                         from f in e.DefaultIfEmpty()
                         where db.Username == username
                         && d.AddressTypeID == 1
                         && b.isDeleted != 1
                         select f).OrderByDescending(m => m.AddressID);

            return query.FirstOrDefault();


        }

        public virtual Tbl_Address GetAddressByAddressID(int addressID)
        {
            var query = context.Tbl_Address.Where(m => m.AddressID == addressID).FirstOrDefault();

            return query;
        }

        public virtual Tbl_Address GetAddressBySupplierID(int SupplierID)
        {
            return (from a in context.Tbl_OrgAddress
                    join b in context.Tbl_Address on a.AddressID equals b.AddressID
                    where a.SupplierID == SupplierID
                    && a.AddressTypeID == 1
                    select b).FirstOrDefault();
        }

        public virtual List<AddressHomeAdmin> GetListDataAddressForHomeAdmin(int languageID)
        {
            var dataOrganization = (from a in context.Tbl_Organization
                                    join be in context.Tbl_BusinessEntity on a.BusinessEntityID equals be.Id into join_be
                                    from jbe in join_be.DefaultIfEmpty()
                                    join lp in context.Tbl_LocalizedProperty.Where(w => w.LocaleKeyGroup == "Tbl_BusinessEntity" && w.LocaleKey == "BusinessEntityDisplay" && w.LanguageID == languageID) on jbe.Id equals lp.EntityID into join_lp
                                    from jlp in join_lp.DefaultIfEmpty()
                                    join oa in context.Tbl_OrgAddress.Where(w => w.AddressTypeID == 1) on a.SupplierID equals oa.SupplierID into join_oa
                                    from joa in join_oa.DefaultIfEmpty()
                                    join ad in context.Tbl_Address on joa.AddressID equals ad.AddressID into join_ad
                                    from jad in join_ad.DefaultIfEmpty()
                                    select new
                                    {
                                        SupplierID = a.SupplierID,
                                        BuyerName = languageID == 1 ? a.CompanyName_Inter + " " + jlp.LocaleValue : jbe.BusinessEntityDisplay + " " + a.CompanyName_Local,
                                        Address_Inter = jad.HouseNo_Inter
                                                 + " " + jad.VillageNo_Inter
                                                 + " " + jad.Lane_Inter
                                                 + " " + jad.Road_Inter
                                                 + " " + jad.SubDistrict_Inter
                                                 + " " + jad.City_Inter
                                                 + " " + jad.State_Inter
                                                 + " " + jad.PostalCode,
                                        Address_Local = jad.HouseNo_Local
                                                 + " " + jad.VillageNo_Local
                                                 + " " + jad.Lane_Local
                                                 + " " + jad.Road_Local
                                                 + " " + jad.SubDistrict_Local
                                                 + " " + jad.City_Local
                                                 + " " + jad.State_Local
                                                 + " " + jad.PostalCode,
                                        Orgid = a.OrgID
                                    }).ToList();
            var result = (from n in dataOrganization
                          select new AddressHomeAdmin
                          {
                              SupplierID = n.SupplierID,
                              Address = languageID == 1 ? n.Address_Inter : n.Address_Local,
                              BuyerName = n.BuyerName,
                              Orgid = n.Orgid
                          }).ToList();
            return result;
        }

        public virtual void UpdateAddress(Tbl_Address model, int updateBy)
        {
            try
            {

                if (model != null)
                {

                    var addressTemp = context.Tbl_Address.Find(model.AddressID);
                    //addressTemp = model;
                    addressTemp.HouseNo_Local = model.HouseNo_Local;
                    addressTemp.VillageNo_Local = model.VillageNo_Local;
                    addressTemp.Lane_Local = model.Lane_Local;
                    addressTemp.Road_Local = model.Road_Local;
                    addressTemp.SubDistrict_Local = model.SubDistrict_Local;
                    addressTemp.City_Local = model.City_Local;
                    addressTemp.State_Local = model.State_Local;

                    addressTemp.HouseNo_Inter = model.HouseNo_Inter;
                    addressTemp.VillageNo_Inter = model.VillageNo_Inter;
                    addressTemp.Lane_Inter = model.Lane_Inter;
                    addressTemp.Road_Inter = model.Road_Inter;
                    addressTemp.SubDistrict_Inter = model.SubDistrict_Inter;
                    addressTemp.City_Inter = model.City_Inter;
                    addressTemp.State_Inter = model.State_Inter;

                    addressTemp.AddressID = model.AddressID;
                    addressTemp.CountryCode = model.CountryCode;
                    addressTemp.PostalCode = model.PostalCode;

                    context.SaveChanges();

                    _logAddressRepository.Insert(addressTemp.AddressID, "Modify", updateBy);
                    //_logAddressRepository.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual int InsertAddress(Tbl_Address model)
        {
            try
            {
                int addressID = 0;

                if (model != null)
                {
                    context.Tbl_Address.Add(model);
                    context.SaveChanges();

                    addressID = model.AddressID;

                    _logAddressRepository.Insert(addressID, "Add", 1);
                    //_logAddressRepository.Dispose();
                }

                return addressID;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual int InsertAddressReturnaddressID(Tbl_Address model, int updateBy)
        {
            try
            {
                int addressID = 0;

                if (model != null)
                {
                    context.Tbl_Address.Add(model);
                    context.SaveChanges();

                    addressID = model.AddressID;

                    _logAddressRepository.Insert(addressID, "Add", updateBy);
                }

                return addressID;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void Delete(int addressID, int updateBy)
        {
            try
            {
                var addressModel = context.Tbl_Address.Find(addressID);

                if (addressModel != null)
                {
                    context.Tbl_Address.Remove(addressModel);

                    _logAddressRepository.Insert(addressID, "Remove", updateBy);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {


                throw;
            }
        }

        public virtual TrackingDictionary GetTrackingStatus()
        {
            //pass class for getTrackingStatus
            var result = context.GetTrackingStatus<CompanyAddress>();

            return result;

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
