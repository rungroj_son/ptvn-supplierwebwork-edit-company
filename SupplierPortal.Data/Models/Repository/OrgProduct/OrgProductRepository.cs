﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogOrgProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgProduct
{
    public partial class OrgProductRepository : IOrgProductRepository
    {
        private SupplierPortalEntities context;
        ILogOrgProductRepository _logOrgProductRepository; 

        public OrgProductRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgProductRepository = new LogOrgProductRepository();
        }

        public OrgProductRepository(
            SupplierPortalEntities context,
            LogOrgProductRepository logOrgProductRepository
            )
        {
            this.context = context;
            _logOrgProductRepository = logOrgProductRepository;
        }

        public virtual IEnumerable<OrgProductGridViewModel> GetOrgProductBySupplierIDAndLanguageID(int supplierID, int languageID)
        {
            var query = from a in context.Tbl_OrgProduct
                        join b in context.Tbl_ProductType on a.ProductTypeID equals b.ProductTypeID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_Product on a.ProductCode equals e.ProductCode into f
                        from g in f.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        select new OrgProductGridViewModel
                        {
                            SupplierID = supplierID,
                            ProdTypeID = a.ProductTypeID,
                            ProductCode = a.ProductCode,
                            ProductType = ((from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == d.ProductTypeID
                                            && ml.LanguageID == languageID
                                            && ml.LocaleKeyGroup == "Tbl_ProductType"
                                            && ml.LocaleKey == "ProductType"
                                            select ml.LocaleValue
                                       )).FirstOrDefault() ?? d.ProductType,
                            ProductName = ((from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == g.Id
                                            && ml.LanguageID == languageID
                                            && ml.LocaleKeyGroup == "Tbl_Product"
                                            && ml.LocaleKey == "ProductName"
                                            select ml.LocaleValue
                                       )).FirstOrDefault() ?? g.ProductName,
                        };

            var result = query.ToList();

            return result;
        }

        public virtual bool OrgProductExists(int supplierID, string productCode, int productTypeID)
        {
            var count = context.Tbl_OrgProduct.Count(m => m.SupplierID == supplierID && m.ProductCode == productCode && m.ProductTypeID == productTypeID);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool OrgProductExists(int supplierID, string productCode)
        {
            var count = context.Tbl_OrgProduct.Count(m => m.SupplierID == supplierID && m.ProductCode == productCode);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual IEnumerable<ProductTypeAddModel> GetProductTypeBySupplierIDAndProductCode(int supplierID, string productCode)
        {
            var query = from db in context.Tbl_OrgProduct
                        where db.SupplierID == supplierID
                        && db.ProductCode == productCode
                        select new ProductTypeAddModel
                        {
                            ProductTypeID = db.ProductTypeID
                        };
            var result = query.ToList();

            return result;
        }

        public virtual void Insert(int supplierID, int productTypeID, string productCode, int updateBy)
        {
            try
            {
                var _orgProduct = new Tbl_OrgProduct()
                {
                    SupplierID = supplierID,
                    ProductTypeID = productTypeID,
                    ProductCode = productCode
                };
                context.Tbl_OrgProduct.Add(_orgProduct);
                context.SaveChanges();

                _logOrgProductRepository.Insert(supplierID, productTypeID, productCode, "Add", updateBy);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Delete(int supplierID, int productTypeID, string productCode, int updateBy)
        {
            try
            {
                var _orgProduct = context.Tbl_OrgProduct.Where(m => m.SupplierID == supplierID && m.ProductTypeID == productTypeID && m.ProductCode == productCode).FirstOrDefault();

                if (_orgProduct != null)
                {
                    context.Tbl_OrgProduct.Remove(_orgProduct);
                    context.SaveChanges();

                    _logOrgProductRepository.Insert(supplierID, productTypeID, productCode, "Remove", updateBy);
                }



            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Delete(int supplierID, string productCode, int updateBy)
        {
            try
            {
                var _orgProduct = context.Tbl_OrgProduct.Where(m => m.SupplierID == supplierID && m.ProductCode == productCode).ToList();

                if (_orgProduct.Count > 0)
                {
                    foreach (var itemlist in _orgProduct)
                    {
                        context.Tbl_OrgProduct.Remove(itemlist);
                        context.SaveChanges();

                        _logOrgProductRepository.Insert(itemlist.SupplierID, itemlist.ProductTypeID, itemlist.ProductCode, "Remove", updateBy);
                    }

                   
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
