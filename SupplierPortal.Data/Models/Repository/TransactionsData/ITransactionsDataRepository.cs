﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.TransactionsData
{
    public partial interface ITransactionsDataRepository
    {

        bool MergUserAccount(string masterUsername, string mergUsername, out string returnMessage);
    }
}
