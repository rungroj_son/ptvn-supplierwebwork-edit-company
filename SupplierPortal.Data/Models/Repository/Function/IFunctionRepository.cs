﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Function
{
    public partial interface IFunctionRepository
    {
        Boolean GetResultByQuery(string sqlQuery);

        void Dispose();
    }
}
