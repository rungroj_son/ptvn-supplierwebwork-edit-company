﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.MenuPortal;

namespace SupplierPortal.Data.Models.Repository.MenuPortal
{
    public partial class MenuPortalRepository : IMenuPortalRepository
    {

        private SupplierPortalEntities context;


        public MenuPortalRepository()
        {
            context = new SupplierPortalEntities();
        }

        public MenuPortalRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_MenuPortal> GetMenuTop()
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.MenuLevel == 1 && db.Section == "Top" && db.IsActive == 1
                        orderby db.SeqNo
                        select db;
            var menuTop = query.ToList();

            return menuTop;
        }


        public virtual IEnumerable<Tbl_MenuPortal> GetMenuSub()
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.MenuLevel == 2 && db.IsActive == 1
                        orderby db.SeqNo
                        select db;
            var menuTop = query.ToList();

            return menuTop;
        }

        public virtual IEnumerable<SubMenuPortal> GetSubMenuPortal()
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.MenuLevel == 2 && db.IsActive == 1
                        orderby db.SeqNo
                        select new SubMenuPortal
                        {
                            MenuID = db.MenuID,
                            MenuName = db.MenuName,
                            ResourceName = db.ResourceName,
                            MenuLevel = db.MenuLevel,
                            Section = db.Section,
                            SeqNo = db.SeqNo,
                            SystemGrpID = db.SystemGrpID,
                            SystemPageID = db.SystemPageID,
                            ControllerName = db.ControllerName,
                            ActionName = db.ActionName,
                            SystemURL = db.SystemURL,
                            PrivilegeTypeID = db.PrivilegeTypeID,
                            Icon = db.Icon,
                            IsActive = db.IsActive,
                            PageID = db.SystemPageID,
                            NotificationID = db.NotificationID

                        };

            //var query = from db in context.Tbl_MenuPortal
            //            join sp in context.Tbl_SystemPage
            //            on db.SystemPageID equals sp.PageID into sp2
            //            from fg in sp2.Where(x => x.SystemID == db.SystemID).DefaultIfEmpty()
            //            where db.MenuLevel == 2 && db.IsActive == 1
            //            orderby db.SeqNo
            //            select new SubMenuPortal
            //              {
            //                  MenuID = db.MenuID,
            //                  MenuName = db.MenuName,
            //                  ResourceName = db.ResourceName,
            //                  MenuLevel = db.MenuLevel,
            //                  Section = db.Section,
            //                  SeqNo = db.SeqNo,
            //                  SystemID = db.SystemID,
            //                  SystemPageID = db.SystemPageID,
            //                  ControllerName = db.ControllerName,
            //                  ActionName = db.ActionName,
            //                  SystemURL = db.SystemURL,
            //                  PrivilegeTypeID = db.PrivilegeTypeID,
            //                  Icon = db.Icon,
            //                  IsActive = db.IsActive,
            //                  PageID = fg.PageID,
            //                  NotificationID = db.NotificationID

            //              };

            var menuSub = query.ToList();

            return menuSub;
        }


        public virtual Tbl_MenuPortal GetMenuById(int menuID)
        {

            var result = context.Tbl_MenuPortal.Find(menuID);

            return result;
        }

        public virtual IEnumerable<Tbl_MenuPortal> GetMenu()
        {

            var result = context.Tbl_MenuPortal.ToList();

            return result;
        }

        public virtual Tbl_MenuPortal GetMenuByMenuName(string menuName)
        {

            var result = context.Tbl_MenuPortal.Where(m => m.MenuName == menuName && m.IsActive == 1).FirstOrDefault();

            return result;
        }

        //public virtual PrivilegeMenuPortalModels GetPrivilegeMenu(string username, int menuID)
        //{

        //    var query = from db in context.Tbl_UserRole
        //                join us in context.Tbl_User on db.UserID equals us.UserID into jus
        //                from user in jus.DefaultIfEmpty()
        //                join ar in context.Tbl_ACL_RolePrivilege on db.RoleID equals ar.RoleID into dr
        //                from dbar in dr
        //                join ap in context.Tbl_ACL_Privilege on dbar.ACL_ID equals ap.ACL_ID into dp
        //                from apdp in dp
        //                join mp in context.Tbl_MenuPortalPrivilegeMapping on apdp.ACL_ID equals mp.ACL_ID into amp
        //                from apmp in amp
        //                where user.Username == username
        //                && apmp.MenuID == menuID
        //                select new PrivilegeMenuPortalModels
        //                {
        //                    Username = user.Username,
        //                    PrivilegeCode = apdp.PrivilegeCode,
        //                    MenuID = apmp.MenuID,
        //                    isAllowAccess = apmp.isAllowAccess??0
        //                };

        //    return query.FirstOrDefault();
        //}


        public virtual IEnumerable<Tbl_MenuPortal> GetMenuProfile()
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.Section == "Profile" && db.IsActive == 1
                        orderby db.SeqNo
                        select db;
            var menuTop = query.ToList();

            return menuTop;
        }

        public IEnumerable<Tbl_MenuPortal> GetNotificationMenu() 
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.Section.Contains("NotificationBox") && db.IsActive == 1
                        select db;

            return query.ToList();
        }

        public IEnumerable<Tbl_MenuPortal> GetNotificationMenuByParentMenuID(int ParentMenuID) 
        {
            var query = from db in context.Tbl_MenuPortal
                        where db.ParentMenuID == ParentMenuID && db.IsActive == 1
                        select db;

            return query.ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
