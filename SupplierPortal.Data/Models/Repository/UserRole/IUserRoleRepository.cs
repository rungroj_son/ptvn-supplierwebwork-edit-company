﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserRole
{
    public partial interface IUserRoleRepository
    {

        Tbl_UserRole GetUserRoleByUsername(string username);

        void Insert(Tbl_UserRole tbl_UserRole);

        void Update(Tbl_UserRole tbl_UserRole,int roleID);

        void Dispose();
    }
}
