﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserRole
{
    public partial class UserRoleRepository : IUserRoleRepository
    {
        private SupplierPortalEntities context;


        public UserRoleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public UserRoleRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }



        public virtual Tbl_UserRole GetUserRoleByUsername(string username)
        {
            var query = from a in context.Tbl_UserRole
                        join b in context.Tbl_User on a.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username == username
                        select a;

            var result = query.FirstOrDefault();

            return result;

        }

        public virtual void Insert(Tbl_UserRole tbl_UserRole)
        {
            try
            {

                if (tbl_UserRole != null)
                {

                    context.Tbl_UserRole.Add(tbl_UserRole);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public virtual void Update(Tbl_UserRole tbl_UserRole,int roleID)
        {
            try
            {

                if (tbl_UserRole != null)
                {

                    var userRoleTmp = context.Tbl_UserRole.Find(tbl_UserRole.UserID, tbl_UserRole.RoleID);
                    if (userRoleTmp!=null)
                    {
                        var tmpNewUserRole = new Tbl_UserRole();
                        tmpNewUserRole.UserID = userRoleTmp.UserID;
                        tmpNewUserRole.RoleID = roleID;

                        context.Tbl_UserRole.Remove(userRoleTmp);

                        context.Tbl_UserRole.Add(tmpNewUserRole);
                        
                        context.SaveChanges();
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }



        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
