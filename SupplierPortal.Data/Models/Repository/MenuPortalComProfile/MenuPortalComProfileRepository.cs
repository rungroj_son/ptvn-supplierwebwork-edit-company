﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.MenuPortalComProfile
{
    public partial class MenuPortalComProfileRepository : IMenuPortalComProfileRepository
    {
        private SupplierPortalEntities context;


        public MenuPortalComProfileRepository()
        {
            context = new SupplierPortalEntities();
        }

        public MenuPortalComProfileRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_MenuPortal_CompanyProfile> GetMenuCompanyProfile()
        {
            var query = from a in context.Tbl_MenuPortal_CompanyProfile
                        where a.IsActive == 1
                        orderby a.MenuLevel,a.ParentMenuID,a.SeqNo
                        select a;

            var result = query.ToList();

            return result;

        }

        public virtual string GetResourceName(string pathName)
        {
            var query = (from a in context.Tbl_MenuPortal_CompanyProfile
                        where a.ActionName == pathName
                        select a.ResourceName).FirstOrDefault();

            return query; 
        }

        public virtual string GetResourceNameForTitleMenu(string pathName)
        {
            var query = (from a in context.Tbl_MenuPortal_CompanyProfile
                        where a.ControllerName == pathName
                        select a.ResourceName).FirstOrDefault();

            return query; 
        }
        
        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
