﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.MenuPortalComProfile
{
    public partial interface IMenuPortalComProfileRepository
    {
        IEnumerable<Tbl_MenuPortal_CompanyProfile> GetMenuCompanyProfile();

        string GetResourceName(string pathName);

        string GetResourceNameForTitleMenu(string pathName);

        void Dispose();
    }
}
