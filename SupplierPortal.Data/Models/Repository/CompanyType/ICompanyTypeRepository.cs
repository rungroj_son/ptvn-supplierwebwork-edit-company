﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.CompanyType
{
    public partial interface ICompanyTypeRepository
    {
        IEnumerable<string> GetCompanyTypeByLanguageId(int languageId);

        IEnumerable<CompanyTypeModels> GetCompanyTypeByMultiLanguageId();

        void Dispose();
    }
}
