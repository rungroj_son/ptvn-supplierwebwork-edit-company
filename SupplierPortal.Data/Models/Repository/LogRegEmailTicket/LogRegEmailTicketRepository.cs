﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegEmailTicket
{
    public partial class LogRegEmailTicketRepository : ILogRegEmailTicketRepository
    {
        SupplierPortalEntities context;

        public LogRegEmailTicketRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegEmailTicketRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool Insert(string ticketcode, string logAction)
        {
            try
            {
                var _emlTicket = context.Tbl_RegEmailTicket.FirstOrDefault(m => m.TicketCode == ticketcode);

                var _logRegEmailTicket = new Tbl_LogRegEmailTicket()
                {
                    TicketCode = _emlTicket.TicketCode,
                    RegisteredEmail = _emlTicket.RegisteredEmail,
                    LanguageCode = _emlTicket.LanguageCode,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logRegEmailTicket != null)
                {
                    context.Tbl_LogRegEmailTicket.Add(_logRegEmailTicket);
                    context.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual void Insert(string ticketcode, string logAction, int updateBy)
        {
            try
            {
                var tempModel = context.Tbl_RegEmailTicket.Where(m => m.TicketCode == ticketcode).FirstOrDefault();

                var model = new Tbl_LogRegEmailTicket()
                {
                    TicketCode = tempModel.TicketCode,
                    RegisteredEmail = tempModel.RegisteredEmail,
                    LanguageCode = tempModel.LanguageCode,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                context.Tbl_LogRegEmailTicket.Add(model);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
