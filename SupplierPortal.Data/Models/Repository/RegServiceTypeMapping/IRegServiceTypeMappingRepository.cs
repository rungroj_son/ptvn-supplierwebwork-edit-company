﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using System.Collections;

namespace SupplierPortal.Data.Models.Repository.RegServiceTypeMapping
{
    public partial interface IRegServiceTypeMappingRepository
    {

        void InsertByValue(int regID, int serviceTypeID, string invitationCode);

        void UpdateByValue(int regID, int serviceTypeID, string invitationCode);

        void RemoveByValue(int regID, int serviceTypeID);

        bool CheckExistsServiceMappingByValue(int regID, int serviceTypeID);

        void RemoveByRegID(int regId);

        IEnumerable<Tbl_RegServiceTypeMapping> GetRegServiceListByRegID(int regID);

        void Dispose();
    }
}
