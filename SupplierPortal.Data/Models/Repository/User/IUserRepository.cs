﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.User;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;

namespace SupplierPortal.Data.Models.Repository.User
{
    public partial interface IUserRepository
    {
        UserContactPersonDataModels GetUserContactInfoByUsername(string username);

        UserContactResponse GetUserInfoFromProcurementBoard(string username);

        Tbl_User FindAllByUsername(string username);

        Tbl_User FindByUsernameIsActive(string username);

        Tbl_User FindByUsername(string username);

        Tbl_User FindUserByUserID(int userID);

        int GetUserIDByUsername(string username);

        string FindOrganizNameByUsername(string username);

        string FindOrgIDByUsername(string username);

        void Update(Tbl_User tbl_User);

        void Insert(Tbl_User tbl_User);

        void DeleteUser(string username);

        void DisableUser(string username);

        void EnableUser(string username);

        void UpdateIsAcceptermId(int UserId, int Id);

        IQueryable<UserManageModels> GetUserAll();

        IQueryable<UserManageModels> GetUserAllByOrgID(string orgId);

        IQueryable<UserInformationModels> GetUserInformationByOrgID(string orgId);

        bool UsernameExists(string username);

        void UpdateIsActivated(int userID);

        string GetUsernameByUserID(int userID);

        void Dispose();
    }
}
