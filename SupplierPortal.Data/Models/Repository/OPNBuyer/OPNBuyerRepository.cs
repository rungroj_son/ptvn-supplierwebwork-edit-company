﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OPNBuyer
{
    public partial class OPNBuyerRepository : IOPNBuyerRepository
    {
        private SupplierPortalEntities context;

        public OPNBuyerRepository()
        {
            context = new SupplierPortalEntities();
        }

        public OPNBuyerRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual bool verifyInvitationCode(string invitationCode)
        {
            bool isValid = false;

            DateTime now = DateTime.UtcNow;


            var query = from a in context.Tbl_OPNBuyer
                        where a.InvitationCode == invitationCode
                        && (now >= a.EffectiveDate && now <= a.ExpireDate)
                        select a;

            Tbl_OPNBuyer result = query.FirstOrDefault();

            if (result != null)
            {
              if(result.ID > 0)
                {
                    isValid = true;
                }
            }

            return isValid;

        }
    }
}
