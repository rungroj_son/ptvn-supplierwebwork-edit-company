﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgContactPerson
{
    public partial class LogOrgContactPersonRepository : ILogOrgContactPersonRepository
    {
        private SupplierPortalEntities context;


        public LogOrgContactPersonRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgContactPersonRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int supplierID,int contactID, string logAction, string updateBy)
        {
            try
            {
                var _orgContactPerson = context.Tbl_OrgContactPerson.FirstOrDefault(m => m.SupplierID == supplierID && m.ContactID == contactID);

                var _user = context.Tbl_User.FirstOrDefault(m => m.Username == updateBy);

                int userID = 0;
                if (_user != null)
                {
                    userID = _user.UserID;
                }

                if (_orgContactPerson != null)
                {
                    var _logorgContactPerson = new Tbl_LogOrgContactPerson()
                       {
                           SupplierID = _orgContactPerson.SupplierID,
                           ContactID = _orgContactPerson.ContactID,
                           ContactTypeID = _orgContactPerson.ContactTypeID,
                           isPrimaryContact = _orgContactPerson.isPrimaryContact,
                           NewContactID = _orgContactPerson.NewContactID,
                           UpdateBy = userID,
                           UpdateDate = DateTime.UtcNow,
                           LogAction = logAction
                       };

                    context.Tbl_LogOrgContactPerson.Add(_logorgContactPerson);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Insert(int supplierID, int contactID, string logAction, int updateBy)
        {
            try
            {
                var _orgContactPerson = context.Tbl_OrgContactPerson.FirstOrDefault(m => m.SupplierID == supplierID && m.ContactID == contactID);

                if (_orgContactPerson != null)
                {
                    var _logorgContactPerson = new Tbl_LogOrgContactPerson()
                    {
                        SupplierID = _orgContactPerson.SupplierID,
                        ContactID = _orgContactPerson.ContactID,
                        ContactTypeID = _orgContactPerson.ContactTypeID,
                        isPrimaryContact = _orgContactPerson.isPrimaryContact,
                        NewContactID = _orgContactPerson.NewContactID,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgContactPerson.Add(_logorgContactPerson);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
