﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgAddress
{
    public partial interface ILogOrgAddressRepository
    {
        void Insert(int AddressID, int AddressTypeID, int UpdateBy, string logAction);

        void Dispose();
    }
}
