﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrgProductCategory;
using SupplierPortal.Data.Models.SupportModel.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ProductCategory
{
    public partial class ProductCategoryRepository : IProductCategoryRepository
    {
        private SupplierPortalEntities context;
        //IOrganizationRepository _organizationRepository;
        LogOrgProductCategoryRepository _logOrgProductCategoryRepository;

        public ProductCategoryRepository()
        {
            context = new SupplierPortalEntities();
            //_organizationRepository = new OrganizationRepository();         
            _logOrgProductCategoryRepository = new LogOrgProductCategoryRepository();
        }

        public ProductCategoryRepository
            (
            SupplierPortalEntities context,
            //OrganizationRepository organizationRepository
            LogOrgProductCategoryRepository logOrgProductCategoryRepository
            )
        {
            this.context = context;
            //_organizationRepository = organizationRepository;
            _logOrgProductCategoryRepository = logOrgProductCategoryRepository;
        }

        public IEnumerable<Tbl_ProductCategoryLev1> GetProductCat1(int languageId)
        {
            var query = (from p in context.Tbl_ProductCategoryLev1
                         select p)
                         .OrderBy(o => o.CategoryID_Lev1 < 0)
                         .ThenBy(p => languageId == 1 ? p.CategoryName_Inter : p.CategoryName_Local);
            return query.ToList();
        }

        public IEnumerable<Tbl_ProductCategoryLev2> GetProductCat2(int idCat1, int languageId)
        {
            //var query = from a in context.Tbl_ProductCategoryLev2
            //            where a.CategoryID_Lev1 == idCat1
            //            select a;

            var query = (from p in context.Tbl_ProductCategoryLev2
                         where p.CategoryID_Lev1 == idCat1
                         select p)
                       .OrderBy(o => o.CategoryID_Lev2 < 0)
                       .ThenBy(p => languageId == 1 ? p.CategoryName_Inter : p.CategoryName_Local);

            //var query = (from p in context.Tbl_ProductCategoryLev2
            //             where p.CategoryID_Lev1 == idCat1 && p.CategoryID_Lev2 >= 0
            //             select p)
            //             .OrderBy(p => languageId == 1 ? p.CategoryName_Inter : p.CategoryName_Local).ToList()
            //             .Union(from o in context.Tbl_ProductCategoryLev2
            //                    where o.CategoryID_Lev1 == idCat1 && o.CategoryID_Lev2 < 0
            //                    select o);

            return query.ToList();
        }

        public IEnumerable<Tbl_ProductCategoryLev3> GetProductCat3(int idCat2, int languageId)
        {
            //var query = from a in context.Tbl_ProductCategoryLev3
            //            where a.CategoryID_Lev2 == idCat2
            //            select a;

            var query = (from p in context.Tbl_ProductCategoryLev3
                         where p.CategoryID_Lev2 == idCat2
                         select p)
                      .OrderBy(o => o.CategoryID_Lev3 < 0)
                      .ThenBy(p => languageId == 1 ? p.CategoryName_Inter : p.CategoryName_Local);

            //var query = (from p in context.Tbl_ProductCategoryLev3
            //             where p.CategoryID_Lev2 == idCat2 && p.CategoryID_Lev3 >= 0
            //             select p)
            //             .OrderBy(p => languageId == 1 ? p.CategoryName_Inter : p.CategoryName_Local).ToList()
            //             .Union(from o in context.Tbl_ProductCategoryLev3
            //                    where o.CategoryID_Lev2 == idCat2 && o.CategoryID_Lev3 < 0
            //                    select o);

            return query.ToList();
        }

        public IEnumerable<CategoryModel> GetItemCat1ByCat3(int?[] idCat3)
        {
            var listExcept = idCat3.ToList();
            var query = from a in context.Tbl_ProductCategoryLev1
                        join b in context.Tbl_ProductCategoryLev2 on a.CategoryID_Lev1 equals b.CategoryID_Lev1
                        join c in context.Tbl_ProductCategoryLev3 on b.CategoryID_Lev2 equals c.CategoryID_Lev2
                        //where listExcept.Any(w => w.Equals(c.CategoryID_Lev3))
                        select new CategoryModel
                        {
                            CategoryCode = a.CategoryCode,
                            CategoryDesc_Inter = a.CategoryDesc_Inter,
                            CategoryDesc_Local = a.CategoryDesc_Local,
                            CategoryID_Lev1 = a.CategoryID_Lev1,
                            CategoryName_Inter = a.CategoryName_Inter,
                            CategoryName_Local = a.CategoryName_Local,
                            CategoryLv3 = c
                        };
            return query.ToList().Where(q => listExcept.Any(w => w.Equals(q.CategoryLv3.CategoryID_Lev3)));
        }

        public IEnumerable<Tbl_ProductCategoryLev3> GetItemCat3(int[] idCat3)
        {
            var listExcept = idCat3.ToList();
            var query = from a in context.Tbl_ProductCategoryLev3
                        where listExcept.Any(w => w.Equals(a.CategoryID_Lev3))
                        select a;
            return query.ToList();
        }

        public IEnumerable<Tbl_OrgProductCategory> GetOrgCatDataBySupplierID(int SupplierID)
        {
            var query = from a in context.Tbl_OrgProductCategory
                        where a.SupplierID == SupplierID
                        select a;

            return query.ToList();
        }

        public void Insert(IEnumerable<Tbl_OrgProductCategory> model)
        {
            int RegID = 0;

            try
            {

                foreach (var item in model)
                {
                    context.Tbl_OrgProductCategory.Add(item);
                    context.SaveChanges();

                    _logOrgProductCategoryRepository.Insert(item.Id, item.SupplierID ?? 0, item.CategoryID_Lev3 ?? 0, item.ProductTypeID ?? 0, "UpDate");
                }

                //_logProductCategoryRepository.Insert(model, "Add");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void RemoveDataOrgProductCategory(int SupplierID)
        {
            try
            {
                var data = context.Tbl_OrgProductCategory.Where(a => a.SupplierID == SupplierID);
                context.Tbl_OrgProductCategory.RemoveRange(data);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }


        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
