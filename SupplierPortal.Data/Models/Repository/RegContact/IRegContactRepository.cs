﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegContact
{
    public partial interface IRegContactRepository
    {
        IEnumerable<Tbl_RegContact> GetRegContactByContactid(int contactid);

        void InsertContact(RegisContinuePortalModel _model, out int _contactid);

        bool UpdateContact(RegisContinuePortalModel _model, int _contactid);

        int InsertReturnContactID(Tbl_RegContact model);

        void UpdateRegContact(Tbl_RegContact model);

        void Dispose();
    }
}
