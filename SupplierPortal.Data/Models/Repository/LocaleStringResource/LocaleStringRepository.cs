﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using SupplierPortal.Data.Models;
using SupplierPortal.Core.Caching;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Helper;


namespace SupplierPortal.Data.Models.Repository.LocaleStringResource
{
    public partial class LocaleStringRepository : ILocaleStringRepository
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// </remarks>
        private const string LOCALSTRINGRESOURCES_ALL_KEY = "Portal.lsr.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// {1} : resource key
        /// </remarks>
        private const string LOCALSTRINGRESOURCES_BY_RESOURCENAME_KEY = "Portal.lsr.{0}-{1}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string LOCALSTRINGRESOURCES_PATTERN_KEY = "Portal.lsr.";

        #endregion

        private readonly ICacheManager _cacheManager;

        private SupplierPortalEntities context;


        public LocaleStringRepository()
        {
            context = new SupplierPortalEntities();
            _cacheManager = new MemoryCacheManager();
        }

        public LocaleStringRepository(SupplierPortalEntities ctx, MemoryCacheManager cacheManager)
        {
            this.context = ctx;
            this._cacheManager = cacheManager;
        }

        public virtual string GetResource(string resourceKey)
        {
            //HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];
            string languageId = "";
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            int languageID = Convert.ToInt32(languageId);

            if (cultureCookie != null)
            {
                //int languageID = Convert.ToInt32(cultureCookie.Value);
                return GetResource(resourceKey, languageID);
            }
            else { return ""; }


        }

        public virtual string GetResource(string resourceKey, string languageId)
        {


            if (!string.IsNullOrEmpty(languageId))
            {
                int languageID = Convert.ToInt32(languageId);
                return GetResource(resourceKey, languageID);
            }
            else { return ""; }


        }

        


        public virtual string GetResource(string resourceKey, int languageId)
        {
            string result = string.Empty;
            var resources = GetAllResourceValues(languageId);

            if (!string.IsNullOrEmpty(resourceKey))
            {
                resourceKey = resourceKey.Trim().ToLowerInvariant();
                if (resources.ContainsKey(resourceKey))
                {
                    result = resources[resourceKey].Value;
                }
            }

            return result;
        }

        public virtual Dictionary<string, KeyValuePair<int, string>> GetAllResourceValues(int languageId)
        {
            string key = string.Format(LOCALSTRINGRESOURCES_ALL_KEY, languageId);
            return _cacheManager.Get(key, () =>
            {
                var query = from db in context.Tbl_LocaleStringResource
                            where db.LanguageID == languageId
                            select db;
                var locales = query.ToList();
                var dictionary = new Dictionary<string, KeyValuePair<int, string>>();
                foreach (var locale in locales)
                {
                    //var resourceName = (!string.IsNullOrEmpty(locale.ResourceName)) ? locale.ResourceName.ToLowerInvariant() : "";
                    if (!string.IsNullOrEmpty(locale.ResourceName))
                    {
                        var resourceName = locale.ResourceName.ToLowerInvariant();

                        if (!dictionary.ContainsKey(resourceName))
                            dictionary.Add(resourceName, new KeyValuePair<int, string>(locale.Id, locale.ResourceValue));
                    }

                }

                return dictionary;
            });
        }

        public virtual string getStringResourceByResourceName(string resourceName)
        {
            string result = string.Empty;
            var query = from db in context.Tbl_AppConfig
                        where db.Name == resourceName
                        select db;
            result = query == null ? "" : query.FirstOrDefault().Value;

            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }


    }
}
