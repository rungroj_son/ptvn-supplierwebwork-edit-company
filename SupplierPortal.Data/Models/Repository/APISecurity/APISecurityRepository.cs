﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.APISecurity
{
    public partial class APISecurityRepository : IAPISecurityRepository
    {

        private SupplierPortalEntities context;

        public APISecurityRepository()
        {
            context = new SupplierPortalEntities();
        }

        public APISecurityRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual bool CheckAPISecurityByNameAndIpAddress(string name, string ip_Address)
        {
            bool chk = false;

            var tbl_API = context.Tbl_API.Where(m => m.Name == name).FirstOrDefault();
            if(tbl_API != null)
            {
                if(tbl_API.isCheckAPISecurity != 1)
                {
                    chk = true;
                    return chk;
                }
            }

            var query = from a in context.Tbl_API
                        join b in context.Tbl_APISecurity on a.APIId equals b.APIId
                        where a.Name == name
                        && b.IPAddress_Client == ip_Address
                        select a;

            var result = query.ToList();

            if(result.Count() > 0)
            {
                chk = true;
            }

            return chk;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
