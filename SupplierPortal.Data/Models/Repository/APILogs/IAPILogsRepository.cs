﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.APILogs
{
    public partial interface IAPILogsRepository
    {
        void Insert(Tbl_APILogs apiLogs);

        void InsertLogByParam(
            string reqID = "",
            string events = "",
            string iPAddress_Client = "",
            string aIKey = "",
            string aPIName = "",
            string return_Code = "",
            string return_Status = "",
            string return_Message = "",
            string data = "",
            string userGUID = "",
            int systemID = 0);

        void Dispose();
    }
}
