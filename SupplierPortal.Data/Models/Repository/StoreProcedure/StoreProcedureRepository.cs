﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.StoreProcedure
{
    public partial class StoreProcedureRepository:IStoreProcedureRepository
    {
        private SupplierPortalEntities context;

        public StoreProcedureRepository()
        {
            context = new SupplierPortalEntities();
        }

        public StoreProcedureRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<ProductSearchResultModel> GetResultStoreprocByQuery(string sqlQuery)
        {
            var _result = context.Database.SqlQuery<ProductSearchResultModel>(sqlQuery).ToList();

            return _result;
        }

        public int GetResultVerifyPostalCodeByQuery(string sqlQuery)
        {
            var _result = context.Database.SqlQuery<int>(sqlQuery).FirstOrDefault();

            return _result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
