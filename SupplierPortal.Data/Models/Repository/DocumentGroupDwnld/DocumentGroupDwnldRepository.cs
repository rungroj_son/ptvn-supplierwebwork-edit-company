﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.DocumentGroupDwnld
{
    public partial class DocumentGroupDwnldRepository : IDocumentGroupDwnldRepository
    {

        private SupplierPortalEntities context;


        public DocumentGroupDwnldRepository()
        {
            context = new SupplierPortalEntities();
        }

        public DocumentGroupDwnldRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_DocumentGroupDwnld> List()
        {
            var query = from db in context.Tbl_DocumentGroupDwnld
                        where db.IsActive == 1
                        select db;
            var result = query.ToList();

            return result;
        }


        public void Dispose()
        {
            context.Dispose();
        }
    }
}
