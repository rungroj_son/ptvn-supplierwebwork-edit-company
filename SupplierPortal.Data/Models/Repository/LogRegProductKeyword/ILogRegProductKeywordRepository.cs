﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegProductKeyword
{
    public partial interface ILogRegProductKeywordRepository
    {
        void Insert(int id ,int regid, string keyword, string logAction);

        void Dispose();
    }
}
