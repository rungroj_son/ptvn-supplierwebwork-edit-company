﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogOrgCertifiedStdAttachment;

namespace SupplierPortal.Data.Models.Repository.OrgCertifiedStdAttachment
{
    public partial class OrgCertifiedStdAttachmentRepository : IOrgCertifiedStdAttachmentRepository
    {

        private SupplierPortalEntities context;
        ILogOrgCertifiedStdAttachmentRepository _logOrgCertifiedStdAttachmentRepository; 

        public OrgCertifiedStdAttachmentRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgCertifiedStdAttachmentRepository = new LogOrgCertifiedStdAttachmentRepository();
        }

        public OrgCertifiedStdAttachmentRepository(
            SupplierPortalEntities context,
            LogOrgCertifiedStdAttachmentRepository logOrgCertifiedStdAttachmentRepository

            )
        {
            this.context = context;
            _logOrgCertifiedStdAttachmentRepository = logOrgCertifiedStdAttachmentRepository;
        }

        public virtual void Insert(Tbl_OrgCertifiedStdAttachment model, int updateBy)
        {
            try
            {

                if (model != null)
                {

                    context.Tbl_OrgCertifiedStdAttachment.Add(model);
                    context.SaveChanges();

                    _logOrgCertifiedStdAttachmentRepository.Insert(model.CertId, model.AttachmentID, "Add", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual IEnumerable<Tbl_OrgCertifiedStdAttachment> GetOrgCertifiedStdAttachmentByCertId(int certId)
        {
            var query = context.Tbl_OrgCertifiedStdAttachment.Where(m => m.CertId == certId).ToList();

            return query;
        }


        public virtual void DeleteByCertIdAndAttachmentID(int certId,int attachmentID, int updateBy)
        {
            try
            {
                var model = context.Tbl_OrgCertifiedStdAttachment.Where(m => m.CertId == certId && m.AttachmentID == attachmentID).FirstOrDefault();

                if (model !=null )
                {
                    _logOrgCertifiedStdAttachmentRepository.Insert(model.CertId, model.AttachmentID, "Remove", updateBy);

                    context.Tbl_OrgCertifiedStdAttachment.Remove(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual Tbl_OrgCertifiedStdAttachment GetOrgCertifiedStdAttachmentByAttachmentID(int attachmentID)
        {
            var query = context.Tbl_OrgCertifiedStdAttachment.Where(m => m.AttachmentID == attachmentID).FirstOrDefault();

            return query;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
