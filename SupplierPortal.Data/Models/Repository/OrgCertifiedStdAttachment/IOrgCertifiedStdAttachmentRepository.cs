﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgCertifiedStdAttachment
{
    public partial interface IOrgCertifiedStdAttachmentRepository
    {
        void Insert(Tbl_OrgCertifiedStdAttachment model, int updateBy);

        IEnumerable<Tbl_OrgCertifiedStdAttachment> GetOrgCertifiedStdAttachmentByCertId(int certId);

        void DeleteByCertIdAndAttachmentID(int certId,int attachmentID, int updateBy);

        Tbl_OrgCertifiedStdAttachment GetOrgCertifiedStdAttachmentByAttachmentID(int attachmentID);

        void Dispose();
    }
}
