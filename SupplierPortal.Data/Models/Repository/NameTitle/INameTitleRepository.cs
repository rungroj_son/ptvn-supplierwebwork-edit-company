﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.NameTitle
{
    public partial interface INameTitleRepository
    {
        IEnumerable<TempForMultiLang> GetMultiLangNameTitleByLangid(int languageID);
		
		IEnumerable<Tbl_NameTitle> GetNameTitleList(int languageID);

        string GetTitleNameByIdAndLanguageID(int titleID,int languageID);

        void Dispose();
    }
}
