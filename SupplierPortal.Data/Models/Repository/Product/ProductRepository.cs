﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using System.Data.SqlClient;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.Product
{
    public partial class ProductRepository : IProductRepository
    {
        private SupplierPortalEntities context;

        public ProductRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ProductRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLang> GetMultiLangProductByLangid(int languageID)
        {
            ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_Product", "ProductName", languageID);
            _repo.Dispose();

            if (result.Count() > 0)
            {
                return (from list in result
                        select new TempForMultiLang
                        {
                            EntityID = list.EntityID,
                            EntityValue = list.LocaleValue,
                        }).ToList<TempForMultiLang>();
            }
            else
            {
                return (from list in context.Tbl_Product
                        select new TempForMultiLang
                        {
                            EntityID = list.Id,
                            EntityValue = list.ProductName,
                        }).ToList<TempForMultiLang>();
            }
        }

        public virtual IEnumerable<TempProductModel> GetMultiLangProductByMany(int languageID, string[] strid)
        {
            int[] ids = strid.Select(int.Parse).ToArray();
            int?[] id = new int?[ids.Length];
            for (int i = 0; i < ids.Length; i++)
            {
                id[i] = ids[i];
            }

            return (from product in context.Tbl_Product
                    where id.Contains(product.Id)
                         from localizedProperty in context.Tbl_LocalizedProperty
                              .Where(mapping => mapping.EntityID == product.Id
                              && mapping.LocaleKeyGroup == "Tbl_Product"
                              && mapping.LocaleKey == "ProductName"
                              && mapping.LanguageID == languageID).DefaultIfEmpty()
                              select new TempProductModel
                              {
                                 ID = product.Id,
                                 ProductCode = product.ProductCode,
                                 ProductName = localizedProperty.LocaleValue ?? product.ProductName,
                              }).OrderBy(x => x.ID).ToList<TempProductModel>();
        }

        public virtual IEnumerable<TempProductModel> GetMultiLangProductByManyCode(int languageID, string[] productcode)
        {
            return (from product in context.Tbl_Product
                    where productcode.Contains(product.ProductCode)
                    from localizedProperty in context.Tbl_LocalizedProperty
                         .Where(mapping => mapping.EntityID == product.Id
                         && mapping.LocaleKeyGroup == "Tbl_Product"
                         && mapping.LocaleKey == "ProductName"
                         && mapping.LanguageID == languageID).DefaultIfEmpty()
                    select new TempProductModel
                    {
                        ID = product.Id,
                        ProductCode = product.ProductCode,
                        ProductName = localizedProperty.LocaleValue ?? product.ProductName,
                    }).OrderBy(x => x.ID).ToList<TempProductModel>();
        }

        //public IQueryable<TempProductModel> GetQrbMultiLangProductByLangid(int languageID)
        //{
        //    IQueryable<TempProductModel> query = from product in context.Tbl_Product
        //                                         from localizedProperty in context.Tbl_LocalizedProperty
        //                                                  .Where(mapping => mapping.EntityID == product.Id
        //                                                  && mapping.LocaleKeyGroup == "Tbl_Product"
        //                                                  && mapping.LocaleKey == "ProductName"
        //                                                  && mapping.LanguageID == languageID).DefaultIfEmpty()
        //                                         select new TempProductModel
        //                                         {
        //                                             ID = product.Id,
        //                                             ProductCode = product.ProductCode,
        //                                             ProductName = localizedProperty.LocaleValue ?? product.ProductName,
        //                                         };
        //    return query;
        //}

        public IQueryable<ProductModel> GetQrbMultiLangProductByLangid(int languageID)
        {
            IQueryable<ProductModel> query = from a in context.Tbl_Product
                                             join b in context.Tbl_LocalizedProperty on a.Id equals b.EntityID into c
                                             from d in c.Where(m => m.LanguageID == languageID && m.LocaleKeyGroup == "Tbl_Product" && m.LocaleKey == "ProductName").DefaultIfEmpty()
                                             select new ProductModel
                                                 {
                                                     ID = a.Id,
                                                     ProductCode = a.ProductCode,
                                                     ProductName = d.LocaleValue ?? a.ProductName,
                                                 };

            return query;
        }
        //public IQueryable<ProductModel> GetProductByLanguageID(int languageID)
        //{
        //    IQueryable<ProductModel> query = from product in context.Tbl_Product
        //                                         from localizedProperty in context.Tbl_LocalizedProperty
        //                                                  .Where(mapping => mapping.EntityID == product.Id
        //                                                  && mapping.LocaleKeyGroup == "Tbl_Product"
        //                                                  && mapping.LocaleKey == "ProductName"
        //                                                  && mapping.LanguageID == languageID).DefaultIfEmpty()
        //                                     select new ProductModel
        //                                         {
        //                                             ID = product.Id,
        //                                             ProductCode = product.ProductCode,
        //                                             ProductName = localizedProperty.LocaleValue ?? product.ProductName,
        //                                         };
        //    return query;
        //}

        public IQueryable<ProductModel> GetProductByLanguageID(int languageID)
        {

            IQueryable<ProductModel> query = from a in context.Tbl_Product
                                    select new ProductModel
                                    {
                                        ID = a.Id,
                                        ProductCode = a.ProductCode,
                                        ProductName = (from lp in context.Tbl_LocalizedProperty
                                                       where lp.EntityID == a.Id
                                                      && lp.LocaleKeyGroup == "Tbl_Product"
                                                      && lp.LocaleKey == "ProductName"
                                                      && lp.LanguageID == languageID
                                                       select lp.LocaleValue).FirstOrDefault() ?? a.ProductName
                                    };

             //IQueryable<ProductModel> result = query
            return query;
        }

        public IQueryable<ProductModel> GetProductAll()
        {

            var query = from a in context.Tbl_Product
                        join b in context.Tbl_LocalizedProperty on a.Id equals b.EntityID into c
                        from d in c.Where(m => m.LocaleKeyGroup == "Tbl_Product" && m.LocaleKey == "ProductName").DefaultIfEmpty()
                        select new ProductModel
                        {
                            ID = a.Id,
                            ProductCode = a.ProductCode,
                            ProductName = d.LocaleValue ?? a.ProductName,
                        };

            //IQueryable<ProductModel> result = query
            return query;
        }

        public IQueryable<ProductModel> GetProducRange(int skip, int take, string filter)
        {
            int languageID = LanguageHelper.GetIntLanguageIDCurrent();

            var query = from a in context.Tbl_Product
                        join b in context.Tbl_LocalizedProperty on a.Id equals b.EntityID into c
                        from d in c.Where(m => m.LocaleKeyGroup == "Tbl_Product" && m.LocaleKey == "ProductName" && m.LanguageID == languageID).DefaultIfEmpty()
                        select new ProductModel
                        {
                            ID = a.Id,
                            ProductCode = a.ProductCode,
                            ProductName = d.LocaleValue ?? a.ProductName,
                        };

            IQueryable<ProductModel> result = query.Where(c => c.ProductName.StartsWith(filter)).OrderBy(c => c.ProductName).Skip(skip).Take(take);

            return result;
        }

        public virtual IEnumerable<ProductCategoryResultModels> GetSearchProductandService(string languageID, string keySearch)
        {
            //var query = context.ProductandService_find(languageID, keySearch);

            var query = context.Database.SqlQuery<ProductCategoryResultModels>("SD_ProductCategorySearch_Sel @LangID,@Keytext", new SqlParameter("@LangID", languageID), new SqlParameter("@Keytext", keySearch)).ToList();
            //var query = context.Database.SqlQuery<ProductandServiceResultModels>("ProductandService_find @languageID,@keySearch", new SqlParameter("@languageID", languageID), new SqlParameter("@keySearch", keySearch)).ToList();
            ProductandServiceResultModels models = new ProductandServiceResultModels();
            //string sqlQuery = "exec ProductandService_find";

            //models = query;


            return query;

        }

        public virtual TempProductModel GetMultiLangProductByProductCode(int languageID, int productID)
        {
            var query = from db in context.Tbl_Product
                        where db.Id == productID
                        select new TempProductModel
                        {
                            ID = db.Id,
                            ProductCode = db.ProductCode,
                            ProductName = (from ml in context.Tbl_LocalizedProperty
                                           where ml.EntityID == db.Id
                                           && ml.LocaleKeyGroup == "Tbl_Product"
                                           && ml.LocaleKey == "ProductName"
                                           && ml.LanguageID == languageID
                                           select ml.LocaleValue).FirstOrDefault() ?? db.ProductName
                        };

            var result = query.FirstOrDefault();

            return result; 
        
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
