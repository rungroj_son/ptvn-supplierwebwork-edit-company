﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgFacility
{
    public partial class LogOrgFacilityRepository : ILogOrgFacilityRepository
    {
        private SupplierPortalEntities context;

        public LogOrgFacilityRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogOrgFacilityRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int id, int UpdateBy, string logAction)
        {
            try
            {
                var LogOrgAdd = context.Tbl_OrgFacility.FirstOrDefault(m => m.FacilityID == id);

                if (LogOrgAdd != null)
                {
                    var _logOrg = new Tbl_LogOrgFacility()
                    {
                        FacilityID = LogOrgAdd.FacilityID,
                        SupplierID = LogOrgAdd.SupplierID,
                        SeqNo = LogOrgAdd.SeqNo,
                        FacilityName = LogOrgAdd.FacilityName,
                        FacilityTypeID = LogOrgAdd.FacilityTypeID,
                        FaxExt = LogOrgAdd.FaxExt,
                        FaxNo = LogOrgAdd.FaxNo,
                        MobileNo = LogOrgAdd.MobileNo,
                        PhoneExt = LogOrgAdd.PhoneExt,
                        PhoneNo = LogOrgAdd.PhoneNo,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                        WebSite=LogOrgAdd.WebSite,
                        AddressID=LogOrgAdd.AddressID
                    };

                    context.Tbl_LogOrgFacility.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }
        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
