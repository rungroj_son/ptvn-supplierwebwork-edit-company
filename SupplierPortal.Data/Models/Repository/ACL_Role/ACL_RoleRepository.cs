﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ACL_Role
{
    public partial class ACL_RoleRepository : IACL_RoleRepository
    {

        private SupplierPortalEntities context;

        public ACL_RoleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ACL_RoleRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }


        public virtual IEnumerable<Tbl_ACL_Role> GetRoleList()
        {
            return context.Tbl_ACL_Role.Where(m => m.RoleID != 1 && m.RoleID != 4).OrderBy(m => m.RoleName).ToList();
        }

        public virtual IEnumerable<Tbl_ACL_Role> GetRoleListAll()
        {
            return context.Tbl_ACL_Role.OrderBy(m => m.RoleName).ToList();
        }

        public virtual string GetRoleNameByRoleID(int sysRoleID)
        {
            var result = context.Tbl_ACL_SystemRole.Where(b => b.SysRoleID == sysRoleID)
                     .FirstOrDefault();
            return result.SysRoleName.ToString();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
