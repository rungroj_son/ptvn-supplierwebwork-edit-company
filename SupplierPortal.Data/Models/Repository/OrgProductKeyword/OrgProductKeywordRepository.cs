﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogOrgProductKeyword;

namespace SupplierPortal.Data.Models.Repository.OrgProductKeyword
{
    public partial class OrgProductKeywordRepository : IOrgProductKeywordRepository
    {
        private SupplierPortalEntities context;
        ILogOrgProductKeywordRepository _logOrgProductKeywordRepository;

        public OrgProductKeywordRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgProductKeywordRepository = new LogOrgProductKeywordRepository();
        }

        public OrgProductKeywordRepository(
            SupplierPortalEntities context,
            LogOrgProductKeywordRepository logOrgProductKeywordRepository
            )
        {
            this.context = context;
            _logOrgProductKeywordRepository = logOrgProductKeywordRepository;
        }

        public virtual IEnumerable<Tbl_OrgProductKeyword>GetProductKeywordBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_OrgProductKeyword
                        where a.SupplierID == supplierID
                        select a;

            return query.ToList();
        }

        public void removeKeyWordList(IEnumerable<Tbl_OrgProductKeyword> Data)
        {
            foreach (var item in Data)
            {
                context.Tbl_OrgProductKeyword.Remove(item);
                context.SaveChanges();
            }

        }
        public virtual void Insert(int supplierID, string keyword, int updateBy)
        {
            try
            {
                var _orgProductKeyword = new Tbl_OrgProductKeyword()
                {
                    SupplierID = supplierID,
                    Keyword = keyword
                };
                context.Tbl_OrgProductKeyword.Add(_orgProductKeyword);
                context.SaveChanges();

                _logOrgProductKeywordRepository.Insert(supplierID, keyword, "Add", updateBy);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Delete(int supplierID, string keyword, int updateBy)
        {
            try
            {
                var _orgProductKeyword = context.Tbl_OrgProductKeyword.Where(m => m.SupplierID == supplierID && m.Keyword.Trim() == keyword.Trim()).FirstOrDefault();

                if (_orgProductKeyword != null)
                {
                    context.Tbl_OrgProductKeyword.Remove(_orgProductKeyword);
                    context.SaveChanges();

                    _logOrgProductKeywordRepository.Insert(supplierID, keyword, "Remove", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual bool OrgProductKeywordExists(int supplierID, string keyword)
        {
            var count = context.Tbl_OrgProductKeyword.Count(m => m.SupplierID == supplierID && m.Keyword.Trim() == keyword.Trim());

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
