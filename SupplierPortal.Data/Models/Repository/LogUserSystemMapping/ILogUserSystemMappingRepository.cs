﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogUserSystemMapping
{
    public partial interface ILogUserSystemMappingRepository
    {
        void Insert(Tbl_LogUserSystemMapping model);

        void Dispose();
    }
}
