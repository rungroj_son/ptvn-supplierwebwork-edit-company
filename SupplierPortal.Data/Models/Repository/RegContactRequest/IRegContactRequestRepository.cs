﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.RegContactRequest
{
    public partial interface IRegContactRequestRepository
    {
        void Insert(Tbl_RegContactRequest regContactRequest);

        int InsertReturnUserReqId(Tbl_RegContactRequest regContactRequest);

        Tbl_RegContactRequest GetRegContactRequestByUserReqId(int userReqId);

        List<Tbl_RegContactRequest> GetRegContactRequestByRegListAndStatusList(List<int> listUserRegId, List<int> listStatusId);

        void Dispose();
    }
}
