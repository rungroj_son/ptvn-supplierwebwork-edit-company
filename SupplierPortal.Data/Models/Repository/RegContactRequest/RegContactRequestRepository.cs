﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SupplierPortal.Data.Models.Repository.RegContactRequest
{
    public partial class RegContactRequestRepository : IRegContactRequestRepository
    {

        private SupplierPortalEntities context;

        public RegContactRequestRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegContactRequestRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(Tbl_RegContactRequest regContactRequest)
        {
            try
            {
                if (regContactRequest != null)
                {


                    context.Tbl_RegContactRequest.Add(regContactRequest);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual int InsertReturnUserReqId(Tbl_RegContactRequest regContactRequest)
        {
            int userReqId = 0;
            try
            {
                if (regContactRequest != null)
                {


                    context.Tbl_RegContactRequest.Add(regContactRequest);

                    context.SaveChanges();

                    userReqId = regContactRequest.UserReqId;
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return userReqId;
        }

        public virtual Tbl_RegContactRequest GetRegContactRequestByUserReqId(int userReqId)
        {
            var result = context.Tbl_RegContactRequest.Where(m => m.UserReqId == userReqId).FirstOrDefault();

            return result;
        }

        public virtual List<Tbl_RegContactRequest> GetRegContactRequestByRegListAndStatusList(List<int> listUserRegId, List<int> listStatusId)
        {
            var query = from a in context.Tbl_RegContactRequest
                        where listUserRegId.Contains(a.UserReqId)
                         && listStatusId.Contains((int)a.UserReqStatusID)
                        && a.isDeleted != 1
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
