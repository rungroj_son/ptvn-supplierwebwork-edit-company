﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Country
{
    public partial interface ICountryRepository
    {
        IEnumerable<Tbl_Country> GetCountryList();

        IEnumerable<Tbl_Country> GetCountryListByCountryCode(string countrycode);

        string GetCountryNameByCountryCode(string countrycode);

        Tbl_Country GetCountryByCountryCode(string countrycode);

        IEnumerable<Tbl_Country> GetCountry(string searchTerm, int pageSize, int pageNum);

        int GetCountryCount(string searchTerm, int pageSize, int pageNum);

        void Dispose();
    }
}
