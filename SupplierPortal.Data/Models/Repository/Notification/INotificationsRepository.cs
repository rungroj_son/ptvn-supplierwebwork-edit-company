﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.Notification
{
    public partial interface INotificationsRepository
    {
        Tbl_Notification GetDataByNotiID(int NotiID);

        void changeIsRead(int NotiID);

        void changeIsDelete(int NotiID);

        void UpdateStopTimeWhenRead(int NotiID,DateTime stopTime);

        void Dispose();
    }
}
