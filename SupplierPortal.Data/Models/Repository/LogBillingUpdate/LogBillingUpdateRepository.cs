﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogBillingUpdate
{
    public partial class LogBillingUpdateRepository
    {
         private SupplierPortalEntities context;

        public LogBillingUpdateRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogBillingUpdateRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_BillingDetailLogUpdate GetLastUpdate()
        {
            var query = from db in context.Tbl_BillingDetailLogUpdate                       
                        select db;
            return query.FirstOrDefault();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
