﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProductKeyword
{
    public partial class LogOrgProductKeywordRepository : ILogOrgProductKeywordRepository
    {
        private SupplierPortalEntities context;

        public LogOrgProductKeywordRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgProductKeywordRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int supplierID, string keyword, string logAction, int updateBy)
        {
            try
            {
                var _logOrgProductKeyword = new Tbl_LogOrgProductKeyword()
                {
                    SupplierID = supplierID,
                    Keyword = keyword,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                context.Tbl_LogOrgProductKeyword.Add(_logOrgProductKeyword);
                context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
