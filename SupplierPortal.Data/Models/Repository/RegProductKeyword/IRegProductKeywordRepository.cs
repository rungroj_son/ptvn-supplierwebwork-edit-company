﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegProductKeyword
{
    public partial interface IRegProductKeywordRepository
    {
        void Insert(Tbl_RegProductKeyword model);

        void Edit(Tbl_RegProductKeyword model);

        IEnumerable<Tbl_RegProductKeyword> GetRegProductKeywordByRegID(int regid);

        void removeKeyWordList(IEnumerable<Tbl_RegProductKeyword> Data);

        void RemoveByRegID(int regId);

        void Dispose();
    }
}
