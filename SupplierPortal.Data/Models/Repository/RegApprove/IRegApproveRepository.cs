﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegApprove
{
    public interface IRegApproveRepository
    {
        Tbl_RegApprove GetRegApproveByRegID(int regID);
    }
}
