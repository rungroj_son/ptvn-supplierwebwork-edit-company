﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.ServiceType
{
    public partial class ServiceTypeRepository : IServiceTypeRepository
    {

        private SupplierPortalEntities context;

        public ServiceTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ServiceTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual IEnumerable<LocalizedModel> GetServiceTypeList(int languageID)
        {

            var query = from db in context.Tbl_ServiceType
                        where db.isActive ==1
                        orderby db.ServiceTypeID
                        select new LocalizedModel
                        {
                            EntityID = db.ServiceTypeID,
                            EntityValue = ((from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == db.ServiceTypeID
                                            && ml.LanguageID == languageID
                                            && ml.LocaleKeyGroup == "Tbl_ServiceType"
                                            && ml.LocaleKey == "ServiceType"
                                            select ml.LocaleValue
                                           )).FirstOrDefault() ?? db.ServiceType
                        };


            var result = query.ToList();

            return result;

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
