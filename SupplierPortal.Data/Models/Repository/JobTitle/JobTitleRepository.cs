﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.JobTitle
{
    public partial class JobTitleRepository : IJobTitleRepository
    {

        private SupplierPortalEntities context;


        public JobTitleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public JobTitleRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<TempForMultiLang> GetJobTitleList(int languageID)
        {

            var query = from db in context.Tbl_JobTitle
                        orderby db.SeqNo
                        select new TempForMultiLang
                        {
                            EntityID = db.JobTitleID,
                            EntityValue = ((from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == db.JobTitleID
                                            && ml.LanguageID == languageID
                                            && ml.LocaleKeyGroup == "Tbl_JobTitle"
                                            && ml.LocaleKey == "JobTitleName"
                                            select ml.LocaleValue
                                           )).FirstOrDefault() ?? db.JobTitleName
                        };


            var result = query.ToList();

            return result;

        }

        public virtual string GetJobTitleNameByIdAndLanguageID(int jobTitleID, int languageID)
        {
            var query = from a in context.Tbl_JobTitle
                        where a.JobTitleID == jobTitleID
                        select new
                        {
                            EntityValue = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == a.JobTitleID
                                          && lp.LocaleKeyGroup == "Tbl_JobTitle"
                                          && lp.LocaleKey == "JobTitleName"
                                          && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.JobTitleName
                        };

            var result = query.FirstOrDefault();

            return result.EntityValue.ToString();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
