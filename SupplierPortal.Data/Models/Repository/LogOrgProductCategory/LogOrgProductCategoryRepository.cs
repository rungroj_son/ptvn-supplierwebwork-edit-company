﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProductCategory
{
    public partial class LogOrgProductCategoryRepository
    {
         private SupplierPortalEntities context;

        public LogOrgProductCategoryRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgProductCategoryRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual void Insert(int id,int supplierID, int categoryID_Lev3, int productTypeID, string logAction)
        {
            try
            {

                var model = new Tbl_LogOrgProductCategory
                {
                    Id = id,
                    SupplierID = supplierID,
                    CategoryID_Lev3 = categoryID_Lev3,
                    ProductTypeID = productTypeID,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                if (model != null)
                {
                    context.Tbl_LogOrgProductCategory.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
