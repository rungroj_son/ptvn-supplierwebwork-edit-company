﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_State
{
    public partial class AutoComplete_StateRepository : IAutoComplete_StateRepository
    {
        private SupplierPortalEntities context;

        public AutoComplete_StateRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AutoComplete_StateRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_AutoComplete_State> GetStateByMany(string countrycode, int languageID)
        {
            return (from list in context.Tbl_AutoComplete_State
                    where list.ContryCode == countrycode
                    && list.LanguageID == languageID
                    orderby list.StateName
                    select list).ToList<Tbl_AutoComplete_State>();
        }

        public virtual IEnumerable<Tbl_AutoComplete_State> GetStateListByCountrycode(string countrycode)
        {
            return (from list in context.Tbl_AutoComplete_State
                    where list.ContryCode == countrycode
                    orderby list.StateName
                    select list).ToList<Tbl_AutoComplete_State>();
        }

        public IQueryable<Tbl_AutoComplete_State> GetStateAllByCountrycodeAndLanguageID(string countrycode, int languageID)
        {

            IQueryable<Tbl_AutoComplete_State> query = context.Tbl_AutoComplete_State.Where(m => m.ContryCode == countrycode && m.LanguageID == languageID).OrderBy(c => c.StateName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_State> GetStateAllByCountrycode(string countrycode)
        {

            IQueryable<Tbl_AutoComplete_State> query = context.Tbl_AutoComplete_State.Where(m => m.ContryCode == countrycode).OrderBy(c => c.StateName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_State> GetStateRange(int skip, int take, string filter)
        {

            IQueryable<Tbl_AutoComplete_State> query = context.Tbl_AutoComplete_State.Where(c => c.StateName.StartsWith(filter)).OrderBy(c => c.StateName).Skip(skip).Take(take);
            return query;
        }

        public IEnumerable<Tbl_AutoComplete_State> GetStateList(string term)
        {
            var query = (from a in context.Tbl_AutoComplete_State
                         where a.StateName.ToLower().Contains(term.Trim().ToLower())
                         select a).Take(20);

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
