﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogRegServiceTypeMapping
{
    public partial interface ILogRegServiceTypeMappingRepository
    {
        void Insert(int trackingItemID, int updateBy, string logAction);

        void Dispose();
    }
}
