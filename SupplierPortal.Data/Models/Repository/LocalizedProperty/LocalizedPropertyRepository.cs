﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using System.Reflection.Emit;
using System.Data.Common;
using System.Dynamic;
using System.ComponentModel;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Data.Models.Repository.Language;

namespace SupplierPortal.Data.Models.Repository.LocalizedProperty
{
    public partial class LocalizedPropertyRepository : ILocalizedPropertyRepository
    {

        private SupplierPortalEntities context;

        public LocalizedPropertyRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LocalizedPropertyRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual string GetLocalizedValue(string sqlQuery)
        {
            string localizedValue = "";

            try
            {
                localizedValue = context.Database.SqlQuery<string>(sqlQuery).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw;
            }

            return localizedValue;
        }

        //public virtual dynamic GetLocalizedDynamicByStore(string languageID, string localeKeyGroup, string pkTableMain)
        //{
        //    dynamic result = null;

        //    var res = context.LocalizedProperty_find(languageID, localeKeyGroup, pkTableMain);

        //    return result;
        //}

        public virtual dynamic GetLocalizedListByStore(string sqlQuery)
        {
            List<Dictionary<string, object>> result = null;
            try
            {
                using (var ctx = new SupplierPortalEntities())
                using (var cmd = ctx.Database.Connection.CreateCommand())
                {
                    ctx.Database.Connection.Open();
                    cmd.CommandText = sqlQuery;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var model = this.Read(reader).ToList();

                        return model;
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }

        public virtual dynamic GetLocalizedByStore(string sqlQuery)
        {
            List<Dictionary<string, object>> result = null;
            try
            {
                using (var ctx = new SupplierPortalEntities())
                using (var cmd = ctx.Database.Connection.CreateCommand())
                {
                    ctx.Database.Connection.Open();
                    cmd.CommandText = sqlQuery;
                    using (var reader = cmd.ExecuteReader())
                    {
                        var model = this.Read(reader).FirstOrDefault();

                        return model;
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }

        public List<Dictionary<string, object>> Read(DbDataReader reader)
        {
            List<Dictionary<string, object>> expandolist = new List<Dictionary<string, object>>();
            foreach (var item in reader)
            {
                IDictionary<string, object> expando = new ExpandoObject();
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(item))
                {
                    var obj = propertyDescriptor.GetValue(item);
                    expando.Add(propertyDescriptor.Name, obj);
                }
                expandolist.Add(new Dictionary<string, object>(expando));
            }
            return expandolist;
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetLocalizedPropByEntityid(int entityid)
        {
            return (from list in context.Tbl_LocalizedProperty
                    where list.EntityID == entityid
                    select list).ToList<Tbl_LocalizedProperty>();
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetLocalizedPropAll()
        {
            var query = from db in context.Tbl_LocalizedProperty
                        select db;

            var result = query.ToList();

            return result;
        }

        public IQueryable<Tbl_LocalizedProperty> GetLocalizedPropLoadAll()
        {

            IQueryable<Tbl_LocalizedProperty> query = context.Tbl_LocalizedProperty;
            return query;
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetAllLangLocalizedPropByMany(int _entityid, string _localekeygroup, string _localekey)
        {
            return (from list in context.Tbl_LocalizedProperty
                    where list.EntityID == _entityid
                    && list.LocaleKeyGroup == _localekeygroup
                    && list.LocaleKey == _localekey
                    select list).ToList<Tbl_LocalizedProperty>();
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetAllEntityidLocalizedPropByMany(string _localekeygroup, string _localekey, int _languageid)
        {
            return (from list in context.Tbl_LocalizedProperty
                    where list.LocaleKeyGroup == _localekeygroup
                    && list.LocaleKey == _localekey
                    && list.LanguageID == _languageid
                    select list).ToList<Tbl_LocalizedProperty>();
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetAllLocalizedPropByMany(int _entityid, string _localekeygroup, string _localekey, int _languageid)
        {
            return (from list in context.Tbl_LocalizedProperty
                    where list.EntityID == _entityid
                    && list.LocaleKeyGroup == _localekeygroup
                    && list.LocaleKey == _localekey
                    && list.LanguageID == _languageid
                    select list).ToList<Tbl_LocalizedProperty>();
        }

        public virtual IEnumerable<Tbl_LocalizedProperty> GetSomeEntityLocalizedPropByMany(int[] _entityid, string _localekeygroup, string _localekey, int _languageid)
        {
            int?[] ids = new int?[_entityid.Length];
            for (int i = 0; i < _entityid.Length; i++)
            {
                ids[i] = _entityid[i];
            }
            return (from list in context.Tbl_LocalizedProperty
                    where ids.Contains(list.EntityID)
                    && list.LocaleKeyGroup == _localekeygroup
                    && list.LocaleKey == _localekey
                    && list.LanguageID == _languageid
                    select list).ToList<Tbl_LocalizedProperty>();
        }

        public virtual bool InsertLocalized(int? _entityid, int? _languageid, string _localekeygroup, string _localekey, string _localevalue)
        {
            try
            {
                var _LocalizedProp = new Tbl_LocalizedProperty()
                    {
                        EntityID = _entityid,
                        LanguageID = _languageid,
                        LocaleKeyGroup = _localekeygroup,
                        LocaleKey = _localekey,
                        LocaleValue = _localevalue
                    };
                if (_LocalizedProp != null)
                {
                    context.Tbl_LocalizedProperty.Add(_LocalizedProp);
                    context.SaveChanges();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool UpdateLocalized(int? _entityid, int? _languageid, string _localekeygroup, string _localekey, string _localevalue)
        {
            try
            {
                var _LocalizedProp = context.Tbl_LocalizedProperty.FirstOrDefault(m => m.EntityID == _entityid
                    && m.LanguageID == _languageid && m.LocaleKeyGroup == _localekeygroup && m.LocaleKey == _localekey);
                _LocalizedProp.EntityID = _entityid;
                _LocalizedProp.LanguageID = _languageid;
                _LocalizedProp.LocaleKeyGroup = _localekeygroup;
                _LocalizedProp.LocaleKey = _localekey;
                _LocalizedProp.LocaleValue = _localevalue;

                if (_LocalizedProp != null)
                {
                    context.Entry(_LocalizedProp).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual Tbl_LocalizedProperty GetEntityidLocalizedProp(int _entityid, string _localekeygroup, string _localekey, int _languageid)
        {

            var query = from a in context.Tbl_LocalizedProperty
                        where a.EntityID == _entityid
                        && a.LocaleKeyGroup == _localekeygroup
                        && a.LocaleKey == _localekey
                        && a.LanguageID == _languageid
                        select a;


            var result = query.FirstOrDefault();


            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
