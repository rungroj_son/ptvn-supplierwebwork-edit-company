﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegContactRequestServiceTypeMapping
{
    public partial interface ILogRegContactReqServiceTypeMappingRepository
    {

        void Insert(int Id, int updateBy, string logAction);

        void Dispose();
    }
}
