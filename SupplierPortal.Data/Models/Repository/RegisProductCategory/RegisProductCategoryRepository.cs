﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogProductCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegisProductCategory
{
    public partial class RegisProductCategoryRepository : IRegisProductCategoryRepository
    {
        private SupplierPortalEntities context;
        ILogProductCategoryRepository _logProductCategoryRepository;

        public RegisProductCategoryRepository()
        {
            context = new SupplierPortalEntities();
            _logProductCategoryRepository = new LogProductCategoryRepository();
        }

        public RegisProductCategoryRepository(
            SupplierPortalEntities ctx,
            LogProductCategoryRepository logProductCategoryRepository
            )
        {
            this.context = ctx;
            _logProductCategoryRepository = logProductCategoryRepository;
        }

        public virtual void Insert(Tbl_RegProductCategory model)
        {
            int RegID = 0;

            try
            {
                context.Tbl_RegProductCategory.Add(model);
                context.SaveChanges();

                RegID = model.RegID ?? 0;

                _logProductCategoryRepository.Insert(model.Id,RegID, model.CategoryID_Lev3 ?? 0, model.ProductTypeID ?? 0, "Add");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Edit(Tbl_RegProductCategory model)
        {
            int RegID = 0;

            try
            {
                context.Tbl_RegProductCategory.Add(model);
                context.SaveChanges();

                RegID = model.RegID ?? 0;

                _logProductCategoryRepository.Insert(model.Id, RegID, model.CategoryID_Lev3 ?? 0, model.ProductTypeID ?? 0, "Edit");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Tbl_RegProductCategory> GetProductCatRegis(int? RegisId)
        {

            var query = (from p in context.Tbl_RegProductCategory
                         where p.RegID == RegisId
                         select p);
            return query.ToList();
        }

        public void RemoveDataOrgProductCategory(int RegisId)
        {
            try
            {
                var data = context.Tbl_RegProductCategory.Where(a => a.RegID == RegisId);
                context.Tbl_RegProductCategory.RemoveRange(data);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
