﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogUser
{
    public partial interface ILogUserRepository
    {
        void Insert(int userID, string logAction, string updateByUsrename);

        void Dispose();
    }
}
