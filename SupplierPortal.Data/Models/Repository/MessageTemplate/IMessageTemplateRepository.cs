﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.MessageTemplate
{
    public partial interface IMessageTemplateRepository
    {
        Tbl_MessageTemplate GetMessageTemplateByName(string name);
        void Dispose();
    }
}
