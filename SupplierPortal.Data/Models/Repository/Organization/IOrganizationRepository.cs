﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;

namespace SupplierPortal.Data.Models.Repository.Organization
{
    public partial interface IOrganizationRepository
    {
        IEnumerable<Tbl_Organization> GetOrganizationAll();

        IEnumerable<Tbl_Organization> GetDataByTaxID(string taxid);

        IEnumerable<Tbl_Organization> GetOrganizationByInfosConcat(string infosconcat);

        Tbl_Organization GetDataBySupplierID(int SupplierID);
		
		Tbl_Organization GetOrganizationByOrgID(string orgID);

        int GetSupplierIDByUsername(string username);

        bool CheckDuplicateOrganization(string infosconcat);

        bool CheckDuplicateOrganizationVerifyString(string infosconcat);

        Tbl_Organization GetOrganizationByInfosConcatRegis(string infosconcat);

        void Update(Tbl_Organization model, int UpdateBy);

        bool CheckDuplicateOrganizationByTaxIDAndBranchNo(string taxID, string branchNo);

        Tbl_Organization GetOrganizationByTaxIDAndBranchNo(string taxID, string branchNo);

        TrackingDictionary GetTrackingStatus();

        void UpdateLastUpdate(int supplierID, int UpdateBy);

        CompanyGeneralModel GetDataBySupplierIDAndlanguageID(int supID, int languageID);

        void Dispose();
    }
}
