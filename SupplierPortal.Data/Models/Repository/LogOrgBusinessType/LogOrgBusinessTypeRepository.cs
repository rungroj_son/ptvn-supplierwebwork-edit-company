﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgBusinessType
{
    public partial class LogOrgBusinessTypeRepository : ILogOrgBusinessTypeRepository
    {
        private SupplierPortalEntities context;

        public LogOrgBusinessTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgBusinessTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int id,int supplierID, int businessTypeID, string logAction, int updateBy)
        {
            try
            {
                var _logOrgBusinessType = new Tbl_LogOrgBusinessType()
                {
                    SupplierID = supplierID,
                    BusinessTypeID = businessTypeID,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                context.Tbl_LogOrgBusinessType.Add(_logOrgBusinessType);
                context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
