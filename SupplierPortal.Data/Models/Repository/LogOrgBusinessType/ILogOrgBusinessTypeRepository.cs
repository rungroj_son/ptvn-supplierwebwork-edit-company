﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgBusinessType
{
    public partial interface ILogOrgBusinessTypeRepository
    {

        void Insert(int id,int supplierID, int businessTypeID, string logAction,int updateBy);

        void Dispose();
    }
}
