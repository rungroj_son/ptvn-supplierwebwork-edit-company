﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;

namespace SupplierPortal.Data.Models.Repository.TrackingComprofile
{
    public partial interface ITrackingComprofileRepository
    {
        void Insert(string Variable, int SupID, string value, int ReqId, int EntityID, int status);

        void Update(string Variable, int SupID, string value, int ReqId, int EntityID);

        IEnumerable<Tbl_TrackingCompProfile> GetDataBySupplierIDandTrackingKey(int EntityID, string trackingKey, string trackingKeyGrp);

        IEnumerable<Tbl_TrackingCompProfile> GetDataRefBySupplierIDandTrackingKey(int EntityID, string trackingKey, string trackingKeyGrp);

        Tbl_TrackingRequest GetDataByUser(int UserID);

        int InsertReqAndRetrun(int UpdateBy ,int supID);

        IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingReqID(int trackingReqID);

        IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingItemID(int trackingItemID);

        IEnumerable<Tbl_TrackingCompProfile> GetdataRefByAddressID(int AddressID);

        Tbl_TrackingCompProfile GetdataRefByfieldID_AddressID(int fieldID ,int AddresID);

        //string GetNewKeyValue(int SupID, string Variable);

        //Tbl_TrackingCompProfile GetDataStatusNotEqual(string variable);

        Tbl_TrackingCompProfile GetTrackingCompProfileWaitingApproveByTrackingItemID(int trackingItemID);

        void Update(Tbl_TrackingCompProfile tbl_TrackingCompProfile, int updateBy);

        Tbl_PortalFieldConfig GetDataOtherFiled(string Variable);

        Tbl_PortalFieldConfig GetDataOtherFiledByID(int ID);

        void Dispose();
    }
}
