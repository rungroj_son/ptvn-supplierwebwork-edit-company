﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.Locale
{
    public partial class LocaleRepository : ILocaleRepository
    {
        private SupplierPortalEntities context;

        public LocaleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LocaleRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_Locale> LocaleList()
        {
            return context.Tbl_Locale.OrderBy(m => m.LocaleDescription).ToList();
        }

        public virtual Tbl_Locale GetLocaleByLocaleName(string localeName)
        {
            return context.Tbl_Locale.Where(m => m.LocaleName == localeName).FirstOrDefault();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
