﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using System.Web;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Data.Models.Repository.RegAttachment
{
    public partial class RegAttachmentRepository : IRegAttachmentRepository
    {
        private SupplierPortalEntities context;

        public RegAttachmentRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegAttachmentRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegID(int regid)
        {
            return (from list in context.Tbl_RegAttachment
                    where list.RegID == regid
                    select list).ToList<Tbl_RegAttachment>();
        }

        public virtual IEnumerable<Tbl_RegAttachment> GetRegAttachmentByMany(int regid, int attachmentid, string attachmentname)
        {
            return (from list in context.Tbl_RegAttachment
                    where list.RegID == regid
                    && list.AttachmentID == attachmentid
                    && list.AttachmentName == attachmentname
                    select list).ToList<Tbl_RegAttachment>();
        }

        public virtual List<Tbl_RegAttachment> GetRegAttachmentByManyRegID(List<int> listRegID)
        {
            return (from list in context.Tbl_RegAttachment
                    where listRegID.Contains(list.RegID)
                    select list).ToList<Tbl_RegAttachment>();
        }
        //public virtual IEnumerable<RegAttachmentGridViewModel> GetMultiLangRegAttachmentByMany(int regid, int languageID)
        //{
        //    int rownum = 0;
        //    var query = from RAT in context.Tbl_RegAttachment
        //                where RAT.RegID == regid
        //                orderby RAT.AttachmentName descending
        //                join CDT in context.Tbl_CustomerDocType on RAT.DocumentTypeID equals CDT.DocumentTypeID into ps
        //                from ds in ps.DefaultIfEmpty()

        //                select new RegAttachmentGridViewModel
        //                {
        //                    RowNumber = rownum + 1,
        //                    AttachmentID = RAT.AttachmentID,
        //                    DocumentTypeID = RAT.DocumentTypeID,
        //                    DocumentType = ((from LCP in context.Tbl_LocalizedProperty
        //                                     where LCP.EntityID == ds.DocumentTypeID
        //                                    && LCP.LanguageID == languageID
        //                                    && LCP.LocaleKeyGroup == "Tbl_CustomerDocType"
        //                                    && LCP.LocaleKey == "DocumentName"
        //                                     select LCP.LocaleValue
        //                                    )).FirstOrDefault() ?? ds.DocumentType,
        //                    AttachmentName = RAT.AttachmentName,
        //                };
        //    return query.ToList<RegAttachmentGridViewModel>();
        //}

        public virtual IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegIDAndDocumentTypeID(int regid, int documentTypeID)
        {

            var query = from a in context.Tbl_RegAttachment
                        join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                        where a.RegID == regid
                        && b.DocumentTypeID == documentTypeID
                        select a;


            var result = query.ToList();

            return result;
        }


        public virtual IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegIDAndDocumentNameID(int regid, int documentNameID)
        {

            var query = from a in context.Tbl_RegAttachment
                        where a.RegID == regid
                        && a.DocumentNameID == documentNameID
                        select a;


            var result = query.ToList();

            return result;
        }
        public virtual RegAttachmentListModels GetRegAttachmentListByRegIDAndDocumentTypeID(int regid, int documentTypeID, int companyTypeID)
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            RegAttachmentListModels models = new RegAttachmentListModels();

            var query1 = from a in context.Tbl_RegAttachment
                         join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                         where a.RegID == regid
                         && b.DocumentTypeID == documentTypeID
                         select a;

            //var query2 = (from a in context.Tbl_CustomerDocName
            //              where a.DocumentTypeID == documentTypeID
            //             select a).Distinct();

            var query2 = (from a in context.Tbl_CustomerDocName
                          where a.DocumentTypeID == documentTypeID
                          select new CustomerDocNameModels
                          {
                              DocumentNameID = a.DocumentNameID,
                              DocumentName = (from lp in context.Tbl_LocalizedProperty
                                              where lp.EntityID == a.DocumentNameID
                                              && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                              && lp.LocaleKey == "DocumentName"
                                              && lp.LanguageID == languageId
                                              select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName,
                              SeqNo = a.SeqNo ?? 0,
                              DocumentTypeID = a.DocumentTypeID ?? 0
                          }).Distinct();

            var result1 = query1.ToList();

            var result2 = query2.ToList();


            models.RegAttachment = result1;
            models.DocumentName = result2;
            models.CompanyTypeID = companyTypeID;


            return models;
        }

        public virtual void Insert(Tbl_RegAttachment tbl_RegAttachment)
        {
            try
            {

                if (tbl_RegAttachment != null)
                {

                    context.Tbl_RegAttachment.Add(tbl_RegAttachment);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual bool RegAttachmentExists(string guidName, int regID)
        {
            var count = context.Tbl_RegAttachment.Count(m => m.AttachmentNameUnique == guidName && m.RegID == regID);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool RegAttachmentExistsFileName(string fileName, int regID)
        {
            var count = context.Tbl_RegAttachment.Count(m => m.AttachmentName == fileName && m.RegID == regID);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool RegAttachmentExistsByDocumentNameID(int documentNameID, int regID)
        {
            var count = context.Tbl_RegAttachment.Count(m => m.DocumentNameID == documentNameID && m.RegID == regID);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual Tbl_RegAttachment GetRegAttachmentByGuidName(string guidName)
        {

            var query = from db in context.Tbl_RegAttachment
                        where db.AttachmentNameUnique == guidName
                        select db;


            var result = query.FirstOrDefault();

            return result;
        }

        public virtual void Delete(int regid, int attachmentid)
        {
            try
            {
                var regAttachmentTmp = context.Tbl_RegAttachment.Find(regid, attachmentid);

                if (regAttachmentTmp != null)
                {

                    context.Tbl_RegAttachment.Remove(regAttachmentTmp);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void RemoveByRegID(int regId)
        {
            var data = context.Tbl_RegAttachment.Where(a => a.RegID == regId);
            context.Tbl_RegAttachment.RemoveRange(data);
            context.SaveChanges();
        }

        public virtual int GetRegAttachmentCountByRegIDAndDocumentTypeID(int regID, int documentTypeID)
        {
            var query = from a in context.Tbl_RegAttachment
                        join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                        where a.RegID == regID
                        && b.DocumentTypeID == documentTypeID
                        && a.RegID == regID
                        select a;

            var result = query.Count();

            return result;
        }
        //public virtual bool Insert(int regid, int producttypeid, string productcode)
        //{
        //    try
        //    {
        //        var _regProduct = new Tbl_RegProduct()
        //        {
        //            RegID = regid,
        //            ProductTypeID = producttypeid,
        //            ProductCode = productcode
        //        };
        //        if (_regProduct != null)
        //        {
        //            context.Tbl_RegProduct.Add(_regProduct);
        //            context.SaveChanges();

        //            ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
        //            _logRegProduct.Insert(_regProduct.RegID, _regProduct.ProductTypeID, _regProduct.ProductCode, "Add");
        //            _logRegProduct.Dispose();
        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public virtual bool Delete(int regid, int producttypeid, string productcode)
        //{
        //    try
        //    {
        //        var _regProduct = context.Tbl_RegProduct.FirstOrDefault(m => ((m.RegID == regid) && (m.ProductTypeID == producttypeid) && (m.ProductCode == productcode)));
        //        if (_regProduct != null)
        //        {
        //            context.Tbl_RegProduct.Remove(_regProduct);

        //            ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
        //            _logRegProduct.Insert(_regProduct.RegID, _regProduct.ProductTypeID, _regProduct.ProductCode, "Delete");
        //            _logRegProduct.Dispose();

        //            context.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
