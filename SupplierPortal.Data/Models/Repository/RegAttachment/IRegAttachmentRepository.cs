﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegAttachment
{
    public partial interface IRegAttachmentRepository
    {
        IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegID(int regid);

        IEnumerable<Tbl_RegAttachment> GetRegAttachmentByMany(int regid, int attachmentid, string attachmentname);

        //IEnumerable<RegAttachmentGridViewModel> GetMultiLangRegAttachmentByMany(int regid, int languageID);

        IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegIDAndDocumentTypeID(int regid, int documentTypeID);

        IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegIDAndDocumentNameID(int regid, int documentNameID);

        List<Tbl_RegAttachment> GetRegAttachmentByManyRegID(List<int> listRegID);

        Tbl_RegAttachment GetRegAttachmentByGuidName(string guidName);

        RegAttachmentListModels GetRegAttachmentListByRegIDAndDocumentTypeID(int regid, int documentTypeID, int companyTypeID);

        void Insert(Tbl_RegAttachment model);

        void Delete(int regid, int attachmentid);

        void RemoveByRegID(int regId);

        bool RegAttachmentExists(string guidName, int regID);

        bool RegAttachmentExistsFileName(string fileName, int regID);

        bool RegAttachmentExistsByDocumentNameID(int documentNameID, int regID);

        int GetRegAttachmentCountByRegIDAndDocumentTypeID(int regID, int documentTypeID);

        void Dispose();
    }
}
