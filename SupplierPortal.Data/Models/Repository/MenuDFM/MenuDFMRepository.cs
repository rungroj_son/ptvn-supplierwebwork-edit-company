﻿using SupplierPortal.Data.Models.MappingTable;
using System.Collections.Generic;
using System.Linq;

namespace SupplierPortal.Data.Models.Repository.MenuDFM
{
    public partial class MenuDFMRepository : IMenuDFMRepository
    {
        private SupplierPortalEntities context;


        public MenuDFMRepository()
        {
            context = new SupplierPortalEntities();
        }

        public MenuDFMRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_MenuDFM GetMenuDFMByMenuID(int menuID)
        {
            var result = context.Tbl_MenuDFM.Where(m => m.MenuID == menuID).FirstOrDefault();

            return result;
        }

        public virtual Tbl_MenuDFM GetFirstOrDefaultMenuDFMByMenu()
        {
            var result = context.Tbl_MenuDFM.FirstOrDefault();

            return result;
        }
    }
}
