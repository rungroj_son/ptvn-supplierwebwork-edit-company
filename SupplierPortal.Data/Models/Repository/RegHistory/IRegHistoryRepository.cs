﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.RegHistory
{
    public partial interface IRegHistoryRepository
    {
        void Insert(Tbl_RegHistory model);

        void Dispose();
    }
}
