﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SupplierPortalReportGroup
{
    public partial interface IPortalReportsGroupRepository
    {
        IEnumerable<Tbl_SupplierPortalReportGroup> List();
    }
}
