﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using System.Web;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.LogUserServiceMapping;

namespace SupplierPortal.Data.Models.Repository.UserServiceMapping
{
    public partial class UserServiceMappingRepository : IUserServiceMappingRepository
    {
        private SupplierPortalEntities context;
        ILogUserServiceMappingRepository _logUserServiceMappingRepository;


        public UserServiceMappingRepository()
        {
            context = new SupplierPortalEntities();
            _logUserServiceMappingRepository = new LogUserServiceMappingRepository();
        }

        public UserServiceMappingRepository(
            SupplierPortalEntities ctx,
            LogUserServiceMappingRepository logUserServiceMappingRepository
            )
        {
            this.context = ctx;
            _logUserServiceMappingRepository = logUserServiceMappingRepository;
        }

        public virtual IEnumerable<UserServiceAvailableModels> GetUserServiceAvailableByUserID(int userID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query1 = from a in context.Tbl_Service
                         join b in context.Tbl_UserServiceMapping on a.ServiceID equals b.ServiceID into c
                         from d in c.Where(m => m.UserID == userID).DefaultIfEmpty()
                         join e in context.Tbl_System on a.ServiceID equals e.ServiceID into f
                         from g in f.DefaultIfEmpty()
                         join h in context.Tbl_UserSystemMapping on g.SystemID equals h.SystemID into i
                         from j in i.Where(m => m.UserID == userID).DefaultIfEmpty()
                         where a.isActive == 1
                         select new UserServiceAvailableModels
                         {
                             ServiceID = d.ServiceID,
                             ServiceName = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.ServiceID
                                            && lp.LocaleKeyGroup == "Tbl_Service"
                                            && lp.LocaleKey == "ServiceName"
                                            && lp.LanguageID == languageId
                                            select lp.LocaleValue).FirstOrDefault() ?? a.ServiceName,
                             IsActive = d.isActive ?? 0,
                             IsEnableService = (j == null ? d.isActive ?? 0 : j.isEnableService ?? 0),
                             SeqNo = a.SeqNo
                         };

            var result1 = query1.ToList();

            var result2 = (from a in result1
                           group a by new
                           {
                               a.ServiceID,
                               a.ServiceName,
                               a.IsActive,
                               a.SeqNo
                           } into gcs
                           select new UserServiceAvailableModels
                           {
                               ServiceID = gcs.Key.ServiceID,
                               ServiceName = gcs.Key.ServiceName,
                               IsActive = gcs.Key.IsActive,
                               IsEnableService = gcs.Sum(x => x.IsEnableService),
                               SeqNo = gcs.Key.SeqNo
                           }).ToList().OrderBy(m => m.SeqNo);

            return result2;
        }

        public virtual IEnumerable<Tbl_UserServiceMapping> GetUserServiceMappingByUserID(int userID)
        {
            var query = from a in context.Tbl_UserServiceMapping
                        where a.UserID == userID
                        select a;

            return query.ToList();
        }

        public virtual Tbl_UserServiceMapping GetUserServiceMappingByUserIDAndServiceName(int userID, string serviceName)
        {
            var query = from a in context.Tbl_UserServiceMapping
                        join b in context.Tbl_Service on a.ServiceID equals b.ServiceID
                        where a.UserID == userID && b.ServiceName == serviceName && a.isActive == 1
                        select a;

            return query.FirstOrDefault();
        }

        public virtual IEnumerable<byte[]> GetIconNotificationByUserID(int userID)
        {
            var query = (from a in context.Tbl_UserServiceMapping
                         join b in context.Tbl_Service on a.ServiceID equals b.ServiceID
                         where a.UserID == userID
                         && a.isActive == 1
                         select b.ImageBinary).ToList();
            return query; //query.FirstOrDefault();
        }

        public virtual void Insert(Tbl_UserServiceMapping model, int updateBy)
        {
            try
            {

                if (model != null)
                {

                    context.Tbl_UserServiceMapping.Add(model);
                    context.SaveChanges();

                    var _logModel = new Tbl_LogUserServiceMapping()
                    {
                        UserID = model.UserID,
                        ServiceID = model.ServiceID,
                        isActive = model.isActive,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Add"
                    };

                    _logUserServiceMappingRepository.Insert(_logModel);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public virtual bool CheckExistsUserServiceMapping(int userID, int serviceID)
        {
            var model = context.Tbl_UserServiceMapping.Where(m => m.UserID == userID && m.ServiceID == serviceID).ToList();

            if (model.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
