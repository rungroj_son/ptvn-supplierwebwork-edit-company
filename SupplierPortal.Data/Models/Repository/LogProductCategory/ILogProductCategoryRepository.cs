﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogProductCategory
{
    public partial interface ILogProductCategoryRepository
    {
        void Insert(int id,int regid, int categoryID_Lev3, int productTypeID, string logAction);

        void Dispose();
    }
}
