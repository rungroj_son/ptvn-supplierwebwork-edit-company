﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogProductCategory
{
    public partial class LogProductCategoryRepository : ILogProductCategoryRepository
    {
        private SupplierPortalEntities context;

        public LogProductCategoryRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogProductCategoryRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual void Insert(int id,int regid, int categoryID_Lev3, int productTypeID, string logAction)
        {
            try
            {

                var model = new Tbl_LogRegProductCategory
                {
                    Id = id,
                    RegID = regid,
                    CategoryID_Lev3 = categoryID_Lev3,
                    ProductTypeID = productTypeID,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                if (model != null)
                {
                    context.Tbl_LogRegProductCategory.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
