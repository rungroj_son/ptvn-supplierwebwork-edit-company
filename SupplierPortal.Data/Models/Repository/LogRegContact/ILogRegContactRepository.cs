﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegContact
{
    public partial interface ILogRegContactRepository
    {
        bool Insert(int contactid, string logAction);

        void Dispose();
    }
}
