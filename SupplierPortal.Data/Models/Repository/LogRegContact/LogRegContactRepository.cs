﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegContact
{
    public partial class LogRegContactRepository : ILogRegContactRepository
    {
        private SupplierPortalEntities context;

        public LogRegContactRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegContactRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool Insert(int contactid, string logAction)
        {
            try
            {
                var _regContact = context.Tbl_RegContact.FirstOrDefault(m => m.ContactID == contactid);

                var _logRegContact = new Tbl_LogRegContact()
                {
                    ContactID = _regContact.ContactID,
                    Title = _regContact.TitleID.ToString(),
                    FirstName_Local = _regContact.FirstName_Local,
                    FirstName_Inter = _regContact.FirstName_Inter,
                    LastName_Local = _regContact.LastName_Local,
                    LastName_Inter = _regContact.LastName_Inter,
                    JobTitleID = _regContact.JobTitleID,
                    OtherJobTitle = _regContact.OtherJobTitle,
                    Department = _regContact.Department,
                    PhoneCountryCode = _regContact.PhoneCountryCode,
                    PhoneNo = _regContact.PhoneNo,
                    PhoneExt = _regContact.PhoneExt,
                    MobileCountryCode = _regContact.MobileCountryCode,
                    MobileNo = _regContact.MobileNo,
                    Email = _regContact.Email,
                    UpdateBy = 1,
                    UpdateDate = DateTime.Now,
                    LogAction = logAction
                };
                if (_logRegContact != null)
                {
                    context.Tbl_LogRegContact.Add(_logRegContact);
                    context.SaveChanges();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
