﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgRefCustomer
{
    public partial class LogOrgRefCustomerRepository : ILogOrgRefCustomerRepository
    {
        private SupplierPortalEntities context;

        public LogOrgRefCustomerRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogOrgRefCustomerRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int id, int UpdateBy, string logAction)
        {
            try
            {
                var LogOrgAdd = context.Tbl_OrgRefCustomer.FirstOrDefault(m => m.Id == id);

                if (LogOrgAdd != null)
                {
                    var _logOrg = new Tbl_LogOrgRefCustomer()
                    {
                        Id = LogOrgAdd.Id,
                        SupplierID = LogOrgAdd.SupplierID,
                        SeqNo = LogOrgAdd.SeqNo,
                        CompanyName_Local = LogOrgAdd.CompanyName_Local,
                        CompanyName_Inter = LogOrgAdd.CompanyName_Inter,
                        BusinessEntityID = LogOrgAdd.BusinessEntityID,
                        OtherBusinessEntity = LogOrgAdd.OtherBusinessEntity,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgRefCustomer.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
