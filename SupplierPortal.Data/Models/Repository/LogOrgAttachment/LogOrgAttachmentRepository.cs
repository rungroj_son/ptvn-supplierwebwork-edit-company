﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgAttachment
{
    public partial class LogOrgAttachmentRepository : ILogOrgAttachmentRepository
    {
        private SupplierPortalEntities context;

        public LogOrgAttachmentRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgAttachmentRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void InsertLogAdd(int supplierID, int attachmentID, string logAction)
        {
            try
            {
                var _orgAttachmentTmp = context.Tbl_OrgAttachment.FirstOrDefault(m => m.SupplierID == supplierID && m.AttachmentID == attachmentID);

                if (_orgAttachmentTmp != null)
                {
                    var _logOrgAttachment = new Tbl_LogOrgAttachment()
                    {
                        SupplierID = _orgAttachmentTmp.SupplierID,
                        AttachmentID = _orgAttachmentTmp.AttachmentID,
                        AttachmentName = _orgAttachmentTmp.AttachmentName,
                        AttachmentNameUnique = _orgAttachmentTmp.AttachmentNameUnique,
                        SizeAttach = _orgAttachmentTmp.SizeAttach,
                        DocumentNameID = _orgAttachmentTmp.DocumentNameID,
                        OtherDocument = _orgAttachmentTmp.OtherDocument,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgAttachment.Add(_logOrgAttachment);
                    context.SaveChanges();
                }
                
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void InsertLogAdd(int supplierID, string attachmentNameUnique, string logAction)
        {
            try
            {
                var _orgAttachmentTmp = context.Tbl_OrgAttachment.FirstOrDefault(m => m.SupplierID == supplierID && m.AttachmentNameUnique == attachmentNameUnique);

                if (_orgAttachmentTmp != null)
                {
                    var _logOrgAttachment = new Tbl_LogOrgAttachment()
                    {
                        SupplierID = _orgAttachmentTmp.SupplierID,
                        AttachmentID = _orgAttachmentTmp.AttachmentID,
                        AttachmentName = _orgAttachmentTmp.AttachmentName,
                        AttachmentNameUnique = _orgAttachmentTmp.AttachmentNameUnique,
                        SizeAttach = _orgAttachmentTmp.SizeAttach,
                        DocumentNameID = _orgAttachmentTmp.DocumentNameID,
                        OtherDocument = _orgAttachmentTmp.OtherDocument,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgAttachment.Add(_logOrgAttachment);
                    context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void InsertLogDelete(int supplierID, int attachmentID, string logAction)
        {
            try
            {
                var _orgAttachmentTmp = context.Tbl_OrgAttachment.FirstOrDefault(m => m.SupplierID == supplierID && m.AttachmentID == attachmentID);

                var _logOrgAttachment = new Tbl_LogOrgAttachment()
                {
                    SupplierID = _orgAttachmentTmp.SupplierID,
                    AttachmentID = _orgAttachmentTmp.AttachmentID,
                    AttachmentName = _orgAttachmentTmp.AttachmentName,
                    AttachmentNameUnique = _orgAttachmentTmp.AttachmentNameUnique,
                    SizeAttach = _orgAttachmentTmp.SizeAttach,
                    DocumentNameID = _orgAttachmentTmp.DocumentNameID,
                    OtherDocument = _orgAttachmentTmp.OtherDocument,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logOrgAttachment != null)
                {
                    context.Tbl_LogOrgAttachment.Add(_logOrgAttachment);
                    context.SaveChanges();
                }


            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Insert(int supplierID, int attachmentID, string logAction, int updateBy)
        {
            try
            {
                var _orgAttachmentTmp = context.Tbl_OrgAttachment.Where(m => m.SupplierID == supplierID && m.AttachmentID == attachmentID).FirstOrDefault();

                if (_orgAttachmentTmp != null)
                {
                    var _logOrgAttachment = new Tbl_LogOrgAttachment()
                    {
                        SupplierID = _orgAttachmentTmp.SupplierID,
                        AttachmentID = _orgAttachmentTmp.AttachmentID,
                        AttachmentName = _orgAttachmentTmp.AttachmentName,
                        AttachmentNameUnique = _orgAttachmentTmp.AttachmentNameUnique,
                        SizeAttach = _orgAttachmentTmp.SizeAttach,
                        DocumentNameID = _orgAttachmentTmp.DocumentNameID,
                        OtherDocument = _orgAttachmentTmp.OtherDocument,
                        isDeleted = _orgAttachmentTmp.isDeleted??0,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgAttachment.Add(_logOrgAttachment);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
