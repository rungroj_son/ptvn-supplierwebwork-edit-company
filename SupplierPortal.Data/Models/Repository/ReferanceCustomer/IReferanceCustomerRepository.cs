﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ReferanceCustomer
{
    public partial interface IReferanceCustomerRepository
    {
        List<Tbl_OrgRefCustomer> GetDataBySupplierID(int SupplierID);

        void Dispose();
    }
}
