﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ReferanceCustomer
{
    public partial class ReferanceCustomerRepository : IReferanceCustomerRepository
    {
        private SupplierPortalEntities context;

        public ReferanceCustomerRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ReferanceCustomerRepository
            (
            SupplierPortalEntities context
            )
        {
            this.context = context;
        }

        public virtual List<Tbl_OrgRefCustomer> GetDataBySupplierID(int SupplierID)
        {

            return context.Tbl_OrgRefCustomer.Where(w => w.SupplierID == SupplierID).ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
        
    }
}
