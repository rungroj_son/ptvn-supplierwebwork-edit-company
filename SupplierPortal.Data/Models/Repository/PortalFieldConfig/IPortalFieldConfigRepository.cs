﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.PortalFieldConfig
{
    public partial interface IPortalFieldConfigRepository
    {
        bool CheckPortalFieldConfigIsTrackingByFieldName(string fieldName);

        IEnumerable<Tbl_PortalFieldConfig> GetPortalFieldConfigIsTrackingByFieldGroup(string fieldGroup);

        Tbl_PortalFieldConfig GetdataForcheckRef(int fieldID);

        Tbl_PortalFieldConfig GetdataCheckOtherField(string variable);

        Tbl_PortalFieldConfig GetdataByID(int fieldID);

        void Dispose();
    }
}
