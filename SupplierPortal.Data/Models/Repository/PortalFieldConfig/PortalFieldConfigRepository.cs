﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.PortalFieldConfig
{
    public partial class PortalFieldConfigRepository : IPortalFieldConfigRepository
    {

        private SupplierPortalEntities context;

        public PortalFieldConfigRepository()
        {
            context = new SupplierPortalEntities();
        }

        public PortalFieldConfigRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual bool CheckPortalFieldConfigIsTrackingByFieldName(string fieldName)
        {
            bool isTracking = false;

            var result = context.Tbl_PortalFieldConfig.Where(m => m.FieldName == fieldName && m.isTrackModify == 1).FirstOrDefault();

            if (result != null)
            {
                isTracking = true;
            }

            return isTracking;
        }

        public virtual IEnumerable<Tbl_PortalFieldConfig> GetPortalFieldConfigIsTrackingByFieldGroup(string fieldGroup)
        {
            var query = from a in context.Tbl_PortalFieldConfig
                        where a.isTrackModify == 1
                        && a.FieldGroup == fieldGroup
                        select a;

            return query.ToList();
        }

        public virtual Tbl_PortalFieldConfig GetdataForcheckRef(int fieldID)
        {
            var query = from a in context.Tbl_PortalFieldConfig
                        where a.isTrackModify == 1
                        && a.FieldID == fieldID
                        select a;
            return query.FirstOrDefault();
        }

        public virtual Tbl_PortalFieldConfig GetdataCheckOtherField(string variable) 
        { 
            var query = from a in context.Tbl_PortalFieldConfig
                        where a.Variable == variable
                        && a.OtherFieldID != 0
                        select a;

            return query.FirstOrDefault();       
        }

        public virtual Tbl_PortalFieldConfig GetdataByID(int fieldID)
        {
            var query = from a in context.Tbl_PortalFieldConfig
                        where a.FieldID == fieldID
                        select a;

            return query.FirstOrDefault();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
