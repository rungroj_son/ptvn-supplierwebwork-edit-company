﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;


namespace SupplierPortal.Data.Models.Repository.DocumentDwnld
{
    public partial class DocumentDwnldRepository : IDocumentDwnldRepository
    {
        private SupplierPortalEntities context;


        public DocumentDwnldRepository()
        {
            context = new SupplierPortalEntities();
        }

        public DocumentDwnldRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }


        public virtual IEnumerable<Tbl_DocumentDwnld> List()
        {
            var query = from db in context.Tbl_DocumentDwnld
                        where db.IsActive == 1
                        select db;
            var result = query.ToList();

            return result;
        }

        public virtual  IEnumerable<Tbl_DocumentDwnld> ListByLG(int languageID)
        {
            var query = from db in context.Tbl_DocumentDwnld
                        where db.IsActive == 1 && db.LanguageId == languageID
                        select db;
            var result = query.ToList();

            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
