﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgShareholder
{
    public partial class LogOrgShareholderRepository : ILogOrgShareholderRepository
    {
        private SupplierPortalEntities context;

        public LogOrgShareholderRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogOrgShareholderRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int id, int UpdateBy, string logAction)
        {
            try
            {
                var LogOrgAdd = context.Tbl_OrgShareholder.FirstOrDefault(m => m.Id == id);

                if (LogOrgAdd != null)
                {
                    var _logOrg = new Tbl_LogOrgShareholder()
                    {
                        Id = LogOrgAdd.Id,
                        SupplierID = LogOrgAdd.SupplierID,
                        SeqNo = LogOrgAdd.SeqNo,
                        TitleID = LogOrgAdd.TitleID,
                        FirstName_Local = LogOrgAdd.FirstName_Local,
                        FirstName_Inter = LogOrgAdd.FirstName_Inter,
                        LastName_Local = LogOrgAdd.LastName_Local,
                        LastName_Inter = LogOrgAdd.LastName_Inter,
                        CountyCode = LogOrgAdd.CountyCode,
                        IdentityNo = LogOrgAdd.IdentityNo,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgShareholder.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
