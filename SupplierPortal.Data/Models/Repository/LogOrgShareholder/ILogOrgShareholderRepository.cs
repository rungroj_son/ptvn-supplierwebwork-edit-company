﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgShareholder
{
    public partial interface ILogOrgShareholderRepository
    {
        void Insert(int id, int UpdateBy, string logAction);

        void Dispose();
    }
}
