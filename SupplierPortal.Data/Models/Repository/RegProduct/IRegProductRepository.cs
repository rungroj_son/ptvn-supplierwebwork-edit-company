﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegProduct
{
    public partial interface IRegProductRepository
    {
        IEnumerable<Tbl_RegProduct> GetRegProductByRegID(int regid);

        IEnumerable<ProductTypeAddModel> GetProductTypeByRegIDAndProductCode(int regID, string productCode);

        IEnumerable<Tbl_RegProduct> GetRegProductByMany(int regid, int producttypeid, string productcode);

        bool Insert(int regid, int producttypeid, string productcode);

        bool Delete(int regid, int producttypeid, string productcode);

        bool Delete(int regid, string productcode);

        bool Update(int regid,int oldProducttypeid, int producttypeid, string productcode);

        IEnumerable<RegProductGridViewModel> GetMultiLangRegProductByMany(int regid, int languageID);

        bool RegProductExists(int regid, string productCode);

        bool RegProductExists(int regid, string productCode, int producttypeid);

        void Dispose();
    }
}
