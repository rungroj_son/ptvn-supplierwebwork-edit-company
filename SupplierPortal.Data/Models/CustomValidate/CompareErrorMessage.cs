﻿using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System;
using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace SupplierPortal.Data.CustomValidate
{
    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CompareErrorMessage : CompareAttribute, IClientValidatable
    {

        private string _resourceValue = string.Empty;

        public CompareErrorMessage(string otherProperty, string resourceKey)
            : base(otherProperty)
        {
            OtherProperty = otherProperty;
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }

        public string OtherProperty { get; private set; }

        public static string FormatPropertyForClientValidation(string property)
        {
            if (property == null)
            {
                throw new ArgumentException("Error", "property");
            }
            return "*." + property;
        }

        IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(ResourceKey);
            _repo.Dispose();

            //var rule = new ModelClientValidationRule();
            //rule.ErrorMessage = _resourceValue;
            //rule.ValidationType = "equalto";
            //rule.ValidationParameters["otherpropertyname"] = OtherProperty;
            //rule.ValidationParameters. = ResourceKey;
            //yield return rule;
            yield return new ModelClientValidationEqualToRule(_resourceValue, FormatPropertyForClientValidation(OtherProperty));
        }
    }
}
