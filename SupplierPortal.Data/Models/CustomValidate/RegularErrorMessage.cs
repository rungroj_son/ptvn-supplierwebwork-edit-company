﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;

namespace SupplierPortal.Data.CustomValidate
{
    public class RegularErrorMessage : RegularExpressionAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string ResourceKey { get; set; }
        public string Pattern { get; private set; }

        public RegularErrorMessage(string pattern, string resourceKey)
            : base(pattern)
        {
            Pattern = pattern;
            ResourceKey = resourceKey;
        }

        IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {

            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(ResourceKey);
            _repo.Dispose();

            yield return new ModelClientValidationRegexRule(_resourceValue, Pattern);
        }
    }
}
