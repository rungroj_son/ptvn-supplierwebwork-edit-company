//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AutoUpdateUser
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public Nullable<int> isSuccess { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public Nullable<System.DateTime> ImportDate { get; set; }
    }
}
