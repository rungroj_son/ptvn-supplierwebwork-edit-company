//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    
    public partial class SupVerBlockSupplier_Sel_Result
    {
        public string Username { get; set; }
        public int UserID { get; set; }
        public string BranchNo { get; set; }
        public string Email { get; set; }
        public string OrgID { get; set; }
        public string TaxID { get; set; }
        public string NameInter { get; set; }
        public string NameLocal { get; set; }
        public string InvName_Inter { get; set; }
        public string InvName_Local { get; set; }
        public int OrgBlockStatusID { get; set; }
        public int UserBlockStatusID { get; set; }
        public int OrgBlockStatusID_SWW { get; set; }
        public int UserBlockStatusID_SWW { get; set; }
        public string orgStatusName { get; set; }
        public string userStatusName { get; set; }
        public string orgStatusNameSWW { get; set; }
        public string userStatusNameSWW { get; set; }
    }
}
