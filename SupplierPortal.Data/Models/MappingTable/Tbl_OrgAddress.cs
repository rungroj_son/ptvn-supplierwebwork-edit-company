//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_OrgAddress
    {
        public int SupplierID { get; set; }
        public int AddressID { get; set; }
        public int AddressTypeID { get; set; }
        public Nullable<int> SeqNo { get; set; }
    
        public virtual Tbl_Address Tbl_Address { get; set; }
        public virtual Tbl_AddressType Tbl_AddressType { get; set; }
        public virtual Tbl_Organization Tbl_Organization { get; set; }
    }
}
