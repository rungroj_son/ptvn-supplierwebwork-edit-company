//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_AutoComplete_City
    {
        public int Id { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public Nullable<int> StateID { get; set; }
        public string CountryCode { get; set; }
        public int LanguageID { get; set; }
    }
}
