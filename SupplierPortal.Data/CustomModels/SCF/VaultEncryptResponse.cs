﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public partial class VaultEncryptResponse
    {
        public string request_id { get; set; }
        public string lease_id { get; set; }
        public bool renewable { get; set; }
        public int lease_duration { get; set; }
        public VaultDecryptRequest data { get; set; }
        public string wrap_info { get; set; }
        public string warnings { get; set; }
        public string auth { get; set; }
    }
}
