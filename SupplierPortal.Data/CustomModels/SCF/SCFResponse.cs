﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public partial class SCFResponse
    {
        public bool isError { get; set; }

        public string msgError { get; set; }

        public int scfRequestId { get; set; }
    }
}
