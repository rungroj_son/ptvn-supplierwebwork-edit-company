﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public partial class UserSessionSCF
    {
        public string userGuid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string ipAddress { get; set; }
        public string browserType { get; set; }
        public Nullable<System.DateTime> loginTime { get; set; }
        public Nullable<System.DateTime> logoutTime { get; set; }
        public Nullable<System.DateTime> lastAccessed { get; set; }
        public Nullable<int> isTimeout { get; set; }
        public string sessionData { get; set; }
    }
}
