﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public partial class SupplierContactInfo
    {
        public string companyName { get; set; }
        public string companyNameLocal { get; set; }
        public string contactName { get; set; }
        public string contactNameLocal { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
    }
}
