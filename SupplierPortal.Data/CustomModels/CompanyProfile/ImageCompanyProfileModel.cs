﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.CompanyProfile
{
    public partial class ImageCompanyProfileModel
    {
        public int attachmentId { get; set; }
        public string attachmentName { get; set; }
        public string attachmentNameUnique { get; set; }
        public Nullable<int> documentNameId { get; set; }

    }
}
