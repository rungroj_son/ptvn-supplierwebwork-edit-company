﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class ConnectJobResponse
    {
        public int regId { get; set; }

        public bool isError { get; set; }

        public string type { get; set; }

        public string errorMsg { get; set; }
    }
}
