﻿using System.Collections.Generic;

namespace SupplierPortal.Data.CustomModels.SearchResult
{
    public partial class AdvanceSearch
    {
        public string column { get; set; }
        public string conditionType { get; set; }
        public string operatorType { get; set; }
        public string value { get; set; }
        public List<string> listValue { get; set; }
    }
}
