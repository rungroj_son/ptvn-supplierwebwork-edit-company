﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.Buyer
{
    public class BuyerUserModel
    {
        public string Username { get; set; }

        public int EID { get; set; }

        public int EPSysUserID { get; set; }

    }
}
