﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.User
{
    public class UserRequest
    {
        public string username { get; set; }

        public string password { get; set; }
    }
}
