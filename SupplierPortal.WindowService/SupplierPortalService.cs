﻿using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace SupplierPortal.WindowService
{
    public partial class SupplierPortalService : ServiceBase
    {
        private static short _intInterval = 60;// 60 minutes
        private static string _scheduledHour = "1";
        //private static DateTime _scheduledTime;
        private static System.Timers.Timer timerExecution = null;
        WriteLog writeLog;
        public SupplierPortalService()
        {
            writeLog = new WriteLog();
            InitializeComponent();

            ReadConfigurationSettings();
            writeLog.WriteToFile("InitializeComponent " + DateTime.Now);

            timerExecution = new System.Timers.Timer(1000);
            timerExecution.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerExecutionEvent);
            timerExecution.Enabled = false;
        }

        protected override void OnStart(string[] args)
        {
            ReadConfigurationSettings();

            timerExecution.Interval = _intInterval * (60 * 1000);
            timerExecution.Enabled = true;
            timerExecution.Start();
            writeLog.WriteToFile("Service Start " + DateTime.Now);
        }

        protected override void OnStop()
        {
            timerExecution.Enabled = false;
            timerExecution.Stop();
            writeLog.WriteToFile("Service Stop " + DateTime.Now);
        }

        protected override void OnPause()
        {
            timerExecution.Enabled = false;
            timerExecution.Stop();
            writeLog.WriteToFile("Service Pause " + DateTime.Now);
        }

        protected override void OnContinue()
        {
            ReadConfigurationSettings();

            timerExecution.Interval = _intInterval * (60 * 1000);
            timerExecution.Enabled = true;
            timerExecution.Start();
            writeLog.WriteToFile("Service Countinue " + DateTime.Now);
        }

        private async void OnTimerExecutionEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            SupplierPortalNotification noti = null;
            bool result = false;
            try
            {
                timerExecution.Stop();

                writeLog.WriteToFile("Check condiition " + DateTime.Now);
                //if (!string.IsNullOrEmpty(_scheduledHour))
                //{
                //    _scheduledTime = DateTime.Today.AddHours(Double.Parse(_scheduledHour));
                //    if (_scheduledTime < DateTime.Now)
                //    {
                //        noti = new SupplierPortalNotification();
                //        result = await noti.SendMailWelcome();
                //    }
                //}
                //else
                //{
                //    noti = new SupplierPortalNotification();
                //    result = await noti.SendMailWelcome();
                //}

                noti = new SupplierPortalNotification();
                result = await noti.CheckStatusSendMailWelcome();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    writeLog.WriteToFile("OnTimerExecutionEvent " + DateTime.Now + " " + ex.InnerException);
                else
                    writeLog.WriteToFile("OnTimerExecutionEvent " + DateTime.Now + " " + ex.Message);

            }
            finally
            {
                if (timerExecution.Enabled == false)
                {
                    timerExecution.Enabled = true;
                }

                if(result)
                    writeLog.WriteToFile("Service Run Time Succcessful " + DateTime.Now);
                else
                    writeLog.WriteToFile("Service Run Time Fails " + DateTime.Now);

                if (noti != null)
                    noti.Dispose();

                timerExecution.Start();
            }
        }

        private static void ReadConfigurationSettings()
        {
            try
            {
                _scheduledHour = ConfigurationManager.AppSettings["ScheduledHour"];
                _intInterval = Convert.ToInt16(ConfigurationManager.AppSettings["IntInterval"]);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
