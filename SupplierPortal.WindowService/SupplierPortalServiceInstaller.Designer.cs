﻿namespace SupplierPortal.WindowService
{
    partial class SupplierPortalServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.supplierPortalProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.supplierPortalInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 

            this.supplierPortalProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.supplierPortalProcessInstaller.Password = null;
            this.supplierPortalProcessInstaller.Username = null;
            // 
            // serviceInstaller1
            // 
            this.supplierPortalInstaller.Description = "Supplier Portal Services";
            this.supplierPortalInstaller.DisplayName = "Supplier Portal Services";
            this.supplierPortalInstaller.ServiceName = "SupplierPortal";
            this.supplierPortalInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.supplierPortalInstaller,
            this.supplierPortalInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller supplierPortalProcessInstaller;
        private System.ServiceProcess.ServiceInstaller supplierPortalInstaller;
    }
}