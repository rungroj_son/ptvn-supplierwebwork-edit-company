﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.WindowService
{
    public partial class NotificationModel
    {
        public class SendEmailStatus
        {
            public Boolean Success { get; set; }
            public string Message { get; set; }
        }
    }
}
