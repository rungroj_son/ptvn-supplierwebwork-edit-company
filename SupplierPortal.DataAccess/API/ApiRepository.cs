﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.API
{
    public partial class ApiRepository : BaseRepository, IApiRepository
    {
        public virtual IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdateAPI(string username)
        {
            var query = from a in context.Tbl_UserSystemMapping
                        join b in context.Tbl_System on a.SystemID equals b.SystemID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_User on a.UserID equals e.UserID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
                        from j in i.Where(x => x.isDeleted != 1).DefaultIfEmpty()
                        join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
                        from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
                        join n in context.Tbl_SystemConfigureAPI on d.SystemID equals n.SystemID into o
                        from p in o.DefaultIfEmpty()
                        join q in context.Tbl_API on p.APIId equals q.APIId into r
                        from s in r.DefaultIfEmpty()
                        where g.Username == username
                        && d.IsActive == 1
                        && m.isAllowConnecting == 1
                        && p.isActive == 1
                        && s.isActive == 1
                        && s.API_Name == "UpdateUser"
                        select new SystemRoleListModels
                        {
                            Username = username,
                            SystemID = a.SystemID,
                            SystemName = d.SystemName,
                            Description = d.Description,
                            SystemGrpID = d.SystemGrpID,
                            IsShowOnList = d.IsShowOnList,
                            IsShowRoleList = d.IsShowRoleList,
                            API_URL = s.API_URL,
                            API_Key = s.API_Key,
                            API_Name = s.API_Name,
                            //APIName_InsertUser = d.APIName_InsertUser,
                            //APIName_UpdateUser = d.APIName_UpdateUser,
                            //APIName_UpdateSupplier = d.APIName_UpdateSupplier,
                            IsChecked = a.isEnableService ?? 0,
                            SysRoleID = "PrimaryUser"
                        };

            //SysRoleID = "PrimaryUser" ฟิกไว้เพื่อไป Get ค่า SysRoleID ของ User ที่เป็น Primary กรณีที่ต้อง call update ใน user ที่มาจากการ merge OP ซึ่งอาจมาหลาย SysRoleID

            var result = query.Distinct().ToList();

            return result;
        }
    }
}
