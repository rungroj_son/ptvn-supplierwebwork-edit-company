﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.API
{
    public interface IApiRepository
    {
        IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdateAPI(string username);
    }
}
