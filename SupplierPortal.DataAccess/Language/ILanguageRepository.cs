﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Language
{
    public partial interface ILanguageRepository
    {
        IEnumerable<Tbl_Language> GetLanguageIdByCode(string languageCode);

        bool IsLocalLanguage(int languageId);

        List<Tbl_Language> GetLanguage();

        void Dispose();
    }
}
