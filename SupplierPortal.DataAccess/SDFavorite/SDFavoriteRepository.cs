﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.SDFavorite
{
    public class SDFavoriteRepository : BaseRepository, ISDFavoriteRepository
    {
        #region GET
        public virtual IEnumerable<Tbl_SDFavorite> GetSDFavoriteByEID(int EID, string sysUserId)
        {

            
            List<int> userIDIgnore = (from tmp_user in context.Tbl_User.Where(x => x.UserID > 21066)
                                      join tmp_con in context.Tbl_ContactPerson on tmp_user.ContactID equals tmp_con.ContactID
                                      where tmp_user.isAcceptTermOfUse != 1
                                      select tmp_user.UserID).ToList();

            var query = from a in context.Tbl_SDFavorite
                        join b in context.Tbl_User on a.UserID equals b.UserID
                        join c in context.Tbl_ContactPerson on b.ContactID equals c.ContactID
                        join d in context.Tbl_OrgContactPerson on c.ContactID equals d.ContactID
                        where a.EID == EID
                        && a.SysUserID == sysUserId
                        && d.NewContactID == 0


                        // Added 10/01/2017 by Pranop B.
                        && b.IsDeleted != 1
                        && c.isDeleted != 1
                        && !(from i in context.Tbl_UserBlock select i.UserID).Contains(a.UserID ?? 0)//&& !userIDBlock.Contains(b.UserID)
                        && !(from i in context.Tbl_OrgBlock select i.SupplierID).Contains(b.SupplierID ?? 0)//&& !supplierIDBlock.Contains(b.SupplierID)

                        select a;

            var result = query.ToList();

            // Added 04/01/2017 by Pranop B.
            result.RemoveAll(x => userIDIgnore.Contains(x.UserID ?? 0));

            return result;
        }

        public virtual List<Core_SupplierContactFavorite_Result> GetSDFavorites(int eid, int supplierId, int sysUserId)
        {
            List<Core_SupplierContactFavorite_Result> results = context.Core_SupplierContactFavorite(eid, supplierId, sysUserId).ToList();

            return results;
        }

        public virtual Tbl_SDFavorite GetSDFavorite(string sysUserID, int EID, int userID)
        {
            var result = context.Tbl_SDFavorite.Where(m => m.SysUserID == sysUserID && m.EID == EID && m.UserID == userID).FirstOrDefault();

            return result;
        }
        #endregion

        #region Insert
        public virtual int Insert(Tbl_SDFavorite tbl_SDFavorite, int updateBy)
        {
            try
            {
                context.Tbl_SDFavorite.Add(tbl_SDFavorite);
                context.SaveChanges();
                return tbl_SDFavorite.Id;
            }
            catch (Exception e)
            {

                throw;
            }

        }
        #endregion

        #region Delete
        public virtual int Delete(int id, int updateBy)
        {

            try
            {
                var tbl_SDFavoriteTemp = context.Tbl_SDFavorite.Find(id);

                if (tbl_SDFavoriteTemp != null)
                {
                    context.Tbl_SDFavorite.Remove(tbl_SDFavoriteTemp);

                    context.SaveChanges();
                    return id;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion

    }
}
