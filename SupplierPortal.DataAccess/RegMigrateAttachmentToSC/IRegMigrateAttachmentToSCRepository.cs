﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.RegMigrateAttachmentToSC
{
    public interface IRegMigrateAttachmentToSCRepository
    {
        List<Temp_RegMigrateAttachmentToSC> FindAll();

        List<Temp_RegMigrateAttachmentToSC> FindByIsMigrateComplete(bool isMigrateComplete);

        int Insert(Temp_RegMigrateAttachmentToSC attachmentToSC);

        bool Update(Temp_RegMigrateAttachmentToSC attachmentToSC);
    }
}
