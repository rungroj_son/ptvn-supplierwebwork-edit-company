﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.BillingCondition
{
    public partial class UserBillingConditionRepository : BaseRepository, IUserBillingConditionRepository
    {
        #region GET
        public Tbl_UserBillingCondition GetUserBillingCondition(string username)
        {
            Tbl_UserBillingCondition result;
            try
            {
                result = new Tbl_UserBillingCondition();
                result = context.Tbl_UserBillingCondition.FirstOrDefault(f => f.Username == username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion


        #region Update
        public bool UpdateUserBillingCondition(Tbl_UserBillingCondition users)
        {
            bool result = false;
            try
            {
                var contactTemp = this.context.Tbl_UserBillingCondition.Find(users.Id);
                this.context.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
