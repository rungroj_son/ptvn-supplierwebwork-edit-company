﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.NameTitle
{
    public partial interface INameTitleRepository
    {
        List<Tbl_NameTitle> GetNameTitleList(int languageID);

        void Dispose();
    }
}
