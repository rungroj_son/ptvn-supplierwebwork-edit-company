﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.COC
{
    public partial class UserCOCRepository : BaseRepository, IUserCOCRepository
    {
        public ViewOrgVendorConsent GetConsent(string userName)
        {
            return context.ViewOrgVendorConsents.FirstOrDefault(a => a.Username == userName);
        }
    }
}
