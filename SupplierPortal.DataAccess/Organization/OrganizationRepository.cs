﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Organization
{
    public class OrganizationRepository : BaseRepository, IOrganizationRepository
    {
        #region GET
        public Tbl_Organization GetOrganization(int supplierId)
        {
            try
            {
                Tbl_Organization result = context.Tbl_Organization.Where(org => org.SupplierID == supplierId && org.isDeleted == 0).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tbl_Organization GetOrganization(string taxid, string branchNo)
        {
            try
            {
                Tbl_Organization result = context.Tbl_Organization.Where(org => org.TaxID == taxid && org.BranchNo == branchNo).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tbl_Organization> GetOrganizationByTaxIdAndCountryCode(string taxid, string countryCode)
        {
            try
            {
                return context.Tbl_Organization.Where(a => a.TaxID == taxid && a.CountryCode == countryCode).OrderBy(o => o.BranchNo).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrganizationWithPagination_Sel_Result> GetOrganizationWithPagination(int pageIndex, int pageSize, string search, string sorting)
        {
            List<OrganizationWithPagination_Sel_Result> result = null;
            try
            {
                result = new List<OrganizationWithPagination_Sel_Result>();
                result = context.OrganizationWithPagination_Sel(pageIndex, pageSize, search, sorting).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion
    }
}
