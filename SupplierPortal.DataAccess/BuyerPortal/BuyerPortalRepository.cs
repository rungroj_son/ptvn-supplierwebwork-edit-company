﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.BuyerPortal
{
    public partial class BuyerPortalRepository : BaseRepository, IBuyerPortalRepository
    {       
        #region BuyerSupplier

        #region GET User Information by Username
        public IEnumerable<Tbl_BuyerUser> GetBuyerUserInformationByUserName(string userName)
        {
            IEnumerable<Tbl_BuyerUser> result = null;
            try
            {
                var query = from user in context.Tbl_BuyerUser
                            join contact in context.Tbl_BuyerContactPerson on user.ContactID equals contact.ContactID
                            join job in context.Tbl_JobTitle on contact.JobTitleID equals job.JobTitleID
                            join title in context.Tbl_NameTitle on contact.TitleID equals title.TitleID
                            where (user.Login == userName)
                            select user;
                result = query.ToList();

            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        #endregion

        #endregion

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
