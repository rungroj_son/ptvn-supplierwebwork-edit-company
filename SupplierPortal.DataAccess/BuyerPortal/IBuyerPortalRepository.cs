﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.BuyerPortal
{
    public partial interface IBuyerPortalRepository
    {
        IEnumerable<Tbl_BuyerUser> GetBuyerUserInformationByUserName(string userName);
        void Dispose();
    }
}
