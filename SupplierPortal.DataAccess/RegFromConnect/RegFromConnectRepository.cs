﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.RegFromConnect
{
    public class RegFromConnectRepository : BaseRepository, IRegFromConnectRepository
    {

        #region Get
        public Tbl_RegFromConnect GetByRegId(int regId)
        {
            Tbl_RegFromConnect result;
            try
            {
                result = context.Tbl_RegFromConnect.Where(w => w.RegId == regId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Get

        #region Get
        public List<Tbl_RegFromConnect> GetAllByIsJobCompleted(bool isCompleted)
        {
            List<Tbl_RegFromConnect> result = null;

            try
            {
                result = context.Tbl_RegFromConnect.Where(w => w.IsJobCompleted == isCompleted).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Get

        #region Insert
        public int Insert(Tbl_RegFromConnect regFromConnect)
        {
            int result = 0;
            try
            {
                context.Tbl_RegFromConnect.Add(regFromConnect);
                context.SaveChanges();
                result = regFromConnect.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Insert

        #region Update
        public bool Update(Tbl_RegFromConnect regFromConnect)
        {
            bool result = false;
            try
            {
                if (regFromConnect != null)
                {
                    var temp = context.Tbl_RegFromConnect.Find(regFromConnect.Id);
                    temp = regFromConnect;
                    context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Update
    }
}
