﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Country
{
    public class CountryRepository : BaseRepository,ICountryRepository
    {

        #region GET
        public Tbl_Country GetCountry(string countryCode)
        {
            try
            {
                Tbl_Country result = context.Tbl_Country.Where(i => i.CountryCode == countryCode).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
