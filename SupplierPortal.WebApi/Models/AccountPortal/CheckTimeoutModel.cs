﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class CheckTimeoutModel
    {

        [Required]
        public string UserGuid { get; set; }

        public string checkSessionOnly { get; set; }
    }
}