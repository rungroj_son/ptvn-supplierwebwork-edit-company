﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models.Notification
{
    public partial class AddNotificationModel
    {
        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public int TopicID { get; set; }
        public int SystemID { get; set; }
        public IList<SysUserIDResponse> SysUserInfo { get; set; }
        public dynamic DataKey { get; set; }
        public NotificationDataResponse NotificationData { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }
        public dynamic URLParameters { get; set; }
    }

    public class SysUserIDResponse
    {
        public string SysUserID { get; set; }
        public string SupplierShortName { get; set; }
    }

    public class NotificationDataResponse
    {
        public dynamic Standard { get; set; }
        public dynamic EN { get; set; }
        public dynamic TH { get; set; }
    }
}