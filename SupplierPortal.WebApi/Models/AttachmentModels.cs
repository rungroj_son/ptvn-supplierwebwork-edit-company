﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models
{
    public class AttachmentModels
    {
        public string RegID { get; set; }
        public string SupplierID { get; set; }
    }
}