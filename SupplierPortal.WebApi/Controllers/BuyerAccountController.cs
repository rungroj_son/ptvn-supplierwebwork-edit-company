﻿using SupplierPortal.Framework.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.BusinessLogic.BuyerPortal;
using System;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/BuyerAccountPortal")]
    public class BuyerAccountController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IBuyerPortalManager _buyerPortalManager;

        public BuyerAccountController()
        {
            _buyerPortalManager = new BuyerPortalManager();
        }

        [HttpGet]
        [Route("LoginValidate")]
        public HttpResponseMessage LoginValidate(string languageId = "", string browserType = "", string clientIPAddress = "")
        {
            HttpResponseMessage result = null;
            try
            {
                if (string.IsNullOrEmpty(languageId))
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
                }

                result = new HttpResponseMessage();
                // getData from Header
                string publicKey = getHeadersByKey("PublicKey");
                string authInfo = getHeadersByKey("AuthInfo");
                string encodeUserpassword = getHeadersByKey("Userpassword");
                result = _buyerPortalManager.LoginValidate(publicKey, authInfo, encodeUserpassword, languageId, browserType, clientIPAddress);


            }
            catch (Exception ex)
            {
                logger.Error("LoginValidate (API) : " + ex.Message);
            }
            return result;
        }

        #region Function Support
        private string getHeadersByKey(string Key)
        {
            IEnumerable<string> headerValues = this.Request.Headers.GetValues(Key);
            //IEnumerable<string> headerValues = Request.Headers.GetValues(Key);
            return headerValues.FirstOrDefault();
        }
        #endregion
    }
}