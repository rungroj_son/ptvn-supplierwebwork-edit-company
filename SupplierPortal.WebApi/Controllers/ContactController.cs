﻿using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.ContactPerson;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Models.Dto;
using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using System.Net;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/contact")]
    public class ContactController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IContactPersonManager _manager;

        public ContactController()
        {
            _manager = new ContactPersonManager();
        }

        // GET: Contact
        [Route("emailVerify")]
        [HttpGet]
        public HttpResponseMessage emailVerify(string email)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            BaseResponseDto result = new BaseResponseDto();
            try
            {
                if (!ValidateHelper.validateEmailAddress(email))
                {
                    result.isSuccess = false;
                    result.errorMessage = GetLocaleStringResource.GetStringResourceCurrent("_ContactPersion.EmailVerify.InvalidEmail");
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    if (_manager.GetContactPersionByEmail(email))
                    {
                        result.isSuccess = false;
                        result.errorMessage = GetLocaleStringResource.GetStringResourceCurrent("_ContactPersion.EmailVerify.DuplicateEmail");
                    }
                    else
                    {
                        result.isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                logger.Error("ContactController (API) : " + ex.Message);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        [Route("get")]
        public HttpResponseMessage GetContactWithPagination([FromBody] SearchResultRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                SearchResultResponse<List<ContactWithPagination_Sel_Result>> result = new SearchResultResponse<List<ContactWithPagination_Sel_Result>>();
                result = _manager.GetContactWithPagination(request);
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent("_ContactPersion.ErrorMsg.InvalidColumnNameOrFormatValue", System.Text.Encoding.UTF8, "application/json");

                logger.Error("GetOrganizationShortDetail (API) : " + ex.InnerException);
            }

            return response;
        }
    }
}