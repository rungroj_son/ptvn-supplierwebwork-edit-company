﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.BusinessLogic.COC;
using SupplierPortal.Data.Models.MappingTable;
using System.Web.Configuration;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Models.Dto.COC;
using Newtonsoft.Json;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/coc")]
    public class UserCOCController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserCOCManager _manager;

        public UserCOCController()
        {
            _manager = new UserCOCManager();
        }

        [Route("get/{username}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetCodeOfConduct(string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            COCBaseResponseDto result = new COCBaseResponseDto() { isSuccess = false };

            try
            {
                bool useCOC = Convert.ToBoolean(WebConfigurationManager.AppSettings["use_coc"]);
                if (useCOC)
                {
                    ViewOrgVendorConsent consent = this.getConsentConfig(username);
                    if (consent != null)
                    {
                        var results = await _manager.GetCodeOfConduct(consent, username);

                        if (results != null)
                        {
                            result.isSuccess = true;
                            result.data = results;
                            response.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            result.errorMessage = "Not found code of conduct or this user already acknowledge";
                            response.StatusCode = HttpStatusCode.BadRequest;
                        }
                    }
                    else
                    {
                        result.errorMessage = "This user not in condition to acknowledge code of conduct";
                        response.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
                response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UserCOC (API) : " + ex.Message);
            }

            return response;
        }

        [Route("acknowledge/{username}")]
        [HttpPost]
        public async Task<HttpResponseMessage> AcknowledgeCodeOfConduct(string username, [FromBody] ConsentAcceptRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            COCBaseResponseDto result = new COCBaseResponseDto() { isSuccess = false };

            try
            {
                ViewOrgVendorConsent consent = this.getConsentConfig(username);
                if(consent != null)
                {
                    var results = await _manager.AcceptConsent(consent, username, request);
                    if (results != null)
                    {
                        result.isSuccess = true;
                        result.data = results;
                        response.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        result.errorMessage = "Can not accept code of conduct";
                        response.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    result.errorMessage = "Not found code of conduct";
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                
                response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UserCOC (API) : " + ex.Message);
            }

            return response;
        }
            
        private ViewOrgVendorConsent getConsentConfig(string username)
        {
            return _manager.ExistsInCodeOfConduct(username);
        }
        //// GET: UserCOC
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}