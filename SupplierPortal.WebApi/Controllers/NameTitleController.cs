﻿using SupplierPortal.BusinessLogic.NameTitle;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/NameTitle")]
    public class NameTitleController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly INameTitleManager _nameTitleManager;

        public NameTitleController()
        {
            _nameTitleManager = new NameTitleManager();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetNameTitleList")]
        public HttpResponseMessage GetNameTitleList(int languageID)
        {
            HttpResponseMessage response = null;
            try
            {
                List<Tbl_NameTitle> nameTitle = null;
                nameTitle = _nameTitleManager.GetNameTitleList(languageID);

                response = new HttpResponseMessage();
                response.StatusCode = nameTitle.Count() != 0 ? HttpStatusCode.OK : HttpStatusCode.NotFound;
                string jsonResult = new JavaScriptSerializer().Serialize(nameTitle);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetLanguageIdByCode (API) : " + ex.Message);
            }
            return response;
        }
    }
}