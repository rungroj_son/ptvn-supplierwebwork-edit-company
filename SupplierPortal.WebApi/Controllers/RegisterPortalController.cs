﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.WebApi.Models.AccountPortal;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Helpers;
using SupplierPortal.Core.Extension;
using System.Net.Http.Headers;
using SupplierPortal.Services.RegisterPortalManage;
using System.Web.Configuration;
using SupplierPortal.Framework.Localization;
using SupplierPortal.WebApi.Helper;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.Language;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/RegisterPortal")]
    public class RegisterPortalController : ApiController
    {
        #region-------------------------------------Base API Action-------------------------------------------------
        // POST api/RegisterPortal/Register
        [AllowAnonymous]
        [Route("SupplierRegister")]
        public HttpResponseMessage SupplierRegister(RegisterPortalModel model, string languageId = "")
        {
            string messageReturn = "";
            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_Global.APIService.Msg_Error", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;

                var result = new List<Object>();
                return JsonHelper.CompleteJson(result, respError);
            }

            /*TRS 20-11-2014 Begin generate Ticketcode and get Languagecode for set data object to DB*/
            var tbl_DocumentNoFormat = "RegisterTicket".GetPatternByDocName();
            string _charRandom = "", _ticketCode = "";
            foreach (var list in tbl_DocumentNoFormat)
            {
                _charRandom = list.IsRandomCode == 1 ? list.Length.ToString().GetRandomChar(list.Pattern) : "";
                _ticketCode = list.Suffix == "" ? list.Prefix + _charRandom : list.Pattern + _charRandom + list.Suffix;
            }
            var _languageCode = languageId.GetLanguageCodeById();
            //--------------------------------------------------------End----------------------------------------------
            if (model.Insert(_ticketCode, _languageCode))
            {
                try
                {
                    string email = "<a href=\"mailto:" + model.Email + "\"> " + model.Email + " </a>";
                    messageReturn = messageReturn.GetStringResource("_Regis.Msg_TicketGenSuccess", languageId);
                    messageReturn = messageReturn.Replace("<email_address>", email);
                    RegisterService.TicketSendingToEmail(_ticketCode);
                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = "Success";
                    resp.StatusCode = HttpStatusCode.OK;

                    var resultList = new List<ResultRegister>();
                    resultList.Add(new ResultRegister()
                    {
                        Message = messageReturn,
                        Result = true,
                        Email = model.Email,
                        Ticketcode = _ticketCode
                    });
                    return JsonHelper.CompleteJson(resultList, resp);
                }
                catch (Exception ex)
                {
                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = messageReturn.GetStringResource("_Global.APIService.Msg_Error", languageId);
                    respError.StatusCode = HttpStatusCode.BadRequest;

                    var result = new List<Object>();
                    return JsonHelper.CompleteJson(result, respError);
                }
            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<ResultRegister>();
            resultListNotFound.Add(new ResultRegister()
            {
                Message = messageReturn.GetStringResource("_Regis.Msg_TicketGenFailure", languageId),
                Result = false
            });
            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }
        #endregion
    }
       
}