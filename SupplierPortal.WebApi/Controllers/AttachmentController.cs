﻿using SupplierPortal.Framework.Localization;
using SupplierPortal.WebApi.Models.LinkSystem;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Services.LinkSystemManage;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.BusinessMain;
using SupplierPortal.Data.Models.Repository.LogOrgAttachment;
using SupplierPortal.Data.Models.Repository.LogRegAttachment;
using SupplierPortal.Data.Models.Repository.LogRegInfo;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.RegAttachment;
using SupplierPortal.Data.Models.Repository.RegContactRequest;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.Repository.RegHistory;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.WebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.UI;
using SupplierPortal.Data.Models.Repository.APILogs;
using System.Net.Http.Headers;
using SupplierPortal.Data.Models.Repository.APISecurity;
using SupplierPortal.MainFunction.Helper;

namespace SupplierPortal.WebApi.Controllers
{
    [System.Web.Http.RoutePrefix("api/Attachment")]
    public class AttachmentController : System.Web.Http.ApiController
    {
        #region RepositoryMethod
        SupplierPortalEntities _db;
        IAppConfigRepository _appConfigRepository;
        IRegInfoRepository _regInfo;
        IRegHistoryRepository _reghistory;
        IBusinessMainRepository _mainbusinessRepository;
        ILogRegInfoRepository _logReginfoRepository;
        IRegEmailTicketRepository _regETicket;
        IRegContactRequestRepository _regContactRequest;
        INameTitleRepository _nametitleRepository;
        IRegAttachmentRepository _regAttachmentRepository;
        ILogRegAttachmentRepository _logRegAttachmentRepository;
        IOrgAttachmentRepository _orgAttachmentRepository;
        ILogOrgAttachmentRepository _logorgAttachmentRepository;
        IAPILogsRepository _logAPIRepository;
        IAPISecurityRepository _aPISecurityRepository;

        public AttachmentController()
        {
            _db = new SupplierPortalEntities();
            _appConfigRepository = new AppConfigRepository();
            _regInfo = new RegInfoRepository();
            _reghistory = new RegHistoryRepository();
            _mainbusinessRepository = new BusinessMainRepository();
            _logReginfoRepository = new LogRegInfoRepository();
            _regETicket = new RegEmailTicketRepository();
            _regContactRequest = new RegContactRequestRepository();
            _nametitleRepository = new NameTitleRepository();
            _regAttachmentRepository = new RegAttachmentRepository();
            _logRegAttachmentRepository = new LogRegAttachmentRepository();
            _orgAttachmentRepository = new OrgAttachmentRepository();
            _logorgAttachmentRepository = new LogOrgAttachmentRepository();
            _logAPIRepository = new APILogsRepository();
            _aPISecurityRepository = new APISecurityRepository();

        }

        public AttachmentController(
            AppConfigRepository appConfigRepository,
            RegInfoRepository RegInfoRepository,
            SupplierPortalEntities db,
            RegHistoryRepository RegHistory,
            IBusinessMainRepository MainBusinessRepository,
            ILogRegInfoRepository LogRegInfoRepository,
            IRegEmailTicketRepository RegETicket,
            IRegContactRequestRepository RegContactRequest,
            INameTitleRepository NameTitleRepository,
            IRegAttachmentRepository regAttachmentRepository,
            ILogRegAttachmentRepository logRegAttachmentRepository,
            IOrgAttachmentRepository orgAttachmentRepository,
            ILogOrgAttachmentRepository logorgAttachmentRepository,
            IAPILogsRepository logAPIRepository,
            APISecurityRepository aPISecurityRepository
            )
        {
            _db = db;
            _appConfigRepository = appConfigRepository;
            _regInfo = RegInfoRepository;
            _reghistory = RegHistory;
            _mainbusinessRepository = MainBusinessRepository;
            _logReginfoRepository = LogRegInfoRepository;
            _regETicket = RegETicket;
            RegContactRequest = _regContactRequest;
            _nametitleRepository = NameTitleRepository;
            _regAttachmentRepository = regAttachmentRepository;
            _logRegAttachmentRepository = logRegAttachmentRepository;
            _orgAttachmentRepository = orgAttachmentRepository;
            _logorgAttachmentRepository = logorgAttachmentRepository;
            _logAPIRepository = logAPIRepository;
            _aPISecurityRepository = aPISecurityRepository;
        }
        #endregion RepositoryMethod

        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("MoveOrganizationAttachment")]
        public HttpResponseMessage MoveOrganizationAttachment(AttachmentModels model)
        {

            if (model.RegID == "" && model.SupplierID == "")
            {
                //-----------Case Empty Models-------------------------------------
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = "Cannot moving file.";
                respError.StatusCode = HttpStatusCode.BadRequest;

                var result = new List<Object>();


                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _logAPIRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "APIMoveFile",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch //(Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);

            }
            else
            {
                try
                {
                    //-----------Case Not Empty Models-------------------------------------

                    //MoveFileAttachment(model);
                    //return RedirectToAction("LogIn", "Account", new { area = "Admin" });

                    #region
                    int regID = Convert.ToInt32(model.RegID);
                    int supplierID = Convert.ToInt32(model.SupplierID);

                    var RegAttach = _regAttachmentRepository.GetRegAttachmentByRegID(Convert.ToInt32(regID));

                    foreach (var item in RegAttach)
                    {
                        string attachmentURL = WebConfigurationManager.AppSettings["attachmentURL"];
                      
                        string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;
                        //string pathTempRegis = "Register";
                        string pathTempRegis = model.RegID;

                        //----\\192.168.10.56\pathTemp\pathTempRegis\item.AttachmentNameUnique
                        string pathForSavingTemp = attachmentURL + pathTemp + pathTempRegis;

                        string filePathTemp = pathForSavingTemp + "/" + item.AttachmentNameUnique;
                        if (System.IO.File.Exists(filePathTemp))
                        {

                            byte[] fileBytes = System.IO.File.ReadAllBytes(filePathTemp);


                            string virtualFilePath = filePathTemp;
                            string fileName2 = Path.GetFileName(virtualFilePath);

                            string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                            string pathForSaving = attachmentURL + pathSave + model.SupplierID.ToString();

                            string filePath = pathForSaving + "/" + item.AttachmentNameUnique;

                            if (this.CreateFolderIfNeeded(pathForSaving))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                    System.IO.File.Copy(filePathTemp, filePath);
                                }
                                else
                                {
                                    // Move the file.
                                    //System.IO.File.Move(filePathTemp, filePath);
                                    System.IO.File.Copy(filePathTemp, filePath);
                                }

                                //var docID = (from m in _db.Tbl_CustomerDocName
                                //             where m.DocumentNameID == item.DocumentNameID
                                //             select m).FirstOrDefault();
                                //var attID = (from m in _db.Tbl_OrgAttachment
                                //             select m).OrderByDescending(m=>m.AttachmentID).FirstOrDefault();

                                var models = new Tbl_OrgAttachment()
                                {
                                    SupplierID = supplierID,
                                    AttachmentID = 0,
                                    AttachmentName = item.AttachmentName,
                                    AttachmentNameUnique = item.AttachmentNameUnique,
                                    DocumentNameID = item.DocumentNameID,
                                    OtherDocument = item.OtherDocument,
                                    SizeAttach = item.SizeAttach
                                };

                                //tbl_OrgAttach.AttachmentID = item.AttachmentID;
                                //tbl_OrgAttach.AttachmentName = item.AttachmentName;
                                //tbl_OrgAttach.AttachmentNameUnique = item.AttachmentNameUnique;
                                //tbl_OrgAttach.DocumentNameID = item.DocumentNameID;
                                //tbl_OrgAttach.OtherDocument = item.OtherDocument;
                                //tbl_OrgAttach.SizeAttach = item.SizeAttach;
                                //tbl_OrgAttach.SupplierID = supplierID;

                                _orgAttachmentRepository.Insert(models);
                                //_db.OrgAttachment_ins(supplierID, attID.AttachmentID+1, item.AttachmentName, item.AttachmentNameUnique, item.SizeAttach, item.DocumentNameID, item.OtherDocument);

                                _logorgAttachmentRepository.InsertLogAdd(supplierID, item.AttachmentNameUnique, "Add");
                                //}
                            }
                            //}
                        }
                    }

                    #endregion

                    HttpResponseMessage respOK = new HttpResponseMessage();
                    respOK.ReasonPhrase = "Moving file successful.";
                    respOK.StatusCode = HttpStatusCode.OK;

                    var result = new List<Object>();

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, respOK);

                        _logAPIRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "APIMoveFile",
                            ((Int32?)respOK.StatusCode).ToString(),
                            respOK.IsSuccessStatusCode ? "true" : "false",
                            respOK.ReasonPhrase, jsonData, "");
                    }
                    catch //(Exception ex)
                    {
                        throw;
                    }
                    #endregion
                

                    return JsonHelper.CompleteJson(result, respOK);
                }
                catch
                {

                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = "Moving file not successful.";
                    respError.StatusCode = HttpStatusCode.OK;

                    var result = new List<Object>();
                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, respError);

                        _logAPIRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "APIMoveFile",
                            ((Int32?)respError.StatusCode).ToString(),
                            respError.IsSuccessStatusCode ? "true" : "false",
                            respError.ReasonPhrase, jsonData, "");
                    }
                    catch //(Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(result, respError);

            
                }

            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Cannot moving file.";
            respNotFound.StatusCode = HttpStatusCode.BadRequest;

            var resultNotFound = new List<Object>();

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultNotFound, respNotFound);

                _logAPIRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "APIMoveFile",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultNotFound, respNotFound);

        }

        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("MoveToDeleteAttachment")]
        public HttpResponseMessage MoveToDeleteAttachment(AttachmentDeleteModels model)
        {

            try
            {
                #region Move File
                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(model.AttachmentID);
                if (attachmentModel != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                    string pathDelete = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentDelPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + attachmentModel.SupplierID;
                    string pathForDelete = pathAttachment + pathDelete + attachmentModel.SupplierID;
                    string guidName = attachmentModel.AttachmentNameUnique;

                    string filePath = pathForSaving + "/" + guidName;

                    if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                        string virtualFilePath = filePath;
                        string fileName2 = Path.GetFileName(virtualFilePath);

                        //var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        //if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        //{

                        string filePathDel = pathForDelete + "/" + guidName;

                        if (this.CreateFolderIfNeeded(pathForDelete))
                        {
                            // Ensure that the target does not exist. 
                            if (System.IO.File.Exists(filePathDel))
                                System.IO.File.Delete(filePathDel);

                            // Move the file.
                            System.IO.File.Move(filePath, filePathDel);
                        }
                        //}
                    }

                    HttpResponseMessage respOK = new HttpResponseMessage();
                    respOK.ReasonPhrase = "Moving file successful.";
                    respOK.StatusCode = HttpStatusCode.OK;

                    var result = new List<Object>();

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, respOK);

                        _logAPIRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "APIMoveFile",
                            ((Int32?)respOK.StatusCode).ToString(),
                            respOK.IsSuccessStatusCode ? "true" : "false",
                            respOK.ReasonPhrase, jsonData, "");
                    }
                    catch //(Exception ex)
                    {
                        throw;
                    }
                    #endregion


                    return JsonHelper.CompleteJson(result, respOK);
                }
                #endregion

                
                
            }
            catch
            {

                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = "Moving file not successful.";
                respError.StatusCode = HttpStatusCode.OK;

                var result = new List<Object>();
                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _logAPIRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "APIMoveFile",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch //(Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);


            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Cannot moving file.";
            respNotFound.StatusCode = HttpStatusCode.BadRequest;

            var resultNotFound = new List<Object>();

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultNotFound, respNotFound);

                _logAPIRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "APIMoveFile",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultNotFound, respNotFound);

        }

        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.Route("DownloadFileAttachment")]
        public HttpResponseMessage DowmloadFile(AttachmentDownloadModels model)
        {
            try
            {
                #region Check IP Security
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                bool checkIPSecurity = _aPISecurityRepository.CheckAPISecurityByNameAndIpAddress("SWW_DownloadFileAttachment", visitorsIPAddr);

                if (!checkIPSecurity)
                {
                    HttpResponseMessage respIPNotFound = new HttpResponseMessage();
                    respIPNotFound.ReasonPhrase = "IP address is NotFound.";
                    respIPNotFound.StatusCode = HttpStatusCode.NotFound;

                    var result = new List<Object>();
                    #region Insert Log
                    try
                    {
                        string jsonData = JsonHelper.CompleteJsonString(result, respIPNotFound);

                        _logAPIRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "DowmloadFileAttachment",
                            ((Int32?)respIPNotFound.StatusCode).ToString(),
                            respIPNotFound.IsSuccessStatusCode ? "true" : "false",
                            respIPNotFound.ReasonPhrase, jsonData, "");
                    }
                    catch //(Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return respIPNotFound;
                }
                #endregion

                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(model.AttachmentID);

                if (attachmentModel != null)
                {
                    string attachmentURL = WebConfigurationManager.AppSettings["attachmentURL"];
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                    string pathForSaving = pathSave + attachmentModel.SupplierID;
                    string virtualFilePath = pathForSaving + "/" + attachmentModel.AttachmentNameUnique;
                    string filePath = @attachmentURL + @virtualFilePath;
                    if (System.IO.File.Exists(filePath))
                    {
                        FileStream fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                        long contentLength = new FileInfo(filePath).Length;

                        var response = new HttpResponseMessage();
                        response.Content = new StreamContent(fileStream);
                        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        response.Content.Headers.ContentDisposition.FileName = attachmentModel.AttachmentName;
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        response.Content.Headers.ContentLength = contentLength;
                        return response;
                    }
                }



            }
            catch
            {

                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = "Download file not successful.";
                respError.StatusCode = HttpStatusCode.NotFound;

                var result = new List<Object>();
                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _logAPIRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "DowmloadFileAttachment",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch //(Exception ex)
                {
                    throw;
                }
                #endregion

                return respError;
            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "File Is NotFound";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultNotFound = new List<Object>();

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultNotFound, respNotFound);

                _logAPIRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "DowmloadFileAttachment",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return respNotFound;
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

  }
}
