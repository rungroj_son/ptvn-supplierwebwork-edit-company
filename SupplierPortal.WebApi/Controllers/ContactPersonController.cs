﻿using SupplierPortal.BusinessLogic.UserSession;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/SCF")]
    public class ContactPersonController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserSessionManager _userSessionManager;

        public ContactPersonController()
        {

        }

        [AllowAnonymous]
        [Route("Contact")]
        public HttpResponseMessage GetUserSession(string userGuid)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _userSessionManager.GetUserSession(userGuid);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserSession (API) : " + ex.Message);
            }
            return response;
        }

    }
}