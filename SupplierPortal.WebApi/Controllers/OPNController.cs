﻿using SupplierPortal.BusinessLogic.OPN.BuyerSupplierMapping;
using SupplierPortal.BusinessLogic.OPN.OPNOrganization;
using SupplierPortal.BusinessLogic.OPN.SupplierMapping;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using SupplierPortal.Models.Dto.OPN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/opn")]
    public class OPNController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IOPNBuyerSupplierMappingManager _opnBuyerSupplierMappingManager;
        private readonly IOPNSupplierMappingManager _opnSupplierMappingManager;
        private readonly IOPNOrganizationManager iOPNOrganizationManager;

        public OPNController()
        {
            _opnBuyerSupplierMappingManager = new OPNBuyerSupplierMappingManager();
            _opnSupplierMappingManager = new OPNSupplierMappingManager();
            iOPNOrganizationManager = new OPNOrganizationManager();
        }

        [Route("approve")]
        [HttpPost]
        public HttpResponseMessage PISApproveBuyerSupplierMapping([FromBody] int regId)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _opnBuyerSupplierMappingManager.PISApproveBuyerSupplierMapping(regId);

                response = new HttpResponseMessage();

                if (results)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("PISApproveBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

        [Route("jobupdate")]
        [HttpGet]
        public async Task<HttpResponseMessage> JobRunningUpdateBuyerSupplierMappingAsync()
        {
            HttpResponseMessage response = null;
            try
            {

                bool results = await _opnBuyerSupplierMappingManager.JobRunningUpdateBuyerSupplierMapping();
                response = new HttpResponseMessage();

                if (results)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

        [Route("buyersuppliermapping")]
        [HttpPost]
        public HttpResponseMessage InsertBuyerSupplierMapping(RegisterOPNSupplierMapping request)
        {
            HttpResponseMessage response = null;
            try
            {
                if(request == null)
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.BadRequest;
                    return response;
                }
                if(request.regId <= 0
                    || string.IsNullOrEmpty(request.buyerShortName)
                    || string.IsNullOrEmpty(request.email))
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.BadRequest;
                    return response;
                }

                int buyerSupplierMappingId = 0;
                buyerSupplierMappingId = _opnBuyerSupplierMappingManager.InsertOPNBuyerSupplierMapping(request);

                response = new HttpResponseMessage();

                string jsonResult = "";
                if (buyerSupplierMappingId > 0)
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "success", Success = true });
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "unsuccess", Success = false });
                    response.StatusCode = HttpStatusCode.InternalServerError;
                }

                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

        [Route("suppliermapping")]
        [HttpPost]
        public HttpResponseMessage InsertSupplierMapping(RegisterOPNSupplierMapping request)
        {
            HttpResponseMessage response = null;
            try
            {
                if (request == null)
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.BadRequest;
                    return response;
                }
                if (request.regId <= 0
                    || request.supplierUserId <= 0
                    || request.buyerSupplierId <= 0
                    || request.buyerSupplierContactId <= 0
                    || string.IsNullOrEmpty(request.buyerShortName))
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.BadRequest;
                    return response;
                }

                int supplierMappingId = 0;
                supplierMappingId = _opnSupplierMappingManager.InsertOPNSupplierMapping(request);

                response = new HttpResponseMessage();

                string jsonResult = "";
                if (supplierMappingId > 0)
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "success", Success = true });
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "unsuccess", Success = false });
                    response.StatusCode = HttpStatusCode.InternalServerError;
                }

                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

        [HttpPost]
        [Route("supplier/shortdetail")]

        public HttpResponseMessage GetOPNOrganizationShortDetail([FromBody] GetOPNOrganizationShortDetailRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                OPN_OrganizationDto result = null;
                string taxId = request.taxId == null ? "" : request.taxId.Trim();
                string branchNo = request.branchNo == null ? "" : request.branchNo.Trim();
                if (string.IsNullOrEmpty(taxId))
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                result = iOPNOrganizationManager.getOPNOrganizationShortDetail(taxId, branchNo);

                if (result == null)
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                logger.Error("GetOPNOrganizationShortDetail (API) : " + ex.InnerException);
            }

            return response;
        }

        [Route("invitation/buyersuppliermapping")]
        [HttpPost]
        public HttpResponseMessage insertBuyerSupplierMappingByInvitation(Tbl_OPNBuyerSupplierMappingDto request)
        {
            HttpResponseMessage response = null;
            try
            {
                if (request == null)
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.BadRequest;
                    return response;
                }

                int buyerSupplierMappingId = 0;
                buyerSupplierMappingId = _opnBuyerSupplierMappingManager.InsertOPNBuyerSupplierMapping(request);

                response = new HttpResponseMessage();

                string jsonResult = "";
                if (buyerSupplierMappingId > 0)
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "success", Success = true });
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "unsuccess", Success = false });
                    response.StatusCode = HttpStatusCode.InternalServerError;
                }

                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

    }
}