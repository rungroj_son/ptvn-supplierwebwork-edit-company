﻿using SupplierPortal.BusinessLogic.RegFromConnect;
using SupplierPortal.BusinessLogic.RegisterSupplier;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.WebApi.Filters;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/register")]
    public class RegisterSupplierController : ApiController
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IRegisterSupplierManager _registerSupplierManager;
        private readonly IRegFromConnectManager _regFromConnectManager;

        public RegisterSupplierController()
        {
            _registerSupplierManager = new RegisterSupplierManager();
            _regFromConnectManager = new RegFromConnectManager();

        }

        [Route("save")]
        [HttpPost]
        [ValidateModel]
        public async Task<HttpResponseMessage> SupplierRegister(RegisterSupplierRequest model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var supplierResponse = await _registerSupplierManager.SupplierRegister(model);

                string jsonResult = new JavaScriptSerializer().Serialize(supplierResponse);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Error SupplierRegister (API) : " + ex.InnerException);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("contact")]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage AddContactRequest(ContactRequest contactModel)
        {

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var contactResponse = _registerSupplierManager.AddContactRequest(contactModel);

                string jsonResult = new JavaScriptSerializer().Serialize(contactResponse);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Error AddContactRequest (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("job")]
        [HttpGet]
        public async Task<HttpResponseMessage> JobRun()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var contactResponse = await _regFromConnectManager.RunningJobRegisterSupplierAndRequestTojoin();

                string jsonResult = new JavaScriptSerializer().Serialize(contactResponse);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

                logger.Info("Job Update Status SWW to SC : " + jsonResult);
            }
            catch (Exception ex)
            {
                logger.Error("Error JobRun SupplierRegister (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("miniotest")]
        [HttpGet]
        public async Task<HttpResponseMessage> TestConnectMinioServer()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var result = await _registerSupplierManager.TestConnectMinioServer();

                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Error TestConnectMinioServer (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("minioupload")]
        [HttpPost]
        public async Task<HttpResponseMessage> TestMinioDownloadAndUploadAttachment(RegAttachment model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var supplierResponse = await _registerSupplierManager.TestMinioDownloadAndUploadAttachment(model);

                response = new HttpResponseMessage();

                string jsonResult = new JavaScriptSerializer().Serialize(supplierResponse);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
                logger.Error("Success TestMinioDownloadAndUploadAttachment (API) : " + jsonResult);
            }
            catch (Exception ex)
            {
                logger.Error("Error TestMinioDownloadAndUploadAttachment (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }
    }
}