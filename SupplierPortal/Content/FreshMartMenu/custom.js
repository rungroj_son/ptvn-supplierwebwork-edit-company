(function ($) {
    $(document).ready(function () {

        $windowWidth = window.innerWidth || document.documentElement.clientWidth;
        $windowHeight = window.innerHeight || document.documentElement.clientHeight;
        $pageLogoHeight = $("#PageLogo").outerHeight();
        $spyPanelHeight = $("#SpyPanel").outerHeight();

        $(window).scroll(function () {
            if ($(this).scrollTop() > 150) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
        if ($('.fancybox-media').length) {
            $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',

                arrows: false,
                helpers: {
                    media: {},
                    buttons: {}
                }
            });
        }
        $('ul.menu_row li.col li.collapse span.click').click(function () {
            if ($(this).parent('li.collapse').hasClass('closed')) {
                $(this).next('ul').show(200);
                $(this).parent('li.collapse').removeClass('closed');
            }
            else {
                $(this).parent().find('ul').hide(200);
                $(this).parent().find('ul li').addClass('closed');
                $(this).parent('li.collapse').addClass('closed');
            } return false;
        });
        $('ul.menu_row li.col li a').hover(function () {
            $(this).find('.popup-product').stop(true, false).fadeIn();
        },
        function () {
            $(this).find('.popup-product').stop(true, false).fadeOut();
        });


        $("#megamenu").on({
            "mouseenter": function () {
                $('#SpyPanel').addClass("upper");
            },
            "mouseleave": function () {
                $('#SpyPanel').removeClass("upper");
            }
        });

        $('#sort-by').change(function () {
            //console.log("Selected Option:"+$(this).val());
            $('#isotope').isotope({ sortBy: $(this).val() });
        });

        // footer links slide
        $('#BottomLinksButton').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("closed")) {
                $(this).removeClass("closed");
                $('#BottomLinks').show().css("height", "");
                var bottomlinksHeight = $('#BottomLinks').outerHeight();
                $('#BottomLinks').css("height", "0").stop(true, true).animate({ height: bottomlinksHeight }, { step: function () { $(window).scrollTop($("body").height()); } }
                );
            } else {
                $(this).addClass("closed");
                $('#BottomLinks').stop(true, true).slideUp();
            }
        });
        // product hover buttons
        $('.product').each(function () {
            but_n = $(this).find('.buttons').children().size();
            switch (true) {
                case (but_n == 1):
                    $(this).find('.buttons').addClass('full');
                    break;
                case (but_n == 2):
                    $(this).find('.buttons').addClass('one-half');
                    break;
                case (but_n == 3):
                    $(this).find('.buttons').addClass('one-third');
                    break;
                case (but_n == 4):
                    $(this).find('.buttons').addClass('one-fourth');
                    break;
                case (but_n == 5):
                    $(this).find('.buttons').addClass('one-fifth');
                    break;
                case (but_n == 6):
                    $(this).find('.buttons').addClass('one-sixth');
                    break;
                default:
                    break;
            }
        });

        if (isTouchDevice()) {
            $('body').addClass('touchdevice');
        };
        // copy content from left clumn to center column (for mobile devices)
        $('.navigation_panel').each(function () {
            if ($(this).hasClass('move-xs')) {
                $(this).clone().appendTo('#MoveFromNav');
                $(this).removeClass('move-xs').addClass('hidden-xs');
            }
            $('#MoveFromNav').find('.navigation_panel').each(function () {
                $(this).removeClass('navigation_panel').css('height', 'auto');
            })
            // close all left panels
            $(this).addClass('closed');
            // open active panel
            $('.navigation_button').each(function () {
                if ($(this).hasClass('active')) {
                    $('#' + $(this).get(0).id.replace('button', 'panel')).removeClass('closed').addClass('active');
                }
            });
        });

        SetInit($windowWidth, $windowHeight);
        FixPosition($(window).scrollTop(), $windowWidth, $windowHeight, $pageLogoHeight, $spyPanelHeight);
        // compared product delete button
        $('.custom_product .image .delete').click(function () {
            $(this).parent().parent().addClass('hide');
        });
        // search
        $("#SpyPanel .searchBoxInput").focus(function () {
            $('.searchBoxContainer').addClass("focus");

        }).blur(function () {
            $('.searchBoxContainer').removeClass("focus");
        })

        $('#SpyPanel .searchButtonClose').click(function () {
            $(this).parent().find('.searchBoxInput').val("Search");
        });
        // customize selects
        $('.selectpicker').selectpicker({});
        // quantity input
        $('.qty_minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.qty_plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
        // button Catalog for mobile devices
        $(".btn-catalog").click(function () {
            if ($('#collapsed-menu').hasClass('hidden-xs')) {
                $('#collapsed-menu').css('display', 'none').removeClass('hidden-xs').slideToggle('slow');
            } else {
                $("#collapsed-menu").slideToggle('slow');
            }
        });

        // initialize products scroll
        if (!isTouchDevice()) {
            var products_scroll_theme = "product-scroll";
            //console.log('карусель продуктов');
            $(".products_scroll").mCustomScrollbar({
                mouseWheel: false,
                scrollButtons: {
                    enable: false
                },
                horizontalScroll: true,
                scrollInertia: 50,
                autoDraggerLength: false,
                advanced: {
                    autoExpandHorizontalScroll: true,
                    updateOnContentResize: false
                },
                theme: products_scroll_theme
            });
        }
        GalleryZoomScroll($windowWidth);
        // initialize left column scroll
        if ($windowWidth > 767) {
            $(".navigation_panel").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 50,
                advanced: {
                    updateOnContentResize: true
                },
                theme: "dark-thick"
            });

            stopScroll();
        }
        // function for left panel	     
        $('.navigation_button').click(function () {
            $('.navigation_panel').each(function () {
                $(this).addClass('closed');
            });
            $('.navigation_button').each(function () {
                $(this).removeClass('active');
                $('#' + $(this).get(0).id.replace('button', 'panel')).removeClass('active');
            });
            $(this).addClass('active');
            $('#' + $(this).get(0).id.replace('button', 'panel')).removeClass('closed').addClass('active');
            $(".navigation_panel.active").mCustomScrollbar("update");
            stopScroll();
        });
        // active accordion button 
        $(".panel-heading").each(function () {
            if (!$(this).hasClass("active")) {
                $(this).children().find("a").addClass("collapsed")
            }
        });
        $(".panel-heading").click(function () {
            if ($(this).children().find("a.collapsed").length != 0) {
                $(".panel-heading").each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
        // begin collapsed menu
        $('label.tree-toggler').each(function () {
            if ($(this).parent().find('ul.nav-list').length != 0) {
                $(this).append("<span class='collapse_button'>+</span>");
            }
        });
        $('label.tree-toggler span.collapse_button').click(function () {
            if (!$(this).parent().parent().hasClass('active')) {
                $(this).html('&#8211;').addClass('collapsed');
                $(this).parent().parent().children('ul.nav-list').show(300);
                $(this).parent().parent().addClass('active');
                // update scroll
                $(".navigation_panel").mCustomScrollbar("update");
                stopScroll();
            } else {
                $(this).html('+');
                $(this).removeClass('collapsed');
                $(this).parent().parent().find('ul.nav-list').hide(300);
                $(this).parent().parent().removeClass('active');
                $(this).parent().parent().find('li').removeClass('active');
                $(this).parent().parent().find('span.collapse_button').html('+');
                // update scroll
                $(".navigation_panel").mCustomScrollbar("update");
                stopScroll();
            }
        });
        //  end collapsed menu

    });



    $(window).resize(function () {
        $windowWidth = window.innerWidth || document.documentElement.clientWidth;
        $windowHeight = window.innerHeight || document.documentElement.clientHeight;
        $pageLogoHeight = $("#PageLogo").outerHeight();
        $spyPanelHeight = $("#SpyPanel").outerHeight();
        SetInit($windowWidth, $windowHeight);
        FixPosition($(window).scrollTop(), $windowWidth, $windowHeight, $pageLogoHeight, $spyPanelHeight);
        GalleryZoomScroll($windowWidth);

        if ($windowWidth > 767) {
            // update products scroll
            $(".products_scroll").mCustomScrollbar("update");
        }
        if (!$('#BottomLinksButton').hasClass("closed")) {
            $('#BottomLinks').css("height", "auto")
        };
    });


    // $ - Wait until images  are loaded

    $(window).load(function () {
        $(".brands_scroll").mCustomScrollbar("update");
        if ($("#isotope").length != 0) {
            $("#isotope").isotope({
                masonry: {}
            });
        }

    })


    if (document.addEventListener) {
        document.addEventListener("touchmove", Scroll, false);
        document.addEventListener("scroll", Scroll, false);
    } else if (document.attachEvent) {
        document.attachEvent("touchmove", Scroll);
        document.attachEvent("scroll", Scroll);
    }


    function Scroll() {
        FixPosition($(window).scrollTop(), $windowWidth, $windowHeight, $pageLogoHeight, $spyPanelHeight);
    }

    function isTouchDevice() {
        return (typeof (window.ontouchstart) != 'undefined') ? true : false;
    }

    function GalleryZoomScroll(windowWidth) {
        // delete elevateZoom
        $.removeData($("#zoom-big-image img"), 'elevateZoom');
        if ($("#zoom-big-image img").parent('.zoomWrapper').length > 0) {
            zoom_img = $("#zoom-big-image img").clone();
            $("#zoom-big-image > .zoomWrapper").replaceWith(zoom_img);
        }
        $('.zoomContainer').each(function () {
            $(this).remove();
        })

        $("#zoom-gallery-outer").mCustomScrollbar("destroy");

        // initialize scrollbar for zoom gallery	
        if (windowWidth < 640) {

            $("#zoom-gallery-outer").css("height", "inherit");
            $("#zoom-big-image img").css('max-width', '100%');
            $("#zoom-big-image").css("height", '');

            $("#zoom-gallery-outer").mCustomScrollbar({
                mouseWheel: false,
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 50,
                horizontalScroll: true,
                advanced: {
                    updateOnContentResize: true
                },
                theme: "dark"
            });
            stopScroll();

        } else {
            $("#zoom-gallery-outer").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 50,
                advanced: {
                    updateOnContentResize: true
                },
                theme: "dark"
            });


            if ($("#zoom-big-image").length != 0) {
                var zoomH = $("#zoom-big-image img").height();
                var zoomW = $("#zoom-big-image img").width();
                var zoomPos = 1;
                var zoomType = 'window';
                if (windowWidth < 1199) {
                    zoomType = 'inner'
                }
                $("#zoom-big-image img").elevateZoom({
                    responsive: true,
                    easing: false,
                    zoomType: zoomType,
                    gallery: 'zoom-gallery-outer',
                    cursor: "crosshair",
                    borderSize: 2,
                    showLens: true,
                    zoomWindowPosition: zoomPos,
                    zoomWindowHeight: zoomH,
                    zoomWindowWidth: zoomW,
                    borderColour: "#ccc",
                    galleryActiveClass: 'active',
                    imageCrossfade: true,
                    onZoomedImageLoaded: function () {
                        if ($(".zoomWrapper").length != 0) {
                            var zoombigW = $("#zoom-big-image").width();
                            var zoombigH = $("#zoom-big-image").height();
                            $(".zoomWrapper img").css('max-width', zoombigW);
                            $(".zoomWrapper").css('width', zoombigW).css('height', $(".zoomWrapper img").height());
                            $('#zoom-gallery-outer').css('height', $(".zoomWrapper img").height());
                            $("#zoom-gallery-outer").mCustomScrollbar("update");
                            stopScroll();
                        }
                    }
                });
            }


        }
    }
    // stop general scroll when hover on custom scroll
    function stopScroll() {

        $('.mCustomScrollBox:not(".mCSB_horizontal")  .mCSB_container.stopscroll').hover(function () {
            $(document).unbind('mousewheel DOMMouseScroll MozMousePixelScroll');
        });
        $('.mCustomScrollBox:not(".mCSB_horizontal")  .mCSB_container').each(function () {
            $(this).removeClass('stopscroll');
        });

        $('.mCustomScrollBox:not(".mCSB_horizontal")  .mCSB_container:not(".mCS_no_scrollbar")').each(function () {
            $(this).addClass('stopscroll');
        });
        $('.mCustomScrollBox:not(".mCSB_horizontal")  .mCSB_container.stopscroll').hover(function () {
            $(document).bind('mousewheel DOMMouseScroll MozMousePixelScroll', function (e) {
                if (!e) { /* IE7, IE8, Chrome, Safari */
                    e = window.event;
                }
                if (e.preventDefault) { /* Chrome, Safari, Firefox */
                    e.preventDefault();
                }
                e.returnValue = false; /* IE7, IE8 */
            });
        }, function () {
            $(document).unbind('mousewheel DOMMouseScroll MozMousePixelScroll');
        });
    }

    function FixPosition(windowScrollTop, windowWidth, windowHeight, pageLogoHeight, spyPanelHeight) {
        if (windowWidth > 767) {
            if ($("#IndexPage").length) {
                //console.log('indexpage');
                var scrollTop = windowScrollTop;
                var spyPosition = pageLogoHeight - scrollTop;
                if (spyPosition < 0) {
                    $("#SpyPanel").addClass("fix");
                    if (isTouchDevice()) {
                        $("#LogoSmall").fadeIn(0);
                    } else {
                        $("#LogoSmall").fadeIn(500);
                    }

                    $("#SideColumn").addClass("fix").css('top', spyPanelHeight + 5);
                    $("#CenterColumn").css('margin-top', spyPanelHeight);
                    $(".navigation_panel").css('height', windowHeight - spyPanelHeight);
                    $(".navigation_panel.active").mCustomScrollbar("update");
                    stopScroll();
                } else {
                    $("#SpyPanel").removeClass("fix");
                    if (isTouchDevice()) {
                        $("#LogoSmall").fadeOut(0);
                    } else {
                        $("#LogoSmall").fadeOut(500);
                    }
                    $("#SideColumn").removeClass("fix").css('top', 0);
                    $("#CenterColumn").css('margin-top', 0);
                    $(".navigation_panel").css('height', windowHeight - spyPanelHeight - pageLogoHeight);
                    $(".navigation_panel.active").mCustomScrollbar("update");
                    stopScroll();
                }
            } else {
                $("#SpyPanel").addClass("fix");
                $("#LogoSmall").fadeIn(500);
                $("#SideColumn").addClass("fix").css('top', spyPanelHeight);
                $("#CenterColumn").css('margin-top', spyPanelHeight);
                $(".navigation_panel").css('height', windowHeight - spyPanelHeight);
                $(".navigation_panel.active").mCustomScrollbar("update");
                stopScroll();
            }

        } else {
            $("#SpyPanel").removeClass("fix");
            $("#SideColumn").removeClass("fix").css('top', 0);
            $("#CenterColumn").css('margin-top', 0);
            $(".navigation_panel").css('height', 'auto');
            $(".navigation_panel.active").mCustomScrollbar("disable");
        }


        if ($("#zoom-gallery-outer").length != 0) {
            $(".zoomContainer").css('z-index', 1500);
        } else {
            $(".zoomContainer").css('z-index', 2002);
        }
    }

    function SetInit(windowWidth, windowHeight) {
        // megamenu set height
        $("#megamenu li ul.shadow").mCustomScrollbar("destroy");
        $('#megamenu li ul.shadow').css('height', 'auto');
        if (windowHeight - $('#PageLogo').outerHeight() - $('#SpyPanel').outerHeight() < $('#megamenu li ul.shadow').outerHeight()) {
            $('#megamenu li ul.shadow').css('height', windowHeight - $('#PageLogo').outerHeight() - $('#SpyPanel').outerHeight());
            $("#megamenu li ul.shadow").mCustomScrollbar({
                scrollButtons: {
                    enable: false
                },
                scrollInertia: 50,
                advanced: {
                    updateOnContentResize: true
                },
                theme: "dark-thick"
            });
        }
        // calculate bootstraps grid width 
        var col_width = $('#test_col').outerWidth();
        var row_width_b = $('#test_row').outerWidth();
        var row_width = Math.round(row_width_b / col_width) * col_width;
        if (row_width > row_width_b) {
            row_width = row_width_b;
        }
        // megamenu left 
        $("#megamenu > li > ul").css('left', -$('#SideColumn').outerWidth());
        // rev slider width 
        //$(".fullwidthbanner-container").css('width', row_width_b -500 );

        // set product width
        $(".products .product").css('width', col_width - 0.1);
        $(".products_scroll .product").css('width', col_width);
        $(".products_scroll .product:last-child").css('width', col_width - 30);

        // delete elevateZoom
        $.removeData($(".zoom"), 'elevateZoom');
        $('.zoomContainer').each(function () {
            $(this).remove();
        })
        $('.zoomWindowContainer').each(function () {
            $(this).remove();
        })


        if (windowWidth > 640) {

            $(".products .product.product-2x").css('width', 2 * col_width - 0.1);
            // set #SideColumn width
            $("#SideColumn").css('width', $("#SideColumn").parent().width() + 30);
            // set #SpyPanel width
            $("#SpyPanel").css('width', $("#SpyPanel").parent(".row").css('width'));
            // set slider width on index page
            $('.flexslider').css('width', row_width + 'px');
            $('body.touchdevice .flexslider-product').css('width', row_width + 'px');
            // initialize slider on index page 
            $('.flexslider').flexslider({
                animation: "fade",
                animationLoop: true,
                mousewheel: false,
                directionNav: false,
                itemWidth: row_width,
                itemMargin: 0
            });


            // set #LeftNavigation height
            $("#LeftNavigation").css('height', $(document).height());
            // gallery + zoom
            $('.zoom').each(function () {
                var img = new Image();
                var demo_container_width, demo_container_height;
                var current_zoom = $(this);
                img.src = $(this).attr("data-zoom-image");
                img.onload = function () {
                    demo_container_width = this.width;
                    demo_container_height = this.height;
                    if (demo_container_width > row_width) {
                        demo_container_width = row_width;
                    }
                    if (demo_container_height > $(window).outerHeight() - $("#SpyPanel").height()) {
                        demo_container_height = $(window).outerHeight() - $("#SpyPanel").height();
                    }
                    posY = ($(window).outerHeight() - $("#SpyPanel").height() - demo_container_height) / 2 + 5;
                    posX = ($('#test_row').outerWidth() - demo_container_width) / 2 - 15;
                    current_zoom.elevateZoom({
                        cursor: "crosshair",
                        zoomWindowPosition: "demo-container",
                        zoomWindowWidth: demo_container_width,
                        zoomWindowHeight: demo_container_height,
                        zoomWindowOffetx: posX,
                        zoomWindowOffety: posY,
                        borderSize: 0,
                        easing: true,
                        easingAmount: 4,
                        zoomWindowFadeIn: 500,
                        zoomWindowFadeOut: 50
                    });
                }
            });
        }

        if (windowWidth < 767) {
            $("#SpyPanel").css('width', '100%');
            $("#SideColumn").css('width', 'auto');
            $(".navigation_panel").mCustomScrollbar("disable");
            $(".navigation_panel").css('height', 'auto').css('width', 'auto');
            $('.catalog').removeClass('closed');
        } else {
            $('#collapsed-menu').css('display', 'block');
        }
        if (isTouchDevice()) {
            $('.flexslider-product').flexslider({
                selector: ".products > .product",
                animation: "slide",
                prevText: "<i class='typcn typcn-chevron-left'></i>",
                nextText: "<i class='typcn typcn-chevron-right'></i>",
                animationLoop: false,
                slideshow: false,
                itemWidth: col_width,
                itemMargin: 0,
                minItems: 1,
                maxItems: 5,
                controlNav: false,
                touch: false
            });
            $('.flexslider-brands').flexslider({
                selector: ".brands_scroll > img",
                animation: "slide",
                prevText: "<i class='typcn typcn-chevron-left'></i>",
                nextText: "<i class='typcn typcn-chevron-right'></i>",
                animationLoop: false,
                slideshow: false,
                itemWidth: col_width / 3,
                itemMargin: 0,
                minItems: 2,
                maxItems: 5,
                controlNav: false,
                touch: false
            });
        }
        if ($("#isotope").length != 0) {
            $("#isotope").isotope({
                masonry: {},
                getSortData: {
                    name: function ($elem) {
                        return $elem.find('.info a ').text();
                    },
                    price: function ($elem) {
                        return parseFloat($elem.find('.sort-price').text());
                    },
                    rating: function ($elem) {
                        return parseFloat($elem.find('.sort-rating').text());
                    }
                }

            });
        }




    };

})(jQuery);