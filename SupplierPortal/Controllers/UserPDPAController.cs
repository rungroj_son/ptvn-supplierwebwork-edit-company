﻿using Microsoft.Owin.Security;
using SupplierPortal.Core.Caching;
using SupplierPortal.Data.CustomModels.Parameter;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.ServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class UserPDPAController : BaseController
    {

        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private UserPDPAService _userPDPAService;
        private readonly ICacheManager _cacheManager;

        IUserSessionRepository _userSessionRepository;
        public UserPDPAController()
        {
            _userPDPAService = new UserPDPAService();
            _userSessionRepository = new UserSessionRepository();
            _cacheManager = new MemoryCacheManager();
        }


        [AllowAnonymous]
        public async Task<ActionResult> InsertUserPDPA()
        {
            #region----------------------------Set Cookies Language By Link------------------------

            string guid = Session["GUID"].ToString();
            Users_PDPA users = new Users_PDPA()
            {
                Username = guid
            };
            bool result = await _userPDPAService.InsertUserPDPA(users);
            Session["isCheckConsent"] = true;
            #endregion
            return Json(true);
        }

        public IAuthenticationManager Authentication
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> GotoIndex()
        {
            try
            {
                string userGuid = "";

                if (Session["GUID"] != null)
                {
                    userGuid = Session["GUID"].ToString();

                    var userSession = _userSessionRepository.GetUserSession(userGuid);

                    if (userSession != null)
                    {
                        userSession.isTimeout = 1;
                        userSession.LogoutTime = DateTime.UtcNow;
                        _userSessionRepository.Update(userSession);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Logout Controller : " + ex);
            }
            finally
            {
                Authentication.SignOut();
                _cacheManager.Clear();
                Session["username"] = null;
                Session["GUID"] = null;
                Session["Org"] = null;
                Session["user"] = null;
            }

            return Json(true);
        }
    }
}