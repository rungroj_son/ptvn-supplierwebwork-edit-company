﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.LogonAttempt;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgSystemMapping;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.SupportModel;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Services.CompanyProfileManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    [RoleAuthorize(Roles = "Administrator")]
    public class HomeForAdminController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IOrganizationRepository _organizationRepository;
        IAddressRepository _addressRepository;
        IOrgAddressRepository _orgAddressRepository;
        IUserRepository _userRepository;
        ICountryRepository _countryRepository;
        IComProfileDataService _comProfileDataService;
        IBusinessEntityRepository _businessEntityRepository;
        IOrgSystemMappingRepository _orgSystemMappingRepository;
        ISystemConfigureAPIRepository _systemConfigureAPIRepository;
        ILanguageRepository _languageRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IAPILogsRepository _aPILogsRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        IAppConfigRepository _appConfigRepository;
        ILogonAttemptRepository _logonAttemptRepository;

        public HomeForAdminController()
        {
            _organizationRepository = new OrganizationRepository();
            _addressRepository = new AddressRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _userRepository = new UserRepository();
            _countryRepository = new CountryRepository();
            _comProfileDataService = new ComProfileDataService();
            _businessEntityRepository = new BusinessEntityRepository();
            _orgSystemMappingRepository = new OrgSystemMappingRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
            _languageRepository = new LanguageRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _aPILogsRepository = new APILogsRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _appConfigRepository = new AppConfigRepository();
            _logonAttemptRepository = new LogonAttemptRepository();
        }
        public HomeForAdminController
           (
               OrganizationRepository organizationRepository,
               AddressRepository addressRepository,
               OrgAddressRepository orgAddressRepository,
               UserRepository userRepository,
               ICountryRepository countryRepository,
               ComProfileDataService comProfileDataService,
               BusinessEntityRepository businessEntityRepository,
               OrgSystemMappingRepository orgSystemMappingRepository,
               SystemConfigureAPIRepository systemConfigureAPIRepository,
               LanguageRepository languageRepository,
               UserSystemMappingRepository userSystemMappingRepository,
               APILogsRepository aPILogsRepository,
               TrackingComprofileRepository trackingComprofileRepository,
               AppConfigRepository appConfigRepository,
               LogonAttemptRepository logonAttemptRepository
           )
        {
            _organizationRepository = organizationRepository;
            _addressRepository = addressRepository;
            _orgAddressRepository = orgAddressRepository;
            _userRepository = userRepository;
            _countryRepository = countryRepository;
            _comProfileDataService = comProfileDataService;
            _businessEntityRepository = businessEntityRepository;
            _orgSystemMappingRepository = orgSystemMappingRepository;
            _systemConfigureAPIRepository = systemConfigureAPIRepository;
            _languageRepository = languageRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _aPILogsRepository = aPILogsRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _appConfigRepository = appConfigRepository;
            _logonAttemptRepository = logonAttemptRepository;
        }
        // GET: HomeForAdmin
        public ActionResult Index()
        {
            IUserSessionRepository _repoUsersession;
            IUserRepository _repoUser;
            _repoUsersession = new UserSessionRepository();
            _repoUser = new UserRepository();

            if (Session["GUID"] == null)
            {
                //Response.Redirect("~/AccountPortal/Logout");
                return RedirectToAction("Index", "Home");
            }
            else
            {
                string guid = Session["GUID"].ToString();
                var listUser = _repoUsersession.GetUserSession(guid);
                if (listUser.isTimeout == 0)
                {
                    var ChkisAcceptTermOfUse = _repoUser.FindByUsername(listUser.Username);
                    string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];
                    if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0 && !Convert.ToBoolean(uesTermWeomni))
                    {
                        //Response.Redirect("~/Agreement/Index");
                        return RedirectToAction("Index", "Agreement");
                    }
                }
            }

            return View();
        }

        public ActionResult GeneralAddress()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            return View();
        }

        //PartialGridViewOrganization
        public ActionResult GridViewOrganization(DTParameters param)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            #region Find Organization ID

            // Get username from session
            var username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            // Find org id
            var ordId = _userRepository.FindOrgIDByUsername(username);

            #endregion

            #region Filter, Order by, Paging

            // Get all parameters
            var search = param.Search.Value;
            var sortColumn = param.Order[0].Column;
            var sortDirection = param.Order[0].Dir;

            // All rows
            var dataAddressForHomeAdmin = _addressRepository.GetListDataAddressForHomeAdmin(int.Parse(languageID));

            var filtered = dataAddressForHomeAdmin;
            if (!string.IsNullOrEmpty(search))
            {
                var temp = dataAddressForHomeAdmin.ToList().Where(x => x.SupplierID.ToString().Contains(search) ||
                                                   x.BuyerName.Contains(search) ||
                                                   x.Address.Contains(search) ||
                                                   x.Orgid.Contains(search)).ToList();

                filtered = temp;
            }
            // Sort order
            IQueryable<AddressHomeAdmin> sort = filtered.AsQueryable();

            switch (sortColumn)
            {
                //case 0:
                //    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderBy(x => x.Fullname) : filtered.OrderBy(x => x.Fullname);
                //    break;
                case 1:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.SupplierID) : filtered.OrderBy(x => x.SupplierID)).AsQueryable();
                    break;
                case 2:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.BuyerName) : filtered.OrderBy(x => x.BuyerName)).AsQueryable();
                    break;
                case 3:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Address) : filtered.OrderBy(x => x.Address)).AsQueryable();
                    break;
                case 4:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Orgid) : filtered.OrderBy(x => x.Orgid)).AsQueryable();
                    break;
            }

            // Paging
            var result = sort.Skip(param.Start).Take(param.Length);

            #endregion

            #region Return to Client JSON Format

            return Json(new
            {
                draw = param.Draw,
                recordsTotal = dataAddressForHomeAdmin.Count(),
                recordsFiltered = filtered.Count(),
                data = result,
                error = ""
            }, JsonRequestBehavior.AllowGet);

            #endregion
        }

        public ActionResult EditAddress(int supplierID = 0)
        {


            var model = new GeneralAddressHomeAdmin();
            int compAddressID = 0;
            int deliverAddressID = 0;
            model.SupplierID = supplierID;


            //General
            var modelGeneral = _comProfileDataService.GetGeneralComProfileBySupplierID(supplierID);
            model.trackingOrgFields = model.GetTrackingStatus<Tbl_Organization>(supplierID);
            if (modelGeneral != null)
            {
                model.OrgID = modelGeneral.OrgID;

                if (model.trackingOrgFields["CompanyTypeID"].TrackingStatusID == 2)
                    model.CompanyTypeID = int.Parse(model.trackingOrgFields["CompanyTypeID"].NewKeyValue);
                else model.CompanyTypeID = modelGeneral.CompanyTypeID ?? 0;
                if (model.trackingOrgFields["CountryCode"].TrackingStatusID == 2)
                {
                    model.CountryCode = model.trackingOrgFields["CountryCode"].NewKeyValue;
                    string countryName = _countryRepository.GetCountryNameByCountryCode(model.CountryCode);
                    model.CountryName = countryName;
                }
                else model.CountryCode = modelGeneral.CountryCode ?? "";
                if (model.trackingOrgFields["TaxID"].TrackingStatusID == 2)
                    model.TaxID = model.trackingOrgFields["TaxID"].NewKeyValue;
                else model.TaxID = modelGeneral.TaxID ?? "";
                if (model.trackingOrgFields["DUNSNumber"].TrackingStatusID == 2)
                    model.DUNSNumber = model.trackingOrgFields["DUNSNumber"].NewKeyValue;
                else model.DUNSNumber = modelGeneral.DUNSNumber ?? "";
                if (model.trackingOrgFields["BusinessEntityID"].TrackingStatusID == 2)
                    model.BusinessEntityID = int.Parse(model.trackingOrgFields["BusinessEntityID"].NewKeyValue);
                else model.BusinessEntityID = modelGeneral.BusinessEntityID ?? 0;
                if (model.BusinessEntityID == -1)
                {
                    var otherData = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID ?? 0, "BusinessEntityID", "Tbl_Organization");
                    if (otherData.Count() > 0)
                    {
                        foreach (var item in otherData)
                        {
                            model.OtherBusinessEntity = item.NewKeyValue.Split('|').LastOrDefault();
                        }
                    }
                    else model.OtherBusinessEntity = modelGeneral.OtherBusinessEntity ?? "";
                }
                else model.BusinessEntityID = modelGeneral.BusinessEntityID ?? 0;
                if (model.trackingOrgFields["CompanyName_Local"].TrackingStatusID == 2)
                    model.CompanyName_Local = model.trackingOrgFields["CompanyName_Local"].NewKeyValue;
                else model.CompanyName_Local = modelGeneral.CompanyName_Local ?? "";
                if (model.trackingOrgFields["CompanyName_Inter"].TrackingStatusID == 2)
                    model.CompanyName_Inter = model.trackingOrgFields["CompanyName_Inter"].NewKeyValue;
                else model.CompanyName_Inter = modelGeneral.CompanyName_Inter ?? "";
                if (model.trackingOrgFields["BranchNo"].TrackingStatusID == 2)
                    model.BranchNo = model.trackingOrgFields["BranchNo"].NewKeyValue;
                else model.BranchNo = modelGeneral.BranchNo ?? "";
                if (model.trackingOrgFields["BranchName_Local"].TrackingStatusID == 2)
                    model.BranchName_Local = model.trackingOrgFields["BranchName_Local"].NewKeyValue;
                else model.BranchName_Local = modelGeneral.BranchName_Local ?? "";
                if (model.trackingOrgFields["BranchName_Inter"].TrackingStatusID == 2)
                    model.BranchName_Inter = model.trackingOrgFields["BranchName_Inter"].NewKeyValue;
                else model.BranchName_Inter = modelGeneral.BranchName_Inter ?? "";
            }



            //Address
            var orgCompanyAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 1);
            var orgDeliverAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 2);
            if (orgCompanyAddressMapping != null)
            {
                compAddressID = orgCompanyAddressMapping.AddressID;
            }
            if (orgDeliverAddressMapping != null)
            {
                deliverAddressID = orgDeliverAddressMapping.AddressID;
            }
            model.trackingAddressFields = model.GetTrackingStatus<Tbl_Address>(compAddressID);
            var orgCompanyAddress = _addressRepository.GetAddressByAddressID(compAddressID);

            if (orgCompanyAddress != null)
            {
                model.CompanyAddressID = compAddressID;
                if (model.trackingAddressFields["HouseNo_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_HouseNo_Local = model.trackingAddressFields["HouseNo_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_HouseNo_Local = orgCompanyAddress.HouseNo_Local ?? "";

                if (model.trackingAddressFields["HouseNo_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_HouseNo_Inter = model.trackingAddressFields["HouseNo_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_HouseNo_Inter = orgCompanyAddress.HouseNo_Inter ?? "";

                if (model.trackingAddressFields["VillageNo_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_VillageNo_Local = model.trackingAddressFields["VillageNo_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_VillageNo_Local = orgCompanyAddress.VillageNo_Local ?? "";

                if (model.trackingAddressFields["VillageNo_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_VillageNo_Inter = model.trackingAddressFields["VillageNo_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_VillageNo_Inter = orgCompanyAddress.VillageNo_Inter ?? "";

                if (model.trackingAddressFields["Lane_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_Lane_Local = model.trackingAddressFields["Lane_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_Lane_Local = orgCompanyAddress.Lane_Local ?? "";

                if (model.trackingAddressFields["Lane_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_Lane_Inter = model.trackingAddressFields["Lane_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_Lane_Inter = orgCompanyAddress.Lane_Inter ?? "";

                if (model.trackingAddressFields["Road_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_Road_Local = model.trackingAddressFields["Road_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_Road_Local = orgCompanyAddress.Road_Local ?? "";

                if (model.trackingAddressFields["Road_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_Road_Inter = model.trackingAddressFields["Road_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_Road_Inter = orgCompanyAddress.Road_Inter ?? "";

                if (model.trackingAddressFields["SubDistrict_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_SubDistrict_Local = model.trackingAddressFields["SubDistrict_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_SubDistrict_Local = orgCompanyAddress.SubDistrict_Local ?? "";

                if (model.trackingAddressFields["SubDistrict_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_SubDistrict_Inter = model.trackingAddressFields["SubDistrict_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_SubDistrict_Inter = orgCompanyAddress.SubDistrict_Inter ?? "";

                if (model.trackingAddressFields["City_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_City_Local = model.trackingAddressFields["City_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_City_Local = orgCompanyAddress.City_Local ?? "";

                if (model.trackingAddressFields["City_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_City_Inter = model.trackingAddressFields["City_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_City_Inter = orgCompanyAddress.City_Inter ?? "";

                if (model.trackingAddressFields["State_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_State_Local = model.trackingAddressFields["State_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_State_Local = orgCompanyAddress.State_Local ?? "";

                if (model.trackingAddressFields["State_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_State_Inter = model.trackingAddressFields["State_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_State_Inter = orgCompanyAddress.State_Inter ?? "";

                if (model.trackingAddressFields["PostalCode"].TrackingStatusID == 2)
                    model.CompanyAddress_PostalCode = model.trackingAddressFields["PostalCode"].NewKeyValue ?? "";
                else model.CompanyAddress_PostalCode = orgCompanyAddress.PostalCode ?? "";

                if (model.trackingAddressFields["CountryCode"].TrackingStatusID == 2)
                    model.CompanyAddress_CountryCode = model.trackingAddressFields["CountryCode"].NewKeyValue ?? "";
                else model.CompanyAddress_CountryCode = orgCompanyAddress.CountryCode ?? "";
            }
            if (deliverAddressID != compAddressID)
            {
                //get data trackingstatus
                model.trackingDelFields = model.GetTrackingStatus<Tbl_Address>(deliverAddressID);
            }
            var orgDeliverAddress = _addressRepository.GetAddressByAddressID(deliverAddressID);

            if (model.trackingDelFields != null)
            {
                model.DeliverAddressID = deliverAddressID;

                if (model.trackingDelFields["HouseNo_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_HouseNo_Local = model.trackingDelFields["HouseNo_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_HouseNo_Local = orgDeliverAddress.HouseNo_Local ?? "";

                if (model.trackingDelFields["HouseNo_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_HouseNo_Inter = model.trackingDelFields["HouseNo_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_HouseNo_Inter = orgDeliverAddress.HouseNo_Inter ?? "";

                if (model.trackingDelFields["VillageNo_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_VillageNo_Local = model.trackingDelFields["VillageNo_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_VillageNo_Local = orgDeliverAddress.VillageNo_Local ?? "";

                if (model.trackingDelFields["VillageNo_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_VillageNo_Inter = model.trackingDelFields["VillageNo_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_VillageNo_Inter = orgDeliverAddress.VillageNo_Inter ?? "";

                if (model.trackingDelFields["Lane_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_Lane_Local = model.trackingDelFields["Lane_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_Lane_Local = orgDeliverAddress.Lane_Local ?? "";

                if (model.trackingDelFields["Lane_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_Lane_Inter = model.trackingDelFields["Lane_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_Lane_Inter = orgDeliverAddress.Lane_Inter ?? "";

                if (model.trackingDelFields["Road_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_Road_Local = model.trackingDelFields["Road_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_Road_Local = orgDeliverAddress.Road_Local ?? "";

                if (model.trackingDelFields["Road_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_Road_Inter = model.trackingDelFields["Road_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_Road_Inter = orgDeliverAddress.Road_Inter ?? "";

                if (model.trackingDelFields["SubDistrict_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_SubDistrict_Local = model.trackingDelFields["SubDistrict_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_SubDistrict_Local = orgDeliverAddress.SubDistrict_Local ?? "";

                if (model.trackingDelFields["SubDistrict_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_SubDistrict_Inter = model.trackingDelFields["SubDistrict_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_SubDistrict_Inter = orgDeliverAddress.SubDistrict_Inter ?? "";

                if (model.trackingDelFields["City_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_City_Local = model.trackingDelFields["City_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_City_Local = orgDeliverAddress.City_Local ?? "";

                if (model.trackingDelFields["City_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_City_Inter = model.trackingDelFields["City_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_City_Inter = orgDeliverAddress.City_Inter ?? "";

                if (model.trackingDelFields["State_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_State_Local = model.trackingDelFields["State_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_State_Local = orgDeliverAddress.State_Local ?? "";

                if (model.trackingDelFields["State_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_State_Inter = model.trackingDelFields["State_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_State_Inter = orgDeliverAddress.State_Inter ?? "";

                if (model.trackingDelFields["PostalCode"].TrackingStatusID == 2)
                    model.DeliverAddress_PostalCode = model.trackingDelFields["PostalCode"].NewKeyValue ?? "";
                else model.DeliverAddress_PostalCode = orgDeliverAddress.PostalCode ?? "";

                if (model.trackingDelFields["CountryCode"].TrackingStatusID == 2)
                    model.DeliverAddress_CountryCode = model.trackingDelFields["CountryCode"].NewKeyValue ?? "";
                else model.DeliverAddress_CountryCode = orgDeliverAddress.CountryCode ?? "";
            }
            else
            {
                //get data trackingstatus
                model.trackingDelFields = model.GetTrackingStatus<Tbl_Address>(0);
                // model.trackingFieldsDel = new Dictionary<string, TrackingCompProfile>();

                model.DeliverAddressID = deliverAddressID;
                model.DeliverAddress_HouseNo_Local = orgDeliverAddress.HouseNo_Local ?? "";
                model.DeliverAddress_HouseNo_Inter = orgDeliverAddress.HouseNo_Inter ?? "";
                model.DeliverAddress_VillageNo_Inter = orgDeliverAddress.VillageNo_Inter ?? "";
                model.DeliverAddress_Lane_Local = orgDeliverAddress.Lane_Local ?? "";
                model.DeliverAddress_Lane_Inter = orgDeliverAddress.Lane_Inter ?? "";
                model.DeliverAddress_Road_Local = orgDeliverAddress.Road_Local ?? "";
                model.DeliverAddress_Road_Inter = orgDeliverAddress.Road_Inter ?? "";
                model.DeliverAddress_SubDistrict_Local = orgDeliverAddress.SubDistrict_Local ?? "";
                model.DeliverAddress_SubDistrict_Inter = orgDeliverAddress.SubDistrict_Inter ?? "";
                model.DeliverAddress_City_Local = orgDeliverAddress.City_Local ?? "";
                model.DeliverAddress_City_Inter = orgDeliverAddress.City_Inter ?? "";
                model.DeliverAddress_State_Local = orgDeliverAddress.State_Local ?? "";
                model.DeliverAddress_State_Inter = orgDeliverAddress.State_Inter ?? "";
                model.DeliverAddress_PostalCode = orgDeliverAddress.PostalCode ?? "";
                model.DeliverAddress_CountryCode = orgDeliverAddress.CountryCode ?? "";


            }
            //var dMLanguageList = _languageRepository.GetDynamicLanguage();
            //model.DynamicLanguageList = dMLanguageList;
            if (compAddressID == deliverAddressID)
            {
                model.CheckB = true;
            }
            else
            {
                model.CheckB = false;
            }
            ViewBag.ContryCodeListCompany = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");
            ViewBag.ContryCodeListDelive = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");
            ViewBag.ContryCodeList = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");
            //,Dictionary<string,object> customAttr
            var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;
            int companyTypeID = 1;
            if (!string.IsNullOrEmpty(corperate))
            {
                companyTypeID = Convert.ToInt32(corperate);
            }
            else if (model.CompanyTypeID != null)
            {
                companyTypeID = model.CompanyTypeID ?? 1;
            }
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            int languageId = Convert.ToInt32(languageID);
            ViewBag.BusinessEntityList = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

            return View(model);
        }

        [HttpPost]
        public ActionResult EditAddress(GeneralAddressHomeAdmin model)
        {
            string systemUnSuccessCall = "";
            string username = "";
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            try
            {
                //General
                #region General
                var orgModel = _organizationRepository.GetDataBySupplierID(model.SupplierID ?? 0);

                if (orgModel != null)
                {
                    orgModel.CompanyTypeID = model.CompanyTypeID;
                    orgModel.CountryCode = model.CountryCode ?? "";
                    orgModel.TaxID = model.TaxID ?? "";
                    orgModel.DUNSNumber = model.DUNSNumber ?? "";
                    orgModel.BusinessEntityID = model.BusinessEntityID;
                    orgModel.OtherBusinessEntity = model.OtherBusinessEntity ?? "";
                    orgModel.CompanyName_Local = model.CompanyName_Local ?? "";
                    orgModel.CompanyName_Inter = model.CompanyName_Inter ?? "";
                    orgModel.BranchNo = model.BranchNo ?? "";
                    orgModel.BranchName_Local = model.BranchName_Local ?? "";
                    orgModel.BranchName_Inter = model.BranchName_Inter ?? "";

                    _organizationRepository.Update(orgModel, userID);
                    //Update Tbl_Organization.LastUpdate
                    _organizationRepository.UpdateLastUpdate(orgModel.SupplierID, userID);
                }
                #endregion

                //Address
                #region Address
                using (TransactionScope scope = new TransactionScope())
                {

                    #region Update Data
                    var OrgAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.CompanyAddressID ?? 0, 1);

                    if (OrgAdd != null)
                    {
                        var CompanyAdd = _addressRepository.GetAddressByAddressID(model.CompanyAddressID ?? 0);
                        if (CompanyAdd != null)
                        {
                            CompanyAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                            CompanyAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                            CompanyAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                            CompanyAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                            CompanyAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                            CompanyAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                            CompanyAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                            CompanyAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                            CompanyAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                            CompanyAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                            CompanyAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                            CompanyAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                            CompanyAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                            CompanyAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                            CompanyAdd.PostalCode = model.CompanyAddress_PostalCode ?? "";
                            CompanyAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";

                            //update companyAdd
                            _addressRepository.UpdateAddress(CompanyAdd, userID);
                        }

                    }
                    else
                    {
                        var comAdd = new Tbl_Address();

                        comAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                        comAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                        comAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                        comAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                        comAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                        comAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                        comAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                        comAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                        comAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                        comAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                        comAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                        comAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                        comAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                        comAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                        comAdd.PostalCode = model.CompanyAddress_PostalCode ?? "";
                        comAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";
                        //instert companyAdd
                        int comOrg = 0;
                        comOrg = _addressRepository.InsertAddress(comAdd);
                        model.CompanyAddressID = comOrg;

                        var orgAdd = new Tbl_OrgAddress()
                        {
                            SupplierID = model.SupplierID ?? 0,
                            AddressID = model.CompanyAddressID ?? 0,
                            AddressTypeID = 1,
                            SeqNo = 1
                        };
                        _orgAddressRepository.InsertAddress(orgAdd);
                    }

                    if (model.CheckB)
                    {
                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //update OrgAddress

                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                            }


                        }
                        else
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //update OrgAddress
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                            }
                            else
                            {
                                //insert OrgAddress
                                var orgAdd = new Tbl_OrgAddress()
                                {
                                    SupplierID = model.SupplierID ?? 0,
                                    AddressID = model.CompanyAddressID ?? 0,
                                    AddressTypeID = 2,
                                    SeqNo = 1
                                };
                                _orgAddressRepository.InsertAddress(orgAdd);
                            }

                        }
                    }
                    else
                    {
                        var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);

                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var deliverAddress = new Tbl_Address();

                            deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                            deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                            deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                            deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                            deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                            deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                            deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                            deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                            deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                            deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                            deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                            deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                            deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                            deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                            deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                            deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";


                            // insert Tbl_Address
                            int deAddID = 0;
                            deAddID = _addressRepository.InsertAddress(deliverAddress);

                            if (orgDeliverAdd == null)
                            {
                                var orgAdd = new Tbl_OrgAddress()
                                {
                                    SupplierID = model.SupplierID ?? 0,
                                    AddressID = deAddID,
                                    AddressTypeID = 2,
                                    SeqNo = 1
                                };
                                _orgAddressRepository.InsertAddress(orgAdd);
                            }
                            else
                            {
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);
                            }

                        }
                        else
                        {
                            var deliAdd = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);

                            if (deliAdd != null)
                            {
                                deliAdd.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                deliAdd.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                deliAdd.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                deliAdd.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                deliAdd.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                deliAdd.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                deliAdd.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                deliAdd.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                deliAdd.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                deliAdd.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                deliAdd.City_Local = model.DeliverAddress_City_Local ?? "";
                                deliAdd.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                deliAdd.State_Local = model.DeliverAddress_State_Local ?? "";
                                deliAdd.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                deliAdd.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                deliAdd.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                _addressRepository.UpdateAddress(deliAdd, userID);
                            }
                            else
                            {
                                var deliverAddress = new Tbl_Address();

                                deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";


                                // insert Tbl_Address
                                int deAddID = 0;
                                deAddID = _addressRepository.InsertAddress(deliverAddress);

                                if (orgDeliverAdd == null)
                                {
                                    var orgAdd = new Tbl_OrgAddress()
                                    {
                                        SupplierID = model.SupplierID ?? 0,
                                        AddressID = deAddID,
                                        AddressTypeID = 2,
                                        SeqNo = 1
                                    };
                                    _orgAddressRepository.InsertAddress(orgAdd);
                                }
                                else
                                {
                                    _orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);
                                }
                            }
                        }

                    }

                    #endregion


                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not  called and the transaction is rolled back.
                    scope.Complete();
                }
                #endregion

                #region Call API Data

                string guid = "mockGUID";

                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                {
                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                }

                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                var listSystem = _systemConfigureAPIRepository.GetSystemListBySupplierIDForUpdateAPIwhereUpdateSupplier(model.SupplierID ?? 0);

                foreach (var sysMapping in listSystem)
                {
                    string reqID = GeneratorGuid.GetRandomGuid();

                    string Jsonstr = "";

                    string _APIKey = "";

                    string _APIName = "";

                    var apiUpdateModels = GetDataCallUpdateSupplierAPI(sysMapping, reqID, model);
                    Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                    _APIKey = apiUpdateModels.Request.APIKey;
                    _APIName = apiUpdateModels.Request.APIName;

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(sysMapping.API_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpContent content = new StringContent(Jsonstr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    #region Insert API Log Before

                    var _APILogsBefore = new Tbl_APILogs()
                    {
                        ReqID = reqID,
                        SystemID = sysMapping.SystemID,
                        Events = "Request_Before",
                        Events_Time = DateTime.UtcNow,
                        IPAddress_Client = visitorsIPAddr,
                        APIKey = _APIKey,
                        APIName = _APIName,
                        Return_Code = "",
                        Return_Status = "",
                        Return_Message = "",
                        Data = Jsonstr,
                        //Data = responseBody,
                        UserGUID = guid,
                    };

                    _aPILogsRepository.Insert(_APILogsBefore);
                    #endregion
                    logger.Debug("Success Insert LogAPIBefore (SupplierID) " + model.SupplierID);
                    try
                    {
                        #region  API Post Task
                        string responseBody = "";
                        var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
                                                (postTask) =>
                                                {
                                                    bool isCompleted = postTask.IsCompleted;
                                                    bool isFaulted = postTask.IsFaulted;
                                                    if (isCompleted && !isFaulted)
                                                    {
                                                        var response = postTask.Result;
                                                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                                        {
                                                            try
                                                            {
                                                                if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                                {
                                                                    responseBody = response.Content.ReadAsStringAsync().Result;
                                                                    JObject _response = JObject.Parse(responseBody);

                                                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();

                                                                    #region Insert API Log

                                                                    var _APILogs = new Tbl_APILogs()
                                                                    {
                                                                        ReqID = reqID,
                                                                        SystemID = sysMapping.SystemID,
                                                                        Events = "Request",
                                                                        Events_Time = DateTime.UtcNow,
                                                                        IPAddress_Client = visitorsIPAddr,
                                                                        APIKey = _APIKey,
                                                                        APIName = _APIName,
                                                                        Return_Code = return_Code,
                                                                        Return_Status = return_Status,
                                                                        Return_Message = return_Message,
                                                                        Data = Jsonstr,
                                                                        //Data = responseBody,
                                                                        UserGUID = guid,
                                                                    };

                                                                    _aPILogsRepository.Insert(_APILogs);
                                                                    #endregion
                                                                    logger.Debug("Success Insert LogAPI (SupplierID) " + model.SupplierID);
                                                                    if (!Convert.ToBoolean(return_Status))
                                                                    {
                                                                        systemUnSuccessCall += sysMapping.SystemName + ",";
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                #region Insert API Log
                                                                string guidCall = "mockGUID";

                                                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                                                {
                                                                    guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                                                }

                                                                string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                                                var _APILogsCheck = new Tbl_APILogs()
                                                                {
                                                                    ReqID = reqID,
                                                                    SystemID = sysMapping.SystemID,
                                                                    Events = "Request_Check",
                                                                    Events_Time = DateTime.UtcNow,
                                                                    IPAddress_Client = visitorsIPAddrCall,
                                                                    APIKey = _APIKey,
                                                                    APIName = _APIName,
                                                                    Return_Code = "",
                                                                    Return_Status = response.StatusCode.ToString(),
                                                                    Return_Message = responseBody,
                                                                    Data = Jsonstr,
                                                                    //Data = responseBody,
                                                                    UserGUID = guidCall,
                                                                };

                                                                _aPILogsRepository.Insert(_APILogsCheck);


                                                                #endregion
                                                                logger.Error(ex);
                                                                throw;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            #region Insert API Log

                                                            var _APILogs = new Tbl_APILogs()
                                                            {
                                                                ReqID = reqID,
                                                                SystemID = sysMapping.SystemID,
                                                                Events = "Request",
                                                                Events_Time = DateTime.UtcNow,
                                                                IPAddress_Client = visitorsIPAddr,
                                                                APIKey = _APIKey,
                                                                APIName = _APIName,
                                                                Return_Code = "",
                                                                Return_Status = response.StatusCode.ToString(),
                                                                Return_Message = "",
                                                                Data = Jsonstr,
                                                                //Data = responseBody,
                                                                UserGUID = guid,
                                                            };

                                                            _aPILogsRepository.Insert(_APILogs);

                                                            systemUnSuccessCall += sysMapping.SystemName + ",";

                                                            #endregion
                                                            logger.Debug("Success Insert LogAPI (SupplierID) " + model.SupplierID);
                                                        }
                                                    }

                                                });
                        continuationTask.Wait(20000);
                        //continuationTask.Wait(
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        throw;
                    }



                }

                #endregion


                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(model.SupplierID ?? 0, userID);
                #endregion
            }
            catch (Exception)
            {

                throw;
            }


            return RedirectToAction("GeneralAddress");
        }

        public virtual API_UpdateSupplierModel GetDataCallUpdateSupplierAPI(SystemRoleListModels modelsData, string reqID, GeneralAddressHomeAdmin modelUpdate)
        {
            //var user = _userRepository.FindAllByUsername(modelsData.Username);

            string username = "";
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            int isInter = LanguageHelper.GetLanguageIsInter();

            var updateUserModels = new API_UpdateSupplierModel();

            if (user != null)
            {
                var requestData = new requestUpdate();

                requestData.ReqID = reqID;
                requestData.APIName = modelsData.API_Name;
                requestData.APIKey = modelsData.API_Key;

                requestData.Data = new dataUpdate();

                //SupplierInfo
                var supplierInfo = new supplierUpdate()
                {
                    SupplierShortName = modelUpdate.OrgID ?? ""
                };

                requestData.Data.SupplierInfo = supplierInfo;

                //OrgInfo
                var orgModel = _organizationRepository.GetDataBySupplierID(modelUpdate.SupplierID ?? 0);

                var orgInfo = new orgUpdate()
                {
                    OrgID = orgModel.OrgID ?? "",
                    TaxID = orgModel.TaxID ?? "",
                    OrgName_Local = orgModel.CompanyName_Local ?? "",
                    OrgName_Inter = orgModel.CompanyName_Inter ?? "",
                    BranchNo = orgModel.BranchNo ?? "",
                    CompanyType = orgModel.CompanyTypeID ?? 0,
                    BusinessEntityID = orgModel.BusinessEntityID ?? 0,
                    PhoneNo = orgModel.PhoneNo ?? "",
                    PhoneExt = orgModel.PhoneExt ?? "",
                    MobileNo = orgModel.MobileNo ?? "",
                    FaxNo = orgModel.FaxNo ?? "",
                    FaxExt = orgModel.FaxExt ?? ""
                };
                requestData.Data.OrgInfo = orgInfo;

                //CompanyAddress_Local  
                string CompanyAddress_countryName = "";
                if (modelUpdate.CompanyAddress_CountryCode != null)
                {
                    string languageId = "";
                    var countryAddress = _countryRepository.GetCountryByCountryCode(modelUpdate.CompanyAddress_CountryCode);

                    if (countryAddress != null)
                    {
                        if (isInter == 1)
                        {
                            var _tempLanguage = _languageRepository.GetInterLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        else
                        {
                            var _tempLanguage = _languageRepository.GetLocalLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        CompanyAddress_countryName = Framework.Localization.GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", countryAddress.CountryID.ToString());

                    }
                }
                var companyAddress_Local = new companyAddresslocalUpdate()
                {
                    HouseNo = modelUpdate.CompanyAddress_HouseNo_Local ?? "",
                    VillageNo = modelUpdate.CompanyAddress_VillageNo_Local ?? "",
                    Lane = modelUpdate.CompanyAddress_Lane_Local ?? "",
                    Road = modelUpdate.CompanyAddress_Road_Local ?? "",
                    SubDistrict = modelUpdate.CompanyAddress_SubDistrict_Local ?? "",
                    City = modelUpdate.CompanyAddress_City_Local ?? "",
                    State = modelUpdate.CompanyAddress_State_Local ?? "",
                    PostalCode = modelUpdate.CompanyAddress_PostalCode ?? "",
                    CountryCode = modelUpdate.CompanyAddress_CountryCode ?? "",
                    CountryName = CompanyAddress_countryName ?? ""
                };
                requestData.Data.CompanyAddress_Local = companyAddress_Local;

                //CompanyAddress_Inter
                var companyAddress_Inter = new companyAddressInterUpdate()
                {
                    HouseNo = modelUpdate.CompanyAddress_HouseNo_Inter ?? "",
                    VillageNo = modelUpdate.CompanyAddress_VillageNo_Inter ?? "",
                    Lane = modelUpdate.CompanyAddress_Lane_Inter ?? "",
                    Road = modelUpdate.CompanyAddress_Road_Inter ?? "",
                    SubDistrict = modelUpdate.CompanyAddress_SubDistrict_Inter ?? "",
                    City = modelUpdate.CompanyAddress_City_Inter ?? "",
                    State = modelUpdate.CompanyAddress_State_Inter ?? "",
                    PostalCode = modelUpdate.CompanyAddress_PostalCode ?? "",
                    CountryCode = modelUpdate.CompanyAddress_CountryCode ?? "",
                    CountryName = CompanyAddress_countryName ?? ""
                };
                requestData.Data.CompanyAddress_Inter = companyAddress_Inter;

                //DeliveredAddress_Local
                string DeliverAddress_countryName = "";
                if (modelUpdate.CompanyAddress_CountryCode != null)
                {
                    string languageId = "";
                    var countryAddress = _countryRepository.GetCountryByCountryCode(modelUpdate.CompanyAddress_CountryCode);

                    if (countryAddress != null)
                    {
                        if (isInter == 1)
                        {
                            var _tempLanguage = _languageRepository.GetInterLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        else
                        {
                            var _tempLanguage = _languageRepository.GetLocalLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        CompanyAddress_countryName = Framework.Localization.GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", countryAddress.CountryID.ToString());

                    }
                }
                var deliveredAddress_Local = new deliveredAddreslocalUpdate()
                {
                    HouseNo = modelUpdate.DeliverAddress_HouseNo_Local ?? "",
                    VillageNo = modelUpdate.DeliverAddress_VillageNo_Local ?? "",
                    Lane = modelUpdate.DeliverAddress_Lane_Local ?? "",
                    Road = modelUpdate.DeliverAddress_Road_Local ?? "",
                    SubDistrict = modelUpdate.DeliverAddress_SubDistrict_Local ?? "",
                    City = modelUpdate.DeliverAddress_City_Local ?? "",
                    State = modelUpdate.DeliverAddress_State_Local ?? "",
                    PostalCode = modelUpdate.DeliverAddress_PostalCode ?? "",
                    CountryCode = modelUpdate.DeliverAddress_CountryCode ?? "",
                    CountryName = DeliverAddress_countryName ?? ""
                };
                requestData.Data.DeliveredAddress_Local = deliveredAddress_Local;

                //DeliveredAddress_Inter
                var deliveredAddress_Inter = new deliveredAddresInterUpdate()
                {
                    HouseNo = modelUpdate.DeliverAddress_HouseNo_Inter ?? "",
                    VillageNo = modelUpdate.DeliverAddress_VillageNo_Inter ?? "",
                    Lane = modelUpdate.DeliverAddress_Lane_Inter ?? "",
                    Road = modelUpdate.DeliverAddress_Road_Inter ?? "",
                    SubDistrict = modelUpdate.DeliverAddress_SubDistrict_Inter ?? "",
                    City = modelUpdate.DeliverAddress_City_Inter ?? "",
                    State = modelUpdate.DeliverAddress_State_Inter ?? "",
                    PostalCode = modelUpdate.DeliverAddress_PostalCode ?? "",
                    CountryCode = modelUpdate.DeliverAddress_CountryCode ?? "",
                    CountryName = DeliverAddress_countryName ?? ""
                };
                requestData.Data.DeliveredAddress_Inter = deliveredAddress_Inter;

                //EditBy

                var userMappingTbl = _userSystemMappingRepository.GetUserSystemMapping(username);


                var listSysUserID = new List<sysUserID>();

                if (userMappingTbl.Count() > 0)
                {

                    foreach (var valueSys in userMappingTbl)
                    {
                        var sysUserID = new sysUserID();

                        sysUserID.value = valueSys.SysUserID;

                        listSysUserID.Add(sysUserID);
                    }
                }
                else
                {
                    var sysUserID = new sysUserID()
                    {
                        value = ""
                    };

                    listSysUserID.Add(sysUserID);
                }

                addBy editBy = new addBy
                {
                    SysUserID = listSysUserID
                };

                requestData.Data.EditBy = editBy;

                updateUserModels.Request = requestData;

            }

            return updateUserModels;
        }

        public ActionResult UserUnlockAccount()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            return View();
        }

        //PartialGridViewUserUnlockAccount
        public ActionResult GridViewUserUnlockAccount(DTParameters param)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            #region Find Organization ID

            // Get username from session
            var username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            // Find org id
            var ordId = _userRepository.FindOrgIDByUsername(username);

            #endregion

            #region Filter, Order by, Paging

            // Get all parameters
            var search = param.Search.Value;
            var sortColumn = param.Order[0].Column;
            var sortDirection = param.Order[0].Dir;



            #region Declare Config Logon
            //string portal_LogonAttemptAllow = _appConfigRepository.GetValueAppConfigByName("Portal_LogonAttemptAllow");
            //string portal_LogonPeriodCheck = _appConfigRepository.GetValueAppConfigByName("Portal_LogonPeriodCheck");
            string portal_LogonLockTime = _appConfigRepository.GetValueAppConfigByName("Portal_LogonLockTime");

            //int logonAttemptAllow = 0;
            //int logonPeriodCheck = 0;
            int logonLockTime = 0;

            //Int32.TryParse(portal_LogonAttemptAllow, out logonAttemptAllow);
            //Int32.TryParse(portal_LogonPeriodCheck, out logonPeriodCheck);
            Int32.TryParse(portal_LogonLockTime, out logonLockTime);
            #endregion

            // All rows
            var dataUserUnlockAccount = _logonAttemptRepository.GetAllUserLock(int.Parse(languageID), logonLockTime);

            var filtered = dataUserUnlockAccount;
            if (!string.IsNullOrEmpty(search))
            {
                var temp = dataUserUnlockAccount.ToList().Where(x => x.ID.ToString().Contains(search) ||
                                                   x.Username.Contains(search) ||
                                                   x.Fullname.Contains(search) ||
                                                   x.CompanyName.Contains(search)).ToList();

                filtered = temp;
            }
            // Sort order
            IQueryable<UserUnlockAccountModel> sort = filtered.AsQueryable();

            switch (sortColumn)
            {
                //case 0:
                //    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderBy(x => x.Fullname) : filtered.OrderBy(x => x.Fullname);
                //    break;
                case 1:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.ID) : filtered.OrderBy(x => x.ID)).AsQueryable();
                    break;
                case 2:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Username) : filtered.OrderBy(x => x.Username)).AsQueryable();
                    break;
                case 3:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Fullname) : filtered.OrderBy(x => x.Fullname)).AsQueryable();
                    break;
                case 4:
                    sort = ((sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.CompanyName) : filtered.OrderBy(x => x.CompanyName)).AsQueryable();
                    break;
            }

            // Paging
            var result = sort.Skip(param.Start).Take(param.Length);

            #endregion

            #region Return to Client JSON Format

            return Json(new
            {
                draw = param.Draw,
                recordsTotal = dataUserUnlockAccount.Count(),
                recordsFiltered = filtered.Count(),
                data = result,
                error = ""
            }, JsonRequestBehavior.AllowGet);

            #endregion
        }


        public ActionResult UnlockAccount(int ID)
        {
            try
            {
                if (ID != null)
                {
                    string username = "";
                    int userID = 0;
                    if (Session["username"] != null)
                    {
                        username = Session["username"].ToString();
                    }
                    var user = _userRepository.FindByUsername(username);
                    if (user != null)
                    {
                        userID = user.UserID;
                    }
                    var userName = _userRepository.GetUsernameByUserID(userID);
                    _logonAttemptRepository.UnlockAccount(ID, userName);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return RedirectToAction("UserUnlockAccount");
        }

    }
}