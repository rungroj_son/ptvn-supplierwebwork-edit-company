﻿using MarkdownSharp;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class HomeController : ControllerBase
    {
        //ControllerBase
        public ActionResult Index()
        {
            IUserSessionRepository _repoUsersession;
            IUserRepository _repoUser;
            _repoUsersession = new UserSessionRepository();
            _repoUser = new UserRepository();

            if (Session["GUID"] == null)
            {
                //Response.Redirect("~/AccountPortal/Logout");
                return RedirectToAction("Index", "AccountPortal");
            }
            else
            {
                string guid = Session["GUID"].ToString();
                HttpCookie cookie = Request.Cookies["_culture"];
                string languageId = cookie != null ? cookie.Value : "1";

                ViewBag.IsCheckConsent = Session["isCheckConsent"];
                ViewBag.IsAcceptBillingCondition = Session["isAcceptBillingCondition"];
                ViewBag.SCFActionFrom = Session["SCFActionFrom"] != null ? Session["SCFActionFrom"].ToString() : "Webwork";
                ViewBag.IsAcceptCodeOfConduct = Session["IsAcceptCodeOfConduct"];
                //ViewBag.IsAcceptTermCondition = Session["IsAcceptTermCondition"];
                ViewBag.LanguageId = languageId;

                if (Session["RedirectUriMarketPlace"] != null)
                {
                    ViewBag.RedirectUriMarketPlace = Session["RedirectUriMarketPlace"].ToString();
                    Session.Remove("RedirectUriMarketPlace");
                }

                #region True Consent
                string consentDisplay = string.Empty;
                if (Session["IsAcceptCodeOfConduct"] != null && !Convert.ToBoolean(Session["IsAcceptCodeOfConduct"]))
                {
                    consentDisplay = Session["CodeOfConductPrivacyNoticeEN"].ToString();
                    if (languageId.Equals("2"))
                    {
                        consentDisplay = Session["CodeOfConductPrivacyNoticeTH"].ToString();
                    }
                }
                ViewBag.CodeOfConductPrivacyNotice = consentDisplay;
                #endregion True Consent

                //#region Term and Condition
                //if (Session["IsAcceptTermCondition"] != null && !Convert.ToBoolean(Session["IsAcceptTermCondition"]))
                //{
                //    Markdown markdown = new Markdown();
                //    markdown.Transform(Session["TermAndConditionEN"].ToString());

                //    ViewBag.TermAndConditionEN = markdown.Transform(Session["TermAndConditionEN"].ToString());
                //    ViewBag.TermAndConditionTH = markdown.Transform(Session["TermAndConditionTH"].ToString()); 

                //    ViewBag.PrivacyNoticeEN = markdown.Transform(Session["PrivacyNoticeEN"].ToString());
                //    ViewBag.PrivacyNoticeTH = markdown.Transform(Session["PrivacyNoticeTH"].ToString());
                //}
                //#endregion Term and Condition

                var listUser = _repoUsersession.GetUserSession(guid);
                string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];

                if (listUser.isTimeout == 0 && !Convert.ToBoolean(uesTermWeomni))
                {
                    var ChkisAcceptTermOfUse = _repoUser.FindByUsername(listUser.Username);
                    if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0)
                    {
                        //Response.Redirect("~/Agreement/Index");
                        return RedirectToAction("Index", "Agreement");
                    }
                }
            }

            return View();
        }


        public ActionResult Billing()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Manual()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Notification()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}