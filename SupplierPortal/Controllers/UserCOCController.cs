﻿using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.ServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class UserCOCController : BaseController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        IUserSessionRepository _userSessionRepository;
        IUserRepository _userRepository;
        TermAndConditionService _termAndConditionService;
        public UserCOCController()
        {
            _userSessionRepository = new UserSessionRepository();
            _userRepository = new UserRepository();
            _termAndConditionService = new TermAndConditionService();
        }

        // GET: UserCOC
        [HttpPost]
        public async Task<ActionResult> Accept()
        {
            bool isSuccess = false;
            HttpClient clientCoc = new HttpClient();

            string baseURL = WebConfigurationManager.AppSettings["baseURL"];

            string userGuid = Session["GUID"].ToString();
            var userSession = _userSessionRepository.GetUserSession(userGuid);
            var user = _userRepository.FindAllByUsername(userSession.Username);

            string urlClientCoc = string.Format("{0}api/coc/acknowledge/{1}", baseURL, userSession.Username);
            HttpContent content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("consentId", Session["CodeOfConductConsentId"].ToString()),
                new KeyValuePair<string, string>("dataSubject", userSession.Username),
                new KeyValuePair<string, string>("userAgent", Request.UserAgent),
                new KeyValuePair<string, string>("country", user.Locale),
                new KeyValuePair<string, string>("localIp", userSession.IPAddress),
                new KeyValuePair<string, string>("model", DetectDeviceHelper.GetDeviceType(Request.UserAgent)),
                new KeyValuePair<string, string>("osVersion", Request.Browser.Platform)
            });

            HttpResponseMessage responseCoc = clientCoc.PostAsync(urlClientCoc, content).Result;
            if (responseCoc.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Session["IsAcceptCodeOfConduct"] = true;
                isSuccess = true;
            }

            return Json(isSuccess);
        }

        [HttpPost]
        public async Task<ActionResult> AcceptTermAndCondition()
        {
            bool isSuccess = false;
            HttpClient client = new HttpClient();

            string baseURL = WebConfigurationManager.AppSettings["baseURL"];

            string userGuid = Session["GUID"].ToString();
            var userSession = _userSessionRepository.GetUserSession(userGuid);
            var user = _userRepository.FindAllByUsername(userSession.Username);

            string urlClient = string.Format("{0}api/term/acknowledge/{1}", baseURL, userSession.Username);
            HttpContent content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("consentId", Session["TermAndCondtionConsentId"].ToString()),
                new KeyValuePair<string, string>("dataSubject", userSession.Username),
                new KeyValuePair<string, string>("userAgent", Request.UserAgent),
                new KeyValuePair<string, string>("country", user.Locale),
                new KeyValuePair<string, string>("localIp", userSession.IPAddress),
                new KeyValuePair<string, string>("model", DetectDeviceHelper.GetDeviceType(Request.UserAgent)),
                new KeyValuePair<string, string>("osVersion", Request.Browser.Platform)
            });

            HttpResponseMessage responseCoc = client.PostAsync(urlClient, content).Result;
            if (responseCoc.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Session["IsAcceptTermCondition"] = true;
                isSuccess = true;
            }

            return Json(isSuccess);
        }
    }
}