﻿using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.Repository.JobTitle;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.SalePurGroup;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Services.CompanyProfileManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping;
using SupplierPortal.Data.Models.Repository.LogSalePurGroup;
using SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict;
using SupplierPortal.Data.Models.Repository.AutoComplete_City;

namespace SupplierPortal.Controllers
{
    public class CompanyContactController : ControllerBase
    {
        IOrganizationRepository _organizationRepository;
        INameTitleRepository _nameTitleRepository;
        ILanguageRepository _languageRepository;
        IJobTitleRepository _jobTitleRepository;
        IAutoComplete_SubDistrictRepository _autoComplete_subDistrictRepository;
        IAutoComplete_CityRepository _autoComplete_cityRepository;
        IAutoComplete_StateRepository _autoComplete_StateRepository;
        IFunctionRepository _functionRepository;
        IAppConfigRepository _appConfigRepository;
        IContactPersonRepository _contactPersonRepository;
        ICountryRepository _countryRepository;
        IOrgContactPerson _orgContactPerson;
        IUserRepository _userRepository;
        IComProfileDataService _comProfileDataService;
        ISaleInfoRepository _saleInfoRepository;
        IACL_OrgMenuMappingRepository _ACL_OrgMenuMappingRepository;
        ILogSalePurGroupRepository _logSalePurGroupRepository;



        public CompanyContactController()
        {
            _organizationRepository = new OrganizationRepository();
            _nameTitleRepository = new NameTitleRepository();
            _languageRepository = new LanguageRepository();
            _jobTitleRepository = new JobTitleRepository();
            _autoComplete_subDistrictRepository = new AutoComplete_SubDistrictRepository();
            _autoComplete_cityRepository = new AutoComplete_CityRepository();
            _autoComplete_StateRepository = new AutoComplete_StateRepository();
            _functionRepository = new FunctionRepository();
            _appConfigRepository = new AppConfigRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _countryRepository = new CountryRepository();
            _orgContactPerson = new OrgContactPerson();
            _userRepository = new UserRepository();
            _comProfileDataService = new ComProfileDataService();
            _saleInfoRepository = new SaleInfoRepository();
            _ACL_OrgMenuMappingRepository = new ACL_OrgMenuMappingRepository();
            _logSalePurGroupRepository = new LogSalePurGroupRepository();
        }

        public CompanyContactController
            (
            OrganizationRepository organizationRepository,
            NameTitleRepository nameTitleRepository,
            LanguageRepository languageRepository,
            JobTitleRepository jobTitleRepository,
            AutoComplete_SubDistrictRepository autoComplete_subDistrictRepository,
            AutoComplete_CityRepository autoComplete_cityRepository,
            AutoComplete_StateRepository autoComplete_StateRepository,
            FunctionRepository functionRepository,
            AppConfigRepository appConfigRepository,
            ContactPersonRepository contactPersonRepository,
            CountryRepository countryRepository,
            OrgContactPerson orgContactPerson,
            UserRepository userRepository,
            ComProfileDataService comProfileDataService,
            SaleInfoRepository saleInfoRepository,
            ACL_OrgMenuMappingRepository ACL_OrgMenuMappingRepository,
            LogSalePurGroupRepository logSalePurGroupRepository
            )
        {
            _organizationRepository = organizationRepository;
            _nameTitleRepository = nameTitleRepository;
            _languageRepository = languageRepository;
            _jobTitleRepository = jobTitleRepository;
            _autoComplete_subDistrictRepository = autoComplete_subDistrictRepository;
            _autoComplete_cityRepository = autoComplete_cityRepository;
            _autoComplete_StateRepository = autoComplete_StateRepository;
            _functionRepository = functionRepository;
            _appConfigRepository = appConfigRepository;
            _contactPersonRepository = contactPersonRepository;
            _countryRepository = countryRepository;
            _orgContactPerson = orgContactPerson;
            _userRepository = userRepository;
            _comProfileDataService = comProfileDataService;
            _saleInfoRepository = saleInfoRepository;
            _ACL_OrgMenuMappingRepository = ACL_OrgMenuMappingRepository;
            _logSalePurGroupRepository = logSalePurGroupRepository;
        }
        // GET: CompanyContact
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContactPerson()
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            var languageLocal = _languageRepository.GetLocalLanguage();

            var languageInter = _languageRepository.GetInterLanguage();

            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);

            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);

            ViewBag.JobTitle = _jobTitleRepository.GetJobTitleList(languageId);

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);

            var model = new CompanyContactPersonModel();
            //Test Merge
            model.Proc = "Add";
            model.SupplierID = supplierID;
            model.Address_CountryCode = orgModel.CountryCode;
            model.TitleID = 4;
            model.TitleID_Inter = 4;
            model.Address_BuyerList = "";
            model.isAllowAccess = _ACL_OrgMenuMappingRepository.CheckAllowAccessBySupplierIDAndMenuID(model.SupplierID, 25);

            return View(model);
        }


        public ActionResult ContactPersonTable(int supplierID)
        {
            var model = _orgContactPerson.GetOrgContactPersonTableBySupplierID(supplierID);

            return PartialView(@"~/Views/CompanyContact/_ContactPersonTablePartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult ContactPersonSave(CompanyContactPersonModel model)
        {
            try
            {
                string username = "Metro-Admin";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                var orgModel = _organizationRepository.GetDataBySupplierID(model.SupplierID);

                if (model.Proc == "Add")
                {
                    _comProfileDataService.InsertCompanyContactPerson(model, userID);
                }
                else
                {
                    _comProfileDataService.EditCompanyContactPerson(model, userID);
                }

                bool Bool = false;

                Bool = _ACL_OrgMenuMappingRepository.CheckAllowAccessBySupplierIDAndMenuID(model.SupplierID, 25);

                if (Bool && model.JobTitleID == 1)
                {
                    #region product
                    var dataProduct = _saleInfoRepository.GetProduct(model.ContactID);
                    if (dataProduct != null)
                    {
                        _saleInfoRepository.updateProductList(model.ContactID, model.Address_Product, model.SupplierID);
                    }
                    else
                    {
                        _saleInfoRepository.insertProductList(model.ContactID, model.Address_Product, model.SupplierID);
                    }
                    #endregion

                    #region Purgroup
                    var dataBuyer = _saleInfoRepository.GetPurgroup(model.ContactID);
                    _saleInfoRepository.removeBuyerList(dataBuyer);

                    var data = model.Address_BuyerList.Split(',').Select(s => Convert.ToInt32(s));
                    if (model.Address_BuyerList != "")
                    {
                        foreach (var item in data)
                        {
                            _saleInfoRepository.insertBuyerList(model.ContactID, item, model.SupplierID);
                        }
                    }

                    #endregion

                }

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(model.SupplierID, userID);
                #endregion

            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("ContactPerson");
        }

        [HttpPost]
        public ActionResult ContactPersonDelete(int hidContactIDDelete, int hidSupplierIDDelete)
        {
            try
            {
                string username = "Metro-Admin";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                _comProfileDataService.DeleteCompanyContactPerson(hidSupplierIDDelete, hidContactIDDelete, userID);

                var dataProduct = _saleInfoRepository.GetProduct(hidContactIDDelete);
                if (dataProduct != null)
                {
                    _logSalePurGroupRepository.insertLogProductList(dataProduct.ContactID, hidSupplierIDDelete, "Remove");
                    _saleInfoRepository.removeProductList(dataProduct);
                }

                var dataBuyer = _saleInfoRepository.GetPurgroup(hidContactIDDelete);
                foreach (var item in dataBuyer)
                {
                    _logSalePurGroupRepository.insertLogBuyerList(item.ContactID ?? 0, item.PurGroupId ?? 0, hidSupplierIDDelete, "Remove");
                }
                _saleInfoRepository.removeBuyerList(dataBuyer);


                #region Update Tbl_Organization.LastUpdate
                int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
                _organizationRepository.UpdateLastUpdate(supplierID, userID);
                #endregion
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }
            return RedirectToAction("ContactPerson");
        }

        public PartialViewResult EditContactPerson(int contactID)
        {
            string username = "Metro-Admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            var languageLocal = _languageRepository.GetLocalLanguage();

            var languageInter = _languageRepository.GetInterLanguage();


            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);

            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);

            ViewBag.JobTitle = _jobTitleRepository.GetJobTitleList(languageId);

            var model = _comProfileDataService.GetCompanyContactPersonByContactID(contactID);

            model.Proc = "Edit";
            model.SupplierID = supplierID;
            model.ContactID = contactID;
            //model.Address_CountryCode = orgModel.CountryCode;
            var dataProduct = _saleInfoRepository.GetProduct(model.ContactID);
            if (dataProduct != null)
            {
                model.Address_Product = dataProduct.ProductList;
            }

            var dataBuyer = _saleInfoRepository.GetPurgroup(model.ContactID);

            if (dataBuyer != null)
            {
                var data = string.Empty;
                int i = 0;
                foreach (var item in dataBuyer)
                {
                    if (i > 0)
                    {
                        data += "," + item.PurGroupId;
                    }
                    else
                    {
                        data += item.PurGroupId;
                    }
                    i++;
                }
                model.Address_BuyerList = data;
            }
            else
            {
                model.Address_BuyerList = "";
            }

            model.isAllowAccess = _ACL_OrgMenuMappingRepository.CheckAllowAccessBySupplierIDAndMenuID(model.SupplierID, 25);
            return PartialView(@"~/Views/CompanyContact/_ContactFormPartial.cshtml", model);
        }

        public PartialViewResult ResetContactPerson()
        {
            string username = "Metro-Admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            var languageLocal = _languageRepository.GetLocalLanguage();

            var languageInter = _languageRepository.GetInterLanguage();

            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);

            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);

            ViewBag.JobTitle = _jobTitleRepository.GetJobTitleList(languageId);

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);

            var model = new CompanyContactPersonModel();

            model.Proc = "Add";
            model.SupplierID = supplierID;
            model.Address_CountryCode = orgModel.CountryCode;
            model.TitleID = 4;
            model.TitleID_Inter = 4;
            model.isAllowAccess = _ACL_OrgMenuMappingRepository.CheckAllowAccessBySupplierIDAndMenuID(model.SupplierID, 25);
            return PartialView(@"~/Views/CompanyContact/_ContactFormPartial.cshtml", model);
        }


        public ActionResult SubLocalPartial(string term)
        {
            var SubCompLocals = _autoComplete_subDistrictRepository.GetSubDistrictList(term);
            return Json(SubCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityLocalPartial(string term)
        {
            var CityCompLocals = _autoComplete_cityRepository.GetCityList(term);
            return Json(CityCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateLocalPartial(string term)
        {
            var StateCompLocals = _autoComplete_StateRepository.GetStateList(term);
            return Json(StateCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Address_Country(CompanyContactPersonModel model)
        {
            ViewBag.ContryCodeList = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");
            return PartialView(@"~/Views/CompanyContact/_Address_Country.cshtml", model);
        }

        public ActionResult Address_BuyerList(CompanyContactPersonModel model)
        {
            ViewData["BuyerList"] = _saleInfoRepository.GetBuyerList();
            return PartialView(@"~/Views/CompanyContact/_Address_BuyerList.cshtml", model);
        }

        public ActionResult Address_Product(CompanyContactPersonModel model)
        {
            //ViewData["BuyerList"] = _saleInfoRepository.GetBuyerList();
            return PartialView(@"~/Views/CompanyContact/_Address_Product.cshtml", model);
        }

        public ActionResult PostalCodeValidateCompany(string Address_PostalCode, string Address_CountryCode_VI)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            if (string.IsNullOrEmpty(Address_PostalCode))
            {
                Address_PostalCode = "";
            }
            if (string.IsNullOrEmpty(Address_CountryCode_VI))
            {
                Address_CountryCode_VI = "";
            }

            var sqlQuery = "select dbo.VerifyPostalCode('" + Address_PostalCode.ToString() + "','" + Address_CountryCode_VI.ToString() + "')";

            var result = _functionRepository.GetResultByQuery(sqlQuery);
            if (result)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatPostalCode", languageID);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IsEmailAvailableConfig(string email, string initialEmail)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var IsAllowDuplicateEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowDuplicateUserEmail").Value;

            var IsAllowMultipleEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowMultipleUserEmail").Value;

            string messageReturn = "";

            bool IsValid = false;
            if (IsAllowDuplicateEmail != "1")
            {
                bool chk = _contactPersonRepository.EmailExists(email.Trim(), initialEmail.Trim());

                if (!chk)
                {
                    IsValid = true;

                }
                else
                {

                    string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.DuplicateEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsAllowMultipleEmail == "1")
            {
                string emailSeparator = _appConfigRepository.GetAppConfigByName("Portal_EmailSeparator").Value;


                foreach (string value in email.Split(Char.Parse(emailSeparator)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        IsValid = false;
                        string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);

                    }
                    string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                    //if (IsValidEmailAddress(value))
                    if (IsValidEmailAddressRegular(value, pattern))
                    {
                        IsValid = true;
                    }
                    else
                    {

                        string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);

                    }
                }

            }
            else
            {
                string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                //if (IsValidEmailAddress(value))
                if (IsValidEmailAddressRegular(email, pattern))
                {
                    IsValid = true;
                }
                else
                {

                    string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);

                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }


        #region------------------------------------------------Function Helper------------------------------------------------------
        private bool IsValidEmailAddressRegular(string emailAddress, string paatten)
        {
            return new System.ComponentModel.DataAnnotations
                .RegularExpressionAttribute(paatten)
                .IsValid(emailAddress);
        }
        #endregion


    }
}
