﻿using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.CertifiedStd;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.OrgCertifiedStd;
using SupplierPortal.Data.Models.Repository.OrgCertifiedStdAttachment;
using System.IO;
using SupplierPortal.Services.MainService;
using SupplierPortal.Framework.MethodHelper;
using System.Web.Routing;
using SupplierPortal.Data.Helper;
using SupplierPortal.Services.CompanyProfileManage;
using System.Web.Configuration;
using SupplierPortal.Framework.Localization;

namespace SupplierPortal.Controllers
{
    public class CompanyAdditionalController : ControllerBase
    {
        ICertifiedStdRepository _certifiedStdRepository;
        IOrganizationRepository _organizationRepository;
        IAppConfigRepository _appConfigRepository;
        IUserRepository _userRepository;
        IOrgCertifiedStdRepository _orgCertifiedStdRepository;
        IOrgCertifiedStdAttachmentRepository _orgCertifiedStdAttachmentRepository;
        IComProfileDataService _comProfileDataService;

        public CompanyAdditionalController()
        {
            _certifiedStdRepository = new CertifiedStdRepository();
            _organizationRepository = new OrganizationRepository();
            _appConfigRepository = new AppConfigRepository();
            _userRepository = new UserRepository();
            _orgCertifiedStdRepository = new OrgCertifiedStdRepository();
            _orgCertifiedStdAttachmentRepository = new OrgCertifiedStdAttachmentRepository();
            _comProfileDataService = new ComProfileDataService();
        }

        public CompanyAdditionalController
            (
            CertifiedStdRepository certifiedStdRepository,
            OrganizationRepository organizationRepository,
            AppConfigRepository appConfigRepository,
            UserRepository userRepository,
            OrgCertifiedStdRepository orgCertifiedStdRepository,
            OrgCertifiedStdAttachmentRepository orgCertifiedStdAttachmentRepository,
            ComProfileDataService comProfileDataService
            )
        {
            _certifiedStdRepository = certifiedStdRepository;
            _organizationRepository = organizationRepository;
            _appConfigRepository = appConfigRepository;
            _userRepository = userRepository;
            _orgCertifiedStdRepository = orgCertifiedStdRepository;
            _orgCertifiedStdAttachmentRepository = orgCertifiedStdAttachmentRepository;
            _comProfileDataService = comProfileDataService;
        }

        public ActionResult Certification()
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = new CompanyCertificationModel();
            model.Proc = "Add";
            model.SupplierID = supplierID;

            var listOrgCertifiedStdAttachment = new List<Tbl_OrgCertifiedStdAttachment>();

            model.OrgCertifiedStdAttachment = listOrgCertifiedStdAttachment;

            return View(model);
        }

        public ActionResult OrgCertifiedStdTable(int supplierID)
        {
            var model = _comProfileDataService.GetOrgCertifiedStdTableBySupplierID(supplierID);

            return PartialView(@"~/Views/CompanyAdditional/_OrgCertifiedStdTablePartial.cshtml", model);
        }

        public PartialViewResult EditCertification(int certId)
        {
            var model = _comProfileDataService.GetCompanyCertificationByCertId(certId);

            model.Proc = "Edit";

            return PartialView(@"~/Views/CompanyAdditional/_CertificateFormPartial.cshtml", model);
        }

        public PartialViewResult ResetCertification()
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = new CompanyCertificationModel();
            model.Proc = "Add";
            model.SupplierID = supplierID;

            var listOrgCertifiedStdAttachment = new List<Tbl_OrgCertifiedStdAttachment>();

            model.OrgCertifiedStdAttachment = listOrgCertifiedStdAttachment;

            return PartialView(@"~/Views/CompanyAdditional/_CertificateFormPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult CertificationSave(CompanyCertificationModel model)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                if (model.Proc == "Add")
                {

                    #region-----------------------Manage OrgCertifiedStd------------------------

                    var orgCertifiedStd = _orgCertifiedStdRepository.GetOrgCertifiedStdBySupplierID(model.SupplierID);

                    int orgCertifiedStdCount = orgCertifiedStd.Count();

                    int seqNo = orgCertifiedStdCount + 1;

                    DateTime validFrom = DateTime.ParseExact(model.ValidFrom, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    DateTime validTo = DateTime.ParseExact(model.ValidTo, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    var orgCertifiedStdModel = new Tbl_OrgCertifiedStd()
                    {
                        SupplierID = model.SupplierID,
                        SeqNo = seqNo,
                        CertifiedID = model.CertifiedID??0,
                        OtherStandardName = model.OtherStandardName??"",
                        ValidFrom = validFrom,
                        ValidTo = validTo
                    };

                    int certId = _orgCertifiedStdRepository.InsertOrgCertifiedStdReturnCertId(orgCertifiedStdModel, userID);

                    #endregion

                    #region-----------------------Manage OrgCertifiedStdAttachment------------------------

                    var hidFile = Request.Params["hidAttachmentName[]"] ?? string.Empty;

                    var hidGuid = Request.Params["hidAttachmentNameUnique[]"] ?? string.Empty;

                    var splitHidFile = hidFile.Split(',');

                    var splitHidGuid = hidGuid.Split(',');

                    if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                    {
                        for (var i = 0; i < splitHidFile.Length; i++)
                        {

                            string fileName = splitHidFile[i];

                            string guidName = splitHidGuid[i];

                            string decodeFileName = StringHelper.GetMakeASCIIStringToComma(fileName.Trim());

                            UploadAttachment(decodeFileName, guidName, certId, userID);

                        }
                        
                    }

                    #endregion
                }else
                {
                    #region-----------------------Manage OrgCertifiedStd------------------------

                    var orgCertifiedStd = _orgCertifiedStdRepository.GetOrgCertifiedStdByCertId(model.CertId??0);

                    if (orgCertifiedStd != null)
                    {
                        DateTime validFrom = DateTime.ParseExact(model.ValidFrom, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

                        DateTime validTo = DateTime.ParseExact(model.ValidTo, "dd/MM/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        orgCertifiedStd.CertifiedID = model.CertifiedID??0;
                        if(model.CertifiedID != -1)
                        {
                            model.OtherStandardName = "";
                        }
                        orgCertifiedStd.OtherStandardName = model.OtherStandardName ?? "";
                        orgCertifiedStd.ValidFrom = validFrom;
                        orgCertifiedStd.ValidTo = validTo;

                        _orgCertifiedStdRepository.Update(orgCertifiedStd, userID);

                    }
                    #endregion

                    #region-----------------------Manage OrgCertifiedStdAttachment------------------------

                    var hidFile = Request.Params["hidAttachmentName[]"] ?? string.Empty;

                    var hidGuid = Request.Params["hidAttachmentNameUnique[]"] ?? string.Empty;

                    var splitHidFile = hidFile.Split(',');

                    var splitHidGuid = hidGuid.Split(',');

                    if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                    {
                        for (var i = 0; i < splitHidFile.Length; i++)
                        {

                            string fileName = splitHidFile[i];

                            string guidName = splitHidGuid[i];

                            string decodeFileName = StringHelper.GetMakeASCIIStringToComma(fileName.Trim());

                            UploadAttachment(decodeFileName, guidName, model.CertId??0, userID);

                        }

                    }

                    #endregion

                    #region-----------------------Remove Attachment------------------------
                    var hidRemoveFile = Request.Params["hidRemoveAttachmentID[]"] ?? string.Empty;

                    var splitHidRemoveFile = hidRemoveFile.Split(',');

                    if (splitHidRemoveFile.Length > 0)
                    {
                        foreach (var itemlist in splitHidRemoveFile)
                        {
                            if (!string.IsNullOrEmpty(itemlist))
                            {
                                RemoveAttachment(itemlist, userID);
                            }
                            
                        }
                    }
                    #endregion
                }

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(model.SupplierID, userID);
                #endregion
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("Certification");
        }

        [HttpPost]
        public ActionResult CertificationDelete(int hidCertIdDelete, int hidSupplierIDDelete)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                _comProfileDataService.DeleteOrgCertifiedStd(hidCertIdDelete, userID);

                #region-----------------------Update  SeqNo OrgCertifiedStd------------------------
                var orgCertifiedStdList = _orgCertifiedStdRepository.GetOrgCertifiedStdBySupplierID(hidSupplierIDDelete).OrderBy(m=>m.SeqNo);
                int i = 1;
                foreach (var item in orgCertifiedStdList)
                {
                    item.SeqNo = i;
                    _orgCertifiedStdRepository.UpdateNonLog(item);

                    i++;
                }
                #endregion

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(hidSupplierIDDelete, userID);
                #endregion
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }
            return RedirectToAction("Certification");
        }

        public ActionResult CertificateStandard(CompanyCertificationModel model)
        {

            ViewBag.CertificateList = _certifiedStdRepository.GetCertifiedStdAll();
            return PartialView(@"~/Views/CompanyAdditional/_CertificateStandardPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult UploadFileAttachment(FormCollection collection)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            string attachmentName = "FileAttachment";

            string tbxfilename = collection["tbx-file-path"];

            string fileName = "";

            string fileGuidName = "";

            string htmlText = "";

            HttpPostedFileBase myFile = Request.Files[attachmentName];
            bool isUploaded = false;
            string message = "File upload failed";

            var attachmentSize = _appConfigRepository.GetAppConfigByName("Portal_AttachmentSize");
            var maxFileSize = Convert.ToInt32(attachmentSize.Value);

            if (myFile != null && myFile.ContentLength != 0)
            {
                if (myFile.ContentLength > maxFileSize)
                {
                    message = message.GetStringResource("_ComPro.Certification.ValidateMsg.LimitAttachFileSize", languageID);
                    return Json(new { isUploaded = isUploaded, message = message }, "text/html");
                }

                string fileSize = MainService.GetFileSize(myFile.ContentLength);

                string pathTemp = _appConfigRepository.GetAppConfigByName("Portal_SupplierCertificateDocPathTemp").Value;
                string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                string pathTempCertificate = "Certificate";

                string pathForSaving = pathAttachment + pathTemp + pathTempCertificate;
                string pathForDownload = pathTemp + pathTempCertificate;
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string guid = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();



                        //fileGuidName = guid + myFile.;
                        fileGuidName = guid + myFile.FileName.Substring(myFile.FileName.LastIndexOf('.'));



                        myFile.SaveAs(Path.Combine(pathForSaving, fileGuidName));

                        string virtualFilePath = pathForDownload + "/" + fileGuidName;


                        isUploaded = true;
                        message = "File uploaded successfully!";
                        fileName = tbxfilename;
                        //fileGuidName = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();
                        htmlText = GetHtmlText(fileName, virtualFilePath, fileGuidName, guid, fileSize);
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new
            {
                isUploaded = isUploaded,
                message = message,
                fileName = fileName,
                htmlText = htmlText,
                fileGuidName = fileGuidName
            }, "text/html");
        }

        public ActionResult Download(string filePath, string fileName = "")
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;
            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);


            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "download";
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        #region------------------------------------------------Function Helper------------------------------------------------------

        private string GetHtmlText(string fileName, string virtualFilePath, string fileGuidName, string guid, string fileSize = "")
        {
            string link = HtmlHelper.GenerateLink(this.ControllerContext.RequestContext, System.Web.Routing.RouteTable.Routes, fileName, "Default", "Download", "CompanyAdditional", new RouteValueDictionary(new { filePath = virtualFilePath, fileName = fileName }), new Dictionary<string, object> { { "class", "register" } });

            string linkDelete = "<input type=\"image\" src=\"/Content/images/icon/ico-sp_deluser2.gif\" alt=\"Submit\" id=\"deleteAttachment\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete file\" >";

            string encodeFileName = StringHelper.GetMakeCommaToASCIIString(fileName.Trim());

            string htmlTextTable = "";
            htmlTextTable += "<tr class=\"main_table_row \">";
            htmlTextTable += "<td align=\"left\">";
            htmlTextTable += "<div  class=\"file-attachment-download\">" + link + "</div>";
            htmlTextTable += "<input type=\"hidden\" name=\"hidAttachmentName[]\" value=\"" + encodeFileName + "\" />";
            htmlTextTable += "<input type=\"hidden\" name=\"hidAttachmentNameUnique[]\" value=\"" + fileGuidName + "\" />";
            htmlTextTable += "</td>";
            htmlTextTable += "<td align=\"right\">" + fileSize + "</td>";
            htmlTextTable += "<td align=\"center\">" + linkDelete + "</td>";
            htmlTextTable += "</tr>";


            return htmlTextTable;
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private void UploadAttachment(string fileName, string guidName, int certId,int updateBy)
        {
            try
            {

                    string pathTemp = _appConfigRepository.GetAppConfigByName("Portal_SupplierCertificateDocPathTemp").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathTempCertificate = "Certificate";

                    string pathForSavingTemp = pathAttachment + pathTemp + pathTempCertificate;
                    //string pathForSavingTemp = Server.MapPath("~/Register_AttachmentPathTemp/" + regID);

                    string filePathTemp = pathForSavingTemp + "/" + guidName;

                    if (System.IO.File.Exists(filePathTemp))
                    {

                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePathTemp);


                        string virtualFilePath = filePathTemp;
                        string fileName2 = Path.GetFileName(virtualFilePath);
                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {
                            string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierCertificateDocPath").Value;
                            string pathForSaving = pathAttachment + pathSave + certId;

                            string filePath = pathForSaving + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForSaving))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePath))
                                    System.IO.File.Delete(filePath);

                                // Move the file.
                                System.IO.File.Move(filePathTemp, filePath);

                                var models = new Tbl_OrgCertifiedStdAttachment
                                {
                                    CertId = certId,
                                    AttachmentName = fileName,
                                    AttachmentNameUnique = guidName,
                                    SizeAttach = fileTemp.FileContents.Length.ToString()

                                };

                                _orgCertifiedStdAttachmentRepository.Insert(models, updateBy);

                            }
                        }
                    }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveAttachment(string attachmentID, int updateBy)
        {
            try
            {

                var model = _orgCertifiedStdAttachmentRepository.GetOrgCertifiedStdAttachmentByAttachmentID(Convert.ToInt32(attachmentID));

                if (model != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierCertificateDocPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + model.CertId;

                    string filePath = pathForSaving + "/" + model.AttachmentNameUnique;

                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);

                    _orgCertifiedStdAttachmentRepository.DeleteByCertIdAndAttachmentID(model.CertId, model.AttachmentID, 1);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}