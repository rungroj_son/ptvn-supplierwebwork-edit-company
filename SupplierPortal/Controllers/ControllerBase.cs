﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;


namespace SupplierPortal.Controllers
{
    public class ControllerBase : Controller
    {

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            IUserSessionRepository _userSessionRepository;
            IUserRepository _repoUser;
            _userSessionRepository = new UserSessionRepository();
            _repoUser = new UserRepository();

            if (Session["GUID"] == null)
            {
                Response.Redirect("~/Error/Timeout");
            }
            else
            {

                string guid = Session["GUID"].ToString();
                string username = Session["username"].ToString();

                CheckTimeoutModel model = new CheckTimeoutModel
                {
                    UserGuid = guid
                };

                HttpClient client = new HttpClient();

                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format("{0}api/AccountPortal/ActiveUserSession", baseURL);

                HttpResponseMessage response = client.PostAsync(urlAll, content).Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = response.Content.ReadAsStringAsync().Result;
                    try
                    {
                        JObject _response = JObject.Parse(responseBody);

                        var query = from c in _response["data"].Children()
                                    select c;
                        query = query.ToList();

                        var responseResult = (from x in query
                                              select new
                                              {
                                                  isSessionTimeout = (int)x.SelectToken("IsSessionTimeout"),
                                              }).ToList();

                        var respResult = responseResult.Select(x => x.isSessionTimeout).FirstOrDefault();

                        if (respResult != 0)
                        {
                            Response.Redirect("~/Error/Timeout");
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                    }

                    var ChkisAcceptTermOfUse = _repoUser.FindByUsername(username);
                    string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];
                    if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0 && !Convert.ToBoolean(uesTermWeomni))
                    {
                        Response.Redirect("~/Agreement/Index");
                    }

                    if (ChkisAcceptTermOfUse.IsActivated == 0)
                    {
                        Response.Redirect("~/Activated/ChangePassword");
                    }
                }
                else
                {
                    Response.Redirect("~/Error/Timeout");
                }
            }

            return base.BeginExecuteCore(callback, state);
        }
    }
}