﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.Models.Reports
{
    public class ReportsModels
    {
        public string OrgID { get; set; }
        public string UserID { get; set; }
        public string RptID { get; set; }
    }
}