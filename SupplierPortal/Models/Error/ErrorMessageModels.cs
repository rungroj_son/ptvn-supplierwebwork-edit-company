﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace SupplierPortal.Models.Error
{
    public class ErrorMessageModels
    {
        [Required]
        public string UserGUID { get; set; }
        [Required]
        public string ErrorMessage { get; set; }
    }
}