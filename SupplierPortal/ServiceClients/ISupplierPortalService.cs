﻿using SupplierPortal.Models.Dto.DataContract;
using System.Threading.Tasks;

namespace SupplierPortal.ServiceClients
{
    public interface ISupplierPortalService
    {
        #region BuyerSupplier
        #region GET
        Task<GetBuyerSuppliersResponse> GetBuyerSuppliers(GetBuyerSuppliersRequest request);
        Task<GetOrganizationResponse> GetOrganization(int id);
        #endregion
        #endregion

        #region Language
        #region GET
        Task<bool> IsLocalLanguage(string languageId);
        #endregion
        #endregion
    }
}
