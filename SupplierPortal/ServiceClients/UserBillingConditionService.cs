﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class UserBillingConditionService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<Boolean> CheckAcceeptBillingCondition(string username)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/user/billing/check";
                httpResponse = await this.PostApiConnection(url, username);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> UpdateBillingCondition(string userGUID)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/user/billing/update";
                httpResponse = await this.PostApiConnection(url, userGUID);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}