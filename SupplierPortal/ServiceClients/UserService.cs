﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class UserService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<Boolean> UpdateUserActiveInActive(int userId)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/user/updateactive";
                httpResponse = await this.PostApiConnection(url, userId);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}