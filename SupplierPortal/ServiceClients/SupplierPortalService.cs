﻿using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.ServiceClients
{
    public class SupplierPortalService : APIConnectionService, ISupplierPortalService 
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private int _timeOut = 120;

        private string baseURL;
        private string urlGetBuyerSuppliers;
        private string urlGetOrganization;
        private string urlIsLocalLanguage;
        public SupplierPortalService()
        {
            this.baseURL = WebConfigurationManager.AppSettings["baseURL"];
            this.urlGetBuyerSuppliers = WebConfigurationManager.AppSettings["get_buyer_suppliers"];
            this.urlGetOrganization = WebConfigurationManager.AppSettings["get_organization"];
            this.urlIsLocalLanguage = WebConfigurationManager.AppSettings["get_islocal_language"];
        }

        #region BuyerSupplier
        #region GET
        public async Task<GetBuyerSuppliersResponse> GetBuyerSuppliers(GetBuyerSuppliersRequest request)
        {
            GetBuyerSuppliersResponse response = null;
            HttpResponseMessage postTask = null;
            try
            {
                string url = string.Format("{0}{1}", baseURL, urlGetBuyerSuppliers);

                postTask = await this.PostApiConnection(url, request);

                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    GetBuyerSuppliersResponse respond = JsonConvert.Deserialize<GetBuyerSuppliersResponse>(jsonResult);
                    return respond;
                }
                else
                {
                    response = new GetBuyerSuppliersResponse()
                    {
                        Success = false,
                        Message = "System Error"
                    };
                }

            }
            catch (Exception ex)
            {
                response = new GetBuyerSuppliersResponse()
                {
                    Success = false,
                    Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message
                };
            }
            finally
            {
                postTask.Dispose();
            }

            return response;
        }

        public async Task<GetOrganizationResponse> GetOrganization(int id)
        {
            GetOrganizationResponse response = null;
            HttpResponseMessage postTask = null;
            try
            {
                string url = string.Format("{0}{1}?id={2}", baseURL, urlGetOrganization, id);

                postTask = await this.GetApiConnection(url);

                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    GetOrganizationResponse respond = JsonConvert.Deserialize<GetOrganizationResponse>(jsonResult);
                    return respond;
                }
                else
                {
                    response = new GetOrganizationResponse()
                    {
                        Success = false,
                        Message = "System Error"
                    };
                }

            }
            catch (Exception ex)
            {
                response = new GetOrganizationResponse()
                {
                    Success = false,
                    Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message
                };
            }
            finally
            {
                postTask.Dispose();
            }

            return response;
        }

        #endregion
        #endregion

        #region Language
        #region GET
        public async Task<bool> IsLocalLanguage(string languageId)
        {
            bool response = false;
            HttpResponseMessage postTask = null;
            try
            {
                string url = string.Format("{0}{1}?languageId={2}", baseURL, urlIsLocalLanguage, languageId);

                postTask = await this.GetApiConnection(url);
                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    response = JsonConvert.Deserialize<bool>(jsonResult);
                    return response;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                postTask.Dispose();
            }
        }
        #endregion
        #endregion
    }
}