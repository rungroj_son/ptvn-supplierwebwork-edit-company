﻿using SupplierPortal.Data.CustomModels.SCF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.ServiceClients
{
    public class SCFService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string baseURL;
        private string path_scf_validate;
        private string path_scf_save;

        public SCFService()
        {
            this.baseURL = WebConfigurationManager.AppSettings["baseURL"];
            this.path_scf_validate = WebConfigurationManager.AppSettings["path_scf_validate"];
            this.path_scf_save = WebConfigurationManager.AppSettings["path_scf_save"];
        }

        public async Task<SCFResponse> SaveSCFRequest(SCFRequest request)
        {
            SCFResponse result = null;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = string.Format("{0}{1}", baseURL, this.path_scf_save);
                httpResponse = await this.PostApiConnection(url, request);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    result = JsonConvert.Deserialize<SCFResponse>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("InsertOPNBuyerSupplierMapping : " + ex.Message);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<SCFResponse> CheckSCFRequestStatusInProgress(SCFRequest request)
        {
            SCFResponse result = null;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = string.Format("{0}{1}", baseURL, this.path_scf_validate);
                httpResponse = await this.PostApiConnection(url, request);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    result = JsonConvert.Deserialize<SCFResponse>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("InsertOPNSupplierMapping : " + ex.Message);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}