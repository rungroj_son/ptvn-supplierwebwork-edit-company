﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class OPNSupplierService : APIConnectionService
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<Boolean> InsertOPNBuyerSupplierMapping(OPNSupplierRegister request)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/opn/buyersuppliermapping";
                httpResponse = await this.PostApiConnection(url, request);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("InsertOPNBuyerSupplierMapping : "+ ex.Message);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> InsertOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMappingViewModels request)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/opn/invitation/buyersuppliermapping";
                httpResponse = await this.PostApiConnection(url, request);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("InsertOPNBuyerSupplierMapping : " + ex.Message);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> InsertOPNSupplierMapping(OPNSupplierRegister request)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/opn/suppliermapping";
                httpResponse = await this.PostApiConnection(url, request);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("InsertOPNSupplierMapping : " + ex.Message);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}