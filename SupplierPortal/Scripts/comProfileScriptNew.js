﻿function CompanyTypeChangedNew() {
    var inputCheck = false;
    $("input[data-tracking=2]").each(function (index) {
        var idTracking1 = $(this).attr('id');
        if (idTracking1 == "BranchNo") {
            inputCheck = true;
            return false;
        }
        if (inputCheck == false) {
            $("table[data-tracking=2]").each(function (index) {
                var idTracking2 = $(this).attr('id');
                if (idTracking2 == "BranchNo") {
                    inputCheck = true;
                    return false;
                } $("input[data-tracking=5]").each(function (index) {
                    var idTracking1 = $(this).attr('id');
                    if (idTracking1 == "BranchNo") {
                        inputCheck = true;
                        return false;
                    }
                    if (inputCheck == false) {
                        $("table[data-tracking=5]").each(function (index) {
                            var idTracking2 = $(this).attr('id');
                            if (idTracking2 == "BranchNo") {
                                inputCheck = true;
                                return false;
                            }
                        });
                    }
                });
            });
        }
    });
    //console.log(inputCheck);
    if (inputCheck == false) {
        $('input[type=radio][name=CompanyTypeID]').change(function () {
            var companyTypeID = this.value;

            //console.log(companyTypeID);
            BusinessEntityCallback(companyTypeID);

            UpdateItemCompanyNew(companyTypeID);

            UpdateLableTaxNoNew(companyTypeID);

        });
    }
    
}

function CountryChangedNew(countryCode) {

    var companyTypeID = $('input[name=CompanyTypeID]:checked').val();

    
    var inputCheck = false;
    $("input[data-tracking=2]").each(function (index) {
        var idTracking1 = $(this).attr('id');
        if (idTracking1 == "BranchNo") {
            inputCheck = true;
            return false;
        }
        if (inputCheck == false) {
            $("table[data-tracking=2]").each(function (index) {
                var idTracking2 = $(this).attr('id');
                if (idTracking2 == "BranchNo") {
                    inputCheck = true;
                    return false;
                }
            });
        }
    });


    if (inputCheck == false) {

        UpdateItemCompanyNew(companyTypeID);

        UpdatePlaceholderCompany(countryCode);

        UpdatePlaceholderName(countryCode);

        UpdateLableTaxNoNew();
    }
}

function BusinessEntityChangedNew(businessEntity) {
    if (businessEntity == -1) {
        $("#divOtherBusEnt").css("display", "block");
        $("#txtOtherBusEnt").css("display", "block");
        $("#txtOtherBusEnt").attr('readonly', false);

    } else {
        $("#txtOtherBusEnt").css("display", "none");
        $("#divOtherBusEnt").css("display", "none");
        $("#txtOtherBusEnt").val("");

    }
}

function BusinessEntityCallback(companyTypeID) {
    //console.log("BusinessEntityCallback GeneralInformation");
    $.ajax({
        url: '/GeneralInformation/BusinessEntityPartialNew',
        dataType: 'html',
        data: { 'CorperateValue': companyTypeID },
        success: function (data) {
            $('#BusinessEntity').html(data);

            EndCallbackBusinessEntityNew(companyTypeID);
        },
        fail: function (event, data) {

        }
    });
}

function EndCallbackBusinessEntityNew(companyTypeID) {
    var companyType = companyTypeID
    if (companyType == 1) {
        //s.SetValue(4);
        $("#BusinessEntityID").val(4).change();;
        $("#txtOtherBusEnt").css("display", "none");
    } else if (companyType == 2) {
        //s.SetValue(13);
        $("#BusinessEntityID").val(13).change();;
        $("#txtOtherBusEnt").css("display", "none");
    }
}

function UpdateItemCompanyNew(companyTypeID) {
    var companyType = companyTypeID;
    var countryCode = $("#CountryCode option:selected").val();
    //console.log(countryCode);

    if (companyType == 1) {
        if (countryCode == "TH") {

            var branchTypeNotSpeci = $('input:radio[class=BranchType][id=BranchType0]').is(':checked');

            if (branchTypeNotSpeci)
            {
                $('input:radio[class=BranchType][id=BranchType1]').prop('checked', true);
                $("#BranchNo").attr('readonly', true);
                $("#BranchNo").val("00000");
                $("#BranchName_Local").attr('readonly', true);
                $("#BranchName_Local").val("");
                $("#BranchName_Inter").attr('readonly', true);
                $("#BranchName_Inter").val("");
            }

            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', true);
        } else {

            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', false);
        }
    } else if (companyType == 2) {

        $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', false);

    }
}

function CheckBranchNoNew() {

    var companyType = $('input[name=CompanyTypeID]:checked').val();
    var countryCode = $("#CountryCode option:selected").val();

    if ($("#hidBranchNo").val() == "" || $("#hidBranchNo").val() == undefined) {

        $('input:radio[class=BranchType][id=BranchType0]').prop('checked', true);

    } else if ($("#hidBranchNo").val() == "00000") {

        $('input:radio[class=BranchType][id=BranchType1]').prop('checked', true);

    } else {
        $("input[data-tracking=1]").each(function (index) {
            $(this).attr('readonly', false);
        });

        $('input:radio[class=BranchType][id=BranchType2]').prop('checked', true);

        $("#BranchName_Local").attr("placeholder", $("#placeholderBranchLocal").val());
        $("#BranchName_Inter").attr("placeholder", $("#placeholderBranchInter").val());
    }
    if (companyType == 1) {
        if (countryCode == "TH") {
            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', true);
        } else {
            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', false);
        }
    } else if (companyType == 2) {
        //BranchTypeNotSpeci.SetEnabled(true);
    }

    CheckBranchNoTracking();
}

function CheckBranchNoTracking() {
    var chkTracking = $("#BranchNo").attr('data-edit');
    if (chkTracking != undefined) {
        if (chkTracking == "True") {
            //BranchTypeNotSpeci.SetEnabled(true);
            //BranchType.SetEnabled(true);
            $('input:radio[class=BranchType]').prop('disabled', false);
        } else {
            //BranchTypeNotSpeci.SetEnabled(false);
            //BranchType.SetEnabled(false);
            $('input:radio[class=BranchType]').prop('disabled', true);
        }
    }
}


function BranchTypeChangedNew() {

    $('input[type=radio][name=BranchType]').change(function () {
        //console.log(this.value);
        if (this.value == "0") {
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("");
            $("#BranchName_Local").attr('readonly', true);
            $("#BranchName_Local").val("");
            $("#BranchName_Inter").attr('readonly', true);
            $("#BranchName_Inter").val("");
            $("#BranchName_Local").attr("placeholder", "");
            $("#BranchName_Inter").attr("placeholder", "");

        } else if (this.value == "1") {
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("00000");
            $("#BranchName_Local").attr('readonly', true);
            $("#BranchName_Local").val("");
            $("#BranchName_Inter").attr('readonly', true);
            $("#BranchName_Inter").val("");
            $("#BranchName_Local").attr("placeholder", "");
            $("#BranchName_Inter").attr("placeholder", "");
        } else {
            $("#BranchNo").attr('readonly', false);
            $("#BranchNo").val("");
            $("#BranchName_Local").attr('readonly', false);
            $("#BranchName_Local").val("");
            $("#BranchName_Inter").attr('readonly', false);
            $("#BranchName_Inter").val("");
            $("#BranchName_Local").attr("placeholder", $("#placeholderBranchLocal").val());
            $("#BranchName_Inter").attr("placeholder", $("#placeholderBranchInter").val());
        }
    });
}

function DocumentNameChangedNew(docName, docValue) {
    //console.log(docName);
    //console.log(docValue);
    if (docName == "DocumentNameID_-1") {
        var docNameID = docValue;
        if (docNameID == -1) {
            $("#docTypeOther").css("display", "block");
        } else {
            $("#docTypeOther").css("display", "none");
        }
    }
}

function BusinessEntityChangedNew_RefCustomer(businessEntity) {
    if (businessEntity == -1) {
        $("#txtotherBusinessEntity").css("display", "block");

    } else {
        $("#txtotherBusinessEntity").css("display", "none");
        $("#txtotherBusinessEntity").val("");

    }
}
