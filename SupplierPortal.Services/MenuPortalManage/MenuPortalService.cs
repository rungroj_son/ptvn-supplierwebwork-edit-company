﻿using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.Repository.SystemNotification;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.APILogs;
using Newtonsoft.Json;
using SupplierPortal.Services.AccountManage;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;

namespace SupplierPortal.Services.MenuPortalManage
{
    public partial class MenuPortalService : IMenuPortalService
    {

        IMenuPortalRepository _menuPortalRepository;
        ISystemNotificationRepository _systemNotificationRepository;
        IUserRepository _userRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IAPILogsRepository _apiLogsRepository;
        ISystemConfigureAPIRepository _systemConfigureAPIRepository;

        public MenuPortalService()
        {
            _menuPortalRepository = new MenuPortalRepository();
            _systemNotificationRepository = new SystemNotificationRepository();
            _userRepository = new UserRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _apiLogsRepository = new APILogsRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
        }

        public MenuPortalService(
            MenuPortalRepository menuPortalRepository,
            SystemNotificationRepository systemNotificationRepository,
            UserRepository userRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            APILogsRepository apiLogsRepository,
            SystemConfigureAPIRepository systemConfigureAPIRepository
            )
        {
            _menuPortalRepository = menuPortalRepository;
            _systemNotificationRepository = systemNotificationRepository;
            _userRepository = userRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _apiLogsRepository = apiLogsRepository;
            _systemConfigureAPIRepository = systemConfigureAPIRepository;
        }

        public virtual IEnumerable<SubMenuPortal> GetSubMenuPortalNonNotification()
        {
            try
            {

                var subMenu = _menuPortalRepository.GetSubMenuPortal();

                foreach (var item in subMenu)
                {
                    var systemNoti = _systemNotificationRepository.GetSystemShowNotiBySystemGrpIDAndNotificationID(item.SystemGrpID ?? 0, item.NotificationID ?? 0);
                    if (systemNoti.Count() > 0)
                    {
                        item.IsShowNotification = true;
                    }
                }

                return subMenu;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual async Task<HttpResponseMessage> GetResponseAPI(string uri, HttpContent content)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.PostAsync(uri, content);

            return response;
        }

        public virtual int GetNotificationByUserNameAndMenuID(string username, int menuID)
        {
            if (menuID == 40)
            {
                string aa = "";
            }
            int notificationValue = 0;
            try
            {
                var lisNotification = new List<NotificationModels>();

                var menuPortal = _menuPortalRepository.GetMenuById(menuID);

                if (menuPortal != null)
                {
                    int systemGrpID = menuPortal.SystemGrpID ?? 0;
                    int notificationID = menuPortal.NotificationID ?? 0;

                    //var systemNoti = _systemNotificationRepository.GetSystemShowNotiBySystemGrpIDAndNotificationID(systemGrpID, notificationID);

                    var systemNoti = _systemConfigureAPIRepository.GetSystemShowNotiBySystemGrpIDAndNotificationID(systemGrpID, notificationID);

                    string guidCall = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    #region Call API Data
                    foreach (var item in systemNoti)
                    {

                        string reqID = GeneratorGuid.GetRandomGuid();

                        var userLoginMappingTbl = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(username, item.SystemID);

                        if (userLoginMappingTbl.Count() > 0)
                        {
                            string jsonStr = "";

                            string _APIKey = "";

                            string _APIName = "";

                            var apiNotiModels = GetDataCallNotificationAPI(reqID, username, item.SystemID, item.APIName, item.API_Key);

                            jsonStr = JsonConvert.SerializeObject(apiNotiModels);
                            _APIKey = apiNotiModels.Request.APIKey;
                            _APIName = apiNotiModels.Request.APIName;

                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri(item.API_URL);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpContent content = new StringContent(jsonStr);

                            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            string responseBody = "";

                            #region  API Post Task
                            var continuationTask = client.PostAsync(item.API_URL, content).ContinueWith(
                                  (postTask) =>
                                  {
                                      bool isCompleted = postTask.IsCompleted;
                                      bool isFaulted = postTask.IsFaulted;

                                      if (isCompleted && !isFaulted)
                                      {
                                          HttpResponseMessage errMsg = new HttpResponseMessage();
                                          errMsg.StatusCode = System.Net.HttpStatusCode.RequestTimeout;

                                          var response = postTask.Status == TaskStatus.Canceled ? errMsg : postTask.Result;
                                          if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                          {

                                              try
                                              {

                                                  string return_Code = "";
                                                  string return_Status = "";
                                                  string return_Message = "";
                                                  if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                  {
                                                      responseBody = response.Content.ReadAsStringAsync().Result;
                                                      JObject _response = JObject.Parse(responseBody);

                                                      return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                      return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                      return_Message = _response["Response"]["Result"]["Message"].ToString();
                                                  }

                                                  #region Insert API Log

                                                  var _APILogs = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = return_Code,
                                                      Return_Status = return_Status,
                                                      Return_Message = return_Message,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogs);
                                                  #endregion

                                                  #region Get Noti
                                                  if (Convert.ToBoolean(return_Status))
                                                  //if (true)
                                                  {
                                                      API_NotificationResponseModel responseModel = JsonConvert.DeserializeObject<API_NotificationResponseModel>(responseBody);

                                                      if (responseModel.Response.Data.Count() > 0)
                                                      {
                                                          foreach (IList<Notification> dataValue in responseModel.Response.Data.Select(m => m.Notification))
                                                          {
                                                              foreach (var notiItem in dataValue)
                                                              {
                                                                  var notiModel = new NotificationModels()
                                                                  {
                                                                      SystemID = item.SystemID,
                                                                      SystemGrpID = item.SystemGrpID,
                                                                      NotificationID = Convert.ToInt32(notiItem.NotificationID),
                                                                      NotificationValue = Convert.ToInt32(notiItem.NotificationValue)
                                                                  };

                                                                  lisNotification.Add(notiModel);

                                                              }
                                                          }
                                                      }
                                                  }
                                                  #endregion
                                              }

                                              catch (Exception ex)
                                              {
                                                  #region Insert API Log

                                                  var _APILogsCheck = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request_Check",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = "",
                                                      Return_Status = response.StatusCode.ToString(),
                                                      Return_Message = responseBody,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogsCheck);


                                                  #endregion
                                                  throw;
                                              }

                                          }
                                          else
                                          {

                                              #region Insert API Log


                                              var _APILogs = new Tbl_APILogs()
                                              {
                                                  ReqID = reqID,
                                                  SystemID = item.SystemID,
                                                  Events = "Request",
                                                  Events_Time = DateTime.UtcNow,
                                                  IPAddress_Client = visitorsIPAddr,
                                                  APIKey = _APIKey,
                                                  APIName = _APIName,
                                                  Return_Code = "",
                                                  Return_Status = response.StatusCode.ToString(),
                                                  Return_Message = responseBody,
                                                  Data = jsonStr,
                                                  UserGUID = guidCall,
                                              };

                                              _apiLogsRepository.Insert(_APILogs);


                                              #endregion
                                          }
                                      }
                                      else
                                      {
                                          #region Insert API Log

                                          var _APILogs = new Tbl_APILogs()
                                          {
                                              ReqID = reqID,
                                              SystemID = item.SystemID,
                                              Events = "Request",
                                              Events_Time = DateTime.UtcNow,
                                              IPAddress_Client = visitorsIPAddr,
                                              APIKey = _APIKey,
                                              APIName = _APIName,
                                              Return_Code = "",
                                              Return_Status = "",
                                              Return_Message = "NO RESPONSE",
                                              Data = jsonStr,
                                              //Data = responseBody,
                                              UserGUID = guidCall,
                                          };

                                          _apiLogsRepository.Insert(_APILogs);
                                          #endregion
                                      }


                                  });
                            #endregion

                            continuationTask.Wait(10000);

                        }

                    }
                    #endregion

                    var newlisNotification = lisNotification.GroupBy(d => new { d.SystemGrpID, d.NotificationID })
                    .Select(
                    g => new NotificationModels
                    {
                        SystemID = g.First().SystemID,
                        SystemGrpID = g.Key.SystemGrpID,
                        NotificationID = g.Key.NotificationID,
                        NotificationValue = g.Sum(s => s.NotificationValue),
                    }
                    ).ToList();

                    var notimodel = newlisNotification.Where(m => m.SystemGrpID == systemGrpID && m.NotificationID == notificationID).FirstOrDefault();

                    if (notimodel != null)
                    {
                        notificationValue = notimodel.NotificationValue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }



            return notificationValue;
        }

        public virtual int GetNotificationBoxByUserNameAndMenuID(string username, int menuID)
        {
            int notificationValue = 0;
            try
            {
                var lisNotification = new List<NotificationModels>();

                var menuPortal = _menuPortalRepository.GetMenuById(menuID);

                if (menuPortal != null)
                {
                    int systemGrpID = menuPortal.SystemGrpID ?? 0;
                    int notificationID = menuPortal.NotificationID ?? 0;

                    var systemNoti = _systemConfigureAPIRepository.GetSystemShowNotiBySystemGrpIDAndNotificationID(systemGrpID, notificationID);

                    string guidCall = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    #region Call API Data
                    foreach (var item in systemNoti)
                    {

                        string reqID = GeneratorGuid.GetRandomGuid();

                        var userLoginMappingTbl = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(username, item.SystemID);

                        if (userLoginMappingTbl.Count() > 0)
                        {
                            string jsonStr = "";

                            string _APIKey = "";

                            string _APIName = "";

                            var apiNotiModels = GetDataCallNotificationAPI(reqID, username, item.SystemID, item.APIName, item.API_Key);

                            jsonStr = JsonConvert.SerializeObject(apiNotiModels);
                            _APIKey = apiNotiModels.Request.APIKey;
                            _APIName = apiNotiModels.Request.APIName;

                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri(item.API_URL);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpContent content = new StringContent(jsonStr);

                            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            string responseBody = "";

                            #region  API Post Task
                            var continuationTask = client.PostAsync(item.API_URL, content).ContinueWith(
                                  (postTask) =>
                                  {
                                      bool isCompleted = postTask.IsCompleted;
                                      bool isFaulted = postTask.IsFaulted;

                                      if (isCompleted && !isFaulted)
                                      {
                                          //var response = postTask.Result.EnsureSuccessStatusCode();
                                          var response = postTask.Result;
                                          if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                          {

                                              try
                                              {

                                                  string return_Code = "";
                                                  string return_Status = "";
                                                  string return_Message = "";
                                                  if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                  {
                                                      responseBody = response.Content.ReadAsStringAsync().Result;
                                                      JObject _response = JObject.Parse(responseBody);

                                                      return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                      return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                      return_Message = _response["Response"]["Result"]["Message"].ToString();
                                                  }

                                                  #region Insert API Log

                                                  var _APILogs = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = return_Code,
                                                      Return_Status = return_Status,
                                                      Return_Message = return_Message,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogs);
                                                  #endregion

                                                  #region Insert API Log

                                                  var _APILogs2 = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "CheckDataNoti",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = return_Code,
                                                      Return_Status = return_Status,
                                                      Return_Message = return_Message,
                                                      Data = responseBody,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogs2);
                                                  #endregion

                                                  #region Get Noti
                                                  if (Convert.ToBoolean(return_Status))
                                                  //if (true)
                                                  {
                                                      API_NotificationResponseModel responseModel = JsonConvert.DeserializeObject<API_NotificationResponseModel>(responseBody);

                                                      if (responseModel.Response.Data.Count() > 0)
                                                      {
                                                          foreach (IList<Notification> dataValue in responseModel.Response.Data.Select(m => m.Notification))
                                                          {
                                                              foreach (var notiItem in dataValue)
                                                              {
                                                                  var notiModel = new NotificationModels()
                                                                  {
                                                                      SystemID = item.SystemID,
                                                                      SystemGrpID = item.SystemGrpID,
                                                                      NotificationID = Convert.ToInt32(notiItem.NotificationID),
                                                                      NotificationValue = Convert.ToInt32(notiItem.NotificationValue)
                                                                  };

                                                                  lisNotification.Add(notiModel);

                                                              }
                                                          }
                                                      }
                                                  }
                                                  #endregion
                                              }

                                              catch (Exception ex)
                                              {
                                                  #region Insert API Log

                                                  var _APILogsCheck = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request_Check",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = "",
                                                      Return_Status = response.StatusCode.ToString(),
                                                      Return_Message = responseBody,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogsCheck);


                                                  #endregion
                                                  throw;
                                              }

                                          }
                                          else
                                          {

                                              #region Insert API Log


                                              var _APILogs = new Tbl_APILogs()
                                              {
                                                  ReqID = reqID,
                                                  SystemID = item.SystemID,
                                                  Events = "Request",
                                                  Events_Time = DateTime.UtcNow,
                                                  IPAddress_Client = visitorsIPAddr,
                                                  APIKey = _APIKey,
                                                  APIName = _APIName,
                                                  Return_Code = "",
                                                  Return_Status = response.StatusCode.ToString(),
                                                  Return_Message = responseBody,
                                                  Data = jsonStr,
                                                  UserGUID = guidCall,
                                              };

                                              _apiLogsRepository.Insert(_APILogs);


                                              #endregion
                                          }
                                      }
                                      else
                                      {
                                          #region Insert API Log

                                          var _APILogs = new Tbl_APILogs()
                                          {
                                              ReqID = reqID,
                                              SystemID = item.SystemID,
                                              Events = "Request",
                                              Events_Time = DateTime.UtcNow,
                                              IPAddress_Client = visitorsIPAddr,
                                              APIKey = _APIKey,
                                              APIName = _APIName,
                                              Return_Code = "",
                                              Return_Status = "",
                                              Return_Message = "NO RESPONSE",
                                              Data = jsonStr,
                                              //Data = responseBody,
                                              UserGUID = guidCall,
                                          };

                                          _apiLogsRepository.Insert(_APILogs);
                                          #endregion
                                      }


                                  });
                            #endregion

                            continuationTask.Wait(10000);

                        }

                    }
                    #endregion


                    var newlisNotification = lisNotification.GroupBy(d => new { d.SystemGrpID, d.NotificationID })
                    .Select(
                    g => new NotificationModels
                    {
                        SystemID = g.First().SystemID,
                        SystemGrpID = g.Key.SystemGrpID,
                        NotificationID = g.Key.NotificationID,
                        NotificationValue = g.Sum(s => s.NotificationValue),
                    }
                    ).ToList();

                    if (menuPortal.Section == "NotificationBox")
                    {
                        var menuNotiByParent = _menuPortalRepository.GetNotificationMenuByParentMenuID(menuPortal.MenuID);

                        var parentID = new List<int>();

                        foreach (var item in menuNotiByParent)
                        {
                            parentID.Add(item.NotificationID ?? 0);
                        }

                        //var notiSumAll = newlisNotification.Where(m => m.SystemGrpID == systemGrpID).Sum(m=>m.NotificationValue);
                        var notiSumAll = from a in newlisNotification
                                         where a.SystemGrpID == systemGrpID
                                         && parentID.Contains(a.NotificationID ?? 0)
                                         select a;

                        notificationValue = notiSumAll.Sum(m => m.NotificationValue);

                    }
                    else
                    {
                        var notimodel = newlisNotification.Where(m => m.SystemGrpID == systemGrpID && m.NotificationID == notificationID).FirstOrDefault();

                        if (notimodel != null)
                        {
                            notificationValue = notimodel.NotificationValue;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return notificationValue;
        }

        public virtual NotificationBoxModels GetNotificationBoxAllByUserNameAndMenuID(string username, int menuID)
        {
            var notificationBox = new NotificationBoxModels();
            var notificationResponseList = new List<NotificationResponse>();
            notificationBox.NotificationResponse = notificationResponseList;
            try
            {
                var lisNotification = new List<NotificationModels>();

                var menuPortal = _menuPortalRepository.GetMenuById(menuID);

                if (menuPortal != null)
                {
                    int systemGrpID = menuPortal.SystemGrpID ?? 0;
                    int notificationID = menuPortal.NotificationID ?? 0;

                    var systemNoti = _systemConfigureAPIRepository.GetSystemShowNotiBySystemGrpIDAndNotificationID(systemGrpID, notificationID);

                    string guidCall = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    #region Call API Data
                    foreach (var item in systemNoti)
                    {

                        string reqID = GeneratorGuid.GetRandomGuid();

                        var userLoginMappingTbl = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(username, item.SystemID);

                        if (userLoginMappingTbl.Count() > 0)
                        {
                            string jsonStr = "";

                            string _APIKey = "";

                            string _APIName = "";

                            var apiNotiModels = GetDataCallNotificationAPI(reqID, username, item.SystemID, item.APIName, item.API_Key);

                            jsonStr = JsonConvert.SerializeObject(apiNotiModels);
                            _APIKey = apiNotiModels.Request.APIKey;
                            _APIName = apiNotiModels.Request.APIName;

                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri(item.API_URL);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpContent content = new StringContent(jsonStr);

                            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            string responseBody = "";

                            #region  API Post Task
                            var continuationTask = client.PostAsync(item.API_URL, content).ContinueWith(
                                  (postTask) =>
                                  {
                                      bool isCompleted = postTask.IsCompleted;
                                      bool isFaulted = postTask.IsFaulted;

                                      if (isCompleted && !isFaulted)
                                      {
                                          var response = postTask.Result;
                                          if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                          {

                                              try
                                              {

                                                  string return_Code = "";
                                                  string return_Status = "";
                                                  string return_Message = "";
                                                  if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                  {
                                                      responseBody = response.Content.ReadAsStringAsync().Result;
                                                      JObject _response = JObject.Parse(responseBody);

                                                      return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                      return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                      return_Message = _response["Response"]["Result"]["Message"].ToString();
                                                  }

                                                  #region Insert API Log

                                                  var _APILogs = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = return_Code,
                                                      Return_Status = return_Status,
                                                      Return_Message = return_Message,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogs);
                                                  #endregion

                                                  #region Insert API Log

                                                  var _APILogs2 = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "CheckDataNoti",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = return_Code,
                                                      Return_Status = return_Status,
                                                      Return_Message = return_Message,
                                                      Data = responseBody,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogs2);
                                                  #endregion

                                                  #region Get Noti
                                                  if (Convert.ToBoolean(return_Status))
                                                  //if (true)
                                                  {
                                                      API_NotificationResponseModel responseModel = JsonConvert.DeserializeObject<API_NotificationResponseModel>(responseBody);

                                                      if (responseModel.Response.Data.Count() > 0)
                                                      {
                                                          foreach (IList<Notification> dataValue in responseModel.Response.Data.Select(m => m.Notification))
                                                          {
                                                              foreach (var notiItem in dataValue)
                                                              {
                                                                  var notiModel = new NotificationModels()
                                                                  {
                                                                      SystemID = item.SystemID,
                                                                      SystemGrpID = item.SystemGrpID,
                                                                      NotificationID = Convert.ToInt32(notiItem.NotificationID),
                                                                      NotificationValue = Convert.ToInt32(notiItem.NotificationValue)
                                                                  };

                                                                  lisNotification.Add(notiModel);

                                                              }
                                                          }
                                                      }
                                                  }
                                                  #endregion
                                              }

                                              catch (Exception ex)
                                              {
                                                  #region Insert API Log

                                                  var _APILogsCheck = new Tbl_APILogs()
                                                  {
                                                      ReqID = reqID,
                                                      SystemID = item.SystemID,
                                                      Events = "Request_Check",
                                                      Events_Time = DateTime.UtcNow,
                                                      IPAddress_Client = visitorsIPAddr,
                                                      APIKey = _APIKey,
                                                      APIName = _APIName,
                                                      Return_Code = "",
                                                      Return_Status = response.StatusCode.ToString(),
                                                      Return_Message = responseBody,
                                                      Data = jsonStr,
                                                      UserGUID = guidCall,
                                                  };

                                                  _apiLogsRepository.Insert(_APILogsCheck);


                                                  #endregion
                                                  throw;
                                              }

                                          }
                                          else
                                          {

                                              #region Insert API Log


                                              var _APILogs = new Tbl_APILogs()
                                              {
                                                  ReqID = reqID,
                                                  SystemID = item.SystemID,
                                                  Events = "Request",
                                                  Events_Time = DateTime.UtcNow,
                                                  IPAddress_Client = visitorsIPAddr,
                                                  APIKey = _APIKey,
                                                  APIName = _APIName,
                                                  Return_Code = "",
                                                  Return_Status = response.StatusCode.ToString(),
                                                  Return_Message = responseBody,
                                                  Data = jsonStr,
                                                  UserGUID = guidCall,
                                              };

                                              _apiLogsRepository.Insert(_APILogs);


                                              #endregion
                                          }
                                      }
                                      else
                                      {
                                          #region Insert API Log

                                          var _APILogs = new Tbl_APILogs()
                                          {
                                              ReqID = reqID,
                                              SystemID = item.SystemID,
                                              Events = "Request",
                                              Events_Time = DateTime.UtcNow,
                                              IPAddress_Client = visitorsIPAddr,
                                              APIKey = _APIKey,
                                              APIName = _APIName,
                                              Return_Code = "",
                                              Return_Status = "",
                                              Return_Message = "NO RESPONSE",
                                              Data = jsonStr,
                                              //Data = responseBody,
                                              UserGUID = guidCall,
                                          };

                                          _apiLogsRepository.Insert(_APILogs);
                                          #endregion
                                      }


                                  });
                            #endregion

                            continuationTask.Wait(10000);

                        }

                    }
                    #endregion


                    var newlisNotification = lisNotification.GroupBy(d => new { d.SystemGrpID, d.NotificationID })
                    .Select(
                    g => new NotificationModels
                    {
                        SystemID = g.First().SystemID,
                        SystemGrpID = g.Key.SystemGrpID,
                        NotificationID = g.Key.NotificationID,
                        NotificationValue = g.Sum(s => s.NotificationValue),
                    }
                    ).ToList();


                    foreach (var item in newlisNotification.Where(m => m.SystemGrpID == systemGrpID))
                    {
                        var notificationResponse = new NotificationResponse
                        {
                            NotificationID = item.NotificationID ?? 0,
                            NotificationValue = item.NotificationValue
                        };

                        notificationBox.NotificationResponse.Add(notificationResponse);
                    }

                    //if (menuPortal.Section == "NotificationBox")
                    //{
                    //    var menuNotiByParent = _menuPortalRepository.GetNotificationMenuByParentMenuID(menuPortal.MenuID);

                    //    var parentID = new List<int>();

                    //    foreach (var item in menuNotiByParent)
                    //    {
                    //        parentID.Add(item.NotificationID ?? 0);
                    //    }

                    //    //var notiSumAll = newlisNotification.Where(m => m.SystemGrpID == systemGrpID).Sum(m=>m.NotificationValue);
                    //    var notiSumAll = from a in newlisNotification
                    //                     where a.SystemGrpID == systemGrpID
                    //                     && parentID.Contains(a.NotificationID ?? 0)
                    //                     select a;

                    //    notificationValue = notiSumAll.Sum(m => m.NotificationValue);

                    //}
                    //else
                    //{
                    //    var notimodel = newlisNotification.Where(m => m.SystemGrpID == systemGrpID && m.NotificationID == notificationID).FirstOrDefault();

                    //    if (notimodel != null)
                    //    {
                    //        notificationValue = notimodel.NotificationValue;
                    //    }
                    //}

                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return notificationBox;
        }

        public virtual API_NotificationRequestModel GetDataCallNotificationAPI(string reqID, string username, int systemID, string APIName, string APIKey)
        {
            var notiRequestModel = new API_NotificationRequestModel();

            var requestData = new Request();

            requestData.ReqID = reqID;
            requestData.APIKey = APIKey;
            requestData.APIName = APIName;

            requestData.Data = new DataRequest();

            var listLoginID = new List<LoginID>();

            var userModel = _userRepository.FindByUsername(username);

            var userLoginMappingTbl = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(username, systemID);

            if (userLoginMappingTbl.Count() > 0)
            {
                foreach (var valueSys in userLoginMappingTbl)
                {
                    var loginID = new LoginID();

                    loginID.value = valueSys.SysUserID;

                    listLoginID.Add(loginID);
                }
            }

            var supplierInfo = new SupplierInfo
            {
                SupplierShortName = userModel.OrgID
            };

            notiRequestModel.Request = requestData;
            requestData.Data.LoginID = listLoginID;
            requestData.Data.SupplierInfo = supplierInfo;

            return notiRequestModel;
        }
    }


}
