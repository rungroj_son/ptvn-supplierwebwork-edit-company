﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.ProfileManage
{
    public partial interface IProfileTransactions
    {
        bool MergUserAccount(string masterUsername, List<UserMergModels> mergUsername, string usernameUpdateBy, out string returnMessage);

        //Task<ResultMergeModel> MergUserAccount2(string masterUsername, List<UserMergModels> mergUsername, string usernameUpdateBy);
    }
}
