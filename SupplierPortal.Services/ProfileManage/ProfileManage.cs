﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.SystemRole;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.ACL_Role;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.UserRole;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.CustomerDocNameMapping;
using SupplierPortal.Data.Models.Repository.SystemNotification;


namespace SupplierPortal.Services.ProfileManage
{
    public static class ProfileManage
    {

        public static IQueryable<UserManageModels> UserList(string orgID)
        {

           
                IUserRepository _repo = new UserRepository(); 

                return _repo.GetUserAllByOrgID(orgID).OrderBy(m => m.Username);
           

        }

        public static IQueryable<UserInformationModels> UserInformationList(string orgID)
        {


            IUserRepository _repo = new UserRepository();

            return _repo.GetUserInformationByOrgID(orgID).OrderBy(m => m.Username);

        }
        public static IQueryable<UserInformationModels> ListAllUserInformation(string username)
        {
            IUserRepository userRepository = new UserRepository();
            var ordId = userRepository.FindOrgIDByUsername(username);

            return userRepository.GetUserInformationByOrgID(ordId).OrderBy(m => m.Username); ;
        }

        //public static IQueryable<SystemRoleModels> SystemList
        //{

        //    get
        //    {
        //        ISystemRepository _repo = new SystemRepository(); ;

        //        return _repo.GetSystemAll().OrderBy(m => m.SystemName);
        //    }

        //}

        public static int CheckIsAdmin(string username)
        {
            IContactPersonRepository _repo = new ContactPersonRepository(); 

            return _repo.ContactPersonIsAdmin(username);         
        }

        public static int GetUserRoleIDByUsername(string username)
        {
            int roleID =  0;

            IUserRoleRepository _repo = new UserRoleRepository();

            var model = _repo.GetUserRoleByUsername(username);

            if (model!=null)
            {
                roleID = model.RoleID;
            }

            return roleID;
        }

        public static bool CheckIsAdminBestSupply(string username)
        {
            IContactPersonRepository _repo = new ContactPersonRepository(); ;

            return _repo.CheckIsAdminBestSupply(username);
        }

        public static bool CheckIsUserSystemMapping(string username,int systemID)
        {
            bool chk = false;

            IUserSystemMappingRepository _repo = new UserSystemMappingRepository(); 

            var userMapping = _repo.GetUserSystemMappingEnableService(username, systemID);

            if (userMapping.Count()>0)
            {
                chk = true;
            }

            return chk;
        }

        public static bool CheckIsUserSystemMappingByGroup(string username, int systemGrpId)
        {
            bool chk = false;

            IUserSystemMappingRepository _repo = new UserSystemMappingRepository(); ;

            var userMapping = _repo.GetUserSystemMappingByGroup(username, systemGrpId);

            if (userMapping.Count() > 0)
            {
                chk = true;
            }

            return chk;
        }

        public static bool CheckIsUserSystemMappingMenuByGroup(string username, int systemGrpId)
        {
            bool chk = false;

            IUserSystemMappingRepository _repo = new UserSystemMappingRepository(); ;

            var userMapping = _repo.GetUserSystemMappingMenuByGroup(username, systemGrpId).FirstOrDefault();

            if (userMapping != null)
            {
                if (userMapping.isAllowConnecting == 1)
                {
                    chk = true;
                }
            }

            return chk;
        }

        public static string GetSysRoleName(int sysRoleID)
        {
            IACL_RoleRepository _repo = new ACL_RoleRepository(); 

            return _repo.GetRoleNameByRoleID(sysRoleID);
        }

        public static IEnumerable<SystemRoles_sel_Result> RoleSelectList(int systemID, int sysRoleID)
        {
            ISystemRepository _repo = new SystemRepository(); 
            var result = _repo.GetSystemRoleSelectList(systemID,sysRoleID);

            return result;
        }

        public static IEnumerable<Tbl_CustomerDocNameMapping> GetDocumentIsRequire(int documentTypeID, int companyTypeID, string countryCode)
        {

            ICustomerDocNameMappingRepository _repo = new CustomerDocNameMappingRepository();

            var model = _repo.GetDocumentIsRequire(documentTypeID, companyTypeID, countryCode);


            return model;
        }

        public static int GetMenuMappingEnable(string username, int systemGrpID)
        {
            ISystemNotificationRepository _systemNotificationRepository = new SystemNotificationRepository();
            IUserSystemMappingRepository _userSystemMappingRepository = new UserSystemMappingRepository();

            int isEnable = 4;

            // Fix check for ไปหน้า  Topic  ค่า isEnable ==> 0 = ไปหน้า SystemUserDisabled , 1 = ลิ้งออกไป BSP , 2 = ไปหน้า  ServiceSuspend, 3 = ไปหน้า ServiceCancelled ,4 = ไม่โชว์ลิ้ง (ไม่่มีใน Tbl_UserServiceMapping)
           
            var systemModel = _systemNotificationRepository.GetSystemBySystemGrpID(systemGrpID);
            if (systemModel.Count()>0)
            {
                foreach (var item in systemModel)
                {
                    var checkUserMapping = _userSystemMappingRepository.GetUserSystemMapping(username, item.SystemID);

                    if (checkUserMapping.Count() > 0)
                    {
                        var checkUserSystemMapping = _userSystemMappingRepository.GetUserSystemMappingMenu(username, item.SystemID).FirstOrDefault();
                        if (checkUserSystemMapping != null)
                        {
                            // Fix check for ไปหน้า  Topic/ServiceCancelled 
                            if ((checkUserSystemMapping.isCancelService ?? 0) == 1)
                            {
                                return 3;
                            }

                            // Fix check for ไปหน้า  Topic/ServiceSuspend 
                            if ((checkUserSystemMapping.isSuspendService ?? 0) == 1)
                            {
                                return 2;
                            }

                            isEnable = checkUserSystemMapping.isAllowConnecting ?? 0;
                            if (isEnable == 1)
                            {
                                return isEnable;
                            }
                        }
                    }                   
                }
            }

            return isEnable;
        }

        public static bool CheckBestSupplyIsSuspendOrCancelledAndNonisAllowConnecting(string username)
        {
            bool chk = false;

            IUserSystemMappingRepository _userSystemMappingRepository = new UserSystemMappingRepository();

            var checkUserSystemMapping = _userSystemMappingRepository.GetUserSystemMappingMenu(username, 1).FirstOrDefault();

            if (checkUserSystemMapping != null)
            {
                if ((checkUserSystemMapping.isCancelService ?? 0) == 1)
                {
                    return true;
                }

                if ((checkUserSystemMapping.isSuspendService ?? 0) == 1)
                {
                    return true;
                }

                if ((checkUserSystemMapping.isAllowConnecting ?? 0) != 1)
                {
                    return true;
                }
            }else
            {
                // กันข้อมูลไม่ถูก ถ้าไม่มีข้อมูลใน Tbl_OrgSystemMapping ก็ให้เป็น case isAllowConnecting = 0
                return true;
            }

            return chk;
        }

        public static bool CheckIsUserSystemMappingByUserID(int userID, int systemID)
        {
            bool chk = false;

            IUserSystemMappingRepository _repo = new UserSystemMappingRepository();

            var userMapping = _repo.GetUserSystemMapping(userID, systemID);

            if (userMapping.Count() > 0)
            {
                chk = true;
            }

            return chk;
        }

        public static bool CheckIsUserSystemMappingEnableServiceByUserID(int userID, int systemID)
        {
            bool chk = false;

            IUserSystemMappingRepository _repo = new UserSystemMappingRepository();

            var userMapping = _repo.GetUserSystemMappingEnableService(userID, systemID);

            if (userMapping.Count() > 0)
            {
                chk = true;
            }

            return chk;
        }
    }
}
