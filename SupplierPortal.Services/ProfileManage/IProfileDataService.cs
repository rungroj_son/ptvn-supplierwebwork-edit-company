﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.ProfileManage
{
    public partial interface IProfileDataService
    {

        ViewDataProfileModels GetProfileData(string username);

        string CallAPIInsertOrUpdateBSPUserPreference(BSP_UserPreferenceModels model, string languageId);

        string UpdateGeneralData(GeneralModels model);

        Task<string> UpdateGeneralData2(GeneralModels model);

        //void DisableOrEnableUser(ManageUserModels models);

        void UpdateSessionData(BSP_UserPreferenceModels model);

        bool CheckSupplierMergUserInCurrentUser(string currentUsername, string mergUsername);

        bool CheckUserMappingBSPInGroup(string currentUsername, string mergUsername);

        API_UpdateUserModels GetDataCallUpdateUserAPI(SystemRoleListModels modelsData, string reqID, string userInsert, string action = "UpdateUser");

        void CallAPIAddNotification(BSP_UserPreferenceModels model);

    }
}
