﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.SupportModel.SupplyManagement;

namespace SupplierPortal.Services.SupplyManagement
{
    public partial interface IReportService
    {

        ViewDataReportModels ReportsListByUser(string username);

    }
}
