﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;

namespace SupplierPortal.Services.SupplyManagement
{
    public static class ReportManage
    {
        public static int GetReportsMappingEnable(string username, int systemID)
        {
            IUserSystemMappingRepository _userSystemMappingRepository = new UserSystemMappingRepository();

            int isEnable = 0;

            // Fix check for ไปหน้า  Topic  ค่า isEnable ==> 0 = ไปหน้า SystemUserDisabled , 1 = ลิ้งออกไป BSP , 2 = ไปหน้า  ServiceSuspend, 3 = ไปหน้า ServiceCancelled 

            var checkUserMapping = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(username, systemID);
            if (checkUserMapping.Count() > 0)
            {
                var checkUserSystemMapping = _userSystemMappingRepository.GetUserSystemMappingMenu(username, systemID).FirstOrDefault();
                if (checkUserSystemMapping != null)
                {
                    // Fix check for ไปหน้า  Topic/ServiceCancelled 
                    if ((checkUserSystemMapping.isCancelService ?? 0) == 1)
                    {
                        return 3;
                    }

                    // Fix check for ไปหน้า  Topic/ServiceSuspend 
                    if ((checkUserSystemMapping.isSuspendService ?? 0) == 1)
                    {
                        return 2;
                    }

                    isEnable = checkUserSystemMapping.isAllowConnecting ?? 0;
                    if (isEnable == 1)
                    {
                        return isEnable;
                    }
                }
            }

            return isEnable;
        }
    }
}
