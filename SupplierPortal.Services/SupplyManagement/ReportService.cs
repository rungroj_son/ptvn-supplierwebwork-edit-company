﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.SupportModel.SupplyManagement;
using SupplierPortal.Data.Models.Repository.SupplierPortalReportGroup;
using SupplierPortal.Data.Models.Repository.SupplierPortalReports;
using SupplierPortal.Data.Models.Repository.UserReportMapping;
using SupplierPortal.Data.Models.Repository.User;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;


namespace SupplierPortal.Services.SupplyManagement
{
    public partial class ReportService : IReportService
    {

        IPortalReportsGroupRepository _reportsGroupRepository;
        IPortalReportsRepository _reportsRepository;
        IUserRepository _userRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IUserReportMappingRepository _userReportMappingRepository;


        public ReportService()
        {
            _reportsGroupRepository = new PortalReportsGroupRepository();
            _reportsRepository = new PortalReportsRepository();
            _userRepository = new UserRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _userReportMappingRepository = new UserReportMappingRepository();
        }

        public ReportService(
            PortalReportsGroupRepository reportsGroupRepository,
            PortalReportsRepository reportsRepository,
            UserRepository userRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            UserReportMappingRepository userReportMappingRepository
            )
        {
            _reportsGroupRepository = reportsGroupRepository;
            _reportsRepository = reportsRepository;
            _userRepository = userRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _userReportMappingRepository = userReportMappingRepository;
        }

        public virtual ViewDataReportModels ReportsListByUser(string username)
        {
            //username = "TestUser";

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value;
            }

            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            ViewDataReportModels model = new ViewDataReportModels();

            var user = _userRepository.FindByUsername(username);

            string userID = _userSystemMappingRepository.GetUserIDFirstOrDefault(username, 5);

            var reports = _reportsRepository.GetReportsListByUser(username);

            var reportsGroupList = _reportsGroupRepository.List();

            var reportsGroup = from c in reportsGroupList
                      // where reports.Any(x => x.ReporGroupID == c.ReportGroupID)
                       select c;

            List<ReportGroupModels> reportGroupModels = new List<ReportGroupModels>();
            List<ReportListModels> reportListModels = new List<ReportListModels>();

            foreach (var resultreportsGroup in reportsGroup)
            {
                reportGroupModels.Add(new ReportGroupModels { 
                    ReportGroupID = resultreportsGroup.ReportGroupID,
                    ReportGroupName = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_SupplierPortalReportGroup", "ReportGroupName", resultreportsGroup.ReportGroupID.ToString()) 
                });

            }
            

            foreach (var resultreports in reports)
            {
                int isEnableService = 1;
                if (resultreports.SystemID != 0)
                {
                    var userMapping = _userSystemMappingRepository.GetUserSystemMappingByUsernameAndSystemID(username, resultreports.SystemID??0);

                    if (userMapping != null)
                    {
                        isEnableService = userMapping.isEnableService??0;
                    }else
                    {
                        isEnableService = 0;
                    }
                }else
                {
                    int tempUserID = _userRepository.GetUserIDByUsername(username);
                    var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(tempUserID, resultreports.ReportID);
                    if (userReportMapping!= null)
                    {
                        isEnableService = userReportMapping.isEnable??0;
                    }else
                    {
                        isEnableService = 0;
                    }
                }
                //UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                //urlLink = url.Action(resultreports.ActionName, resultreports.ControllerName, null);

                reportListModels.Add(new ReportListModels
                {
                    ReportName = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_SupplierPortalReports", "ReportName", resultreports.ReportID.ToString()),
                    ReportDetails = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_SupplierPortalReports", "Description", resultreports.ReportID.ToString()),
                    Action = resultreports.ActionName,
                    Controller = resultreports.ControllerName,
                    OrgID = user.OrgID,
                    UserID = userID,
                    RptID = resultreports.ReportID,
                    ReportGroupID = resultreports.ReporGroupID,
                    SystemID = resultreports.SystemID,
                    SystemPageID = resultreports.SystemPageID,
                    SystemURL = resultreports.SystemURL,
                    isEnableService = isEnableService
                });

            }
            model.allReportGroups = reportGroupModels;
            model.allReports = reportListModels;

            return model;
        }

    }
}
