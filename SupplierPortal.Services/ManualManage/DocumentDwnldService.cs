﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.DocumentDwnld;
using SupplierPortal.Data.Models.Repository.DocumentGroupDwnld;
using SupplierPortal.Data.Models.SupportModel.Manual;
using SupplierPortal.Framework.Localization;
using System.Web;


namespace SupplierPortal.Services.ManualManage
{
    public partial class DocumentDwnldService : IDocumentDwnldService
    {
        IDocumentDwnldRepository _documentDwnldRepository;
        IDocumentGroupDwnldRepository _documentGroupDwnldRepository;

        public DocumentDwnldService()
        {
            _documentDwnldRepository = new DocumentDwnldRepository();
            _documentGroupDwnldRepository = new DocumentGroupDwnldRepository();

        }

        public DocumentDwnldService(
           DocumentDwnldRepository documentDwnldRepository,
           DocumentGroupDwnldRepository documentGroupDwnldRepository

           )
        {
            _documentDwnldRepository = documentDwnldRepository;
            _documentGroupDwnldRepository = documentGroupDwnldRepository;

        }

        public virtual ViewDataDocumentManual DocumentManualList()
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value;
            }

            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            ViewDataDocumentManual model = new ViewDataDocumentManual();

            var documentGroup = _documentGroupDwnldRepository.List();

            //var documentList = _documentDwnldRepository.List();

            var documentList = _documentDwnldRepository.ListByLG(int.Parse(languageID));

            List<DocumentGroupModels> documentGroupModels = new List<DocumentGroupModels>();

            List<DocumentListModels> documentListModels = new List<DocumentListModels>();

            foreach (var resultDocumentGroup in documentGroup)
            {
                documentGroupModels.Add(new DocumentGroupModels
                {
                    DocGrpID = resultDocumentGroup.DocGrpID,
                    DocumentGrp = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_DocumentGroupDwnld", "DocumentGrp", resultDocumentGroup.DocGrpID.ToString())
                });

            }

            if (documentList.Count() > 0)
            {
                foreach (var resultDocumentList in documentList)
                {
                    documentListModels.Add(new DocumentListModels
                    {
                        Id = resultDocumentList.Id,
                        DocumentName = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_DocumentDwnld", "DocumentName", resultDocumentList.Id.ToString()),
                        DocumentPath = resultDocumentList.DocumentPath,
                        DocGrpID = resultDocumentList.DocGrpID
                    });
                }
            }
            else
            {
                var docList = _documentDwnldRepository.List();
                foreach (var resultDocumentList in docList)
                {
                    documentListModels.Add(new DocumentListModels
                    {
                        Id = resultDocumentList.Id,
                        DocumentName = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_DocumentDwnld", "DocumentName", resultDocumentList.Id.ToString()),
                        DocumentPath = resultDocumentList.DocumentPath,
                        DocGrpID = resultDocumentList.DocGrpID
                    });
                }
            }

            model.allDocumentGroups = documentGroupModels;
            model.allDocuments = documentListModels;

            return model;
        }
    }
}
