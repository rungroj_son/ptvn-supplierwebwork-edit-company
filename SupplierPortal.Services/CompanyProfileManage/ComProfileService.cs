﻿using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.OrgProduct;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public static class ComProfileService
    {

        public static List<ProductTypeAddModel> GetProductTypeBySupplierIDAndProductCode(int supplierID, string productCode)
        {

            IOrgProductRepository _repo = new OrgProductRepository();

            var models = _repo.GetProductTypeBySupplierIDAndProductCode(supplierID, productCode);

            return models.ToList();

        }

        public static int GetOrgAttachmentCountBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID)
        {
            IOrgAttachmentRepository _orgAttachmentRepository = new OrgAttachmentRepository();

            int countAttachment = _orgAttachmentRepository.GetOrgAttachmentCountBySupplierIDAndDocumentTypeID(supplierID, documentTypeID);

            return countAttachment;
        }

        public static int GetOrgAttachmentTrackingCountBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID)
        {
            IOrgAttachmentRepository _orgAttachmentRepository = new OrgAttachmentRepository();
            ITrackingOrgAttachmentRepository  _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();

            int countResult = 0;

            int countAttachment = _orgAttachmentRepository.GetOrgAttachmentCountBySupplierIDAndDocumentTypeID(supplierID, documentTypeID);

            int nonShow = 0;
            var orgAttachmentTracking = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingBySupplierIDAndDocumentTypeID(supplierID, documentTypeID);
            foreach (var item in orgAttachmentTracking)
            {
                if (item.OldAttachmentID != 0 && item.NewAttachmentID != 0)
                {
                    //มาจากเคส ลบไฟล์แล้วเพิ่มไฟล์มาใหม่
                    nonShow = nonShow + 1;
                }
                else if (item.OldAttachmentID != 0 && item.NewAttachmentID == 0)
                {
                    //มาจากเคส ลบไฟล์แล้วwไม่เพิ่มไฟล์มาใหม่ จะขึ้นสีแดง
                    nonShow = nonShow + 1;
                }
            }

            if (countAttachment > nonShow)
            {
                countResult = countAttachment - nonShow;
            }else
            {
                countResult = countAttachment;
            }
            return countResult;
        }

    }
}
