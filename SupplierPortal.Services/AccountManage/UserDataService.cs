﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.Repository.User;
using System.Text;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Services.AccountManage
{
    public static class UserDataService
    {

        public static string GetUserShowByUsername(string username)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            IUserRepository _repo = new UserRepository();

            IContactPersonRepository _repoContact = new ContactPersonRepository();

            string userShow = "";

            StringBuilder builder = new StringBuilder();

            var result = _repo.FindByUsername(username);

            if (result != null)
            {
                int contactID = Convert.ToInt32(result.ContactID);
                var resultContact = _repoContact.GetContactPersonByContectID(contactID);
                //userShow = result.FirstName + result.MiddleName + result.LastName;
                if (resultContact != null)
                {
                    if (isInter==1)
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Inter))
                        {
                            builder.Append(resultContact.FirstName_Inter + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Inter))
                        {
                            builder.Append(resultContact.LastName_Inter);
                        }

                    }else
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Local))
                        {
                            builder.Append(resultContact.FirstName_Local + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Local))
                        {
                            builder.Append(resultContact.LastName_Local);
                        }
                    }
                  
                    
                    userShow = builder.ToString();
                    return userShow;
                }
                
            }

            return userShow;

        }

        public static string GetOrganizByUsername(string username)
        {

            IUserRepository _repo = new UserRepository();

            string organizationName = "";

            organizationName = _repo.FindOrganizNameByUsername(username);

            return organizationName;
        }

        public static Tbl_User GetUserByUsername(string username)
        {

            IUserRepository _repo = null;
            _repo = new UserRepository();

            var result = _repo.FindByUsername(username);          

            return result;
        }

        public static Tbl_User GetUserByToken(string username)
        {

            IUserRepository _repo = null;
            _repo = new UserRepository();

            var result = _repo.FindByUsername(username);

            return result;
        }

        public static string GetUserLanguageIdByCode(string languageCode)
        {

            ILanguageRepository _repo = null;
            _repo = new LanguageRepository();

            var result = _repo.GetLanguageIdByCode(languageCode);

            return result;
        }

        public static string GetUserShowByUsernameAndLanguageId(string username, int languageId)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            IUserRepository _userRepository = new UserRepository();

            IContactPersonRepository _contactPersonRepository = new ContactPersonRepository();

            string userShow = "";

            StringBuilder builder = new StringBuilder();

            var user = _userRepository.FindByUsername(username);

            if (user != null)
            {
                int contactID = Convert.ToInt32(user.ContactID);
                var resultContact = _contactPersonRepository.GetContactPersonByContectID(contactID);

                if (resultContact != null)
                {
                    if (isInter == languageId)
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Inter))
                        {
                            builder.Append(resultContact.FirstName_Inter + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Inter))
                        {
                            builder.Append(resultContact.LastName_Inter);
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Local))
                        {
                            builder.Append(resultContact.FirstName_Local + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Local))
                        {
                            builder.Append(resultContact.LastName_Local);
                        }
                    }

                    userShow = builder.ToString();
                    return userShow;
                }

            }

            return userShow;

        }

        public static string GetOrganizByUsernameAndLanguageId(string username, int languageId)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            IUserRepository _userRepository = new UserRepository();

            IOrganizationRepository _organizationRepository = new OrganizationRepository();

            IBusinessEntityRepository _businessEntityRepository = new BusinessEntityRepository();

             string organizationName = "";

             string businessEntityDisplay = "";

            var user = _userRepository.FindByUsername(username);

            if (user != null)
            {
                var orgModel = _organizationRepository.GetOrganizationByOrgID(user.OrgID);

                if (orgModel != null)
                {
                    var businessEntityDisplayModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(orgModel.BusinessEntityID??0, languageId);

                    if (isInter == languageId)
                    {
                        organizationName = orgModel.CompanyName_Inter;

                        if (businessEntityDisplayModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayModel.BusinessEntityDisplay;

                            if (businessEntityDisplayModel.ConcatStrInter.Trim() == "P")
                            {
                                organizationName = businessEntityDisplay + " " + organizationName;

                            }else
                            {
                                organizationName = organizationName + " " + businessEntityDisplay;
                            }
                        }

                    }else
                    {
                        organizationName = orgModel.CompanyName_Local;

                        if (businessEntityDisplayModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayModel.BusinessEntityDisplay;

                            if (businessEntityDisplayModel.ConcatStrLocal.Trim() == "P")
                            {
                                organizationName = businessEntityDisplay + organizationName;
                            }
                            else
                            {
                                organizationName = organizationName + businessEntityDisplay;
                            }
                        }
                    }
                }
            }

            return organizationName;
        }

        public static string GetUserShowByUsernameLanguageByUserProfile(string username)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            int languageId = 1;

            IUserRepository _userRepository = new UserRepository();

            IContactPersonRepository _contactPersonRepository = new ContactPersonRepository();

            ILanguageRepository _languageRepository = new LanguageRepository();

            string userShow = "";

            StringBuilder builder = new StringBuilder();

            var user = _userRepository.FindByUsername(username);

            if (user != null)
            {
                string languageID = _languageRepository.GetLanguageIdByCode(user.LanguageCode.ToUpper());

                if (!string.IsNullOrEmpty(languageID))
                {
                    languageId = Convert.ToInt32(languageID);
                }

                int contactID = Convert.ToInt32(user.ContactID);
                var resultContact = _contactPersonRepository.GetContactPersonByContectID(contactID);

                if (resultContact != null)
                {
                    if (isInter == languageId)
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Inter))
                        {
                            builder.Append(resultContact.FirstName_Inter + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Inter))
                        {
                            builder.Append(resultContact.LastName_Inter);
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(resultContact.FirstName_Local))
                        {
                            builder.Append(resultContact.FirstName_Local + " ");
                        }

                        if (!string.IsNullOrEmpty(resultContact.LastName_Local))
                        {
                            builder.Append(resultContact.LastName_Local);
                        }
                    }

                    userShow = builder.ToString();
                    return userShow;
                }

            }

            return userShow;

        }

        public static string GetOrganizByUsernameLanguageByUserProfile(string username)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            int languageId = 1;

            IUserRepository _userRepository = new UserRepository();

            IOrganizationRepository _organizationRepository = new OrganizationRepository();

            IBusinessEntityRepository _businessEntityRepository = new BusinessEntityRepository();

            ILanguageRepository _languageRepository = new LanguageRepository();

            string organizationName = "";

            string businessEntityDisplay = "";

            var user = _userRepository.FindByUsername(username);

            if (user != null)
            {
                var orgModel = _organizationRepository.GetOrganizationByOrgID(user.OrgID);

                string languageID = _languageRepository.GetLanguageIdByCode(user.LanguageCode.ToUpper());

                if (!string.IsNullOrEmpty(languageID))
                {
                    languageId = Convert.ToInt32(languageID);
                }

                if (orgModel != null)
                {
                    var businessEntityDisplayModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(orgModel.BusinessEntityID ?? 0, languageId);

                    if (isInter == languageId)
                    {
                        organizationName = orgModel.CompanyName_Inter;

                        if (businessEntityDisplayModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayModel.BusinessEntityDisplay;

                            if (businessEntityDisplayModel.ConcatStrInter.Trim() == "P")
                            {
                                organizationName = businessEntityDisplay + " " + organizationName;

                            }
                            else
                            {
                                organizationName = organizationName + " " + businessEntityDisplay;
                            }
                        }

                    }
                    else
                    {
                        organizationName = orgModel.CompanyName_Local;

                        if (businessEntityDisplayModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayModel.BusinessEntityDisplay;

                            if (businessEntityDisplayModel.ConcatStrLocal.Trim() == "P")
                            {
                                organizationName = businessEntityDisplay + organizationName;
                            }
                            else
                            {
                                organizationName = organizationName + businessEntityDisplay;
                            }
                        }
                    }
                }
            }

            return organizationName;
        }

        
    }
}
