﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.CompanyType;
using SupplierPortal.Data.Models.Repository.CustomerDocType;
using SupplierPortal.Data.Models.Repository.AutoComplete_City;
using SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;



namespace SupplierPortal.Services.RegisterPortalManage
{
    public static class RegisterManage
    {
        public static IEnumerable<CompanyTypeModels> GetCompanyTypeList()
        {
            ICompanyTypeRepository _repo = new CompanyTypeRepository(); ;
            var result = _repo.GetCompanyTypeByMultiLanguageId();

            return result;
        }

        public static IEnumerable<DocumentTypeModels> GetDocumentTypeByCompanyTypeID(int companyTypeID)
        {
            ICustomerDocTypeRepository _repo = new CustomerDocTypeRepository(); ;
            var result = _repo.GetDocumentTypeByCompanyTypeID(companyTypeID);

            return result;
        }

        //public static IQueryable<Tbl_AutoComplete_City> LocalizedProperty(string countrycode, int languageID)
        //{


        //    IAutoComplete_CityRepository _repo = new AutoComplete_CityRepository();

        //    return _repo.GetCityAllByCountrycodeAndLanguageID(countrycode, languageID);
               

        //}

        //public static IQueryable<Tbl_AutoComplete_SubDistrict> LocalizedProperty(string countrycode, int languageID)
        //{

        //    IAutoComplete_SubDistrictRepository _repo = new AutoComplete_SubDistrictRepository();

        //    return _repo.GetSubDistrictAllByCountrycodeAndLanguageID(countrycode, languageID);

        //}

        
    }
}
